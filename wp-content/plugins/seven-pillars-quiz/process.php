<?php
/**
 * 7 Pillars Questionnaire Plugin
 * Functions to process the submitted questionnaire forms answers and store them
 * in the database.
 * File version: 2.0
 */
 
 // Set error reporting on for debugging.
error_reporting(E_ALL);
ini_set("display_errors", 1);

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

global $wpdb;

// Prepare the data we will use.
$user_id = get_current_user_id();
$user_info = get_user_meta( $user_id );
$table_name = $wpdb->prefix . "mc7p";
$column = $_POST['this'];

	$user = new WP_User($user_id);

	$user_email = stripslashes($user->user_email);

// Calculate the sum of all the submitted answers, a.k.a. the score.
$score = 0;
foreach( $_POST as $question => $answer ) {
	$score += intval($answer);
}

if( $column == 'pillar1_score' ) {
// If this is Pillar 1 then also save the user id and the region along with the score to the database.
	$data = array(
	'user_id'	=> $user_id,
	'region'	=> $user_info['territory']['0'],
	$column		=> $score
	);
	
	$wpdb->insert( $table_name, $data );
	$_SESSION['mc7p_form'] = $wpdb->insert_id;
	
	$next_location = get_site_url( null, $_POST['next'], null);

	// $quiz_id = $_SESSION['mc7p_form'];
	// send_email_results($quiz_id);

	if (WP_DEBUG){

		
	}
	
	

} elseif ( $column == 'pillar7_score' ) { 
// If this is Pillar 7 then calculate the sum of all pillars to get the total score.
	$wpdb->update( $table_name, array( $column => $score ), array( 'ID' => $_SESSION['mc7p_form'] ) );
	
	$all_results = $wpdb->get_row( 'SELECT * FROM ' . $table_name . ' WHERE id = ' . $_SESSION['mc7p_form'], ARRAY_A);
	
	$total_score = $all_results['pillar1_score'] + $all_results['pillar2_score'] + $all_results['pillar3_score'] + $all_results['pillar4_score'] + $all_results['pillar5_score'] + $all_results['pillar6_score'] + $all_results['pillar7_score'];
	
	$wpdb->update( $table_name, array( 'total_score' => $total_score ), array( 'ID' => $_SESSION['mc7p_form'] ) );

	$quiz_id = $_SESSION['mc7p_form'];


	send_email_results($quiz_id);

	$next_location = $_POST['next'];
	
} else {
// If this is other than pillars 1 or 7 then just save the socre.
	$wpdb->update( $table_name, array( $column => $score ), array( 'ID' => $_SESSION['mc7p_form'] ) );
	
	$next_location = get_site_url( null, $_POST['next'], null);
	
}

// Redirect to the next pillar or the score sheet.
header( 'Location: ' . $next_location . '?form=' . $_SESSION['mc7p_form'] );



