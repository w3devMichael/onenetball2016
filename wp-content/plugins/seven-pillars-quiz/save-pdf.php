<?php
define( '__ROOT__', $_SERVER['DOCUMENT_ROOT'] );
require_once( __ROOT__ . '/wp-load.php');

// Define environment constants
define('mc7p_path', plugin_dir_path(__FILE__));
define('mc7p_url', plugin_dir_url(__FILE__));

set_include_path(get_include_path() . PATH_SEPARATOR . mc7p_url . "dompdf");
 
require_once( "dompdf/dompdf_config.inc.php" );
 
$dompdf = new DOMPDF();
 
$html = 
 
$dompdf->load_html_file( mc7p_url . 'score-sheet.php?form=' . $_GET['form'] . '&action=savepdf' );
//$dompdf->set_base_path(mc7p_path);
$dompdf->set_paper('letter', 'landscape');
$dompdf->render();
 
$dompdf->stream("Score-sheet.pdf");