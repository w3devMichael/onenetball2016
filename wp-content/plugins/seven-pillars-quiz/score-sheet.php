<?php
/**
 * 7 Pillars Questionnaire Plugin
 * Score sheet.
 * File version: 1.0
 */
 
// error_reporting(E_ALL);
@ini_set("display_errors", 'Off');

 // Load the WordPress environment functions.
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

// Define plugin-specific constants
define('mc7p_path', plugin_dir_path(__FILE__));
define('mc7p_url', plugin_dir_url(__FILE__));

global $wpdb;

// Prepare the data we will need.
$table_name = $wpdb->prefix . "mc7p";
$quiz = $wpdb->get_row( 'SELECT * FROM ' . $table_name . ' WHERE id = ' . $_GET['form'], ARRAY_A);


$user_id = $quiz['user_id'];
$user_info = get_user_meta( $user_id );


$quiz_id = $_GET['form'];

// send_email_results($quiz_id);

// Variables to store averages.
$reg_p1 = 0;
$nal_p1 = 0;
$reg_p2 = 0;
$nal_p2 = 0;
$reg_p3 = 0;
$nal_p3 = 0;
$reg_p4 = 0;
$nal_p4 = 0;
$reg_p5 = 0;
$nal_p5 = 0;
$reg_p6 = 0;
$nal_p6 = 0;
$reg_p7 = 0;
$nal_p7 = 0;
$reg_records = 0;
$nal_records = 0;

// Calculate averages.
$db = $wpdb->get_results( 'SELECT * FROM ' . $table_name, ARRAY_A );
foreach( $db as $record ) {
	if( $record['region'] == $quiz['region'] ) {
		$reg_p1 += $record['pillar1_score'];
		$reg_p2 += $record['pillar2_score'];
		$reg_p3 += $record['pillar3_score'];
		$reg_p4 += $record['pillar4_score'];
		$reg_p5 += $record['pillar5_score'];
		$reg_p6 += $record['pillar6_score'];
		$reg_p7 += $record['pillar7_score'];
		$reg_records += 1;
	}
	$nal_p1 += $record['pillar1_score'];
	$nal_p2 += $record['pillar2_score'];
	$nal_p3 += $record['pillar3_score'];
	$nal_p4 += $record['pillar4_score'];
	$nal_p5 += $record['pillar5_score'];
	$nal_p6 += $record['pillar6_score'];
	$nal_p7 += $record['pillar7_score'];
	$nal_records += 1;
}
$reg_p1 /= $reg_records;
$nal_p1 /= $nal_records;
$reg_p2 /= $reg_records;
$nal_p2 /= $nal_records;
$reg_p3 /= $reg_records;
$nal_p3 /= $nal_records;
$reg_p4 /= $reg_records;
$nal_p4 /= $nal_records;
$reg_p5 /= $reg_records;
$nal_p5 /= $nal_records;
$reg_p6 /= $reg_records;
$nal_p6 /= $nal_records;
$reg_p7 /= $reg_records;
$nal_p7 /= $nal_records;

// Feedback and traffic light are decided here.
if( $quiz['total_score'] > 140 ) {
	$feedback = esc_attr( get_option( 'mc7p_feedback_high' ) );
	$traffic_light = '#C2FFD1';
} elseif( $quiz['total_score'] > 71 && $quiz['total_score'] <= 140 ) {
	$feedback = esc_attr( get_option( 'mc7p_feedback_med' ) );
	$traffic_light = '#FFF0C2';
} elseif( $quiz['total_score'] >= 0 && $quiz['total_score'] <= 70 ) {
	$feedback = esc_attr( get_option( 'mc7p_feedback_low' ) );
	$traffic_light = '#FFCCCC';
}


get_header(); ?>

<section class="page-accent seven-pillars-accent">
    <div class="wrapper">
        <div class="left">
            <h1 class="page-accent-title"><span class="accent">7 PILLARS</span> OF INCLUSION</h1>
        </div>
        <div class="right logos">
            <img src="<?php echo ot_get_option('7pillar_page_logo_1'); ?>" alt="7 Pillars" width="275" height="55"/>
            <img src="<?php echo ot_get_option('7pillar_page_logo_2'); ?>" alt="Play by the Rules" width="182" height="170"/>
        </div>
    </div>
</section>

<section class="tabbed-content seven-pillars-test seven-pillars-test-result-page">

	<!-- <h1>Your Score <?php echo $quiz['total_score']; ?></h1> -->
    <header>
        <div class="wrapper">
            <ol class="progress-indicator">
                <li class="passed">
                    <span>1</span>
                    <strong>Access</strong>
                </li>
                <li class="passed">
                    <span>2</span>
                    <strong>Attitude</strong>
                </li>
                <li class="passed">
                    <span>3</span>
                    <strong>Choice</strong>
                </li>
                <li class="passed">
                    <span>4</span>
                    <strong>Partnerships</strong>
                </li>
                <li class="passed">
                    <span>5</span>
                    <strong>Communication</strong>
                </li>
                <li class="passed">
                    <span>6</span>
                    <strong>Policy</strong>
                </li>
                <li class="passed">
                    <span>7</span>
                    <strong>Opportunities</strong>
                </li> 

            </ol>    
            <h2>Your Results</h2>
           <p>Check out your results below to see how you are progressing!</p>
        </div>
    </header>


    <ol class="test-questions-list test-questions-results wrapper">

    <?php 




    $pillars = ['Access', 'Attitude', 'Choice','Partnerships', 'Communication', 'Policy', 'Opportunities'];

    $count = 0;

    if ($quiz != NULL) : 

    foreach ($quiz as $key => $points) {

        if (startsWith($key, "pillar") ): 

            $result = getResults($points);

            ?>
           
            <li>
            <div>
                <strong>PILLAR <?php echo $count+1 . ' : ' . $pillars[$count]; ?> </strong>
                <div>
                    <div class="status-icon <?php echo $result['statusClass'];  ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $result['icon']; ?>" alt="" width="110" height="110"/>
                        <span><?php echo $result['title'] ?></span>
                    </div>
                    <div class="text"><p>
                        <?php echo $result['description']; ?></p>
                    </div>                        
                </div>
            </div>
        </li> 



        <?php $count++; endif;

    }  
    

    else: ?>

        <li>
            <div>
                <div>
                <p>Sorry there is no results for this user.</p>                         
                </div>
            </div>
        </li> 

    <?php 

    endif;

    ?>
                           
                           
    </ol>
    <div class="padded centered">
        <a href="<?php echo get_site_url(). '/past-results/'; ?>" title="See all your past results" class="site-btn">SEE ALL RESULTS</a>
    </div>
</section>


<?php get_footer(); ?>