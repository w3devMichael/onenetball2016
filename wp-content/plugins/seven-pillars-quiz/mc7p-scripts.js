/**
 * 7 Pillars Questionnaire Plugin
 * File version: 1.0
 */


jQuery(document).ready( function($) {

	$( '.wrap' ).find( '.q-container' ).each(function () {
	
	
		if( $( this ).find( 'input:checked[type="radio"]').val() == 'multi' ) {	
			$( this ).children( 'p.qchoices' ).css( 'display', 'block'); 
		}
	
		$( this ).find( 'input[type="radio"]' ).change( function() { 
			if( $( this ).val() == 'multi' ) {	
				$( this ).parents().children( 'p.qchoices' ).slideDown( 750 ); 
			} else {
				$( this ).parents().children( 'p.qchoices' ).slideUp( 750 ); 
			}
		});
	});
	
});
