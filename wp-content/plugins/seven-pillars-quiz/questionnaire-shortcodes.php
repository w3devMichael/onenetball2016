<?php

/**

 * 7 Pillars Questionnaire Plugin

 * Include to add the shortcodes for the individual pillar questionnaires.

 * File version: 2.1

 */



// PILLAR 1 SHORTCODE FUNCTION

function mc7p_pillar1_shortcode( $atts ) {

	$atts = shortcode_atts( array( 'next' => 'pillar-2-attitudes' ), $atts );



	$output = '<form id="pillar-1-quiz" class="site-form test-form" name="pillar-1-quiz" action="' .  mc7p_url . 'process.php" method="post" >';
	$output .= '<fieldset><legend class="site-legend">NOW COMPLETE THE ACCESS QUESTIONS</legend><ol class="test-questions-list">';
	 

	$output .= '<li><div class="left"><strong>ACTION 1</strong><p>' .  esc_attr( get_option( "mc7p_pillar1_q1_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar1_q1_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q1" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q1_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q1" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q1_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q1" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q1_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q1" value="4"  />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q1_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q1" value="5"  />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q1_choice5" ) ) .'</span>
		</label> 

		</div></li>';	

	} else {

		$output .= ' <div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q1" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q1" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	
	}


	$output .= '<li><div class="left"><strong>ACTION 2</strong><p>' .  esc_attr( get_option( "mc7p_pillar1_q2_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar1_q2_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 2</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q2" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q2_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q2" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q2_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q2" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q2_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q2" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q2_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q2" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q2_choice5" ) ) .'</span>
		</label> 

		</div></li>';	

	} else {

		$output .= ' <div class="right"><strong>ANSWER 2</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q2" value="0" required >
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q2" value="1" required >
			<span>NO</span>
		</label> 

		</div></li>';	
	}


	$output .= '<li><div class="left"><strong>ACTION 3</strong><p>' .  esc_attr( get_option( "mc7p_pillar1_q3_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar1_q3_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q3" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q3_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q3" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q3_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q3" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q3_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q3" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q3_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q3" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q3_choice5" ) ) .'</span>
		</label> 

		</div></li>';	


	} else {

		$output .= '<div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q3" value="0" required >
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q3" value="1" required >
			<span>NO</span>
		</label> 

		</div></li>';	

	}

	

	$output .= '<li><div class="left"><strong>ACTION 4</strong><p>' .  esc_attr( get_option( "mc7p_pillar1_q4_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar1_q4_type" ) ) == 'multi' ) {


		$output .= ' <div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q4" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q4_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q4" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q4_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q4" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q4_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q4" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q4_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q4" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q4_choice5" ) ) .'</span>
		</label> 

		</div></li>';	

	} else {


		$output .= '<div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q4" value="0" required >
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q4" value="1" required >
			<span>NO</span>
		</label> 

		</div></li>';	

	}	

	$output .= '<li><div class="left"><strong>ACTION 5</strong><p>' .  esc_attr( get_option( "mc7p_pillar1_q5_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar1_q5_type" ) ) == 'multi' ) {
		
		$output .= ' <div class="right"><strong>ANSWER 5</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q5" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q5_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q5" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q5_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q5" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q5_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q5" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q5_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q5" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q5_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {


		$output .= ' <div class="right"><strong>ANSWER 5</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q5" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q5" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}


	$output .= '<li><div class="left"><strong>ACTION 6</strong><p>' .  esc_attr( get_option( "mc7p_pillar1_q6_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar1_q6_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q6" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q6_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q6" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q6_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q6" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q6_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q6" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q6_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q6" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q6_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q6" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q6" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 7</strong><p>' .  esc_attr( get_option( "mc7p_pillar1_q7_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar1_q7_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q7" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q7_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q7" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q7_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q7" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q7_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q7" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q7_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q7" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q7_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q7" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q7" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 8</strong><p>' .  esc_attr( get_option( "mc7p_pillar1_q8_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar1_q8_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q8" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q8_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q8" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q8_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q8" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q8_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q8" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q8_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q8" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q8_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q8" value="0"  required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q8" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}
	
	$output .= '<li><div class="left"><strong>ACTION 9</strong><p>' .  esc_attr( get_option( "mc7p_pillar1_q9_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar1_q9_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q9" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q9_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q9" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q9_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q9" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q9_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q9" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q9_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q9" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q9_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q9" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q9" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 10</strong><p>' .  esc_attr( get_option( "mc7p_pillar1_q10_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar1_q10_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q10" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q10_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q10" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q10_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q10" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q10_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q10" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q10_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q10" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar1_q10_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q10" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p1q10" value="1" required  />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<input type="hidden" name="this" value="pillar1_score" /><input type="hidden" name="next" value="' .  $atts["next"] . '" /><div class="submit"><input type="submit" value="Next Pillar..." /></ol></fieldset></form>';


	return $output;

}

add_shortcode( 'mc7p-pillar1-quiz', 'mc7p_pillar1_shortcode' );



// PILLAR 2 SHORTCODE FUNCTION

function mc7p_pillar2_shortcode( $atts ) {

	$atts = shortcode_atts( array( 'next' => 'pillar-3-choices' ), $atts );

	$output = '<form id="pillar-2-quiz" class="site-form test-form" name="pillar-2-quiz" action="' .  mc7p_url . 'process.php" method="post" >';
	$output .= '   <fieldset><legend class="site-legend">NOW COMPLETE THE ATTITUDE QUESTIONS</legend><ol class="test-questions-list">';
	 

	$output .= '<li><div class="left"><strong>ACTION 1</strong><p>' .  esc_attr( get_option( "mc7p_pillar2_q1_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar2_q1_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q1" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q1_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q1" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q1_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q1" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q1_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q1" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q1_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q1" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q1_choice5" ) ) .'</span>
		</label> 

		</div></li>';


	} else {

		$output .= ' <div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q1" value="0" required  />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q1" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	

	}


	$output .= '<li><div class="left"><strong>ACTION 1</strong><p>' .  esc_attr( get_option( "mc7p_pillar2_q2_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar2_q2_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 2</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q2" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q2_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q2" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q2_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q2" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q2_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q2" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q2_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q2" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q2_choice5" ) ) .'</span>
		</label> 

		</div></li>';


	} else {

		$output .= ' <div class="right"><strong>ANSWER 2</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q2" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q2" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	

	}



	$output .= '<li><div class="left"><strong>ACTION 3</strong><p>' .  esc_attr( get_option( "mc7p_pillar2_q3_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar2_q3_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q3" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q3_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q3" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q3_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q3" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q3_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q3" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q3_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q3" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q3_choice5" ) ) .'</span>
		</label> 

		</div></li>';


	} else {

		$output .= ' <div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q3" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q3" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';	


	}



	$output .= '<li><div class="left"><strong>ACTION 4</strong><p>' .  esc_attr( get_option( "mc7p_pillar2_q4_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar2_q4_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q4" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q4_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q4" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q4_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q4" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q4_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q4" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q4_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q4" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q4_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q4" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q4" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';	

	}


	$output .= '<li><div class="left"><strong>ACTION 5</strong><p>' .  esc_attr( get_option( "mc7p_pillar2_q5_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar2_q5_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 5</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q5" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q5_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q5" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q5_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q5" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q5_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q5" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q5_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q5" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q5_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 5</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q5" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q5" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';	

	}

	$output .= '<li><div class="left"><strong>ACTION 6</strong><p>' .  esc_attr( get_option( "mc7p_pillar2_q6_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar2_q6_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q6" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q6_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q6" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q6_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q6" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q6_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q6" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q6_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q6" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q6_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q6" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q6" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';	

	}

	$output .= '<li><div class="left"><strong>ACTION 7</strong><p>' .  esc_attr( get_option( "mc7p_pillar2_q7_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar2_q7_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q7" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q7_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q7" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q7_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q7" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q7_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q7" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q7_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q7" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q7_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q7" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q7" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 8</strong><p>' .  esc_attr( get_option( "mc7p_pillar2_q8_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar2_q8_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q8" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q8_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q8" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q8_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q8" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q8_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q8" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q8_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q8" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q8_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q8" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q8" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 9</strong><p>' .  esc_attr( get_option( "mc7p_pillar2_q9_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar2_q9_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q9" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q9_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q9" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q9_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q9" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q9_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q9" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q9_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q9" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q9_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q9" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q9" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 10</strong><p>' .  esc_attr( get_option( "mc7p_pillar2_q10_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar2_q10_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q10" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q10_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q10" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q10_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q10" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q10_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q10" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q10_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q10" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar2_q10_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q10" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p2q10" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';	


	}
	

	$output .= '<input type="hidden" name="this" value="pillar2_score" /><input type="hidden" name="next" value="' .  $atts["next"] . '" /><div class="submit"><input type="submit" value="Next Pillar..." /></ol></fieldset></form>';

	return $output;

}

add_shortcode( 'mc7p-pillar2-quiz', 'mc7p_pillar2_shortcode' );



// PILLAR 3 SHORTCODE FUNCTION

function mc7p_pillar3_shortcode( $atts ) {

	$atts = shortcode_atts( array( 'next' => 'pillar-4-partnerships' ), $atts );



	
	$output = '<form id="pillar-3-quiz" class="site-form test-form" name="pillar-3-quiz" action="' .  mc7p_url . 'process.php" method="post" >';
	$output .= '   <fieldset><legend class="site-legend">NOW COMPLETE THE CHOICE QUESTIONS</legend><ol class="test-questions-list">';
	 

	$output .= '<li><div class="left"><strong>ACTION 1</strong><p>' .  esc_attr( get_option( "mc7p_pillar3_q1_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar3_q1_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q1" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q1_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q1" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q1_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q1" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q1_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q1" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q1_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q1" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q1_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q1" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q1" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';
	}

	$output .= '<li><div class="left"><strong>ACTION 1</strong><p>' .  esc_attr( get_option( "mc7p_pillar3_q2_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar3_q2_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 2</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q2" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q2_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q2" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q2_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q2" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q2_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q2" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q2_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q2" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q2_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q1" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q1" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 3</strong><p>' .  esc_attr( get_option( "mc7p_pillar3_q3_text" ) ) .'</p></div>';	

	if( esc_Attr( get_option( "mc7p_pillar3_q3_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q3" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q3_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q3" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q3_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q3" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q3_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q3" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q3_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q3" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q3_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q3" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q3" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';


	}

	$output .= '<li><div class="left"><strong>ACTION 4</strong><p>' .  esc_attr( get_option( "mc7p_pillar3_q4_text" ) ) .'</p></div>';		

	if( esc_Attr( get_option( "mc7p_pillar3_q4_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q4" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q4_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q4" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q4_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q4" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q4_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q4" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q4_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q4" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q4_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q4" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q4" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';
	}

	$output .= '<li><div class="left"><strong>ACTION 5</strong><p>' .  esc_attr( get_option( "mc7p_pillar3_q5_text" ) ) .'</p></div>';		


	if( esc_Attr( get_option( "mc7p_pillar3_q5_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q5" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q5_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q5" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q5_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q5" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q5_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q5" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q5_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q5" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q5_choice5" ) ) .'</span>
		</label> 

		</div></li>';


	} else {

		$output .= '<div class="right"><strong>ANSWER 5</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q5" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q5" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';
	}

	$output .= '<li><div class="left"><strong>ACTION 6</strong><p>' .  esc_attr( get_option( "mc7p_pillar3_q6_text" ) ) .'</p></div>';		


	if( esc_Attr( get_option( "mc7p_pillar3_q6_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q6" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q6_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q6" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q6_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q6" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q6_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q6" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q6_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q6" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q6_choice5" ) ) .'</span>
		</label> 

		</div></li>';	

	} else {

		$output .= '<div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q6" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q6" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 7</strong><p>' .  esc_attr( get_option( "mc7p_pillar3_q7_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar3_q7_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q7" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q7_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q7" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q7_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q7" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q7_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q7" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q7_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q7" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q7_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q7" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q7" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 8</strong><p>' .  esc_attr( get_option( "mc7p_pillar3_q8_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar3_q8_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q8" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q8_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q8" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q8_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q8" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q8_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q8" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q8_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q8" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q8_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q8" value="0" />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q8" value="1" />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 9</strong><p>' .  esc_attr( get_option( "mc7p_pillar3_q9_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar3_q9_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q9" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q9_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q9" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q9_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q9" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q9_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q9" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q9_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q9" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q9_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q9" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q9" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 10</strong><p>' .  esc_attr( get_option( "mc7p_pillar3_q10_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar3_q10_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q10" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q10_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q10" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q10_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q10" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q10_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q10" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q10_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q10" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar3_q10_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q10" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p3q10" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<input type="hidden" name="this" value="pillar3_score" /><input type="hidden" name="next" value="' .  $atts["next"] . '" /><div class="submit"><input type="submit" value="Next Pillar..." /></ol></fieldset></form>';



	return $output;

}

add_shortcode( 'mc7p-pillar3-quiz', 'mc7p_pillar3_shortcode' );



// PILLAR 4 SHORCODE FUNCTION

function mc7p_pillar4_shortcode( $atts ) {

	$atts = shortcode_atts( array( 'next' => 'pillar-5-communication' ), $atts );



	$output = '<form id="pillar-4-quiz" class="site-form test-form" name="pillar-4-quiz" action="' .  mc7p_url . 'process.php" method="post" >';
	$output .= '   <fieldset><legend class="site-legend">NOW COMPLETE THE PARTNERSHIPS QUESTIONS</legend><ol class="test-questions-list">';
	 

	$output .= '<li><div class="left"><strong>ACTION 1</strong><p>' .  esc_attr( get_option( "mc7p_pillar4_q1_text" ) ) .'</p></div>';
	
	if( esc_Attr( get_option( "mc7p_pillar4_q1_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q1" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q1_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q1" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q1_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q1" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q1_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q1" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q1_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q1" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q1_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q1" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q1" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 2</strong><p>' .  esc_attr( get_option( "mc7p_pillar4_q2_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar4_q2_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 2</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q2" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q2_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q2" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q2_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q2" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q2_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q2" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q2_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q2" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q2_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 2</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q2" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q2" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 3</strong><p>' .  esc_attr( get_option( "mc7p_pillar4_q3_text" ) ) .'</p></div>';
	

	if( esc_Attr( get_option( "mc7p_pillar4_q3_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q3" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q3_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q3" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q3_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q3" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q3_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q3" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q3_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q3" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q3_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q3" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q3" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 4</strong><p>' .  esc_attr( get_option( "mc7p_pillar4_q4_text" ) ) .'</p></div>';
	
	if( esc_Attr( get_option( "mc7p_pillar4_q4_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q4" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q4_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q4" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q4_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q4" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q4_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q4" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q4_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q4" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q4_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q4" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q4" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 5</strong><p>' .  esc_attr( get_option( "mc7p_pillar4_q5_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar4_q5_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 5</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q5" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q5_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q5" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q5_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q5" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q5_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q5" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q5_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q5" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q5_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 5</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q5" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q5" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 6</strong><p>' .  esc_attr( get_option( "mc7p_pillar4_q6_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar4_q6_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q6" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q6_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q6" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q6_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q6" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q6_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q6" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q6_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q6" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q6_choice5" ) ) .'</span>
		</label> 

		</div></li>';	

	} else {

		$output .= '<div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q6" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q6" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 7</strong><p>' .  esc_attr( get_option( "mc7p_pillar4_q7_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar4_q7_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q7" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q7_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q7" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q7_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q7" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q7_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q7" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q7_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q7" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q7_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q7" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q7" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 8</strong><p>' .  esc_attr( get_option( "mc7p_pillar4_q8_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar4_q8_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q8" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q8_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q8" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q8_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q8" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q8_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q8" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q8_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q8" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q8_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q8" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q8" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 9</strong><p>' .  esc_attr( get_option( "mc7p_pillar4_q9_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar4_q9_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q9" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q9_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q9" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q9_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q9" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q9_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q9" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q9_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q9" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q9_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q9" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q9" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 10</strong><p>' .  esc_attr( get_option( "mc7p_pillar4_q10_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar4_q10_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q10" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q10_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q10" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q10_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q10" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q10_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q10" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q10_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q10" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar4_q10_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q10" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p4q10" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	

	$output .= '<input type="hidden" name="this" value="pillar4_score" /><input type="hidden" name="next" value="' .  $atts["next"] . '" /><div class="submit"><input type="submit" value="Next Pillar..." /></ol></fieldset></form>';



	return $output;

}

add_shortcode( 'mc7p-pillar4-quiz', 'mc7p_pillar4_shortcode' );



// PILLAR 5 SHORTCODE FUNCTION

function mc7p_pillar5_shortcode( $atts ) {

	$atts = shortcode_atts( array( 'next' => 'pillar-6-policy' ), $atts );


	$output = '<form id="pillar-5-quiz" class="site-form test-form" name="pillar-5-quiz" action="' .  mc7p_url . 'process.php" method="post" >';
	$output .= '   <fieldset><legend class="site-legend">NOW COMPLETE THE COMMUNICATION QUESTIONS</legend><ol class="test-questions-list">';
	 

	$output .= '<li><div class="left"><strong>ACTION 1</strong><p>' .  esc_attr( get_option( "mc7p_pillar5_q1_text" ) ) .'</p></div>';
	

	if( esc_Attr( get_option( "mc7p_pillar5_q1_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q1" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q1_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q1" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q1_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q1" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q1_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q1" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q1_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q1" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q1_choice5" ) ) .'</span>
		</label> 

		</div></li>';


	} else {

		$output .= '<div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q1" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q1" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 2</strong><p>' .  esc_attr( get_option( "mc7p_pillar5_q2_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar5_q2_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 2</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q2" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q2_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q2" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q2_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q2" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q2_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q2" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q2_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q2" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q2_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 2</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q2" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q2" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';


	}

	$output .= '<li><div class="left"><strong>ACTION 3</strong><p>' .  esc_attr( get_option( "mc7p_pillar5_q3_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar5_q3_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q3" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q3_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q3" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q3_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q3" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q3_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q3" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q3_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q3" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q3_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q3" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q3" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 4</strong><p>' .  esc_attr( get_option( "mc7p_pillar5_q4_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar5_q4_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q4" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q4_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q4" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q4_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q4" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q4_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q4" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q4_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q4" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q4_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q4" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q4" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 5</strong><p>' .  esc_attr( get_option( "mc7p_pillar5_q5_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar5_q5_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 5</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q5" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q5_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q5" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q5_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q5" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q5_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q5" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q5_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q5" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q5_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 5</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q5" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q5" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 6</strong><p>' .  esc_attr( get_option( "mc7p_pillar5_q6_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar5_q6_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q6" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q6_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q6" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q6_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q6" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q6_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q6" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q6_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q6" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q6_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q6" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q6" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 7</strong><p>' .  esc_attr( get_option( "mc7p_pillar5_q7_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar5_q7_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q7" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q7_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q7" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q7_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q7" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q7_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q7" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q7_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q7" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q7_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q7" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q7" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 8</strong><p>' .  esc_attr( get_option( "mc7p_pillar5_q8_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar5_q8_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q8" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q8_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q8" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q8_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q8" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q8_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q8" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q8_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q8" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q8_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q8" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q8" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 9</strong><p>' .  esc_attr( get_option( "mc7p_pillar5_q9_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar5_q9_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q9" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q9_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q9" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q9_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q9" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q9_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q9" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q9_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q9" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q9_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q9" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q9" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 10</strong><p>' .  esc_attr( get_option( "mc7p_pillar5_q10_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar5_q10_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q10" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q10_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q10" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q10_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q10" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q10_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q10" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q10_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q10" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar5_q10_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q10" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p5q10" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	

	$output .= '<input type="hidden" name="this" value="pillar5_score" /><input type="hidden" name="next" value="' .  $atts["next"] . '" /><div class="submit"><input type="submit" value="Next Pillar..." /></ol></fieldset></form>';



	return $output;

}

add_shortcode( 'mc7p-pillar5-quiz', 'mc7p_pillar5_shortcode' );



// PILLAR 6 SHORTCODE FUNCTION

function mc7p_pillar6_shortcode( $atts ) {

	$atts = shortcode_atts( array( 'next' => 'pillar-7-opportunity' ), $atts );


	$output = '<form id="pillar-6-quiz" class="site-form test-form" name="pillar-6-quiz" action="' .  mc7p_url . 'process.php" method="post" >';
	$output .= '   <fieldset><legend class="site-legend">NOW COMPLETE THE POLICY QUESTIONS</legend><ol class="test-questions-list">';
	 

	$output .= '<li><div class="left"><strong>ACTION 1</strong><p>' .  esc_attr( get_option( "mc7p_pillar6_q1_text" ) ) .'</p></div>';

	
	if( esc_Attr( get_option( "mc7p_pillar6_q1_type" ) ) == 'multi' ) {

			$output .= ' <div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q1" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q1_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q1" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q1_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q1" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q1_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q1" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q1_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q1" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q1_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q1" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q1" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 2</strong><p>' .  esc_attr( get_option( "mc7p_pillar6_q2_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar6_q2_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 2</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q2" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q2_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q2" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q2_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q2" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q2_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q2" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q2_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q2" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q2_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 2</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q2" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q2" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 3</strong><p>' .  esc_attr( get_option( "mc7p_pillar6_q3_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar6_q3_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q3" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q3_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q3" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q3_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q3" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q3_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q3" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q3_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q3" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q3_choice5" ) ) .'</span>
		</label> 

		</div></li>';	

	} else {

		$output .= '<div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q3" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q3" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 4</strong><p>' .  esc_attr( get_option( "mc7p_pillar6_q4_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar6_q4_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q4" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q4_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q4" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q4_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q4" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q4_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q4" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q4_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q4" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q4_choice5" ) ) .'</span>
		</label> 

		</div></li>';	

	} else {

		$output .= '<div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q4" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q4" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 5</strong><p>' .  esc_attr( get_option( "mc7p_pillar6_q5_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar6_q5_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 5</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q5" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q5_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q5" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q5_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q5" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q5_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q5" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q5_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q5" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q5_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 5</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q5" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q5" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 6</strong><p>' .  esc_attr( get_option( "mc7p_pillar6_q6_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar6_q6_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q6" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q6_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q6" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q6_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q6" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q6_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q6" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q6_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q6" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q6_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q6" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q6" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 7</strong><p>' .  esc_attr( get_option( "mc7p_pillar6_q7_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar6_q7_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q7" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q7_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q7" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q7_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q7" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q7_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q7" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q7_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q7" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q7_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q7" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q7" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 8</strong><p>' .  esc_attr( get_option( "mc7p_pillar6_q8_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar6_q8_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q8" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q8_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q8" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q8_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q8" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q8_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q8" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q8_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q8" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q8_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q8" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q8" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 9</strong><p>' .  esc_attr( get_option( "mc7p_pillar6_q9_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar6_q9_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q9" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q9_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q9" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q9_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q9" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q9_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q9" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q9_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q9" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q9_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q9" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q9" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 10</strong><p>' .  esc_attr( get_option( "mc7p_pillar6_q10_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar6_q10_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q10" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q10_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q10" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q10_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q10" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q10_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q10" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q10_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q10" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar6_q10_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q10" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p6q10" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	

	$output .= '<input type="hidden" name="this" value="pillar6_score" /><input type="hidden" name="next" value="' .  $atts["next"] . '" /><div class="submit"><input type="submit" value="Next Pillar..." /></ol></fieldset></form>';



	return $output;

}

add_shortcode( 'mc7p-pillar6-quiz', 'mc7p_pillar6_shortcode' );



// PILLAR 7 SHORTCODE FUNCTION

function mc7p_pillar7_shortcode( $atts ) {

	$atts = shortcode_atts( array( 'next' => mc7p_url . 'score-sheet.php' ), $atts );



	$output = '<form id="pillar-7-quiz" class="site-form test-form" name="pillar-7-quiz" action="' .  mc7p_url . 'process.php" method="post" >';
	$output .= '   <fieldset><legend class="site-legend">NOW COMPLETE THE OPPORTUNITIES QUESTIONS</legend><ol class="test-questions-list">';
	 

	$output .= '<li><div class="left"><strong>ACTION 1</strong><p>' .  esc_attr( get_option( "mc7p_pillar7_q1_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar7_q1_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q1" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q1_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q1" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q1_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q1" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q1_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q1" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q1_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q1" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q1_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 1</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q1" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q1" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}


	$output .= '<li><div class="left"><strong>ACTION 2</strong><p>' .  esc_attr( get_option( "mc7p_pillar7_q2_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar7_q2_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 2</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q2" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q2_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q2" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q2_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q2" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q2_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q2" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q2_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q2" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q2_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 2</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q2" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q2" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 3</strong><p>' .  esc_attr( get_option( "mc7p_pillar7_q3_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar7_q3_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q3" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q3_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q3" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q3_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q3" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q3_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q3" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q3_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q3" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q3_choice5" ) ) .'</span>
		</label> 

		</div></li>';	

	} else {

		$output .= '<div class="right"><strong>ANSWER 3</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q3" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q3" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 4</strong><p>' .  esc_attr( get_option( "mc7p_pillar7_q4_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar7_q4_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q4" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q4_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q4" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q4_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q4" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q4_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q4" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q4_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q4" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q4_choice5" ) ) .'</span>
		</label> 

		</div></li>';		

	} else {

		$output .= '<div class="right"><strong>ANSWER 4</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q4" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q4" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 5</strong><p>' .  esc_attr( get_option( "mc7p_pillar7_q5_text" ) ) .'</p></div>';


	if( esc_Attr( get_option( "mc7p_pillar7_q5_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 5</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q5" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q5_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q5" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q5_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q5" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q5_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q5" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q5_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q5" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q5_choice5" ) ) .'</span>
		</label> 

		</div></li>';		

	} else {

		$output .= '<div class="right"><strong>ANSWER 5</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q5" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q5" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 6</strong><p>' .  esc_attr( get_option( "mc7p_pillar7_q6_text" ) ) .'</p></div>';
	

	if( esc_Attr( get_option( "mc7p_pillar7_q6_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q5" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q5_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q5" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q5_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q5" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q5_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q5" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q5_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q5" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q5_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= '<div class="right"><strong>ANSWER 6</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q6" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q6" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';

	}

	$output .= '<li><div class="left"><strong>ACTION 7</strong><p>' .  esc_attr( get_option( "mc7p_pillar7_q7_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar7_q7_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q7" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q7_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q7" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q7_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q7" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q7_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q7" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q7_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q7" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q7_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 7</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q7" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q7" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 8</strong><p>' .  esc_attr( get_option( "mc7p_pillar7_q8_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar7_q8_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q8" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q8_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q8" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q8_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q8" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q8_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q8" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q8_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q8" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q8_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 8</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q8" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q8" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 9</strong><p>' .  esc_attr( get_option( "mc7p_pillar7_q9_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar7_q9_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q9" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q9_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q9" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q9_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q9" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q9_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q9" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q9_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q9" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q9_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 9</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q9" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q9" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	$output .= '<li><div class="left"><strong>ACTION 10</strong><p>' .  esc_attr( get_option( "mc7p_pillar7_q10_text" ) ) .'</p></div>';

	if( esc_Attr( get_option( "mc7p_pillar7_q10_type" ) ) == 'multi' ) {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q10" value="1" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q10_choice1" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q10" value="2" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q10_choice2" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q10" value="3" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q10_choice3" ) ) .'</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q10" value="4" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q10_choice4" ) ) .'</span>
		</label>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q10" value="5" required />
			<span>'.  esc_attr( get_option( "mc7p_pillar7_q10_choice5" ) ) .'</span>
		</label> 

		</div></li>';

	} else {

		$output .= ' <div class="right"><strong>ANSWER 10</strong>

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q10" value="0" required />
			<span>Yes</span>
		</label> 

		<label class="styled-radio">
			<input type="radio" name="mc7p_p7q10" value="1" required />
			<span>NO</span>
		</label> 

		</div></li>';	


	}

	

	$output .= '<input type="hidden" name="this" value="pillar7_score" /><input type="hidden" name="next" value="' .  $atts["next"] . '" /><div class="submit"><input type="submit" value="Next Pillar..." /></ol></fieldset></form>';



	return $output;

}

add_shortcode( 'mc7p-pillar7-quiz', 'mc7p_pillar7_shortcode' );



// USER'S RESULTS PAGE SHORTCODE

function mc7p_results_user_page() {

	global $wpdb;

	$table_name = $wpdb->prefix . "mc7p";

	$user_id = get_current_user_id();

	$quizes = $wpdb->get_results( 'SELECT * FROM ' . $table_name . ' WHERE user_id = ' . $user_id, ARRAY_A );

	

	$output = '<style type="text/css">

		#results-table table {

			width: 100%;

			min-width: 750px;

		}



		#results-table table, #results-table table th, #results-table table td {

			border: solid 1px #B5B5B5;

			padding: 5px;

			border-collapse: collapse;

			font-size: 11px;

		}



		#results-table table th {

			text-align: center;

			color: #FFFFFF;

			background: #D4D4D4;

			font-size: 13px;

			font-weight: bold;

			cursor: pointer;

		}

	</style>

	<div id="results-table">

		<table>

			<tr class="column-titles">

				<th>Date</th>

				<th>Pillar 1<br />Score</th>

				<th>Pillar 2<br />Score</th>

				<th>Pillar 3<br />Score</th>

				<th>Pillar 4<br />Score</th>

				<th>Pillar 5<br />Score</th>

				<th>Pillar 6<br />Score</th>

				<th>Pillar 7<br />Score</th>

				<th>Total<br />Score</th>

				<th>Results<br />Pages</th>

			</tr>';

	foreach( $quizes as $quiz ) {

		$output .= '	<tr>

				<td>' . date( "d-m-Y", strtotime( $quiz["date_filled"] ) ) . '</td>

				<td>' . $quiz["pillar1_score"] . '</td>

				<td>' . $quiz["pillar2_score"] . '</td>

				<td>' . $quiz["pillar3_score"] . '</td>

				<td>' . $quiz["pillar4_score"] . '</td>

				<td>' . $quiz["pillar5_score"] . '</td>

				<td>' . $quiz["pillar6_score"] . '</td>

				<td>' . $quiz["pillar7_score"] . '</td>

				<td>' . $quiz["total_score"] . '</td>

				<td><a href="' . plugins_url( "score-sheet.php" , __FILE__ ) . '?form=' .  $quiz["id"] . '">View Results</a></td>

			</tr>';

	}

	$output .= '</table></div>';

	

	return $output;



}

add_shortcode( 'mc7p-results-user-page', 'mc7p_results_user_page' );

