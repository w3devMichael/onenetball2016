<?php
/**
 * 7 Pillars Questionnaire Plugin
 * Include to add the plugin admin pages to the admin interface.
 * File version: 1.0
 */


// Pillar 1 settings page.
function mc7p_pillar1_settings() {

	?>
	<div class="wrap">
		<h2>Pillar 1 Settings and Options</h2>
		<p> Options and settings page for pillar 1</p>
		
		<form action="options.php" method="post" id="mc7p_pillar1_settings_form">
			<?php settings_fields('mc7p_p1_settings'); ?>
			<!-- Question 1 -->
			<div class="q-container" id="mc7p_pillar1_q1_container">
				<h3>Question 1</h3>
				<p class="qtext"><label for="mc7p_pillar1_q1_text">Question Text:</label><br />
					<textarea name="mc7p_pillar1_q1_text" id="mc7p_pillar1_q1_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar1_q1_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar1_q1_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q1_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar1_q1_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q1_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar1_q1_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar1_q1_choice1">Choice 1  </label><input type="text" name="mc7p_pillar1_q1_choice1" id="mc7p_pillar1_q1_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar1_q1_choice1')); ?>" /><br />
					<label for="mc7p_pillar1_q1_choice2">Choice 2  </label><input type="text" name="mc7p_pillar1_q1_choice2" id="mc7p_pillar1_q1_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar1_q1_choice2')); ?>" /><br />
					<label for="mc7p_pillar1_q1_choice3">Choice 3  </label><input type="text" name="mc7p_pillar1_q1_choice3" id="mc7p_pillar1_q1_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar1_q1_choice3')); ?>" /><br />
					<!-- <label for="mc7p_pillar1_q1_choice4">Choice 4  </label><input type="text" name="mc7p_pillar1_q1_choice4" id="mc7p_pillar1_q1_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar1_q1_choice4')); ?>" /><br /> -->
					<!-- <label for="mc7p_pillar1_q1_choice5">Choice 5  </label><input type="text" name="mc7p_pillar1_q1_choice5" id="mc7p_pillar1_q1_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar1_q1_choice5')); ?>" /><br /> -->
			</div>
			<!-- Question 2 -->
			<div class="q-container" id="mc7p_pillar1_q2_container">
				<h3>Question 2</h3>
				<p class="qtext"><label for="mc7p_pillar1_q2_text">Question Text:</label><br />
					<textarea name="mc7p_pillar1_q2_text" id="mc7p_pillar1_q2_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar1_q2_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar1_q2_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q2_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar1_q2_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q2_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar1_q2_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar1_q2_choice1">Choice 1  </label><input type="text" name="mc7p_pillar1_q2_choice1" id="mc7p_pillar1_q2_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar1_q2_choice1')); ?>" /><br />
					<label for="mc7p_pillar1_q2_choice2">Choice 2  </label><input type="text" name="mc7p_pillar1_q2_choice2" id="mc7p_pillar1_q2_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar1_q2_choice2')); ?>" /><br />
					<label for="mc7p_pillar1_q2_choice3">Choice 3  </label><input type="text" name="mc7p_pillar1_q2_choice3" id="mc7p_pillar1_q2_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar1_q2_choice3')); ?>" /><br />
					<!-- <label for="mc7p_pillar1_q2_choice4">Choice 4  </label><input type="text" name="mc7p_pillar1_q2_choice4" id="mc7p_pillar1_q2_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar1_q2_choice4')); ?>" /><br /> -->
					<!-- <label for="mc7p_pillar1_q2_choice5">Choice 5  </label><input type="text" name="mc7p_pillar1_q2_choice5" id="mc7p_pillar1_q2_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar1_q2_choice5')); ?>" /><br /> -->
			</div>
			<!-- Question 3 -->
			<div class="q-container" id="mc7p_pillar1_q3_container">
				<h3>Question 3</h3>
				<p class="qtext"><label for="mc7p_pillar1_q3_text">Question Text:</label><br />
					<textarea name="mc7p_pillar1_q3_text" id="mc7p_pillar1_q3_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar1_q3_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar1_q3_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q3_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar1_q3_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q3_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar1_q3_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar1_q3_choice1">Choice 1  </label><input type="text" name="mc7p_pillar1_q3_choice1" id="mc7p_pillar1_q3_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar1_q3_choice1')); ?>" /><br />
					<label for="mc7p_pillar1_q3_choice2">Choice 2  </label><input type="text" name="mc7p_pillar1_q3_choice2" id="mc7p_pillar1_q3_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar1_q3_choice2')); ?>" /><br />
					<label for="mc7p_pillar1_q3_choice3">Choice 3  </label><input type="text" name="mc7p_pillar1_q3_choice3" id="mc7p_pillar1_q3_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar1_q3_choice3')); ?>" /><br />
					<!-- <label for="mc7p_pillar1_q3_choice4">Choice 4  </label><input type="text" name="mc7p_pillar1_q3_choice4" id="mc7p_pillar1_q3_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar1_q3_choice4')); ?>" /><br /> -->
					<!-- <label for="mc7p_pillar1_q3_choice5">Choice 5  </label><input type="text" name="mc7p_pillar1_q3_choice5" id="mc7p_pillar1_q3_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar1_q3_choice5')); ?>" /><br /> -->
			</div>
			<!-- Question 4 -->
			<div class="q-container" id="mc7p_pillar1_q4_container">
				<h3>Question 4</h3>
				<p class="qtext"><label for="mc7p_pillar1_q4_text">Question Text:</label><br />
					<textarea name="mc7p_pillar1_q4_text" id="mc7p_pillar1_q4_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar1_q4_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar1_q4_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q4_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar1_q4_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q4_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar1_q4_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar1_q4_choice1">Choice 1  </label><input type="text" name="mc7p_pillar1_q4_choice1" id="mc7p_pillar1_q4_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar1_q4_choice1')); ?>" /><br />
					<label for="mc7p_pillar1_q4_choice2">Choice 2  </label><input type="text" name="mc7p_pillar1_q4_choice2" id="mc7p_pillar1_q4_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar1_q4_choice2')); ?>" /><br />
					<label for="mc7p_pillar1_q4_choice3">Choice 3  </label><input type="text" name="mc7p_pillar1_q4_choice3" id="mc7p_pillar1_q4_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar1_q4_choice3')); ?>" /><br />
					<!-- <label for="mc7p_pillar1_q4_choice4">Choice 4  </label><input type="text" name="mc7p_pillar1_q4_choice4" id="mc7p_pillar1_q4_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar1_q4_choice4')); ?>" /><br /> -->
					<!-- <label for="mc7p_pillar1_q4_choice5">Choice 5  </label><input type="text" name="mc7p_pillar1_q4_choice5" id="mc7p_pillar1_q4_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar1_q4_choice5')); ?>" /><br /> -->
			</div>
			<!-- Question 5 -->
			<div class="q-container" id="mc7p_pillar1_q5_container">
				<h3>Question 5</h3>
				<p class="qtext"><label for="mc7p_pillar1_q5_text">Question Text:</label><br />
					<textarea name="mc7p_pillar1_q5_text" id="mc7p_pillar1_q5_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar1_q5_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar1_q5_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q5_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar1_q5_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q5_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar1_q5_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar1_q5_choice1">Choice 1  </label><input type="text" name="mc7p_pillar1_q5_choice1" id="mc7p_pillar1_q5_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar1_q5_choice1')); ?>" /><br />
					<label for="mc7p_pillar1_q5_choice2">Choice 2  </label><input type="text" name="mc7p_pillar1_q5_choice2" id="mc7p_pillar1_q5_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar1_q5_choice2')); ?>" /><br />
					<label for="mc7p_pillar1_q5_choice3">Choice 3  </label><input type="text" name="mc7p_pillar1_q5_choice3" id="mc7p_pillar1_q5_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar1_q5_choice3')); ?>" /><br />
					<!-- <label for="mc7p_pillar1_q5_choice4">Choice 4  </label><input type="text" name="mc7p_pillar1_q5_choice4" id="mc7p_pillar1_q5_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar1_q5_choice4')); ?>" /><br /> -->
					<!-- <label for="mc7p_pillar1_q5_choice5">Choice 5  </label><input type="text" name="mc7p_pillar1_q5_choice5" id="mc7p_pillar1_q5_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar1_q5_choice5')); ?>" /><br /> -->
			</div>
			<!-- Question 6 -->
			<div class="q-container" id="mc7p_pillar1_q6_container">
				<h3>Question 6</h3>
				<p class="qtext"><label for="mc7p_pillar1_q6_text">Question Text:</label><br />
					<textarea name="mc7p_pillar1_q6_text" id="mc7p_pillar1_q6_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar1_q6_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar1_q6_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q6_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar1_q6_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q6_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar1_q6_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar1_q6_choice1">Choice 1  </label><input type="text" name="mc7p_pillar1_q6_choice1" id="mc7p_pillar1_q6_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar1_q6_choice1')); ?>" /><br />
					<label for="mc7p_pillar1_q6_choice2">Choice 2  </label><input type="text" name="mc7p_pillar1_q6_choice2" id="mc7p_pillar1_q6_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar1_q6_choice2')); ?>" /><br />
					<label for="mc7p_pillar1_q6_choice3">Choice 3  </label><input type="text" name="mc7p_pillar1_q6_choice3" id="mc7p_pillar1_q6_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar1_q6_choice3')); ?>" /><br />
					<!-- <label for="mc7p_pillar1_q6_choice4">Choice 4  </label><input type="text" name="mc7p_pillar1_q6_choice4" id="mc7p_pillar1_q6_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar1_q6_choice4')); ?>" /><br /> -->
					<!-- <label for="mc7p_pillar1_q6_choice5">Choice 5  </label><input type="text" name="mc7p_pillar1_q6_choice5" id="mc7p_pillar1_q6_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar1_q6_choice5')); ?>" /><br /> -->
			</div>
			<!-- Question 7 -->
			<div class="q-container" id="mc7p_pillar1_q7_container">
				<h3>Question 7</h3>
				<p class="qtext"><label for="mc7p_pillar1_q7_text">Question Text:</label><br />
					<textarea name="mc7p_pillar1_q7_text" id="mc7p_pillar1_q7_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar1_q7_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar1_q7_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q7_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar1_q7_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q7_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar1_q7_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar1_q7_choice1">Choice 1  </label><input type="text" name="mc7p_pillar1_q7_choice1" id="mc7p_pillar1_q7_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar1_q7_choice1')); ?>" /><br />
					<label for="mc7p_pillar1_q7_choice2">Choice 2  </label><input type="text" name="mc7p_pillar1_q7_choice2" id="mc7p_pillar1_q7_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar1_q7_choice2')); ?>" /><br />
					<label for="mc7p_pillar1_q7_choice3">Choice 3  </label><input type="text" name="mc7p_pillar1_q7_choice3" id="mc7p_pillar1_q7_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar1_q7_choice3')); ?>" /><br />
					<!-- <label for="mc7p_pillar1_q7_choice4">Choice 4  </label><input type="text" name="mc7p_pillar1_q7_choice4" id="mc7p_pillar1_q7_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar1_q7_choice4')); ?>" /><br /> -->
					<!-- <label for="mc7p_pillar1_q7_choice5">Choice 5  </label><input type="text" name="mc7p_pillar1_q7_choice5" id="mc7p_pillar1_q7_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar1_q7_choice5')); ?>" /><br /> -->
			</div>
			<!-- Question 8 -->
			<div class="q-container" id="mc7p_pillar1_q8_container">
				<h3>Question 8</h3>
				<p class="qtext"><label for="mc7p_pillar1_q8_text">Question Text:</label><br />
					<textarea name="mc7p_pillar1_q8_text" id="mc7p_pillar1_q8_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar1_q8_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar1_q8_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q8_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar1_q8_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q8_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar1_q8_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar1_q8_choice1">Choice 1  </label><input type="text" name="mc7p_pillar1_q8_choice1" id="mc7p_pillar1_q8_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar1_q8_choice1')); ?>" /><br />
					<label for="mc7p_pillar1_q8_choice2">Choice 2  </label><input type="text" name="mc7p_pillar1_q8_choice2" id="mc7p_pillar1_q8_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar1_q8_choice2')); ?>" /><br />
					<label for="mc7p_pillar1_q8_choice3">Choice 3  </label><input type="text" name="mc7p_pillar1_q8_choice3" id="mc7p_pillar1_q8_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar1_q8_choice3')); ?>" /><br />
					<!-- <label for="mc7p_pillar1_q8_choice4">Choice 4  </label><input type="text" name="mc7p_pillar1_q8_choice4" id="mc7p_pillar1_q8_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar1_q8_choice4')); ?>" /><br /> -->
					<!-- <label for="mc7p_pillar1_q8_choice5">Choice 5  </label><input type="text" name="mc7p_pillar1_q8_choice5" id="mc7p_pillar1_q8_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar1_q8_choice5')); ?>" /><br /> -->
			</div>
			<!-- Question 9 -->
			<div class="q-container" id="mc7p_pillar1_q9_container">
				<h3>Question 9</h3>
				<p class="qtext"><label for="mc7p_pillar1_q9_text">Question Text:</label><br />
					<textarea name="mc7p_pillar1_q9_text" id="mc7p_pillar1_q9_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar1_q9_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar1_q9_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q9_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar1_q9_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q9_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar1_q9_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar1_q9_choice1">Choice 1  </label><input type="text" name="mc7p_pillar1_q9_choice1" id="mc7p_pillar1_q9_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar1_q9_choice1')); ?>" /><br />
					<label for="mc7p_pillar1_q9_choice2">Choice 2  </label><input type="text" name="mc7p_pillar1_q9_choice2" id="mc7p_pillar1_q9_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar1_q9_choice2')); ?>" /><br />
					<label for="mc7p_pillar1_q9_choice3">Choice 3  </label><input type="text" name="mc7p_pillar1_q9_choice3" id="mc7p_pillar1_q9_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar1_q9_choice3')); ?>" /><br />
					<!-- <label for="mc7p_pillar1_q9_choice4">Choice 4  </label><input type="text" name="mc7p_pillar1_q9_choice4" id="mc7p_pillar1_q9_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar1_q9_choice4')); ?>" /><br /> -->
					<!-- <label for="mc7p_pillar1_q9_choice5">Choice 5  </label><input type="text" name="mc7p_pillar1_q9_choice5" id="mc7p_pillar1_q9_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar1_q9_choice5')); ?>" /><br /> -->
			</div>
			<!-- Question 10 -->
			<div class="q-container" id="mc7p_pillar1_q10_container">
				<h3>Question 10</h3>
				<p class="qtext"><label for="mc7p_pillar1_q10_text">Question Text:</label><br />
					<textarea name="mc7p_pillar1_q10_text" id="mc7p_pillar1_q10_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar1_q10_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar1_q10_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q10_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar1_q10_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar1_q10_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar1_q10_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar1_q10_choice1">Choice 1  </label><input type="text" name="mc7p_pillar1_q10_choice1" id="mc7p_pillar1_q10_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar1_q10_choice1')); ?>" /><br />
					<label for="mc7p_pillar1_q10_choice2">Choice 2  </label><input type="text" name="mc7p_pillar1_q10_choice2" id="mc7p_pillar1_q10_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar1_q10_choice2')); ?>" /><br />
					<label for="mc7p_pillar1_q10_choice3">Choice 3  </label><input type="text" name="mc7p_pillar1_q10_choice3" id="mc7p_pillar1_q10_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar1_q10_choice3')); ?>" /><br />
					<!-- <label for="mc7p_pillar1_q10_choice4">Choice 4  </label><input type="text" name="mc7p_pillar1_q10_choice4" id="mc7p_pillar1_q10_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar1_q10_choice4')); ?>" /><br /> -->
					<!-- <label for="mc7p_pillar1_q10_choice5">Choice 5  </label><input type="text" name="mc7p_pillar1_q10_choice5" id="mc7p_pillar1_q10_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar1_q10_choice5')); ?>" /><br /> -->
			</div>
			<p><input type="submit" name="submit" value="Save Options" /></p>
		</form>
	</div>
	<?php
}

// Pillar 2 settings page.
function mc7p_pillar2_settings() {

	?>
	<div class="wrap">
		<h2>Pillar 2 Settings and Options</h2>
		<p> Options and settings page for pillar 2</p>
		
		<form action="options.php" method="post" id="mc7p_pillar2_settings_form">
			<?php settings_fields('mc7p_p2_settings'); ?>
			<!-- Question 1 -->
			<div class="q-container" id="mc7p_pillar2_q1_container">
				<h3>Question 1</h3>
				<p class="qtext"><label for="mc7p_pillar2_q1_text">Question Text:</label><br />
					<textarea name="mc7p_pillar2_q1_text" id="mc7p_pillar2_q1_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar2_q1_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar2_q1_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q1_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar2_q1_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q1_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar2_q1_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar2_q1_choice1">Choice 1  </label><input type="text" name="mc7p_pillar2_q1_choice1" id="mc7p_pillar2_q1_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar2_q1_choice1')); ?>" /><br />
					<label for="mc7p_pillar2_q1_choice2">Choice 2  </label><input type="text" name="mc7p_pillar2_q1_choice2" id="mc7p_pillar2_q1_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar2_q1_choice2')); ?>" /><br />
					<label for="mc7p_pillar2_q1_choice3">Choice 3  </label><input type="text" name="mc7p_pillar2_q1_choice3" id="mc7p_pillar2_q1_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar2_q1_choice3')); ?>" /><br />
					<label for="mc7p_pillar2_q1_choice4">Choice 4  </label><input type="text" name="mc7p_pillar2_q1_choice4" id="mc7p_pillar2_q1_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar2_q1_choice4')); ?>" /><br />
					<label for="mc7p_pillar2_q1_choice5">Choice 5  </label><input type="text" name="mc7p_pillar2_q1_choice5" id="mc7p_pillar2_q1_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar2_q1_choice5')); ?>" /><br />
			</div>
			<!-- Question 2 -->
			<div class="q-container" id="mc7p_pillar2_q2_container">
				<h3>Question 2</h3>
				<p class="qtext"><label for="mc7p_pillar2_q2_text">Question Text:</label><br />
					<textarea name="mc7p_pillar2_q2_text" id="mc7p_pillar2_q2_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar2_q2_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar2_q2_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q2_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar2_q2_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q2_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar2_q2_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar2_q2_choice1">Choice 1  </label><input type="text" name="mc7p_pillar2_q2_choice1" id="mc7p_pillar2_q2_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar2_q2_choice1')); ?>" /><br />
					<label for="mc7p_pillar2_q2_choice2">Choice 2  </label><input type="text" name="mc7p_pillar2_q2_choice2" id="mc7p_pillar2_q2_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar2_q2_choice2')); ?>" /><br />
					<label for="mc7p_pillar2_q2_choice3">Choice 3  </label><input type="text" name="mc7p_pillar2_q2_choice3" id="mc7p_pillar2_q2_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar2_q2_choice3')); ?>" /><br />
					<label for="mc7p_pillar2_q2_choice4">Choice 4  </label><input type="text" name="mc7p_pillar2_q2_choice4" id="mc7p_pillar2_q2_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar2_q2_choice4')); ?>" /><br />
					<label for="mc7p_pillar2_q2_choice5">Choice 5  </label><input type="text" name="mc7p_pillar2_q2_choice5" id="mc7p_pillar2_q2_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar2_q2_choice5')); ?>" /><br />
			</div>
			<!-- Question 3 -->
			<div class="q-container" id="mc7p_pillar2_q3_container">
				<h3>Question 3</h3>
				<p class="qtext"><label for="mc7p_pillar2_q3_text">Question Text:</label><br />
					<textarea name="mc7p_pillar2_q3_text" id="mc7p_pillar2_q3_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar2_q3_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar2_q3_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q3_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar2_q3_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q3_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar2_q3_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar2_q3_choice1">Choice 1  </label><input type="text" name="mc7p_pillar2_q3_choice1" id="mc7p_pillar2_q3_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar2_q3_choice1')); ?>" /><br />
					<label for="mc7p_pillar2_q3_choice2">Choice 2  </label><input type="text" name="mc7p_pillar2_q3_choice2" id="mc7p_pillar2_q3_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar2_q3_choice2')); ?>" /><br />
					<label for="mc7p_pillar2_q3_choice3">Choice 3  </label><input type="text" name="mc7p_pillar2_q3_choice3" id="mc7p_pillar2_q3_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar2_q3_choice3')); ?>" /><br />
					<label for="mc7p_pillar2_q3_choice4">Choice 4  </label><input type="text" name="mc7p_pillar2_q3_choice4" id="mc7p_pillar2_q3_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar2_q3_choice4')); ?>" /><br />
					<label for="mc7p_pillar2_q3_choice5">Choice 5  </label><input type="text" name="mc7p_pillar2_q3_choice5" id="mc7p_pillar2_q3_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar2_q3_choice5')); ?>" /><br />
			</div>
			<!-- Question 4 -->
			<div class="q-container" id="mc7p_pillar2_q4_container">
				<h3>Question 4</h3>
				<p class="qtext"><label for="mc7p_pillar2_q4_text">Question Text:</label><br />
					<textarea name="mc7p_pillar2_q4_text" id="mc7p_pillar2_q4_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar2_q4_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar2_q4_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q4_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar2_q4_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q4_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar2_q4_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar2_q4_choice1">Choice 1  </label><input type="text" name="mc7p_pillar2_q4_choice1" id="mc7p_pillar2_q4_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar2_q4_choice1')); ?>" /><br />
					<label for="mc7p_pillar2_q4_choice2">Choice 2  </label><input type="text" name="mc7p_pillar2_q4_choice2" id="mc7p_pillar2_q4_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar2_q4_choice2')); ?>" /><br />
					<label for="mc7p_pillar2_q4_choice3">Choice 3  </label><input type="text" name="mc7p_pillar2_q4_choice3" id="mc7p_pillar2_q4_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar2_q4_choice3')); ?>" /><br />
					<label for="mc7p_pillar2_q4_choice4">Choice 4  </label><input type="text" name="mc7p_pillar2_q4_choice4" id="mc7p_pillar2_q4_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar2_q4_choice4')); ?>" /><br />
					<label for="mc7p_pillar2_q4_choice5">Choice 5  </label><input type="text" name="mc7p_pillar2_q4_choice5" id="mc7p_pillar2_q4_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar2_q4_choice5')); ?>" /><br />
			</div>
			<!-- Question 5 -->
			<div class="q-container" id="mc7p_pillar2_q5_container">
				<h3>Question 5</h3>
				<p class="qtext"><label for="mc7p_pillar2_q5_text">Question Text:</label><br />
					<textarea name="mc7p_pillar2_q5_text" id="mc7p_pillar2_q5_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar2_q5_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar2_q5_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q5_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar2_q5_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q5_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar2_q5_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar2_q5_choice1">Choice 1  </label><input type="text" name="mc7p_pillar2_q5_choice1" id="mc7p_pillar2_q5_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar2_q5_choice1')); ?>" /><br />
					<label for="mc7p_pillar2_q5_choice2">Choice 2  </label><input type="text" name="mc7p_pillar2_q5_choice2" id="mc7p_pillar2_q5_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar2_q5_choice2')); ?>" /><br />
					<label for="mc7p_pillar2_q5_choice3">Choice 3  </label><input type="text" name="mc7p_pillar2_q5_choice3" id="mc7p_pillar2_q5_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar2_q5_choice3')); ?>" /><br />
					<label for="mc7p_pillar2_q5_choice4">Choice 4  </label><input type="text" name="mc7p_pillar2_q5_choice4" id="mc7p_pillar2_q5_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar2_q5_choice4')); ?>" /><br />
					<label for="mc7p_pillar2_q5_choice5">Choice 5  </label><input type="text" name="mc7p_pillar2_q5_choice5" id="mc7p_pillar2_q5_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar2_q5_choice5')); ?>" /><br />
			</div>
			<!-- Question 6 -->
			<div class="q-container" id="mc7p_pillar2_q6_container">
				<h3>Question 6</h3>
				<p class="qtext"><label for="mc7p_pillar2_q6_text">Question Text:</label><br />
					<textarea name="mc7p_pillar2_q6_text" id="mc7p_pillar2_q6_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar2_q6_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar2_q6_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q6_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar2_q6_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q6_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar2_q6_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar2_q6_choice1">Choice 1  </label><input type="text" name="mc7p_pillar2_q6_choice1" id="mc7p_pillar2_q6_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar2_q6_choice1')); ?>" /><br />
					<label for="mc7p_pillar2_q6_choice2">Choice 2  </label><input type="text" name="mc7p_pillar2_q6_choice2" id="mc7p_pillar2_q6_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar2_q6_choice2')); ?>" /><br />
					<label for="mc7p_pillar2_q6_choice3">Choice 3  </label><input type="text" name="mc7p_pillar2_q6_choice3" id="mc7p_pillar2_q6_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar2_q6_choice3')); ?>" /><br />
					<label for="mc7p_pillar2_q6_choice4">Choice 4  </label><input type="text" name="mc7p_pillar2_q6_choice4" id="mc7p_pillar2_q6_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar2_q6_choice4')); ?>" /><br />
					<label for="mc7p_pillar2_q6_choice5">Choice 5  </label><input type="text" name="mc7p_pillar2_q6_choice5" id="mc7p_pillar2_q6_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar2_q6_choice5')); ?>" /><br />
			</div>
			<!-- Question 7 -->
			<div class="q-container" id="mc7p_pillar2_q7_container">
				<h3>Question 7</h3>
				<p class="qtext"><label for="mc7p_pillar2_q7_text">Question Text:</label><br />
					<textarea name="mc7p_pillar2_q7_text" id="mc7p_pillar2_q7_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar2_q7_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar2_q7_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q7_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar2_q7_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q7_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar2_q7_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar2_q7_choice1">Choice 1  </label><input type="text" name="mc7p_pillar2_q7_choice1" id="mc7p_pillar2_q7_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar2_q7_choice1')); ?>" /><br />
					<label for="mc7p_pillar2_q7_choice2">Choice 2  </label><input type="text" name="mc7p_pillar2_q7_choice2" id="mc7p_pillar2_q7_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar2_q7_choice2')); ?>" /><br />
					<label for="mc7p_pillar2_q7_choice3">Choice 3  </label><input type="text" name="mc7p_pillar2_q7_choice3" id="mc7p_pillar2_q7_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar2_q7_choice3')); ?>" /><br />
					<label for="mc7p_pillar2_q7_choice4">Choice 4  </label><input type="text" name="mc7p_pillar2_q7_choice4" id="mc7p_pillar2_q7_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar2_q7_choice4')); ?>" /><br />
					<label for="mc7p_pillar2_q7_choice5">Choice 5  </label><input type="text" name="mc7p_pillar2_q7_choice5" id="mc7p_pillar2_q7_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar2_q7_choice5')); ?>" /><br />
			</div>
			<!-- Question 8 -->
			<div class="q-container" id="mc7p_pillar2_q8_container">
				<h3>Question 8</h3>
				<p class="qtext"><label for="mc7p_pillar2_q8_text">Question Text:</label><br />
					<textarea name="mc7p_pillar2_q8_text" id="mc7p_pillar2_q8_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar2_q8_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar2_q8_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q8_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar2_q8_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q8_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar2_q8_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar2_q8_choice1">Choice 1  </label><input type="text" name="mc7p_pillar2_q8_choice1" id="mc7p_pillar2_q8_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar2_q8_choice1')); ?>" /><br />
					<label for="mc7p_pillar2_q8_choice2">Choice 2  </label><input type="text" name="mc7p_pillar2_q8_choice2" id="mc7p_pillar2_q8_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar2_q8_choice2')); ?>" /><br />
					<label for="mc7p_pillar2_q8_choice3">Choice 3  </label><input type="text" name="mc7p_pillar2_q8_choice3" id="mc7p_pillar2_q8_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar2_q8_choice3')); ?>" /><br />
					<label for="mc7p_pillar2_q8_choice4">Choice 4  </label><input type="text" name="mc7p_pillar2_q8_choice4" id="mc7p_pillar2_q8_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar2_q8_choice4')); ?>" /><br />
					<label for="mc7p_pillar2_q8_choice5">Choice 5  </label><input type="text" name="mc7p_pillar2_q8_choice5" id="mc7p_pillar2_q8_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar2_q8_choice5')); ?>" /><br />
			</div>
			<!-- Question 9 -->
			<div class="q-container" id="mc7p_pillar2_q9_container">
				<h3>Question 9</h3>
				<p class="qtext"><label for="mc7p_pillar2_q9_text">Question Text:</label><br />
					<textarea name="mc7p_pillar2_q9_text" id="mc7p_pillar2_q9_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar2_q9_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar2_q9_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q9_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar2_q9_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q9_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar2_q9_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar2_q9_choice1">Choice 1  </label><input type="text" name="mc7p_pillar2_q9_choice1" id="mc7p_pillar2_q9_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar2_q9_choice1')); ?>" /><br />
					<label for="mc7p_pillar2_q9_choice2">Choice 2  </label><input type="text" name="mc7p_pillar2_q9_choice2" id="mc7p_pillar2_q9_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar2_q9_choice2')); ?>" /><br />
					<label for="mc7p_pillar2_q9_choice3">Choice 3  </label><input type="text" name="mc7p_pillar2_q9_choice3" id="mc7p_pillar2_q9_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar2_q9_choice3')); ?>" /><br />
					<label for="mc7p_pillar2_q9_choice4">Choice 4  </label><input type="text" name="mc7p_pillar2_q9_choice4" id="mc7p_pillar2_q9_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar2_q9_choice4')); ?>" /><br />
					<label for="mc7p_pillar2_q9_choice5">Choice 5  </label><input type="text" name="mc7p_pillar2_q9_choice5" id="mc7p_pillar2_q9_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar2_q9_choice5')); ?>" /><br />
			</div>
			<!-- Question 10 -->
			<div class="q-container" id="mc7p_pillar2_q10_container">
				<h3>Question 10</h3>
				<p class="qtext"><label for="mc7p_pillar2_q10_text">Question Text:</label><br />
					<textarea name="mc7p_pillar2_q10_text" id="mc7p_pillar2_q10_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar2_q10_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar2_q10_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q10_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar2_q10_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar2_q10_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar2_q10_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar2_q10_choice1">Choice 1  </label><input type="text" name="mc7p_pillar2_q10_choice1" id="mc7p_pillar2_q10_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar2_q10_choice1')); ?>" /><br />
					<label for="mc7p_pillar2_q10_choice2">Choice 2  </label><input type="text" name="mc7p_pillar2_q10_choice2" id="mc7p_pillar2_q10_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar2_q10_choice2')); ?>" /><br />
					<label for="mc7p_pillar2_q10_choice3">Choice 3  </label><input type="text" name="mc7p_pillar2_q10_choice3" id="mc7p_pillar2_q10_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar2_q10_choice3')); ?>" /><br />
					<label for="mc7p_pillar2_q10_choice4">Choice 4  </label><input type="text" name="mc7p_pillar2_q10_choice4" id="mc7p_pillar2_q10_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar2_q10_choice4')); ?>" /><br />
					<label for="mc7p_pillar2_q10_choice5">Choice 5  </label><input type="text" name="mc7p_pillar2_q10_choice5" id="mc7p_pillar2_q10_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar2_q10_choice5')); ?>" /><br />
			</div>
			<p><input type="submit" name="submit" value="Save Options" /></p>
		</form>
	</div>
	<?php
}

// Pillar 3 settings page.
function mc7p_pillar3_settings() {

	?>
	<div class="wrap">
		<h2>Pillar 3 Settings and Options</h2>
		<p> Options and settings page for pillar 3</p>
		
		<form action="options.php" method="post" id="mc7p_pillar3_settings_form">
			<?php settings_fields('mc7p_p3_settings'); ?>
			<!-- Question 1 -->
			<div class="q-container" id="mc7p_pillar3_q1_container">
				<h3>Question 1</h3>
				<p class="qtext"><label for="mc7p_pillar3_q1_text">Question Text:</label><br />
					<textarea name="mc7p_pillar3_q1_text" id="mc7p_pillar3_q1_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar3_q1_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar3_q1_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q1_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar3_q1_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q1_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar3_q1_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar3_q1_choice1">Choice 1  </label><input type="text" name="mc7p_pillar3_q1_choice1" id="mc7p_pillar3_q1_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar3_q1_choice1')); ?>" /><br />
					<label for="mc7p_pillar3_q1_choice2">Choice 2  </label><input type="text" name="mc7p_pillar3_q1_choice2" id="mc7p_pillar3_q1_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar3_q1_choice2')); ?>" /><br />
					<label for="mc7p_pillar3_q1_choice3">Choice 3  </label><input type="text" name="mc7p_pillar3_q1_choice3" id="mc7p_pillar3_q1_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar3_q1_choice3')); ?>" /><br />
					<label for="mc7p_pillar3_q1_choice4">Choice 4  </label><input type="text" name="mc7p_pillar3_q1_choice4" id="mc7p_pillar3_q1_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar3_q1_choice4')); ?>" /><br />
					<label for="mc7p_pillar3_q1_choice5">Choice 5  </label><input type="text" name="mc7p_pillar3_q1_choice5" id="mc7p_pillar3_q1_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar3_q1_choice5')); ?>" /><br />
			</div>
			<!-- Question 2 -->
			<div class="q-container" id="mc7p_pillar3_q2_container">
				<h3>Question 2</h3>
				<p class="qtext"><label for="mc7p_pillar3_q2_text">Question Text:</label><br />
					<textarea name="mc7p_pillar3_q2_text" id="mc7p_pillar3_q2_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar3_q2_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar3_q2_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q2_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar3_q2_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q2_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar3_q2_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar3_q2_choice1">Choice 1  </label><input type="text" name="mc7p_pillar3_q2_choice1" id="mc7p_pillar3_q2_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar3_q2_choice1')); ?>" /><br />
					<label for="mc7p_pillar3_q2_choice2">Choice 2  </label><input type="text" name="mc7p_pillar3_q2_choice2" id="mc7p_pillar3_q2_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar3_q2_choice2')); ?>" /><br />
					<label for="mc7p_pillar3_q2_choice3">Choice 3  </label><input type="text" name="mc7p_pillar3_q2_choice3" id="mc7p_pillar3_q2_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar3_q2_choice3')); ?>" /><br />
					<label for="mc7p_pillar3_q2_choice4">Choice 4  </label><input type="text" name="mc7p_pillar3_q2_choice4" id="mc7p_pillar3_q2_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar3_q2_choice4')); ?>" /><br />
					<label for="mc7p_pillar3_q2_choice5">Choice 5  </label><input type="text" name="mc7p_pillar3_q2_choice5" id="mc7p_pillar3_q2_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar3_q2_choice5')); ?>" /><br />
			</div>
			<!-- Question 3 -->
			<div class="q-container" id="mc7p_pillar3_q3_container">
				<h3>Question 3</h3>
				<p class="qtext"><label for="mc7p_pillar3_q3_text">Question Text:</label><br />
					<textarea name="mc7p_pillar3_q3_text" id="mc7p_pillar3_q3_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar3_q3_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar3_q3_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q3_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar3_q3_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q3_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar3_q3_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar3_q3_choice1">Choice 1  </label><input type="text" name="mc7p_pillar3_q3_choice1" id="mc7p_pillar3_q3_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar3_q3_choice1')); ?>" /><br />
					<label for="mc7p_pillar3_q3_choice2">Choice 2  </label><input type="text" name="mc7p_pillar3_q3_choice2" id="mc7p_pillar3_q3_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar3_q3_choice2')); ?>" /><br />
					<label for="mc7p_pillar3_q3_choice3">Choice 3  </label><input type="text" name="mc7p_pillar3_q3_choice3" id="mc7p_pillar3_q3_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar3_q3_choice3')); ?>" /><br />
					<label for="mc7p_pillar3_q3_choice4">Choice 4  </label><input type="text" name="mc7p_pillar3_q3_choice4" id="mc7p_pillar3_q3_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar3_q3_choice4')); ?>" /><br />
					<label for="mc7p_pillar3_q3_choice5">Choice 5  </label><input type="text" name="mc7p_pillar3_q3_choice5" id="mc7p_pillar3_q3_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar3_q3_choice5')); ?>" /><br />
			</div>
			<!-- Question 4 -->
			<div class="q-container" id="mc7p_pillar3_q4_container">
				<h3>Question 4</h3>
				<p class="qtext"><label for="mc7p_pillar3_q4_text">Question Text:</label><br />
					<textarea name="mc7p_pillar3_q4_text" id="mc7p_pillar3_q4_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar3_q4_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar3_q4_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q4_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar3_q4_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q4_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar3_q4_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar3_q4_choice1">Choice 1  </label><input type="text" name="mc7p_pillar3_q4_choice1" id="mc7p_pillar3_q4_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar3_q4_choice1')); ?>" /><br />
					<label for="mc7p_pillar3_q4_choice2">Choice 2  </label><input type="text" name="mc7p_pillar3_q4_choice2" id="mc7p_pillar3_q4_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar3_q4_choice2')); ?>" /><br />
					<label for="mc7p_pillar3_q4_choice3">Choice 3  </label><input type="text" name="mc7p_pillar3_q4_choice3" id="mc7p_pillar3_q4_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar3_q4_choice3')); ?>" /><br />
					<label for="mc7p_pillar3_q4_choice4">Choice 4  </label><input type="text" name="mc7p_pillar3_q4_choice4" id="mc7p_pillar3_q4_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar3_q4_choice4')); ?>" /><br />
					<label for="mc7p_pillar3_q4_choice5">Choice 5  </label><input type="text" name="mc7p_pillar3_q4_choice5" id="mc7p_pillar3_q4_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar3_q4_choice5')); ?>" /><br />
			</div>
			<!-- Question 5 -->
			<div class="q-container" id="mc7p_pillar3_q5_container">
				<h3>Question 5</h3>
				<p class="qtext"><label for="mc7p_pillar3_q5_text">Question Text:</label><br />
					<textarea name="mc7p_pillar3_q5_text" id="mc7p_pillar3_q5_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar3_q5_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar3_q5_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q5_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar3_q5_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q5_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar3_q5_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar3_q5_choice1">Choice 1  </label><input type="text" name="mc7p_pillar3_q5_choice1" id="mc7p_pillar3_q5_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar3_q5_choice1')); ?>" /><br />
					<label for="mc7p_pillar3_q5_choice2">Choice 2  </label><input type="text" name="mc7p_pillar3_q5_choice2" id="mc7p_pillar3_q5_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar3_q5_choice2')); ?>" /><br />
					<label for="mc7p_pillar3_q5_choice3">Choice 3  </label><input type="text" name="mc7p_pillar3_q5_choice3" id="mc7p_pillar3_q5_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar3_q5_choice3')); ?>" /><br />
					<label for="mc7p_pillar3_q5_choice4">Choice 4  </label><input type="text" name="mc7p_pillar3_q5_choice4" id="mc7p_pillar3_q5_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar3_q5_choice4')); ?>" /><br />
					<label for="mc7p_pillar3_q5_choice5">Choice 5  </label><input type="text" name="mc7p_pillar3_q5_choice5" id="mc7p_pillar3_q5_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar3_q5_choice5')); ?>" /><br />
			</div>
			<!-- Question 6 -->
			<div class="q-container" id="mc7p_pillar3_q6_container">
				<h3>Question 6</h3>
				<p class="qtext"><label for="mc7p_pillar3_q6_text">Question Text:</label><br />
					<textarea name="mc7p_pillar3_q6_text" id="mc7p_pillar3_q6_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar3_q6_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar3_q6_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q6_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar3_q6_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q6_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar3_q6_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar3_q6_choice1">Choice 1  </label><input type="text" name="mc7p_pillar3_q6_choice1" id="mc7p_pillar3_q6_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar3_q6_choice1')); ?>" /><br />
					<label for="mc7p_pillar3_q6_choice2">Choice 2  </label><input type="text" name="mc7p_pillar3_q6_choice2" id="mc7p_pillar3_q6_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar3_q6_choice2')); ?>" /><br />
					<label for="mc7p_pillar3_q6_choice3">Choice 3  </label><input type="text" name="mc7p_pillar3_q6_choice3" id="mc7p_pillar3_q6_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar3_q6_choice3')); ?>" /><br />
					<label for="mc7p_pillar3_q6_choice4">Choice 4  </label><input type="text" name="mc7p_pillar3_q6_choice4" id="mc7p_pillar3_q6_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar3_q6_choice4')); ?>" /><br />
					<label for="mc7p_pillar3_q6_choice5">Choice 5  </label><input type="text" name="mc7p_pillar3_q6_choice5" id="mc7p_pillar3_q6_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar3_q6_choice5')); ?>" /><br />
			</div>
			<!-- Question 7 -->
			<div class="q-container" id="mc7p_pillar3_q7_container">
				<h3>Question 7</h3>
				<p class="qtext"><label for="mc7p_pillar3_q7_text">Question Text:</label><br />
					<textarea name="mc7p_pillar3_q7_text" id="mc7p_pillar3_q7_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar3_q7_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar3_q7_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q7_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar3_q7_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q7_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar3_q7_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar3_q7_choice1">Choice 1  </label><input type="text" name="mc7p_pillar3_q7_choice1" id="mc7p_pillar3_q7_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar3_q7_choice1')); ?>" /><br />
					<label for="mc7p_pillar3_q7_choice2">Choice 2  </label><input type="text" name="mc7p_pillar3_q7_choice2" id="mc7p_pillar3_q7_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar3_q7_choice2')); ?>" /><br />
					<label for="mc7p_pillar3_q7_choice3">Choice 3  </label><input type="text" name="mc7p_pillar3_q7_choice3" id="mc7p_pillar3_q7_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar3_q7_choice3')); ?>" /><br />
					<label for="mc7p_pillar3_q7_choice4">Choice 4  </label><input type="text" name="mc7p_pillar3_q7_choice4" id="mc7p_pillar3_q7_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar3_q7_choice4')); ?>" /><br />
					<label for="mc7p_pillar3_q7_choice5">Choice 5  </label><input type="text" name="mc7p_pillar3_q7_choice5" id="mc7p_pillar3_q7_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar3_q7_choice5')); ?>" /><br />
			</div>
			<!-- Question 8 -->
			<div class="q-container" id="mc7p_pillar3_q8_container">
				<h3>Question 8</h3>
				<p class="qtext"><label for="mc7p_pillar3_q8_text">Question Text:</label><br />
					<textarea name="mc7p_pillar3_q8_text" id="mc7p_pillar3_q8_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar3_q8_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar3_q8_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q8_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar3_q8_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q8_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar3_q8_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar3_q8_choice1">Choice 1  </label><input type="text" name="mc7p_pillar3_q8_choice1" id="mc7p_pillar3_q8_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar3_q8_choice1')); ?>" /><br />
					<label for="mc7p_pillar3_q8_choice2">Choice 2  </label><input type="text" name="mc7p_pillar3_q8_choice2" id="mc7p_pillar3_q8_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar3_q8_choice2')); ?>" /><br />
					<label for="mc7p_pillar3_q8_choice3">Choice 3  </label><input type="text" name="mc7p_pillar3_q8_choice3" id="mc7p_pillar3_q8_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar3_q8_choice3')); ?>" /><br />
					<label for="mc7p_pillar3_q8_choice4">Choice 4  </label><input type="text" name="mc7p_pillar3_q8_choice4" id="mc7p_pillar3_q8_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar3_q8_choice4')); ?>" /><br />
					<label for="mc7p_pillar3_q8_choice5">Choice 5  </label><input type="text" name="mc7p_pillar3_q8_choice5" id="mc7p_pillar3_q8_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar3_q8_choice5')); ?>" /><br />
			</div>
			<!-- Question 9 -->
			<div class="q-container" id="mc7p_pillar3_q9_container">
				<h3>Question 9</h3>
				<p class="qtext"><label for="mc7p_pillar3_q9_text">Question Text:</label><br />
					<textarea name="mc7p_pillar3_q9_text" id="mc7p_pillar3_q9_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar3_q9_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar3_q9_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q9_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar3_q9_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q9_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar3_q9_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar3_q9_choice1">Choice 1  </label><input type="text" name="mc7p_pillar3_q9_choice1" id="mc7p_pillar3_q9_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar3_q9_choice1')); ?>" /><br />
					<label for="mc7p_pillar3_q9_choice2">Choice 2  </label><input type="text" name="mc7p_pillar3_q9_choice2" id="mc7p_pillar3_q9_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar3_q9_choice2')); ?>" /><br />
					<label for="mc7p_pillar3_q9_choice3">Choice 3  </label><input type="text" name="mc7p_pillar3_q9_choice3" id="mc7p_pillar3_q9_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar3_q9_choice3')); ?>" /><br />
					<label for="mc7p_pillar3_q9_choice4">Choice 4  </label><input type="text" name="mc7p_pillar3_q9_choice4" id="mc7p_pillar3_q9_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar3_q9_choice4')); ?>" /><br />
					<label for="mc7p_pillar3_q9_choice5">Choice 5  </label><input type="text" name="mc7p_pillar3_q9_choice5" id="mc7p_pillar3_q9_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar3_q9_choice5')); ?>" /><br />
			</div>
			<!-- Question 10 -->
			<div class="q-container" id="mc7p_pillar3_q10_container">
				<h3>Question 10</h3>
				<p class="qtext"><label for="mc7p_pillar3_q10_text">Question Text:</label><br />
					<textarea name="mc7p_pillar3_q10_text" id="mc7p_pillar3_q10_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar3_q10_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar3_q10_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q10_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar3_q10_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar3_q10_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar3_q10_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar3_q10_choice1">Choice 1  </label><input type="text" name="mc7p_pillar3_q10_choice1" id="mc7p_pillar3_q10_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar3_q10_choice1')); ?>" /><br />
					<label for="mc7p_pillar3_q10_choice2">Choice 2  </label><input type="text" name="mc7p_pillar3_q10_choice2" id="mc7p_pillar3_q10_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar3_q10_choice2')); ?>" /><br />
					<label for="mc7p_pillar3_q10_choice3">Choice 3  </label><input type="text" name="mc7p_pillar3_q10_choice3" id="mc7p_pillar3_q10_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar3_q10_choice3')); ?>" /><br />
					<label for="mc7p_pillar3_q10_choice4">Choice 4  </label><input type="text" name="mc7p_pillar3_q10_choice4" id="mc7p_pillar3_q10_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar3_q10_choice4')); ?>" /><br />
					<label for="mc7p_pillar3_q10_choice5">Choice 5  </label><input type="text" name="mc7p_pillar3_q10_choice5" id="mc7p_pillar3_q10_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar3_q10_choice5')); ?>" /><br />
			</div>
			<p><input type="submit" name="submit" value="Save Options" /></p>
		</form>
	</div>
	<?php
}

// Pillar 4 settings page.
function mc7p_pillar4_settings() {

	?>
	<div class="wrap">
		<h2>Pillar 4 Settings and Options</h2>
		<p> Options and settings page for pillar 4</p>
		
		<form action="options.php" method="post" id="mc7p_pillar4_settings_form">
			<?php settings_fields('mc7p_p4_settings'); ?>
			<!-- Question 1 -->
			<div class="q-container" id="mc7p_pillar4_q1_container">
				<h3>Question 1</h3>
				<p class="qtext"><label for="mc7p_pillar4_q1_text">Question Text:</label><br />
					<textarea name="mc7p_pillar4_q1_text" id="mc7p_pillar4_q1_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar4_q1_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar4_q1_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q1_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar4_q1_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q1_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar4_q1_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar4_q1_choice1">Choice 1  </label><input type="text" name="mc7p_pillar4_q1_choice1" id="mc7p_pillar4_q1_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar4_q1_choice1')); ?>" /><br />
					<label for="mc7p_pillar4_q1_choice2">Choice 2  </label><input type="text" name="mc7p_pillar4_q1_choice2" id="mc7p_pillar4_q1_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar4_q1_choice2')); ?>" /><br />
					<label for="mc7p_pillar4_q1_choice3">Choice 3  </label><input type="text" name="mc7p_pillar4_q1_choice3" id="mc7p_pillar4_q1_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar4_q1_choice3')); ?>" /><br />
					<label for="mc7p_pillar4_q1_choice4">Choice 4  </label><input type="text" name="mc7p_pillar4_q1_choice4" id="mc7p_pillar4_q1_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar4_q1_choice4')); ?>" /><br />
					<label for="mc7p_pillar4_q1_choice5">Choice 5  </label><input type="text" name="mc7p_pillar4_q1_choice5" id="mc7p_pillar4_q1_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar4_q1_choice5')); ?>" /><br />
			</div>
			<!-- Question 2 -->
			<div class="q-container" id="mc7p_pillar4_q2_container">
				<h3>Question 2</h3>
				<p class="qtext"><label for="mc7p_pillar4_q2_text">Question Text:</label><br />
					<textarea name="mc7p_pillar4_q2_text" id="mc7p_pillar4_q2_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar4_q2_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar4_q2_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q2_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar4_q2_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q2_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar4_q2_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar4_q2_choice1">Choice 1  </label><input type="text" name="mc7p_pillar4_q2_choice1" id="mc7p_pillar4_q2_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar4_q2_choice1')); ?>" /><br />
					<label for="mc7p_pillar4_q2_choice2">Choice 2  </label><input type="text" name="mc7p_pillar4_q2_choice2" id="mc7p_pillar4_q2_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar4_q2_choice2')); ?>" /><br />
					<label for="mc7p_pillar4_q2_choice3">Choice 3  </label><input type="text" name="mc7p_pillar4_q2_choice3" id="mc7p_pillar4_q2_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar4_q2_choice3')); ?>" /><br />
					<label for="mc7p_pillar4_q2_choice4">Choice 4  </label><input type="text" name="mc7p_pillar4_q2_choice4" id="mc7p_pillar4_q2_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar4_q2_choice4')); ?>" /><br />
					<label for="mc7p_pillar4_q2_choice5">Choice 5  </label><input type="text" name="mc7p_pillar4_q2_choice5" id="mc7p_pillar4_q2_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar4_q2_choice5')); ?>" /><br />
			</div>
			<!-- Question 3 -->
			<div class="q-container" id="mc7p_pillar4_q3_container">
				<h3>Question 3</h3>
				<p class="qtext"><label for="mc7p_pillar4_q3_text">Question Text:</label><br />
					<textarea name="mc7p_pillar4_q3_text" id="mc7p_pillar4_q3_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar4_q3_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar4_q3_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q3_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar4_q3_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q3_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar4_q3_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar4_q3_choice1">Choice 1  </label><input type="text" name="mc7p_pillar4_q3_choice1" id="mc7p_pillar4_q3_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar4_q3_choice1')); ?>" /><br />
					<label for="mc7p_pillar4_q3_choice2">Choice 2  </label><input type="text" name="mc7p_pillar4_q3_choice2" id="mc7p_pillar4_q3_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar4_q3_choice2')); ?>" /><br />
					<label for="mc7p_pillar4_q3_choice3">Choice 3  </label><input type="text" name="mc7p_pillar4_q3_choice3" id="mc7p_pillar4_q3_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar4_q3_choice3')); ?>" /><br />
					<label for="mc7p_pillar4_q3_choice4">Choice 4  </label><input type="text" name="mc7p_pillar4_q3_choice4" id="mc7p_pillar4_q3_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar4_q3_choice4')); ?>" /><br />
					<label for="mc7p_pillar4_q3_choice5">Choice 5  </label><input type="text" name="mc7p_pillar4_q3_choice5" id="mc7p_pillar4_q3_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar4_q3_choice5')); ?>" /><br />
			</div>
			<!-- Question 4 -->
			<div class="q-container" id="mc7p_pillar4_q4_container">
				<h3>Question 4</h3>
				<p class="qtext"><label for="mc7p_pillar4_q4_text">Question Text:</label><br />
					<textarea name="mc7p_pillar4_q4_text" id="mc7p_pillar4_q4_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar4_q4_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar4_q4_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q4_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar4_q4_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q4_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar4_q4_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar4_q4_choice1">Choice 1  </label><input type="text" name="mc7p_pillar4_q4_choice1" id="mc7p_pillar4_q4_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar4_q4_choice1')); ?>" /><br />
					<label for="mc7p_pillar4_q4_choice2">Choice 2  </label><input type="text" name="mc7p_pillar4_q4_choice2" id="mc7p_pillar4_q4_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar4_q4_choice2')); ?>" /><br />
					<label for="mc7p_pillar4_q4_choice3">Choice 3  </label><input type="text" name="mc7p_pillar4_q4_choice3" id="mc7p_pillar4_q4_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar4_q4_choice3')); ?>" /><br />
					<label for="mc7p_pillar4_q4_choice4">Choice 4  </label><input type="text" name="mc7p_pillar4_q4_choice4" id="mc7p_pillar4_q4_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar4_q4_choice4')); ?>" /><br />
					<label for="mc7p_pillar4_q4_choice5">Choice 5  </label><input type="text" name="mc7p_pillar4_q4_choice5" id="mc7p_pillar4_q4_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar4_q4_choice5')); ?>" /><br />
			</div>
			<!-- Question 5 -->
			<div class="q-container" id="mc7p_pillar4_q5_container">
				<h3>Question 5</h3>
				<p class="qtext"><label for="mc7p_pillar4_q5_text">Question Text:</label><br />
					<textarea name="mc7p_pillar4_q5_text" id="mc7p_pillar4_q5_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar4_q5_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar4_q5_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q5_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar4_q5_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q5_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar4_q5_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar4_q5_choice1">Choice 1  </label><input type="text" name="mc7p_pillar4_q5_choice1" id="mc7p_pillar4_q5_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar4_q5_choice1')); ?>" /><br />
					<label for="mc7p_pillar4_q5_choice2">Choice 2  </label><input type="text" name="mc7p_pillar4_q5_choice2" id="mc7p_pillar4_q5_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar4_q5_choice2')); ?>" /><br />
					<label for="mc7p_pillar4_q5_choice3">Choice 3  </label><input type="text" name="mc7p_pillar4_q5_choice3" id="mc7p_pillar4_q5_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar4_q5_choice3')); ?>" /><br />
					<label for="mc7p_pillar4_q5_choice4">Choice 4  </label><input type="text" name="mc7p_pillar4_q5_choice4" id="mc7p_pillar4_q5_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar4_q5_choice4')); ?>" /><br />
					<label for="mc7p_pillar4_q5_choice5">Choice 5  </label><input type="text" name="mc7p_pillar4_q5_choice5" id="mc7p_pillar4_q5_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar4_q5_choice5')); ?>" /><br />
			</div>
			<!-- Question 6 -->
			<div class="q-container" id="mc7p_pillar4_q6_container">
				<h3>Question 6</h3>
				<p class="qtext"><label for="mc7p_pillar4_q6_text">Question Text:</label><br />
					<textarea name="mc7p_pillar4_q6_text" id="mc7p_pillar4_q6_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar4_q6_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar4_q6_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q6_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar4_q6_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q6_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar4_q6_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar4_q6_choice1">Choice 1  </label><input type="text" name="mc7p_pillar4_q6_choice1" id="mc7p_pillar4_q6_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar4_q6_choice1')); ?>" /><br />
					<label for="mc7p_pillar4_q6_choice2">Choice 2  </label><input type="text" name="mc7p_pillar4_q6_choice2" id="mc7p_pillar4_q6_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar4_q6_choice2')); ?>" /><br />
					<label for="mc7p_pillar4_q6_choice3">Choice 3  </label><input type="text" name="mc7p_pillar4_q6_choice3" id="mc7p_pillar4_q6_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar4_q6_choice3')); ?>" /><br />
					<label for="mc7p_pillar4_q6_choice4">Choice 4  </label><input type="text" name="mc7p_pillar4_q6_choice4" id="mc7p_pillar4_q6_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar4_q6_choice4')); ?>" /><br />
					<label for="mc7p_pillar4_q6_choice5">Choice 5  </label><input type="text" name="mc7p_pillar4_q6_choice5" id="mc7p_pillar4_q6_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar4_q6_choice5')); ?>" /><br />
			</div>
			<!-- Question 7 -->
			<div class="q-container" id="mc7p_pillar4_q7_container">
				<h3>Question 7</h3>
				<p class="qtext"><label for="mc7p_pillar4_q7_text">Question Text:</label><br />
					<textarea name="mc7p_pillar4_q7_text" id="mc7p_pillar4_q7_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar4_q7_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar4_q7_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q7_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar4_q7_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q7_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar4_q7_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar4_q7_choice1">Choice 1  </label><input type="text" name="mc7p_pillar4_q7_choice1" id="mc7p_pillar4_q7_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar4_q7_choice1')); ?>" /><br />
					<label for="mc7p_pillar4_q7_choice2">Choice 2  </label><input type="text" name="mc7p_pillar4_q7_choice2" id="mc7p_pillar4_q7_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar4_q7_choice2')); ?>" /><br />
					<label for="mc7p_pillar4_q7_choice3">Choice 3  </label><input type="text" name="mc7p_pillar4_q7_choice3" id="mc7p_pillar4_q7_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar4_q7_choice3')); ?>" /><br />
					<label for="mc7p_pillar4_q7_choice4">Choice 4  </label><input type="text" name="mc7p_pillar4_q7_choice4" id="mc7p_pillar4_q7_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar4_q7_choice4')); ?>" /><br />
					<label for="mc7p_pillar4_q7_choice5">Choice 5  </label><input type="text" name="mc7p_pillar4_q7_choice5" id="mc7p_pillar4_q7_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar4_q7_choice5')); ?>" /><br />
			</div>
			<!-- Question 8 -->
			<div class="q-container" id="mc7p_pillar4_q8_container">
				<h3>Question 8</h3>
				<p class="qtext"><label for="mc7p_pillar4_q8_text">Question Text:</label><br />
					<textarea name="mc7p_pillar4_q8_text" id="mc7p_pillar4_q8_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar4_q8_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar4_q8_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q8_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar4_q8_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q8_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar4_q8_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar4_q8_choice1">Choice 1  </label><input type="text" name="mc7p_pillar4_q8_choice1" id="mc7p_pillar4_q8_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar4_q8_choice1')); ?>" /><br />
					<label for="mc7p_pillar4_q8_choice2">Choice 2  </label><input type="text" name="mc7p_pillar4_q8_choice2" id="mc7p_pillar4_q8_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar4_q8_choice2')); ?>" /><br />
					<label for="mc7p_pillar4_q8_choice3">Choice 3  </label><input type="text" name="mc7p_pillar4_q8_choice3" id="mc7p_pillar4_q8_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar4_q8_choice3')); ?>" /><br />
					<label for="mc7p_pillar4_q8_choice4">Choice 4  </label><input type="text" name="mc7p_pillar4_q8_choice4" id="mc7p_pillar4_q8_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar4_q8_choice4')); ?>" /><br />
					<label for="mc7p_pillar4_q8_choice5">Choice 5  </label><input type="text" name="mc7p_pillar4_q8_choice5" id="mc7p_pillar4_q8_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar4_q8_choice5')); ?>" /><br />
			</div>
			<!-- Question 9 -->
			<div class="q-container" id="mc7p_pillar4_q9_container">
				<h3>Question 9</h3>
				<p class="qtext"><label for="mc7p_pillar4_q9_text">Question Text:</label><br />
					<textarea name="mc7p_pillar4_q9_text" id="mc7p_pillar4_q9_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar4_q9_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar4_q9_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q9_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar4_q9_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q9_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar4_q9_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar4_q9_choice1">Choice 1  </label><input type="text" name="mc7p_pillar4_q9_choice1" id="mc7p_pillar4_q9_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar4_q9_choice1')); ?>" /><br />
					<label for="mc7p_pillar4_q9_choice2">Choice 2  </label><input type="text" name="mc7p_pillar4_q9_choice2" id="mc7p_pillar4_q9_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar4_q9_choice2')); ?>" /><br />
					<label for="mc7p_pillar4_q9_choice3">Choice 3  </label><input type="text" name="mc7p_pillar4_q9_choice3" id="mc7p_pillar4_q9_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar4_q9_choice3')); ?>" /><br />
					<label for="mc7p_pillar4_q9_choice4">Choice 4  </label><input type="text" name="mc7p_pillar4_q9_choice4" id="mc7p_pillar4_q9_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar4_q9_choice4')); ?>" /><br />
					<label for="mc7p_pillar4_q9_choice5">Choice 5  </label><input type="text" name="mc7p_pillar4_q9_choice5" id="mc7p_pillar4_q9_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar4_q9_choice5')); ?>" /><br />
			</div>
			<!-- Question 10 -->
			<div class="q-container" id="mc7p_pillar4_q10_container">
				<h3>Question 10</h3>
				<p class="qtext"><label for="mc7p_pillar4_q10_text">Question Text:</label><br />
					<textarea name="mc7p_pillar4_q10_text" id="mc7p_pillar4_q10_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar4_q10_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar4_q10_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q10_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar4_q10_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar4_q10_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar4_q10_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar4_q10_choice1">Choice 1  </label><input type="text" name="mc7p_pillar4_q10_choice1" id="mc7p_pillar4_q10_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar4_q10_choice1')); ?>" /><br />
					<label for="mc7p_pillar4_q10_choice2">Choice 2  </label><input type="text" name="mc7p_pillar4_q10_choice2" id="mc7p_pillar4_q10_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar4_q10_choice2')); ?>" /><br />
					<label for="mc7p_pillar4_q10_choice3">Choice 3  </label><input type="text" name="mc7p_pillar4_q10_choice3" id="mc7p_pillar4_q10_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar4_q10_choice3')); ?>" /><br />
					<label for="mc7p_pillar4_q10_choice4">Choice 4  </label><input type="text" name="mc7p_pillar4_q10_choice4" id="mc7p_pillar4_q10_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar4_q10_choice4')); ?>" /><br />
					<label for="mc7p_pillar4_q10_choice5">Choice 5  </label><input type="text" name="mc7p_pillar4_q10_choice5" id="mc7p_pillar4_q10_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar4_q10_choice5')); ?>" /><br />
			</div>
			<p><input type="submit" name="submit" value="Save Options" /></p>
		</form>
	</div>
	<?php
}

// Pillar 5 settings page.
function mc7p_pillar5_settings() {

	?>
	<div class="wrap">
		<h2>Pillar 5 Settings and Options</h2>
		<p> Options and settings page for pillar 5</p>
		
		<form action="options.php" method="post" id="mc7p_pillar5_settings_form">
			<?php settings_fields('mc7p_p5_settings'); ?>
			<!-- Question 1 -->
			<div class="q-container" id="mc7p_pillar5_q1_container">
				<h3>Question 1</h3>
				<p class="qtext"><label for="mc7p_pillar5_q1_text">Question Text:</label><br />
					<textarea name="mc7p_pillar5_q1_text" id="mc7p_pillar5_q1_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar5_q1_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar5_q1_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q1_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar5_q1_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q1_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar5_q1_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar5_q1_choice1">Choice 1  </label><input type="text" name="mc7p_pillar5_q1_choice1" id="mc7p_pillar5_q1_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar5_q1_choice1')); ?>" /><br />
					<label for="mc7p_pillar5_q1_choice2">Choice 2  </label><input type="text" name="mc7p_pillar5_q1_choice2" id="mc7p_pillar5_q1_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar5_q1_choice2')); ?>" /><br />
					<label for="mc7p_pillar5_q1_choice3">Choice 3  </label><input type="text" name="mc7p_pillar5_q1_choice3" id="mc7p_pillar5_q1_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar5_q1_choice3')); ?>" /><br />
					<label for="mc7p_pillar5_q1_choice4">Choice 4  </label><input type="text" name="mc7p_pillar5_q1_choice4" id="mc7p_pillar5_q1_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar5_q1_choice4')); ?>" /><br />
					<label for="mc7p_pillar5_q1_choice5">Choice 5  </label><input type="text" name="mc7p_pillar5_q1_choice5" id="mc7p_pillar5_q1_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar5_q1_choice5')); ?>" /><br />
			</div>
			<!-- Question 2 -->
			<div class="q-container" id="mc7p_pillar5_q2_container">
				<h3>Question 2</h3>
				<p class="qtext"><label for="mc7p_pillar5_q2_text">Question Text:</label><br />
					<textarea name="mc7p_pillar5_q2_text" id="mc7p_pillar5_q2_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar5_q2_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar5_q2_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q2_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar5_q2_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q2_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar5_q2_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar5_q2_choice1">Choice 1  </label><input type="text" name="mc7p_pillar5_q2_choice1" id="mc7p_pillar5_q2_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar5_q2_choice1')); ?>" /><br />
					<label for="mc7p_pillar5_q2_choice2">Choice 2  </label><input type="text" name="mc7p_pillar5_q2_choice2" id="mc7p_pillar5_q2_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar5_q2_choice2')); ?>" /><br />
					<label for="mc7p_pillar5_q2_choice3">Choice 3  </label><input type="text" name="mc7p_pillar5_q2_choice3" id="mc7p_pillar5_q2_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar5_q2_choice3')); ?>" /><br />
					<label for="mc7p_pillar5_q2_choice4">Choice 4  </label><input type="text" name="mc7p_pillar5_q2_choice4" id="mc7p_pillar5_q2_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar5_q2_choice4')); ?>" /><br />
					<label for="mc7p_pillar5_q2_choice5">Choice 5  </label><input type="text" name="mc7p_pillar5_q2_choice5" id="mc7p_pillar5_q2_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar5_q2_choice5')); ?>" /><br />
			</div>
			<!-- Question 3 -->
			<div class="q-container" id="mc7p_pillar5_q3_container">
				<h3>Question 3</h3>
				<p class="qtext"><label for="mc7p_pillar5_q3_text">Question Text:</label><br />
					<textarea name="mc7p_pillar5_q3_text" id="mc7p_pillar5_q3_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar5_q3_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar5_q3_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q3_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar5_q3_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q3_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar5_q3_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar5_q3_choice1">Choice 1  </label><input type="text" name="mc7p_pillar5_q3_choice1" id="mc7p_pillar5_q3_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar5_q3_choice1')); ?>" /><br />
					<label for="mc7p_pillar5_q3_choice2">Choice 2  </label><input type="text" name="mc7p_pillar5_q3_choice2" id="mc7p_pillar5_q3_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar5_q3_choice2')); ?>" /><br />
					<label for="mc7p_pillar5_q3_choice3">Choice 3  </label><input type="text" name="mc7p_pillar5_q3_choice3" id="mc7p_pillar5_q3_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar5_q3_choice3')); ?>" /><br />
					<label for="mc7p_pillar5_q3_choice4">Choice 4  </label><input type="text" name="mc7p_pillar5_q3_choice4" id="mc7p_pillar5_q3_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar5_q3_choice4')); ?>" /><br />
					<label for="mc7p_pillar5_q3_choice5">Choice 5  </label><input type="text" name="mc7p_pillar5_q3_choice5" id="mc7p_pillar5_q3_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar5_q3_choice5')); ?>" /><br />
			</div>
			<!-- Question 4 -->
			<div class="q-container" id="mc7p_pillar5_q4_container">
				<h3>Question 4</h3>
				<p class="qtext"><label for="mc7p_pillar5_q4_text">Question Text:</label><br />
					<textarea name="mc7p_pillar5_q4_text" id="mc7p_pillar5_q4_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar5_q4_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar5_q4_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q4_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar5_q4_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q4_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar5_q4_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar5_q4_choice1">Choice 1  </label><input type="text" name="mc7p_pillar5_q4_choice1" id="mc7p_pillar5_q4_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar5_q4_choice1')); ?>" /><br />
					<label for="mc7p_pillar5_q4_choice2">Choice 2  </label><input type="text" name="mc7p_pillar5_q4_choice2" id="mc7p_pillar5_q4_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar5_q4_choice2')); ?>" /><br />
					<label for="mc7p_pillar5_q4_choice3">Choice 3  </label><input type="text" name="mc7p_pillar5_q4_choice3" id="mc7p_pillar5_q4_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar5_q4_choice3')); ?>" /><br />
					<label for="mc7p_pillar5_q4_choice4">Choice 4  </label><input type="text" name="mc7p_pillar5_q4_choice4" id="mc7p_pillar5_q4_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar5_q4_choice4')); ?>" /><br />
					<label for="mc7p_pillar5_q4_choice5">Choice 5  </label><input type="text" name="mc7p_pillar5_q4_choice5" id="mc7p_pillar5_q4_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar5_q4_choice5')); ?>" /><br />
			</div>
			<!-- Question 5 -->
			<div class="q-container" id="mc7p_pillar5_q5_container">
				<h3>Question 5</h3>
				<p class="qtext"><label for="mc7p_pillar5_q5_text">Question Text:</label><br />
					<textarea name="mc7p_pillar5_q5_text" id="mc7p_pillar5_q5_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar5_q5_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar5_q5_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q5_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar5_q5_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q5_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar5_q5_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar5_q5_choice1">Choice 1  </label><input type="text" name="mc7p_pillar5_q5_choice1" id="mc7p_pillar5_q5_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar5_q5_choice1')); ?>" /><br />
					<label for="mc7p_pillar5_q5_choice2">Choice 2  </label><input type="text" name="mc7p_pillar5_q5_choice2" id="mc7p_pillar5_q5_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar5_q5_choice2')); ?>" /><br />
					<label for="mc7p_pillar5_q5_choice3">Choice 3  </label><input type="text" name="mc7p_pillar5_q5_choice3" id="mc7p_pillar5_q5_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar5_q5_choice3')); ?>" /><br />
					<label for="mc7p_pillar5_q5_choice4">Choice 4  </label><input type="text" name="mc7p_pillar5_q5_choice4" id="mc7p_pillar5_q5_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar5_q5_choice4')); ?>" /><br />
					<label for="mc7p_pillar5_q5_choice5">Choice 5  </label><input type="text" name="mc7p_pillar5_q5_choice5" id="mc7p_pillar5_q5_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar5_q5_choice5')); ?>" /><br />
			</div>
			<!-- Question 6 -->
			<div class="q-container" id="mc7p_pillar5_q6_container">
				<h3>Question 6</h3>
				<p class="qtext"><label for="mc7p_pillar5_q6_text">Question Text:</label><br />
					<textarea name="mc7p_pillar5_q6_text" id="mc7p_pillar5_q6_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar5_q6_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar5_q6_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q6_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar5_q6_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q6_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar5_q6_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar5_q6_choice1">Choice 1  </label><input type="text" name="mc7p_pillar5_q6_choice1" id="mc7p_pillar5_q6_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar5_q6_choice1')); ?>" /><br />
					<label for="mc7p_pillar5_q6_choice2">Choice 2  </label><input type="text" name="mc7p_pillar5_q6_choice2" id="mc7p_pillar5_q6_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar5_q6_choice2')); ?>" /><br />
					<label for="mc7p_pillar5_q6_choice3">Choice 3  </label><input type="text" name="mc7p_pillar5_q6_choice3" id="mc7p_pillar5_q6_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar5_q6_choice3')); ?>" /><br />
					<label for="mc7p_pillar5_q6_choice4">Choice 4  </label><input type="text" name="mc7p_pillar5_q6_choice4" id="mc7p_pillar5_q6_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar5_q6_choice4')); ?>" /><br />
					<label for="mc7p_pillar5_q6_choice5">Choice 5  </label><input type="text" name="mc7p_pillar5_q6_choice5" id="mc7p_pillar5_q6_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar5_q6_choice5')); ?>" /><br />
			</div>
			<!-- Question 7 -->
			<div class="q-container" id="mc7p_pillar5_q7_container">
				<h3>Question 7</h3>
				<p class="qtext"><label for="mc7p_pillar5_q7_text">Question Text:</label><br />
					<textarea name="mc7p_pillar5_q7_text" id="mc7p_pillar5_q7_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar5_q7_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar5_q7_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q7_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar5_q7_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q7_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar5_q7_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar5_q7_choice1">Choice 1  </label><input type="text" name="mc7p_pillar5_q7_choice1" id="mc7p_pillar5_q7_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar5_q7_choice1')); ?>" /><br />
					<label for="mc7p_pillar5_q7_choice2">Choice 2  </label><input type="text" name="mc7p_pillar5_q7_choice2" id="mc7p_pillar5_q7_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar5_q7_choice2')); ?>" /><br />
					<label for="mc7p_pillar5_q7_choice3">Choice 3  </label><input type="text" name="mc7p_pillar5_q7_choice3" id="mc7p_pillar5_q7_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar5_q7_choice3')); ?>" /><br />
					<label for="mc7p_pillar5_q7_choice4">Choice 4  </label><input type="text" name="mc7p_pillar5_q7_choice4" id="mc7p_pillar5_q7_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar5_q7_choice4')); ?>" /><br />
					<label for="mc7p_pillar5_q7_choice5">Choice 5  </label><input type="text" name="mc7p_pillar5_q7_choice5" id="mc7p_pillar5_q7_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar5_q7_choice5')); ?>" /><br />
			</div>
			<!-- Question 8 -->
			<div class="q-container" id="mc7p_pillar5_q8_container">
				<h3>Question 8</h3>
				<p class="qtext"><label for="mc7p_pillar5_q8_text">Question Text:</label><br />
					<textarea name="mc7p_pillar5_q8_text" id="mc7p_pillar5_q8_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar5_q8_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar5_q8_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q8_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar5_q8_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q8_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar5_q8_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar5_q8_choice1">Choice 1  </label><input type="text" name="mc7p_pillar5_q8_choice1" id="mc7p_pillar5_q8_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar5_q8_choice1')); ?>" /><br />
					<label for="mc7p_pillar5_q8_choice2">Choice 2  </label><input type="text" name="mc7p_pillar5_q8_choice2" id="mc7p_pillar5_q8_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar5_q8_choice2')); ?>" /><br />
					<label for="mc7p_pillar5_q8_choice3">Choice 3  </label><input type="text" name="mc7p_pillar5_q8_choice3" id="mc7p_pillar5_q8_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar5_q8_choice3')); ?>" /><br />
					<label for="mc7p_pillar5_q8_choice4">Choice 4  </label><input type="text" name="mc7p_pillar5_q8_choice4" id="mc7p_pillar5_q8_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar5_q8_choice4')); ?>" /><br />
					<label for="mc7p_pillar5_q8_choice5">Choice 5  </label><input type="text" name="mc7p_pillar5_q8_choice5" id="mc7p_pillar5_q8_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar5_q8_choice5')); ?>" /><br />
			</div>
			<!-- Question 9 -->
			<div class="q-container" id="mc7p_pillar5_q9_container">
				<h3>Question 9</h3>
				<p class="qtext"><label for="mc7p_pillar5_q9_text">Question Text:</label><br />
					<textarea name="mc7p_pillar5_q9_text" id="mc7p_pillar5_q9_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar5_q9_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar5_q9_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q9_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar5_q9_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q9_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar5_q9_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar5_q9_choice1">Choice 1  </label><input type="text" name="mc7p_pillar5_q9_choice1" id="mc7p_pillar5_q9_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar5_q9_choice1')); ?>" /><br />
					<label for="mc7p_pillar5_q9_choice2">Choice 2  </label><input type="text" name="mc7p_pillar5_q9_choice2" id="mc7p_pillar5_q9_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar5_q9_choice2')); ?>" /><br />
					<label for="mc7p_pillar5_q9_choice3">Choice 3  </label><input type="text" name="mc7p_pillar5_q9_choice3" id="mc7p_pillar5_q9_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar5_q9_choice3')); ?>" /><br />
					<label for="mc7p_pillar5_q9_choice4">Choice 4  </label><input type="text" name="mc7p_pillar5_q9_choice4" id="mc7p_pillar5_q9_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar5_q9_choice4')); ?>" /><br />
					<label for="mc7p_pillar5_q9_choice5">Choice 5  </label><input type="text" name="mc7p_pillar5_q9_choice5" id="mc7p_pillar5_q9_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar5_q9_choice5')); ?>" /><br />
			</div>
			<!-- Question 10 -->
			<div class="q-container" id="mc7p_pillar5_q10_container">
				<h3>Question 10</h3>
				<p class="qtext"><label for="mc7p_pillar5_q10_text">Question Text:</label><br />
					<textarea name="mc7p_pillar5_q10_text" id="mc7p_pillar5_q10_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar5_q10_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar5_q10_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q10_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar5_q10_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar5_q10_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar5_q10_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar5_q10_choice1">Choice 1  </label><input type="text" name="mc7p_pillar5_q10_choice1" id="mc7p_pillar5_q10_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar5_q10_choice1')); ?>" /><br />
					<label for="mc7p_pillar5_q10_choice2">Choice 2  </label><input type="text" name="mc7p_pillar5_q10_choice2" id="mc7p_pillar5_q10_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar5_q10_choice2')); ?>" /><br />
					<label for="mc7p_pillar5_q10_choice3">Choice 3  </label><input type="text" name="mc7p_pillar5_q10_choice3" id="mc7p_pillar5_q10_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar5_q10_choice3')); ?>" /><br />
					<label for="mc7p_pillar5_q10_choice4">Choice 4  </label><input type="text" name="mc7p_pillar5_q10_choice4" id="mc7p_pillar5_q10_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar5_q10_choice4')); ?>" /><br />
					<label for="mc7p_pillar5_q10_choice5">Choice 5  </label><input type="text" name="mc7p_pillar5_q10_choice5" id="mc7p_pillar5_q10_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar5_q10_choice5')); ?>" /><br />
			</div>
			<p><input type="submit" name="submit" value="Save Options" /></p>
		</form>
	</div>
	<?php
}

// Pillar 6 settings page.
function mc7p_pillar6_settings() {

	?>
	<div class="wrap">
		<h2>Pillar 6 Settings and Options</h2>
		<p> Options and settings page for pillar 6</p>
		
		<form action="options.php" method="post" id="mc7p_pillar6_settings_form">
			<?php settings_fields('mc7p_p6_settings'); ?>
			<!-- Question 1 -->
			<div class="q-container" id="mc7p_pillar6_q1_container">
				<h3>Question 1</h3>
				<p class="qtext"><label for="mc7p_pillar6_q1_text">Question Text:</label><br />
					<textarea name="mc7p_pillar6_q1_text" id="mc7p_pillar6_q1_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar6_q1_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar6_q1_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q1_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar6_q1_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q1_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar6_q1_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar6_q1_choice1">Choice 1  </label><input type="text" name="mc7p_pillar6_q1_choice1" id="mc7p_pillar6_q1_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar6_q1_choice1')); ?>" /><br />
					<label for="mc7p_pillar6_q1_choice2">Choice 2  </label><input type="text" name="mc7p_pillar6_q1_choice2" id="mc7p_pillar6_q1_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar6_q1_choice2')); ?>" /><br />
					<label for="mc7p_pillar6_q1_choice3">Choice 3  </label><input type="text" name="mc7p_pillar6_q1_choice3" id="mc7p_pillar6_q1_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar6_q1_choice3')); ?>" /><br />
					<label for="mc7p_pillar6_q1_choice4">Choice 4  </label><input type="text" name="mc7p_pillar6_q1_choice4" id="mc7p_pillar6_q1_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar6_q1_choice4')); ?>" /><br />
					<label for="mc7p_pillar6_q1_choice5">Choice 5  </label><input type="text" name="mc7p_pillar6_q1_choice5" id="mc7p_pillar6_q1_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar6_q1_choice5')); ?>" /><br />
			</div>
			<!-- Question 2 -->
			<div class="q-container" id="mc7p_pillar6_q2_container">
				<h3>Question 2</h3>
				<p class="qtext"><label for="mc7p_pillar6_q2_text">Question Text:</label><br />
					<textarea name="mc7p_pillar6_q2_text" id="mc7p_pillar6_q2_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar6_q2_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar6_q2_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q2_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar6_q2_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q2_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar6_q2_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar6_q2_choice1">Choice 1  </label><input type="text" name="mc7p_pillar6_q2_choice1" id="mc7p_pillar6_q2_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar6_q2_choice1')); ?>" /><br />
					<label for="mc7p_pillar6_q2_choice2">Choice 2  </label><input type="text" name="mc7p_pillar6_q2_choice2" id="mc7p_pillar6_q2_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar6_q2_choice2')); ?>" /><br />
					<label for="mc7p_pillar6_q2_choice3">Choice 3  </label><input type="text" name="mc7p_pillar6_q2_choice3" id="mc7p_pillar6_q2_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar6_q2_choice3')); ?>" /><br />
					<label for="mc7p_pillar6_q2_choice4">Choice 4  </label><input type="text" name="mc7p_pillar6_q2_choice4" id="mc7p_pillar6_q2_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar6_q2_choice4')); ?>" /><br />
					<label for="mc7p_pillar6_q2_choice5">Choice 5  </label><input type="text" name="mc7p_pillar6_q2_choice5" id="mc7p_pillar6_q2_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar6_q2_choice5')); ?>" /><br />
			</div>
			<!-- Question 3 -->
			<div class="q-container" id="mc7p_pillar6_q3_container">
				<h3>Question 3</h3>
				<p class="qtext"><label for="mc7p_pillar6_q3_text">Question Text:</label><br />
					<textarea name="mc7p_pillar6_q3_text" id="mc7p_pillar6_q3_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar6_q3_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar6_q3_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q3_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar6_q3_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q3_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar6_q3_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar6_q3_choice1">Choice 1  </label><input type="text" name="mc7p_pillar6_q3_choice1" id="mc7p_pillar6_q3_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar6_q3_choice1')); ?>" /><br />
					<label for="mc7p_pillar6_q3_choice2">Choice 2  </label><input type="text" name="mc7p_pillar6_q3_choice2" id="mc7p_pillar6_q3_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar6_q3_choice2')); ?>" /><br />
					<label for="mc7p_pillar6_q3_choice3">Choice 3  </label><input type="text" name="mc7p_pillar6_q3_choice3" id="mc7p_pillar6_q3_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar6_q3_choice3')); ?>" /><br />
					<label for="mc7p_pillar6_q3_choice4">Choice 4  </label><input type="text" name="mc7p_pillar6_q3_choice4" id="mc7p_pillar6_q3_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar6_q3_choice4')); ?>" /><br />
					<label for="mc7p_pillar6_q3_choice5">Choice 5  </label><input type="text" name="mc7p_pillar6_q3_choice5" id="mc7p_pillar6_q3_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar6_q3_choice5')); ?>" /><br />
			</div>
			<!-- Question 4 -->
			<div class="q-container" id="mc7p_pillar6_q4_container">
				<h3>Question 4</h3>
				<p class="qtext"><label for="mc7p_pillar6_q4_text">Question Text:</label><br />
					<textarea name="mc7p_pillar6_q4_text" id="mc7p_pillar6_q4_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar6_q4_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar6_q4_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q4_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar6_q4_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q4_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar6_q4_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar6_q4_choice1">Choice 1  </label><input type="text" name="mc7p_pillar6_q4_choice1" id="mc7p_pillar6_q4_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar6_q4_choice1')); ?>" /><br />
					<label for="mc7p_pillar6_q4_choice2">Choice 2  </label><input type="text" name="mc7p_pillar6_q4_choice2" id="mc7p_pillar6_q4_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar6_q4_choice2')); ?>" /><br />
					<label for="mc7p_pillar6_q4_choice3">Choice 3  </label><input type="text" name="mc7p_pillar6_q4_choice3" id="mc7p_pillar6_q4_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar6_q4_choice3')); ?>" /><br />
					<label for="mc7p_pillar6_q4_choice4">Choice 4  </label><input type="text" name="mc7p_pillar6_q4_choice4" id="mc7p_pillar6_q4_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar6_q4_choice4')); ?>" /><br />
					<label for="mc7p_pillar6_q4_choice5">Choice 5  </label><input type="text" name="mc7p_pillar6_q4_choice5" id="mc7p_pillar6_q4_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar6_q4_choice5')); ?>" /><br />
			</div>
			<!-- Question 5 -->
			<div class="q-container" id="mc7p_pillar6_q5_container">
				<h3>Question 5</h3>
				<p class="qtext"><label for="mc7p_pillar6_q5_text">Question Text:</label><br />
					<textarea name="mc7p_pillar6_q5_text" id="mc7p_pillar6_q5_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar6_q5_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar6_q5_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q5_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar6_q5_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q5_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar6_q5_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar6_q5_choice1">Choice 1  </label><input type="text" name="mc7p_pillar6_q5_choice1" id="mc7p_pillar6_q5_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar6_q5_choice1')); ?>" /><br />
					<label for="mc7p_pillar6_q5_choice2">Choice 2  </label><input type="text" name="mc7p_pillar6_q5_choice2" id="mc7p_pillar6_q5_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar6_q5_choice2')); ?>" /><br />
					<label for="mc7p_pillar6_q5_choice3">Choice 3  </label><input type="text" name="mc7p_pillar6_q5_choice3" id="mc7p_pillar6_q5_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar6_q5_choice3')); ?>" /><br />
					<label for="mc7p_pillar6_q5_choice4">Choice 4  </label><input type="text" name="mc7p_pillar6_q5_choice4" id="mc7p_pillar6_q5_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar6_q5_choice4')); ?>" /><br />
					<label for="mc7p_pillar6_q5_choice5">Choice 5  </label><input type="text" name="mc7p_pillar6_q5_choice5" id="mc7p_pillar6_q5_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar6_q5_choice5')); ?>" /><br />
			</div>
			<!-- Question 6 -->
			<div class="q-container" id="mc7p_pillar6_q6_container">
				<h3>Question 6</h3>
				<p class="qtext"><label for="mc7p_pillar6_q6_text">Question Text:</label><br />
					<textarea name="mc7p_pillar6_q6_text" id="mc7p_pillar6_q6_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar6_q6_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar6_q6_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q6_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar6_q6_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q6_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar6_q6_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar6_q6_choice1">Choice 1  </label><input type="text" name="mc7p_pillar6_q6_choice1" id="mc7p_pillar6_q6_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar6_q6_choice1')); ?>" /><br />
					<label for="mc7p_pillar6_q6_choice2">Choice 2  </label><input type="text" name="mc7p_pillar6_q6_choice2" id="mc7p_pillar6_q6_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar6_q6_choice2')); ?>" /><br />
					<label for="mc7p_pillar6_q6_choice3">Choice 3  </label><input type="text" name="mc7p_pillar6_q6_choice3" id="mc7p_pillar6_q6_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar6_q6_choice3')); ?>" /><br />
					<label for="mc7p_pillar6_q6_choice4">Choice 4  </label><input type="text" name="mc7p_pillar6_q6_choice4" id="mc7p_pillar6_q6_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar6_q6_choice4')); ?>" /><br />
					<label for="mc7p_pillar6_q6_choice5">Choice 5  </label><input type="text" name="mc7p_pillar6_q6_choice5" id="mc7p_pillar6_q6_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar6_q6_choice5')); ?>" /><br />
			</div>
			<!-- Question 7 -->
			<div class="q-container" id="mc7p_pillar6_q7_container">
				<h3>Question 7</h3>
				<p class="qtext"><label for="mc7p_pillar6_q7_text">Question Text:</label><br />
					<textarea name="mc7p_pillar6_q7_text" id="mc7p_pillar6_q7_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar6_q7_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar6_q7_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q7_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar6_q7_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q7_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar6_q7_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar6_q7_choice1">Choice 1  </label><input type="text" name="mc7p_pillar6_q7_choice1" id="mc7p_pillar6_q7_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar6_q7_choice1')); ?>" /><br />
					<label for="mc7p_pillar6_q7_choice2">Choice 2  </label><input type="text" name="mc7p_pillar6_q7_choice2" id="mc7p_pillar6_q7_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar6_q7_choice2')); ?>" /><br />
					<label for="mc7p_pillar6_q7_choice3">Choice 3  </label><input type="text" name="mc7p_pillar6_q7_choice3" id="mc7p_pillar6_q7_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar6_q7_choice3')); ?>" /><br />
					<label for="mc7p_pillar6_q7_choice4">Choice 4  </label><input type="text" name="mc7p_pillar6_q7_choice4" id="mc7p_pillar6_q7_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar6_q7_choice4')); ?>" /><br />
					<label for="mc7p_pillar6_q7_choice5">Choice 5  </label><input type="text" name="mc7p_pillar6_q7_choice5" id="mc7p_pillar6_q7_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar6_q7_choice5')); ?>" /><br />
			</div>
			<!-- Question 8 -->
			<div class="q-container" id="mc7p_pillar6_q8_container">
				<h3>Question 8</h3>
				<p class="qtext"><label for="mc7p_pillar6_q8_text">Question Text:</label><br />
					<textarea name="mc7p_pillar6_q8_text" id="mc7p_pillar6_q8_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar6_q8_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar6_q8_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q8_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar6_q8_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q8_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar6_q8_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar6_q8_choice1">Choice 1  </label><input type="text" name="mc7p_pillar6_q8_choice1" id="mc7p_pillar6_q8_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar6_q8_choice1')); ?>" /><br />
					<label for="mc7p_pillar6_q8_choice2">Choice 2  </label><input type="text" name="mc7p_pillar6_q8_choice2" id="mc7p_pillar6_q8_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar6_q8_choice2')); ?>" /><br />
					<label for="mc7p_pillar6_q8_choice3">Choice 3  </label><input type="text" name="mc7p_pillar6_q8_choice3" id="mc7p_pillar6_q8_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar6_q8_choice3')); ?>" /><br />
					<label for="mc7p_pillar6_q8_choice4">Choice 4  </label><input type="text" name="mc7p_pillar6_q8_choice4" id="mc7p_pillar6_q8_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar6_q8_choice4')); ?>" /><br />
					<label for="mc7p_pillar6_q8_choice5">Choice 5  </label><input type="text" name="mc7p_pillar6_q8_choice5" id="mc7p_pillar6_q8_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar6_q8_choice5')); ?>" /><br />
			</div>
			<!-- Question 9 -->
			<div class="q-container" id="mc7p_pillar6_q9_container">
				<h3>Question 9</h3>
				<p class="qtext"><label for="mc7p_pillar6_q9_text">Question Text:</label><br />
					<textarea name="mc7p_pillar6_q9_text" id="mc7p_pillar6_q9_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar6_q9_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar6_q9_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q9_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar6_q9_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q9_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar6_q9_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar6_q9_choice1">Choice 1  </label><input type="text" name="mc7p_pillar6_q9_choice1" id="mc7p_pillar6_q9_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar6_q9_choice1')); ?>" /><br />
					<label for="mc7p_pillar6_q9_choice2">Choice 2  </label><input type="text" name="mc7p_pillar6_q9_choice2" id="mc7p_pillar6_q9_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar6_q9_choice2')); ?>" /><br />
					<label for="mc7p_pillar6_q9_choice3">Choice 3  </label><input type="text" name="mc7p_pillar6_q9_choice3" id="mc7p_pillar6_q9_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar6_q9_choice3')); ?>" /><br />
					<label for="mc7p_pillar6_q9_choice4">Choice 4  </label><input type="text" name="mc7p_pillar6_q9_choice4" id="mc7p_pillar6_q9_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar6_q9_choice4')); ?>" /><br />
					<label for="mc7p_pillar6_q9_choice5">Choice 5  </label><input type="text" name="mc7p_pillar6_q9_choice5" id="mc7p_pillar6_q9_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar6_q9_choice5')); ?>" /><br />
			</div>
			<!-- Question 10 -->
			<div class="q-container" id="mc7p_pillar6_q10_container">
				<h3>Question 10</h3>
				<p class="qtext"><label for="mc7p_pillar6_q10_text">Question Text:</label><br />
					<textarea name="mc7p_pillar6_q10_text" id="mc7p_pillar6_q10_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar6_q10_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar6_q10_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q10_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar6_q10_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar6_q10_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar6_q10_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar6_q10_choice1">Choice 1  </label><input type="text" name="mc7p_pillar6_q10_choice1" id="mc7p_pillar6_q10_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar6_q10_choice1')); ?>" /><br />
					<label for="mc7p_pillar6_q10_choice2">Choice 2  </label><input type="text" name="mc7p_pillar6_q10_choice2" id="mc7p_pillar6_q10_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar6_q10_choice2')); ?>" /><br />
					<label for="mc7p_pillar6_q10_choice3">Choice 3  </label><input type="text" name="mc7p_pillar6_q10_choice3" id="mc7p_pillar6_q10_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar6_q10_choice3')); ?>" /><br />
					<label for="mc7p_pillar6_q10_choice4">Choice 4  </label><input type="text" name="mc7p_pillar6_q10_choice4" id="mc7p_pillar6_q10_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar6_q10_choice4')); ?>" /><br />
					<label for="mc7p_pillar6_q10_choice5">Choice 5  </label><input type="text" name="mc7p_pillar6_q10_choice5" id="mc7p_pillar6_q10_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar6_q10_choice5')); ?>" /><br />
			</div>
			<p><input type="submit" name="submit" value="Save Options" /></p>
		</form>
	</div>
	<?php
}

// Pillar 7 settings page.
function mc7p_pillar7_settings() {

	?>
	<div class="wrap">
		<h2>Pillar 7 Settings and Options</h2>
		<p> Options and settings page for pillar 7</p>
		
		<form action="options.php" method="post" id="mc7p_pillar7_settings_form">
			<?php settings_fields('mc7p_p7_settings'); ?>
			<!-- Question 1 -->
			<div class="q-container" id="mc7p_pillar7_q1_container">
				<h3>Question 1</h3>
				<p class="qtext"><label for="mc7p_pillar7_q1_text">Question Text:</label><br />
					<textarea name="mc7p_pillar7_q1_text" id="mc7p_pillar7_q1_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar7_q1_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar7_q1_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q1_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar7_q1_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q1_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar7_q1_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar7_q1_choice1">Choice 1  </label><input type="text" name="mc7p_pillar7_q1_choice1" id="mc7p_pillar7_q1_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar7_q1_choice1')); ?>" /><br />
					<label for="mc7p_pillar7_q1_choice2">Choice 2  </label><input type="text" name="mc7p_pillar7_q1_choice2" id="mc7p_pillar7_q1_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar7_q1_choice2')); ?>" /><br />
					<label for="mc7p_pillar7_q1_choice3">Choice 3  </label><input type="text" name="mc7p_pillar7_q1_choice3" id="mc7p_pillar7_q1_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar7_q1_choice3')); ?>" /><br />
					<label for="mc7p_pillar7_q1_choice4">Choice 4  </label><input type="text" name="mc7p_pillar7_q1_choice4" id="mc7p_pillar7_q1_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar7_q1_choice4')); ?>" /><br />
					<label for="mc7p_pillar7_q1_choice5">Choice 5  </label><input type="text" name="mc7p_pillar7_q1_choice5" id="mc7p_pillar7_q1_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar7_q1_choice5')); ?>" /><br />
			</div>
			<!-- Question 2 -->
			<div class="q-container" id="mc7p_pillar7_q2_container">
				<h3>Question 2</h3>
				<p class="qtext"><label for="mc7p_pillar7_q2_text">Question Text:</label><br />
					<textarea name="mc7p_pillar7_q2_text" id="mc7p_pillar7_q2_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar7_q2_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar7_q2_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q2_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar7_q2_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q2_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar7_q2_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar7_q2_choice1">Choice 1  </label><input type="text" name="mc7p_pillar7_q2_choice1" id="mc7p_pillar7_q2_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar7_q2_choice1')); ?>" /><br />
					<label for="mc7p_pillar7_q2_choice2">Choice 2  </label><input type="text" name="mc7p_pillar7_q2_choice2" id="mc7p_pillar7_q2_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar7_q2_choice2')); ?>" /><br />
					<label for="mc7p_pillar7_q2_choice3">Choice 3  </label><input type="text" name="mc7p_pillar7_q2_choice3" id="mc7p_pillar7_q2_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar7_q2_choice3')); ?>" /><br />
					<label for="mc7p_pillar7_q2_choice4">Choice 4  </label><input type="text" name="mc7p_pillar7_q2_choice4" id="mc7p_pillar7_q2_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar7_q2_choice4')); ?>" /><br />
					<label for="mc7p_pillar7_q2_choice5">Choice 5  </label><input type="text" name="mc7p_pillar7_q2_choice5" id="mc7p_pillar7_q2_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar7_q2_choice5')); ?>" /><br />
			</div>
			<!-- Question 3 -->
			<div class="q-container" id="mc7p_pillar7_q3_container">
				<h3>Question 3</h3>
				<p class="qtext"><label for="mc7p_pillar7_q3_text">Question Text:</label><br />
					<textarea name="mc7p_pillar7_q3_text" id="mc7p_pillar7_q3_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar7_q3_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar7_q3_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q3_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar7_q3_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q3_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar7_q3_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar7_q3_choice1">Choice 1  </label><input type="text" name="mc7p_pillar7_q3_choice1" id="mc7p_pillar7_q3_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar7_q3_choice1')); ?>" /><br />
					<label for="mc7p_pillar7_q3_choice2">Choice 2  </label><input type="text" name="mc7p_pillar7_q3_choice2" id="mc7p_pillar7_q3_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar7_q3_choice2')); ?>" /><br />
					<label for="mc7p_pillar7_q3_choice3">Choice 3  </label><input type="text" name="mc7p_pillar7_q3_choice3" id="mc7p_pillar7_q3_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar7_q3_choice3')); ?>" /><br />
					<label for="mc7p_pillar7_q3_choice4">Choice 4  </label><input type="text" name="mc7p_pillar7_q3_choice4" id="mc7p_pillar7_q3_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar7_q3_choice4')); ?>" /><br />
					<label for="mc7p_pillar7_q3_choice5">Choice 5  </label><input type="text" name="mc7p_pillar7_q3_choice5" id="mc7p_pillar7_q3_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar7_q3_choice5')); ?>" /><br />
			</div>
			<!-- Question 4 -->
			<div class="q-container" id="mc7p_pillar7_q4_container">
				<h3>Question 4</h3>
				<p class="qtext"><label for="mc7p_pillar7_q4_text">Question Text:</label><br />
					<textarea name="mc7p_pillar7_q4_text" id="mc7p_pillar7_q4_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar7_q4_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar7_q4_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q4_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar7_q4_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q4_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar7_q4_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar7_q4_choice1">Choice 1  </label><input type="text" name="mc7p_pillar7_q4_choice1" id="mc7p_pillar7_q4_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar7_q4_choice1')); ?>" /><br />
					<label for="mc7p_pillar7_q4_choice2">Choice 2  </label><input type="text" name="mc7p_pillar7_q4_choice2" id="mc7p_pillar7_q4_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar7_q4_choice2')); ?>" /><br />
					<label for="mc7p_pillar7_q4_choice3">Choice 3  </label><input type="text" name="mc7p_pillar7_q4_choice3" id="mc7p_pillar7_q4_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar7_q4_choice3')); ?>" /><br />
					<label for="mc7p_pillar7_q4_choice4">Choice 4  </label><input type="text" name="mc7p_pillar7_q4_choice4" id="mc7p_pillar7_q4_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar7_q4_choice4')); ?>" /><br />
					<label for="mc7p_pillar7_q4_choice5">Choice 5  </label><input type="text" name="mc7p_pillar7_q4_choice5" id="mc7p_pillar7_q4_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar7_q4_choice5')); ?>" /><br />
			</div>
			<!-- Question 5 -->
			<div class="q-container" id="mc7p_pillar7_q5_container">
				<h3>Question 5</h3>
				<p class="qtext"><label for="mc7p_pillar7_q5_text">Question Text:</label><br />
					<textarea name="mc7p_pillar7_q5_text" id="mc7p_pillar7_q5_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar7_q5_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar7_q5_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q5_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar7_q5_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q5_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar7_q5_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar7_q5_choice1">Choice 1  </label><input type="text" name="mc7p_pillar7_q5_choice1" id="mc7p_pillar7_q5_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar7_q5_choice1')); ?>" /><br />
					<label for="mc7p_pillar7_q5_choice2">Choice 2  </label><input type="text" name="mc7p_pillar7_q5_choice2" id="mc7p_pillar7_q5_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar7_q5_choice2')); ?>" /><br />
					<label for="mc7p_pillar7_q5_choice3">Choice 3  </label><input type="text" name="mc7p_pillar7_q5_choice3" id="mc7p_pillar7_q5_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar7_q5_choice3')); ?>" /><br />
					<label for="mc7p_pillar7_q5_choice4">Choice 4  </label><input type="text" name="mc7p_pillar7_q5_choice4" id="mc7p_pillar7_q5_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar7_q5_choice4')); ?>" /><br />
					<label for="mc7p_pillar7_q5_choice5">Choice 5  </label><input type="text" name="mc7p_pillar7_q5_choice5" id="mc7p_pillar7_q5_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar7_q5_choice5')); ?>" /><br />
			</div>
			<!-- Question 6 -->
			<div class="q-container" id="mc7p_pillar7_q6_container">
				<h3>Question 6</h3>
				<p class="qtext"><label for="mc7p_pillar7_q6_text">Question Text:</label><br />
					<textarea name="mc7p_pillar7_q6_text" id="mc7p_pillar7_q6_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar7_q6_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar7_q6_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q6_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar7_q6_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q6_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar7_q6_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar7_q6_choice1">Choice 1  </label><input type="text" name="mc7p_pillar7_q6_choice1" id="mc7p_pillar7_q6_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar7_q6_choice1')); ?>" /><br />
					<label for="mc7p_pillar7_q6_choice2">Choice 2  </label><input type="text" name="mc7p_pillar7_q6_choice2" id="mc7p_pillar7_q6_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar7_q6_choice2')); ?>" /><br />
					<label for="mc7p_pillar7_q6_choice3">Choice 3  </label><input type="text" name="mc7p_pillar7_q6_choice3" id="mc7p_pillar7_q6_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar7_q6_choice3')); ?>" /><br />
					<label for="mc7p_pillar7_q6_choice4">Choice 4  </label><input type="text" name="mc7p_pillar7_q6_choice4" id="mc7p_pillar7_q6_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar7_q6_choice4')); ?>" /><br />
					<label for="mc7p_pillar7_q6_choice5">Choice 5  </label><input type="text" name="mc7p_pillar7_q6_choice5" id="mc7p_pillar7_q6_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar7_q6_choice5')); ?>" /><br />
			</div>
			<!-- Question 7 -->
			<div class="q-container" id="mc7p_pillar7_q7_container">
				<h3>Question 7</h3>
				<p class="qtext"><label for="mc7p_pillar7_q7_text">Question Text:</label><br />
					<textarea name="mc7p_pillar7_q7_text" id="mc7p_pillar7_q7_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar7_q7_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar7_q7_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q7_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar7_q7_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q7_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar7_q7_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar7_q7_choice1">Choice 1  </label><input type="text" name="mc7p_pillar7_q7_choice1" id="mc7p_pillar7_q7_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar7_q7_choice1')); ?>" /><br />
					<label for="mc7p_pillar7_q7_choice2">Choice 2  </label><input type="text" name="mc7p_pillar7_q7_choice2" id="mc7p_pillar7_q7_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar7_q7_choice2')); ?>" /><br />
					<label for="mc7p_pillar7_q7_choice3">Choice 3  </label><input type="text" name="mc7p_pillar7_q7_choice3" id="mc7p_pillar7_q7_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar7_q7_choice3')); ?>" /><br />
					<label for="mc7p_pillar7_q7_choice4">Choice 4  </label><input type="text" name="mc7p_pillar7_q7_choice4" id="mc7p_pillar7_q7_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar7_q7_choice4')); ?>" /><br />
					<label for="mc7p_pillar7_q7_choice5">Choice 5  </label><input type="text" name="mc7p_pillar7_q7_choice5" id="mc7p_pillar7_q7_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar7_q7_choice5')); ?>" /><br />
			</div>
			<!-- Question 8 -->
			<div class="q-container" id="mc7p_pillar7_q8_container">
				<h3>Question 8</h3>
				<p class="qtext"><label for="mc7p_pillar7_q8_text">Question Text:</label><br />
					<textarea name="mc7p_pillar7_q8_text" id="mc7p_pillar7_q8_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar7_q8_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar7_q8_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q8_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar7_q8_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q8_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar7_q8_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar7_q8_choice1">Choice 1  </label><input type="text" name="mc7p_pillar7_q8_choice1" id="mc7p_pillar7_q8_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar7_q8_choice1')); ?>" /><br />
					<label for="mc7p_pillar7_q8_choice2">Choice 2  </label><input type="text" name="mc7p_pillar7_q8_choice2" id="mc7p_pillar7_q8_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar7_q8_choice2')); ?>" /><br />
					<label for="mc7p_pillar7_q8_choice3">Choice 3  </label><input type="text" name="mc7p_pillar7_q8_choice3" id="mc7p_pillar7_q8_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar7_q8_choice3')); ?>" /><br />
					<label for="mc7p_pillar7_q8_choice4">Choice 4  </label><input type="text" name="mc7p_pillar7_q8_choice4" id="mc7p_pillar7_q8_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar7_q8_choice4')); ?>" /><br />
					<label for="mc7p_pillar7_q8_choice5">Choice 5  </label><input type="text" name="mc7p_pillar7_q8_choice5" id="mc7p_pillar7_q8_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar7_q8_choice5')); ?>" /><br />
			</div>
			<!-- Question 9 -->
			<div class="q-container" id="mc7p_pillar7_q9_container">
				<h3>Question 9</h3>
				<p class="qtext"><label for="mc7p_pillar7_q9_text">Question Text:</label><br />
					<textarea name="mc7p_pillar7_q9_text" id="mc7p_pillar7_q9_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar7_q9_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar7_q9_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q9_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar7_q9_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q9_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar7_q9_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar7_q9_choice1">Choice 1  </label><input type="text" name="mc7p_pillar7_q9_choice1" id="mc7p_pillar7_q9_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar7_q9_choice1')); ?>" /><br />
					<label for="mc7p_pillar7_q9_choice2">Choice 2  </label><input type="text" name="mc7p_pillar7_q9_choice2" id="mc7p_pillar7_q9_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar7_q9_choice2')); ?>" /><br />
					<label for="mc7p_pillar7_q9_choice3">Choice 3  </label><input type="text" name="mc7p_pillar7_q9_choice3" id="mc7p_pillar7_q9_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar7_q9_choice3')); ?>" /><br />
					<label for="mc7p_pillar7_q9_choice4">Choice 4  </label><input type="text" name="mc7p_pillar7_q9_choice4" id="mc7p_pillar7_q9_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar7_q9_choice4')); ?>" /><br />
					<label for="mc7p_pillar7_q9_choice5">Choice 5  </label><input type="text" name="mc7p_pillar7_q9_choice5" id="mc7p_pillar7_q9_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar7_q9_choice5')); ?>" /><br />
			</div>
			<!-- Question 10 -->
			<div class="q-container" id="mc7p_pillar7_q10_container">
				<h3>Question 10</h3>
				<p class="qtext"><label for="mc7p_pillar7_q10_text">Question Text:</label><br />
					<textarea name="mc7p_pillar7_q10_text" id="mc7p_pillar7_q10_text" cols="100" rows="3"><?php echo esc_attr(get_option('mc7p_pillar7_q10_text')); ?></textarea></p>
				<p class="qtype"><label for="mc7p_pillar7_q10_type">Type of question:</label>&nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q10_type" value="yesno" <?php echo (esc_attr(get_option('mc7p_pillar7_q10_type')) == 'yesno' ? 'checked' : ''); ?>>Yes / No Question &nbsp;&nbsp;&nbsp;
					<input type="radio" name="mc7p_pillar7_q10_type" value="multi" <?php echo (esc_attr(get_option('mc7p_pillar7_q10_type')) == 'multi' ? 'checked' : ''); ?>>Multiple Choice</p>
				<p class="qchoices"><label>Choices:</label><br />
					<label for="mc7p_pillar7_q10_choice1">Choice 1  </label><input type="text" name="mc7p_pillar7_q10_choice1" id="mc7p_pillar7_q10_choice1" value="<?php echo esc_attr(get_option('mc7p_pillar7_q10_choice1')); ?>" /><br />
					<label for="mc7p_pillar7_q10_choice2">Choice 2  </label><input type="text" name="mc7p_pillar7_q10_choice2" id="mc7p_pillar7_q10_choice2" value="<?php echo esc_attr(get_option('mc7p_pillar7_q10_choice2')); ?>" /><br />
					<label for="mc7p_pillar7_q10_choice3">Choice 3  </label><input type="text" name="mc7p_pillar7_q10_choice3" id="mc7p_pillar7_q10_choice3" value="<?php echo esc_attr(get_option('mc7p_pillar7_q10_choice3')); ?>" /><br />
					<label for="mc7p_pillar7_q10_choice4">Choice 4  </label><input type="text" name="mc7p_pillar7_q10_choice4" id="mc7p_pillar7_q10_choice4" value="<?php echo esc_attr(get_option('mc7p_pillar7_q10_choice4')); ?>" /><br />
					<label for="mc7p_pillar7_q10_choice5">Choice 5  </label><input type="text" name="mc7p_pillar7_q10_choice5" id="mc7p_pillar7_q10_choice5" value="<?php echo esc_attr(get_option('mc7p_pillar7_q10_choice5')); ?>" /><br />
			</div>
			<p><input type="submit" name="submit" value="Save Options" /></p>
		</form>
	</div>
	<?php
}

function mc7p_results_admin_page() {
	global $wpdb;
	$table_name = $wpdb->prefix . "mc7p";
	$quizes = $wpdb->get_results( 'SELECT * FROM ' . $table_name, ARRAY_A );
	// Get averages.
	$nal_p1 = 0;
	$nal_p2 = 0;
	$nal_p3 = 0;
	$nal_p4 = 0;
	$nal_p5 = 0;
	$nal_p6 = 0;
	$nal_p7 = 0;
	foreach( $quizes as $record ) {
		$nal_p1 += $record['pillar1_score'];
		$nal_p2 += $record['pillar2_score'];
		$nal_p3 += $record['pillar3_score'];
		$nal_p4 += $record['pillar4_score'];
		$nal_p5 += $record['pillar5_score'];
		$nal_p6 += $record['pillar6_score'];
		$nal_p7 += $record['pillar7_score'];
		$nal_total += $record['total_score'];
		$nal_records += 1;
	}
	$nal_p1 /= $nal_records;
	$nal_p2 /= $nal_records;
	$nal_p3 /= $nal_records;
	$nal_p4 /= $nal_records;
	$nal_p5 /= $nal_records;
	$nal_p6 /= $nal_records;
	$nal_p7 /= $nal_records;
	$nal_total /= $nal_records;

	
	?>
	<div class="wrap">
		<h2>The 7 pillars Questionnaire Results Page</h2>
		<p> Results of all the questionnaires filled by any user will be found here...</p>
		<p>Click on the headers to sort the table by that header.</p>
		
		<div id="results-table">
			<table class="sortable">
				<tr class="column-titles">
					<th>Date</th>
					<th>User</th>
					<!-- <th>State<br />Territory</th>
					<th>Deliverer<br />Type</th> -->
					<th>Pillar 1<br />Score</th>
					<th>Pillar 2<br />Score</th>
					<th>Pillar 3<br />Score</th>
					<th>Pillar 4<br />Score</th>
					<th>Pillar 5<br />Score</th>
					<th>Pillar 6<br />Score</th>
					<th>Pillar 7<br />Score</th>
					<th>Total<br />Score</th>
					<th>Results<br />Pages</th>
				</tr>
<?php
foreach( $quizes as $quiz ) {
	$user_info = get_user_meta( $quiz['user_id'] );

	//var_dump($user_info);
?>	
				<tr>
					<td><?php echo date( 'd-m-Y H:i:s', strtotime( $quiz['date_filled'] ) ); ?></td>
					<td><?php echo $user_info['nickname'][0]; ?> <?php echo $user_info['last_name'][0]; ?></td>
					<!-- <td><?php echo $quiz['region']; ?></td>
					<td><?php echo $user_info['deliverer_type'][0]; ?></td> -->
					<td><?php echo $quiz['pillar1_score']; ?></td>
					<td><?php echo $quiz['pillar2_score']; ?></td>
					<td><?php echo $quiz['pillar3_score']; ?></td>
					<td><?php echo $quiz['pillar4_score']; ?></td>
					<td><?php echo $quiz['pillar5_score']; ?></td>
					<td><?php echo $quiz['pillar6_score']; ?></td>
					<td><?php echo $quiz['pillar7_score']; ?></td>
					<td><?php echo $quiz['total_score']; ?></td>
					<td><a href="<?php echo plugins_url( 'score-sheet.php' , __FILE__ ); ?>?form=<?php echo $quiz['id']; ?>">View Results</a></td>
				</tr>
<?php } ?>
				<!-- <tr style="font-weight: bold;">
					<td colspan="4" style="border: 0; text-align:right;">National Averages</td>
					<td><?php echo $nal_p1; ?></td>
					<td><?php echo $nal_p2; ?></td>
					<td><?php echo $nal_p3; ?></td>
					<td><?php echo $nal_p4; ?></td>
					<td><?php echo $nal_p5; ?></td>
					<td><?php echo $nal_p6; ?></td>
					<td><?php echo $nal_p7; ?></td>
					<td><?php echo $nal_total; ?></td>
				</tr> -->
			</table>
		</div>

	</div>
	<?php

}