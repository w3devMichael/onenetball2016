<?php
/** 
 * Plugin Name: 7 Pillars Questionnaire Plugin
 * Plugin URI: http://carlostapia.com/wp/seven-pillars/
 * Description: The 7 pillars Questionnaire Plugin is a plugin that adds the functionality of a 7-part questionnaire to WordPress and saves the results of all completed questionnaires for statistical purposes.
 * Author: Carlos Tapia
 * Version: 1.1
 * Author URI: http://www.carlostapia.com
 */


// Start a session if not started yet
function register_session(){
    if( !session_id() ) {
        session_start();
	}
}
add_action('init','register_session');

// Define environment constants
define('mc7p_path', plugin_dir_path(__FILE__));
define('mc7p_url', plugin_dir_url(__FILE__));

// Tasks that need to be run on plugin activation
function mc7p_activate()
{
	global $wpdb;
	
	$table_name = $wpdb->prefix . "mc7p";
	
	// will return NULL if there isn't one
	if ( $wpdb->get_var('SHOW TABLES LIKE ' . $table_name) != $table_name ) {
		$sql = 'CREATE TABLE ' . $table_name . '( 
				id INTEGER(10) UNSIGNED AUTO_INCREMENT,
				date_filled TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
				user_id INTEGER(10),
				region VARCHAR (255),
				pillar1_score INTEGER(10),
				pillar2_score INTEGER(10),
				pillar3_score INTEGER(10),
				pillar4_score INTEGER(10),
				pillar5_score INTEGER(10),
				pillar6_score INTEGER(10),
				pillar7_score INTEGER(10),
				total_score INTEGER(10),
				PRIMARY KEY  (id) );';
		
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
		
		add_option('mc7p_database_version','1.0');
	}
}
register_activation_hook(__FILE__,'mc7p_activate');

// Register settings input fields before use them in the settings pages.
include( 'options-init.php' );

// Add .js and .css files necessary for this plugin. Only to the 7P plugin admin pages.
function mc7p_admin_scripts_and_styles($hook) {
	if ( strpos( $hook, 'mc7p' ) !== false ) {
		wp_enqueue_style( 'mc7p-admin-style', plugins_url( 'mc7p-admin.css', __FILE__ ), array(), null, 'all' );
		wp_enqueue_script( 'mc7p-admin-js', plugins_url( 'mc7p-scripts.js', __FILE__ ), array( 'jquery' ), null, true );
		wp_enqueue_script( 'mc7p-sorttable', plugins_url( 'sorttable.js', __FILE__ ), array( 'jquery' ), null, false );
	}
}
add_action( 'admin_enqueue_scripts', 'mc7p_admin_scripts_and_styles' );

// Scripts and styles for general pages.
function mc7p_scripts_and_styles() {
	//wp_enqueue_style( 'mc7p-gen-styles', plugins_url( 'mc7p-general.css', __FILE__ ), array(), null, 'all' );
	wp_enqueue_script( 'mc7p-jquery-session', plugins_url( 'jquery.session.js', __FILE__ ), array( 'jquery'), null,  true );
	wp_enqueue_script( 'mc7p-gen-js', plugins_url( 'mc7p-general.js', __FILE__ ), array( 'jquery'), null,  true );
}
add_action( 'wp_enqueue_scripts', 'mc7p_scripts_and_styles' );

// Admin sidebar settings group and 1st page.
function mc7p_settings_group() {

	?>
	<div class="wrap">
		<h2>The 7 pillars Questionnaire Settings</h2>
		<p>The 7 pillars Questionnaire Plugin is a plugin that adds the functionality of a 7-part questionnaire.</p>
		<p>Each of the 7 parts of the complete questionnaire can be added to any page with the following shortcodes:</p>
		<p><ul>
			<li><code>[mc7p-pillar1-quiz]</code></li>
			<li><code>[mc7p-pillar2-quiz]</code></li>
			<li><code>[mc7p-pillar3-quiz]</code></li>
			<li><code>[mc7p-pillar4-quiz]</code></li>
			<li><code>[mc7p-pillar5-quiz]</code></li>
			<li><code>[mc7p-pillar6-quiz]</code></li>
			<li><code>[mc7p-pillar7-quiz]</code></li>
		</ul></p>
		<p>To add each of the parts of the questionnaire just insert the above shortcodes in newly created blank pages.</code>.</p>
		<p>A history results page is also included that will show the logged in user the results of all the times he/she has completed the questionnaire. This results page can be created by adding the shortcode <code>[mc7p-results-user-page]</code> to a newly created blank page.</p>
		<p> The text for the questions of each pillar can be configured in the respective pillar settings pages which can be found on the left sidebar. Also the wording for the feedback the user will get when finishing the questionnaire is configured below. </p>
		<form action="options.php" method="post" id="mc7p_general_settings_form">
			<?php settings_fields('mc7p_general_settings'); ?>
			<p class="qtext"><label for="mc7p_feedback_low">Feedback wording for a 0 - 70 score:</label><br /><textarea name="mc7p_feedback_low" id="mc7p_feedback_low" cols="100" rows="5"><?php echo esc_attr(get_option('mc7p_feedback_low')); ?></textarea></p>
			<p class="qtext"><label for="mc7p_feedback_med">Feedback wording for a 71 - 140 score:</label><br /><textarea name="mc7p_feedback_med" id="mc7p_feedback_med" cols="100" rows="5"><?php echo esc_attr(get_option('mc7p_feedback_med')); ?></textarea></p>
			<p class="qtext"><label for="mc7p_feedback_high">Feedback wording for a 141 - 210 score:</label><br /><textarea name="mc7p_feedback_high" id="mc7p_feedback_high" cols="100" rows="5"><?php echo esc_attr(get_option('mc7p_feedback_high')); ?></textarea></p>
			<p><input type="submit" name="submit" value="Save Options" /></p>
		</form>

	</div>
	<?php

}

include( 'admin-pages.php' );

// Plug Settings pages in WP.
function mc7p_plugin_admin_pages() {
	// Settings group and 1st page.
	add_menu_page('7 Pillars Questionnaire Settings', '7 Pillars Settings', 'manage_options', 'mc7p-plugin', 'mc7p_settings_group');
	
	// Pillar 1 settings page within the settings group.
	add_submenu_page('mc7p-plugin', '7 Pillars Questionnaire Plugin - Pillar 1', 'Pillar 1', 'manage_options', 'mc7p-pillar1-settings', 'mc7p_pillar1_settings');
	// Pillar 2 settings page within the settings group.
	add_submenu_page('mc7p-plugin', '7 Pillars Questionnaire Plugin - Pillar 2', 'Pillar 2', 'manage_options', 'mc7p-pillar2-settings', 'mc7p_pillar2_settings');
	// Pillar 3 settings page within the settings group.
	add_submenu_page('mc7p-plugin', '7 Pillars Questionnaire Plugin - Pillar 3', 'Pillar 3', 'manage_options', 'mc7p-pillar3-settings', 'mc7p_pillar3_settings');
	// Pillar 4 settings page within the settings group.
	add_submenu_page('mc7p-plugin', '7 Pillars Questionnaire Plugin - Pillar 4', 'Pillar 4', 'manage_options', 'mc7p-pillar4-settings', 'mc7p_pillar4_settings');
	// Pillar 5 settings page within the settings group.
	add_submenu_page('mc7p-plugin', '7 Pillars Questionnaire Plugin - Pillar 5', 'Pillar 5', 'manage_options', 'mc7p-pillar5-settings', 'mc7p_pillar5_settings');
	// Pillar 6 settings page within the settings group.
	add_submenu_page('mc7p-plugin', '7 Pillars Questionnaire Plugin - Pillar 6', 'Pillar 6', 'manage_options', 'mc7p-pillar6-settings', 'mc7p_pillar6_settings');
	// Pillar 7 settings page within the settings group.
	add_submenu_page('mc7p-plugin', '7 Pillars Questionnaire Plugin - Pillar 7', 'Pillar 7', 'manage_options', 'mc7p-pillar7-settings', 'mc7p_pillar7_settings');
	add_submenu_page('mc7p-plugin', '7 Pillars Questionnaire Plugin - Results Page', 'Results', 'manage_options', 'mc7p-results-admin-page', 'mc7p_results_admin_page');
}
add_action('admin_menu', 'mc7p_plugin_admin_pages');

// Shortcodes to add the pillar questionnaires to pages.
include( 'questionnaire-shortcodes.php' );