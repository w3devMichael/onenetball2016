<?php
/**
 * 7 Pillars Questionnaire Plugin
 * Include to register all the plugin options, questions text and answers included, 
 * in the options db table.
 * File version: 1.0
 */

// Register settings input fields before use them in the settings pages.
function mc7p_options_init() {
	// User feedback wording settings
	register_setting('mc7p_general_settings', 'mc7p_feedback_high');
	register_setting('mc7p_general_settings', 'mc7p_feedback_med');
	register_setting('mc7p_general_settings', 'mc7p_feedback_low');

	// Pillar 1 Question 1 Options.
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q1_text');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q1_type');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q1_choice1');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q1_choice2');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q1_choice3');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q1_choice4');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q1_choice5');
	
	// Pillar 1 Question 2 Options.
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q2_text');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q2_type');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q2_choice1');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q2_choice2');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q2_choice3');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q2_choice4');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q2_choice5');
	
	// Pillar 1 Question 3 Options.
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q3_text');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q3_type');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q3_choice1');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q3_choice2');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q3_choice3');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q3_choice4');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q3_choice5');
	
	// Pillar 1 Question 4 Options.
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q4_text');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q4_type');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q4_choice1');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q4_choice2');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q4_choice3');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q4_choice4');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q4_choice5');
	
	// Pillar 1 Question 5 Options.
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q5_text');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q5_type');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q5_choice1');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q5_choice2');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q5_choice3');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q5_choice4');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q5_choice5');
	
	// Pillar 1 Question 6 Options.
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q6_text');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q6_type');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q6_choice1');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q6_choice2');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q6_choice3');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q6_choice4');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q6_choice5');

	// Pillar 1 Question 7 Options.
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q7_text');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q7_type');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q7_choice1');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q7_choice2');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q7_choice3');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q7_choice4');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q7_choice5');

	// Pillar 1 Question 8 Options.
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q8_text');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q8_type');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q8_choice1');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q8_choice2');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q8_choice3');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q8_choice4');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q8_choice5');

	// Pillar 1 Question 9 Options.
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q9_text');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q9_type');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q9_choice1');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q9_choice2');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q9_choice3');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q9_choice4');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q9_choice5');

	// Pillar 1 Question 10 Options.
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q10_text');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q10_type');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q10_choice1');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q10_choice2');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q10_choice3');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q10_choice4');
	register_setting('mc7p_p1_settings', 'mc7p_pillar1_q10_choice5');
	
	// Pillar 2 Question 1 Options.
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q1_text');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q1_type');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q1_choice1');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q1_choice2');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q1_choice3');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q1_choice4');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q1_choice5');
	
	// Pillar 2 Question 2 Options.
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q2_text');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q2_type');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q2_choice1');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q2_choice2');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q2_choice3');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q2_choice4');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q2_choice5');
	
	// Pillar 2 Question 3 Options.
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q3_text');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q3_type');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q3_choice1');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q3_choice2');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q3_choice3');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q3_choice4');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q3_choice5');
	
	// Pillar 2 Question 4 Options.
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q4_text');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q4_type');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q4_choice1');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q4_choice2');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q4_choice3');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q4_choice4');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q4_choice5');
	
	// Pillar 2 Question 5 Options.
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q5_text');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q5_type');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q5_choice1');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q5_choice2');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q5_choice3');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q5_choice4');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q5_choice5');
	
	// Pillar 2 Question 6 Options.
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q6_text');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q6_type');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q6_choice1');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q6_choice2');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q6_choice3');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q6_choice4');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q6_choice5');

	// Pillar 2 Question 7 Options.
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q7_text');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q7_type');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q7_choice1');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q7_choice2');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q7_choice3');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q7_choice4');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q7_choice5');

	// Pillar 2 Question 8 Options.
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q8_text');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q8_type');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q8_choice1');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q8_choice2');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q8_choice3');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q8_choice4');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q8_choice5');

	// Pillar 2 Question 9 Options.
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q9_text');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q9_type');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q9_choice1');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q9_choice2');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q9_choice3');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q9_choice4');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q9_choice5');

	// Pillar 2 Question 10 Options.
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q10_text');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q10_type');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q10_choice1');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q10_choice2');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q10_choice3');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q10_choice4');
	register_setting('mc7p_p2_settings', 'mc7p_pillar2_q10_choice5');
	
	// Pillar 3 Question 1 Options.
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q1_text');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q1_type');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q1_choice1');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q1_choice2');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q1_choice3');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q1_choice4');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q1_choice5');
	
	// Pillar 3 Question 2 Options.
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q2_text');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q2_type');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q2_choice1');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q2_choice2');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q2_choice3');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q2_choice4');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q2_choice5');
	
	// Pillar 3 Question 3 Options.
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q3_text');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q3_type');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q3_choice1');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q3_choice2');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q3_choice3');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q3_choice4');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q3_choice5');
	
	// Pillar 3 Question 4 Options.
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q4_text');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q4_type');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q4_choice1');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q4_choice2');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q4_choice3');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q4_choice4');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q4_choice5');
	
	// Pillar 3 Question 5 Options.
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q5_text');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q5_type');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q5_choice1');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q5_choice2');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q5_choice3');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q5_choice4');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q5_choice5');
	
	// Pillar 3 Question 6 Options.
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q6_text');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q6_type');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q6_choice1');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q6_choice2');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q6_choice3');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q6_choice4');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q6_choice5');

	// Pillar 3 Question 7 Options.
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q7_text');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q7_type');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q7_choice1');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q7_choice2');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q7_choice3');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q7_choice4');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q7_choice5');

	// Pillar 3 Question 8 Options.
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q8_text');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q8_type');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q8_choice1');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q8_choice2');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q8_choice3');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q8_choice4');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q8_choice5');

	// Pillar 3 Question 9 Options.
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q9_text');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q9_type');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q9_choice1');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q9_choice2');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q9_choice3');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q9_choice4');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q9_choice5');

	// Pillar 3 Question 10 Options.
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q10_text');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q10_type');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q10_choice1');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q10_choice2');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q10_choice3');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q10_choice4');
	register_setting('mc7p_p3_settings', 'mc7p_pillar3_q10_choice5');
	
	// Pillar 4 Question 1 Options.
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q1_text');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q1_type');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q1_choice1');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q1_choice2');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q1_choice3');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q1_choice4');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q1_choice5');
	
	// Pillar 4 Question 2 Options.
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q2_text');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q2_type');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q2_choice1');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q2_choice2');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q2_choice3');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q2_choice4');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q2_choice5');
	
	// Pillar 4 Question 3 Options.
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q3_text');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q3_type');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q3_choice1');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q3_choice2');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q3_choice3');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q3_choice4');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q3_choice5');
	
	// Pillar 4 Question 4 Options.
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q4_text');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q4_type');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q4_choice1');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q4_choice2');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q4_choice3');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q4_choice4');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q4_choice5');
	
	// Pillar 4 Question 5 Options.
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q5_text');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q5_type');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q5_choice1');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q5_choice2');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q5_choice3');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q5_choice4');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q5_choice5');
	
	// Pillar 4 Question 6 Options.
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q6_text');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q6_type');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q6_choice1');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q6_choice2');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q6_choice3');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q6_choice4');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q6_choice5');

	// Pillar 4 Question 7 Options.
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q7_text');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q7_type');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q7_choice1');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q7_choice2');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q7_choice3');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q7_choice4');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q7_choice5');

	// Pillar 4 Question 8 Options.
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q8_text');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q8_type');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q8_choice1');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q8_choice2');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q8_choice3');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q8_choice4');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q8_choice5');

	// Pillar 4 Question 9 Options.
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q9_text');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q9_type');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q9_choice1');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q9_choice2');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q9_choice3');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q9_choice4');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q9_choice5');

	// Pillar 4 Question 10 Options.
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q10_text');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q10_type');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q10_choice1');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q10_choice2');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q10_choice3');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q10_choice4');
	register_setting('mc7p_p4_settings', 'mc7p_pillar4_q10_choice5');
	
	// Pillar 5 Question 1 Options.
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q1_text');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q1_type');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q1_choice1');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q1_choice2');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q1_choice3');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q1_choice4');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q1_choice5');
	
	// Pillar 5 Question 2 Options.
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q2_text');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q2_type');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q2_choice1');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q2_choice2');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q2_choice3');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q2_choice4');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q2_choice5');
	
	// Pillar 5 Question 3 Options.
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q3_text');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q3_type');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q3_choice1');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q3_choice2');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q3_choice3');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q3_choice4');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q3_choice5');
	
	// Pillar 5 Question 4 Options.
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q4_text');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q4_type');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q4_choice1');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q4_choice2');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q4_choice3');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q4_choice4');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q4_choice5');
	
	// Pillar 5 Question 5 Options.
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q5_text');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q5_type');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q5_choice1');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q5_choice2');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q5_choice3');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q5_choice4');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q5_choice5');
	
	// Pillar 5 Question 6 Options.
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q6_text');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q6_type');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q6_choice1');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q6_choice2');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q6_choice3');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q6_choice4');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q6_choice5');

	// Pillar 5 Question 7 Options.
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q7_text');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q7_type');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q7_choice1');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q7_choice2');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q7_choice3');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q7_choice4');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q7_choice5');

	// Pillar 5 Question 8 Options.
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q8_text');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q8_type');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q8_choice1');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q8_choice2');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q8_choice3');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q8_choice4');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q8_choice5');

	// Pillar 5 Question 9 Options.
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q9_text');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q9_type');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q9_choice1');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q9_choice2');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q9_choice3');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q9_choice4');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q9_choice5');

	// Pillar 5 Question 10 Options.
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q10_text');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q10_type');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q10_choice1');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q10_choice2');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q10_choice3');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q10_choice4');
	register_setting('mc7p_p5_settings', 'mc7p_pillar5_q10_choice5');
	
	// Pillar 6 Question 1 Options.
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q1_text');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q1_type');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q1_choice1');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q1_choice2');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q1_choice3');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q1_choice4');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q1_choice5');
	
	// Pillar 6 Question 2 Options.
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q2_text');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q2_type');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q2_choice1');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q2_choice2');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q2_choice3');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q2_choice4');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q2_choice5');
	
	// Pillar 6 Question 3 Options.
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q3_text');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q3_type');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q3_choice1');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q3_choice2');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q3_choice3');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q3_choice4');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q3_choice5');
	
	// Pillar 6 Question 4 Options.
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q4_text');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q4_type');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q4_choice1');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q4_choice2');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q4_choice3');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q4_choice4');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q4_choice5');
	
	// Pillar 6 Question 5 Options.
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q5_text');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q5_type');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q5_choice1');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q5_choice2');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q5_choice3');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q5_choice4');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q5_choice5');
	
	// Pillar 6 Question 6 Options.
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q6_text');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q6_type');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q6_choice1');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q6_choice2');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q6_choice3');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q6_choice4');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q6_choice5');

	// Pillar 6 Question 7 Options.
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q7_text');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q7_type');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q7_choice1');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q7_choice2');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q7_choice3');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q7_choice4');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q7_choice5');

	// Pillar 6 Question 8 Options.
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q8_text');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q8_type');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q8_choice1');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q8_choice2');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q8_choice3');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q8_choice4');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q8_choice5');

	// Pillar 6 Question 9 Options.
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q9_text');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q9_type');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q9_choice1');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q9_choice2');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q9_choice3');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q9_choice4');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q9_choice5');

	// Pillar 6 Question 10 Options.
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q10_text');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q10_type');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q10_choice1');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q10_choice2');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q10_choice3');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q10_choice4');
	register_setting('mc7p_p6_settings', 'mc7p_pillar6_q10_choice5');
	
	// Pillar 7 Question 1 Options.
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q1_text');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q1_type');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q1_choice1');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q1_choice2');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q1_choice3');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q1_choice4');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q1_choice5');
	
	// Pillar 7 Question 2 Options.
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q2_text');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q2_type');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q2_choice1');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q2_choice2');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q2_choice3');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q2_choice4');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q2_choice5');
	
	// Pillar 7 Question 3 Options.
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q3_text');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q3_type');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q3_choice1');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q3_choice2');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q3_choice3');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q3_choice4');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q3_choice5');
	
	// Pillar 7 Question 4 Options.
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q4_text');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q4_type');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q4_choice1');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q4_choice2');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q4_choice3');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q4_choice4');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q4_choice5');
	
	// Pillar 7 Question 5 Options.
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q5_text');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q5_type');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q5_choice1');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q5_choice2');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q5_choice3');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q5_choice4');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q5_choice5');
	
	// Pillar 7 Question 6 Options.
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q6_text');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q6_type');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q6_choice1');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q6_choice2');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q6_choice3');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q6_choice4');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q6_choice5');

	// Pillar 7 Question 7 Options.
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q7_text');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q7_type');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q7_choice1');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q7_choice2');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q7_choice3');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q7_choice4');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q7_choice5');

	// Pillar 7 Question 8 Options.
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q8_text');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q8_type');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q8_choice1');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q8_choice2');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q8_choice3');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q8_choice4');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q8_choice5');

	// Pillar 7 Question 9 Options.
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q9_text');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q9_type');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q9_choice1');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q9_choice2');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q9_choice3');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q9_choice4');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q9_choice5');

	// Pillar 7 Question 10 Options.
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q10_text');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q10_type');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q10_choice1');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q10_choice2');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q10_choice3');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q10_choice4');
	register_setting('mc7p_p7_settings', 'mc7p_pillar7_q10_choice5');
	
}
add_action('admin_init', 'mc7p_options_init');
