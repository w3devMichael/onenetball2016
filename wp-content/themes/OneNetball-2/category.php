<?php get_header(); ?>

<section class="page-accent image-bottom">
    <div class="wrapper">
        <div class="left">
            <h1 class="page-accent-title"><?php single_cat_title(); ?></h1>
        </div>
        <div class="right">
            <img src="<?php bloginfo('template_directory'); ?>/images/temp/page-accent-image-bottom.jpg" alt="" width="" height=""/>
        </div>
    </div>
</section>

<section class="news-section news-inner">

    <h2 class="sr-only">NEWS</h2>

    <ul class="news-list wrapper">  

    	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 

    		$format = (get_post_format()) ? get_post_format() : "standard";

    		get_template_part('templates/format/' . $format );

    	?>

    	<?php endwhile; endif; ?>
    
    </ul>

</section>


<?php 
	get_template_part( 'templates/content', 'newsletter' );
?>

<?php get_footer(); ?>