<?php

if (!function_exists('file_require')) {

    /*
     * The function allows us to include deep directory PHP files if they exist in child theme path.
     * Otherwise it works just regularly include main theme files.
     */

    function file_require($file, $uri = false) {
        $file = str_replace("\\", "/", $file); // Replaces If the customer runs on Win machine. Otherway it doesn't perform
        if (is_child_theme()) {
            if (!$uri) {
                $dir = str_replace("\\", "/", get_template_directory());
                $replace = str_replace("\\", "/", get_stylesheet_directory());
                $file_exist = str_replace($dir, $replace, $file);
                $file = str_replace($replace, $dir, $file);
            } else {
                $dir = get_template_directory_uri();
                $replace = get_stylesheet_directory_uri();
                $file_exist = str_replace($dir, $replace, $file);

                $file_child_url = str_replace($dir, get_stylesheet_directory(), $file);
                if( file_exists($file_child_url) ){
                    return $file_exist;
                }
            }

            if( file_exists($file_exist) ){
                $file_child = str_replace($dir, $replace, $file);
                return $file_child;
            }
            return $file;

        } else {
            return $file;
        }
    }
}


function get_template_html( $template_name, $attributes = null ) {
    if ( ! $attributes ) {
      $attributes = array();
    }

    ob_start();
    do_action( 'personalize_login_before_' . $template_name );
    require( 'templates/' . $template_name . '.php');
    do_action( 'personalize_login_after_' . $template_name );

    $html = ob_get_contents();
    ob_end_clean();
    return $html;    
}

add_action( 'template_redirect', 'redirect_to_specific_page' );

function redirect_to_specific_page() {

if ( (is_page('inclusion-action-survey') || is_page('7-pillars-of-inclusion') || is_page('update-details'))  && ! is_user_logged_in() ) {

  $url = get_option( 'siteurl' );

  $login_page =  $url . '/login-page';

  wp_redirect( $login_page, 301 ); 

  exit;
  
  }


  if ( is_page('login-page') && is_user_logged_in() ) {

  $url = get_option( 'siteurl' );

  $login_page =  $url . '/inclusion-action-survey/';

  wp_redirect( $login_page, 301 ); 

  exit;
  
  }
}


/// IMAGE SIZES ////////////////
///
////////////////////////////////

add_image_size( 'news-article-thumb', 348, 270, true );




////////////////////////
// 7 Pillars includes 
/// /////////////////////







/////////////////////////////////////////////////////////////////////////////
//    CLASSES
/////////////////////////////////////////////////////////////////////////////

require_once file_require(get_template_directory() . '/framework/classes/CPT_columns.php');
require_once file_require(get_template_directory() . '/framework/classes/custom-post-type-archive-menu-links.php');


/////////////////////////////////////////////////////////////////////////////
//    FUNCTIONS
/////////////////////////////////////////////////////////////////////////////

require_once file_require(get_template_directory() . '/framework/functions/init.php');




/////////////////////////////////////////////////////////////////////////////
//    WIDGET AND PLUGINS
/////////////////////////////////////////////////////////////////////////////

require_once file_require(get_template_directory() . '/framework/widgets/init-widget.php');

require_once file_require(get_template_directory() . '/framework/functions/required-plugins.php');



///// functions for 7 pillars /////////////////////

require_once file_require(get_template_directory() . '/7-pillars-includes/functions.php');
require_once file_require(get_template_directory() . '/7-pillars-includes/progress-html.php');


/////////////////////////////////////////////////////////////////////////////
//    OPTION TREE
/////////////////////////////////////////////////////////////////////////////

add_filter( 'ot_theme_mode', '__return_true' );

add_filter( 'ot_show_pages', '__return_false' );

add_filter( 'ot_show_new_layout', '__return_false' );

add_filter( 'ot_theme_mode', '__return_true' );

require_once file_require(get_template_directory() . '/framework/option-tree/ot-loader.php' );

require_once file_require(get_template_directory() . '/framework/option-tree/theme-options.php' );

require_once file_require(get_template_directory() . '/framework/option-tree/metaboxes.php' );


add_action('admin_head', 'wpbuilder_admin_head', 2);
function wpbuilder_admin_head() {
    echo "<script type='text/javascript' src='" . get_template_directory_uri() . "/framework/assets/custom.js'></script>";
    echo "<link rel='stylesheet' id='css-wpbuilder-css'  href='" . get_template_directory_uri() . "/framework/assets/style.css' type='text/css' media='all' />";
}

function main_options_save_ajax() { 
    check_ajax_referer('_wpnonce', '_wpnonce' );

    $data = $_POST;
    unset($data['option_page'], $data['action'], $data['_wpnonce'], $data['_wp_http_referer']);

    if ( update_option('option_tree', $data ) ) {
        die(1);
    } else {
        die (0);
    }
}
add_action('wp_ajax_main_options_save', 'main_options_save_ajax' );


add_filter( 'ot_upload_text', 'theme_upload_text');

function theme_upload_text() {
    return 'Add/upload media to the website';
};

add_filter( 'ot_theme_options_page_title', 'options_page_title');
function options_page_title() {
    return 'Theme Options';
};

add_filter( 'ot_theme_options_menu_title', 'options_page_menu_title');
function options_page_menu_title() {
    return 'Theme Options';
};

/*  Import Export
/* ------------------------------------ */
add_action( 'init', 'register_options_pages' );
function register_options_pages() {

   if ( is_admin() && function_exists( 'ot_register_settings' ) ) {
    ot_register_settings( 
      array(
        array( 
          'id'              => 'import_export',
          'pages'           => array(
            array(
              'id'              => 'import_export',
              'parent_slug'     => 'themes.php',
              'page_title'      => '',
              'menu_title'      => 'Options Backup',
              'capability'      => 'edit_theme_options',
              'menu_slug'       => 'wpbuilder-theme-backup',
              'icon_url'        => null,
              'position'        => null,
              'updated_message' => 'Options updated.',
              'reset_message'   => 'Options reset.',
              'button_text'     => 'Save Changes',
              'show_buttons'    => false,
              'screen_icon'     => 'themes',
              'contextual_help' => null,
              'sections'        => array(
                array(
                  'id'          => 'wpbuilder_import_export',
                  'title'       => '<i class="layout_wpbuilder"></i>Import/Export',
                )
              ),
              'settings'        => array(
                array(
                    'id'          => 'import_data_text',
                    'label'       => 'Import Theme Options',
                    'desc'        => 'Theme Options',
                    'std'         => '',
                    'type'        => 'import-data',
                    'section'     => 'wpbuilder_import_export',
                    'rows'        => '',
                    'post_type'   => '',
                    'taxonomy'    => '',
                    'class'       => ''
                  ),
                  array(
                    'id'          => 'export_data_text',
                    'label'       => 'Export Theme Options',
                    'desc'        => 'Theme Options',
                    'std'         => '',
                    'type'        => 'export-data',
                    'section'     => 'wpbuilder_import_export',
                    'rows'        => '',
                    'post_type'   => '',
                    'taxonomy'    => '',
                    'class'       => ''
                  )
              )
            )
          )
        )
      )
    );
  }
}
if ( ! function_exists( 'ot_type_import_data' ) ) {
    function ot_type_import_data() {
        echo '<form method="post" id="import-data-form">';
        wp_nonce_field( 'import_data_form', 'import_data_nonce' );
        echo '<div class="format-setting type-textarea has-desc">';
        echo '<div class="description">';

        if ( OT_SHOW_SETTINGS_IMPORT ) 
            echo '<p>' . __( 'Only after you\'ve imported the Settings should you try and update your Theme Options.', 'option-tree' ) . '</p>';
            echo '<p>' . __( 'To import your Theme Options copy and paste what appears to be a random string of alpha numeric characters into this textarea and press the "Import Theme Options" button.', 'option-tree' ) . '</p>';
            echo '<button class="option-tree-ui-button blue right hug-right">' . __( 'Import Theme Options', 'option-tree' ) . '</button>';
            echo '</div>';
            echo '<div class="format-setting-inner">';
            echo '<textarea rows="10" cols="40" name="import_data" id="import_data" class="textarea"></textarea>';
            echo '</div>';
            echo '</div>';
            echo '</form>';
    }
}
if ( ! function_exists( 'ot_type_export_data' ) ) {
    function ot_type_export_data() {
        echo '<div class="format-setting type-textarea simple has-desc">';
        echo '<div class="description">';
        echo '<p>' . __( 'Export your Theme Options data > Save the file for importing into another install of WordPress later. Alternatively, you could just paste it into the <code>Appearance->Options Backup->Import</code> <strong>Theme Options</strong> textarea on another web site.', 'option-tree' ) . '</p>';
        echo '<input class="option-tree-ui-button blue right hug-right" value="Export as file" type="button" onclick="doDL(document.getElementById(\'export_data\').value)" />';
        echo '</div>';

        $data = get_option( 'option_tree' );
        $data = ! empty( $data ) ? ot_encode( serialize( $data ) ) : '';

        echo '<div class="format-setting-inner">';
        echo '<textarea rows="10" cols="40" name="export_data" id="export_data" class="textarea">' . $data . '</textarea>';
        echo '</div>';
        echo '</div>';
    ?>
        <script type="text/javascript">
            function saveAs(uri, filename) {
              var link = document.createElement('a');
              if (typeof link.download === 'string') {
                link.href = uri;
                link.download = filename;
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
              } else {
                window.open(uri);
              }
            }

            function doDL(s){
                var file = "data:x-application/text," + escape(s);
                saveAs(file, 'export-theme-options.txt');
            }
        </script>
    <?php
    }
}

/////////////////////////////////////////////////////////////////////////////
//    VISUAL COMPOSER
/////////////////////////////////////////////////////////////////////////////
require_once file_require(get_template_directory() . '/framework/visual-composer/vc_layouts.php');
require_once file_require(get_template_directory() . '/framework/visual-composer/vc_extend.php');




// custom post types for awards 




