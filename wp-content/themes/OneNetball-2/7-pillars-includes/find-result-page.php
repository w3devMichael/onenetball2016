<?php 
session_start();
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

global $wpdb;

$url = $_SERVER['HTTP_REFERER'];

if (isset($_GET['quiz_id']) && isset($_GET['current'])){

	
	// clear the session
	$_SESSION['mc7p_form'] = null;
	// get the quiz id reequest 
	$quiz_id = $_GET['quiz_id'];
	$current_step = $_GET['current'];

	$_SESSION['mc7p_form'] = $quiz_id;

	// get all 7 pillar pages
	$pages = get_pages(array(
		'meta_key' => '_wp_page_template',
		'meta_value' => 'page-7pillars.php'
	));

	// loop through all the pages with the template for page-7pilillars
	foreach ($pages as $page) {
			$step_numb = get_post_meta($page->ID, 'step_numb')[0];
			if ($step_numb == $current_step){
				$url = get_page_link($page->ID);
			}		
		}// end foreach
}

// Redirect to the next pillar or the score sheet.
header( 'Location: ' . $url . '?form=' . $_SESSION['mc7p_form'] );

?>