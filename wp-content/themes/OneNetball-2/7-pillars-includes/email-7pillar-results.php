<?php 

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

global $wpdb;

?>

<!DOCTYPE html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>One Netball</title>

  <style type="text/css">
        body {margin: 0; padding: 0; min-width: 100%!important;}
        .content {width: 100%; max-width: 600px;}  

        a:link    {color:#ffffff; background-color:transparent; text-decoration:none}
        a:visited {color:#ffffff; background-color:transparent; text-decoration:none}
        a:hover   {color:#15365A; background-color:transparent; text-decoration:none}

     .ReadMsgBody { width: 100%;}

     .ExternalClass {width: 100%;}

     /*gmail fix*/
     span.im{ color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;}

  </style>
</head>

<body bgcolor="#EC6666">
  <!-- outter table wrapper -->
  <table cellspacing="0" cellpadding="0" border="0" width="100%">
  	<tr>
  		<td bgcolor="#EC6666">

      <!-- inner table table -->
      <table width="600" border="0" cellpadding="0" align="center">
        <tbody>
          <tr>
            <td>
      
              <!-- date table -->
              <table width="600" border="0" cellpadding="10" bgcolor="D9D6D1">
                <tbody>
                  <tr>
                    <td>
                      <h1 style="font-family:Helvetica, Arial, sans-serif; font-size:10px; text-align:left"><strong><?php echo date('l d F Y'); ?></strong></h1>
                    </td>
                  </tr>
                </tbody>
              </table>

              <!-- header -->
              <table width="600" border="0" cellpadding="10" bgcolor="#FFFFFF">
                <tbody>
                  <tr>
                    <td>
                      <a href="<?php get_site_url('/'); ?>" target="_blank"><img style="border:0" src="<?php echo get_template_directory_uri(); ?>/images/APONNA_Logo_colour.png" width="300" height="36" alt="Logo"/></a>
                    </td>
                    <td align="right">
                      <a href="https://www.facebook.com/NetballAust/?fref=nf" target="_blank"><img style="border:0" src="<?php echo get_template_directory_uri(); ?>/images/facebook.png" width="30" alt="Facebook"/></a>
                    </td>
                    <td align="right">
                      <a href="https://twitter.com/NetballAust?lang=en" target="_blank"><img style="border:0" src="<?php echo get_template_directory_uri(); ?>/images/Twitter.png" width="30" alt="Twitter"/></a>
                    </td>
                    <td align="right">
                      <a href="https://www.instagram.com/netballaust/" target="_blank"><img style="border:0" src="<?php echo get_template_directory_uri(); ?>/images/Insta.png" width="30" alt="Twitter"/></a>
                    </td>
                  </tr>
                </tbody>
              </table>

              <!-- Nav -->
              <table width="600" border="0" bgcolor="#A0D6DB" cellpadding="8">
                <tbody>
                  <tr>
                    <td class="nav-links">
                      <a href="<?php echo get_site_url(); ?>/ambassadors/" target="_blank" style="font-family:Helvetica, Arial, sans-serif; font-size:10px; text-align:center; font-weight:bold; text-decoration:none; color:#ffffff;">MEET THE AMBASSADORS</a>
                    </td>
                    <td>
                      <a href="<?php echo get_site_url(); ?>/get-involved/" target="_blank" style="font-family:Helvetica, Arial, sans-serif; font-size:10px; text-align:center; font-weight:bold; text-decoration:none; color:#ffffff;">GET INVOLVED</a>
                    </td>
                    <td>
                      <a href="<?php echo get_site_url(); ?>/latest-news/" target="_blank" style="font-family:Helvetica, Arial, sans-serif; font-size:10px; text-align:center; font-weight:bold; text-decoration:none; color:#ffffff;">GET THE LATEST NEWS</a>
                    </td>
                    <td>
                      <a href="<?php echo get_site_url(); ?>/resources/" target="_blank" style="font-family:Helvetica, Arial, sans-serif; font-size:10px; text-align:center; font-weight:bold; text-decoration:none; color:#ffffff;">ACCESS RESOURCES</a>
                    </td>
                    <td>
                      <a href="<?php echo get_site_url(); ?>/get-help/" target="_blank" style="font-family:Helvetica, Arial, sans-serif; font-size:10px; text-align:center; font-weight:bold; text-decoration:none; color:#ffffff;">GET HELP</a>
                    </td>
                  </tr>
                </tbody>
              </table>
    
              <table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="D9D6D1">
                <tbody>
                  <tr><td height="25" colspan="3"></td></tr>

                  <tr>
                    <td width="20"></td>
                    <td valign="top" width="200px">
                      <h1 style="font-family:Helvetica, Arial, sans-serif; font-size:30px; color:#743C77; text-align:left; font-weight:bold; margin:0;"><?php echo $intro_paragraph['headline']; ?></h1>
                    </td>
                    <td width="20"></td>
                  </tr>

                  <tr>
                    <td width="20"></td>
                    <td>
                      <p style="font-family:helvetica, arial, sans-serif; font-size: 14px; color: #000000; text-align:left; line-height:23px;"><?php echo $intro_paragraph['first']; ?></p>
                    </td>
                    <td width="20"></td>
                  </tr>

                  <tr>
                    <td width="20"></td>
                    <td>
                      <h3 style="font-family:helvetica, arial, sans-serif; font-size: 18px; color: #74358b; text-align:left; line-height:30px; margin:0; margin-bottom: 15px;"><?php echo $intro_paragraph['second']; ?></h3>
                    </td>
                    <td width="20"></td>
                  </tr>

                  <tr>
                    <td width="20"></td>
                    <td>
                      <div>
                        <!--[if mso]>
                          <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="<?php echo get_site_url('/'); ?>" style="height:30px;v-text-anchor:middle;width:175px;" arcsize="14%" stroke="f" fillcolor="#EC6666">
                            <w:anchorlock/>
                            <center>
                          <![endif]-->
                              <a href="<?php echo get_site_url('/'); ?>" target="_blank" style="background-color:#EC6666;border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:30px;text-align:center;text-decoration:none;width:175px;-webkit-text-size-adjust:none;"><?php echo $intro_paragraph['call_to_action']; ?></a>
                          <!--[if mso]>
                            </center>
                          </v:roundrect>
                        <![endif]-->
                      </div>
                    </td>
                    <td width="20"></td>
                  </tr>

                  <tr><td height="25" colspan="3"></td></tr>

                </tbody>
              </table>
      
              <!-- white background / results -->
              <table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                <tbody>

                  <tr><td height="20" colspan="5"></td></tr>

                  <!--result -->
                  <?php $answer1 = getResults($result['pillar1_score']);  ?>
                  <tr>
                    <td width="20"></td>
                    <td valign="top" width="94">
                      <img src="<?php echo get_template_directory_uri(). '/images/' . $answer1['email-icon']; ?>">
                    </td>
                    <td width="20"></td>
                    <td width="446">

                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;">
                          
                            <h2 style="font-family: helvetica, arial, sans-serif; font-size:18px; color:#74358b; text-transform:uppercase; margin:0;">Pillar 1: Access</h2>
                            <p style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;"><?php echo $answer1['description']; ?></p>
                          </td>
                        </tr>
                      </table> 
                    
                    </td>
                    <td width="20"></td>
                  </tr>

                  <tr><td height="30" colspan="5"></td></tr>

                  <!--result -->
                  <?php $answer2 = getResults($result['pillar2_score']);  ?>
                  <tr>
                    <td width="20"></td>
                    <td valign="top" width="94">
                      <img src="<?php echo get_template_directory_uri(). '/images/' . $answer2['email-icon']; ?>">
                    </td>
                    <td width="20"></td>
                    <td width="446">

                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;">
                          

                            <h2 style="font-family: helvetica, arial, sans-serif; font-size:18px; color:#74358b; text-transform:uppercase; margin:0;">Pillar 2: Attitude</h2>
                            <p style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;"><?php echo $answer2['description']; ?></p>
                          </td>
                        </tr>
                      </table> 
                    
                    </td>
                    <td width="20"></td>
                  </tr>

                  <tr><td height="30" colspan="5"></td></tr>

                  <!--result -->
                  <?php $answer3 = getResults($result['pillar3_score']);  ?>
                  <tr>
                    <td width="20"></td>
                    <td valign="top" width="94">
                      <img src="<?php echo get_template_directory_uri(). '/images/' . $answer3['email-icon']; ?>">
                    </td>
                    <td width="20"></td>
                    <td width="446" valign="top">

                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;">
                          
                            <h2 style="font-family: helvetica, arial, sans-serif; font-size:18px; color:#74358b; text-transform:uppercase; margin:0;">Pillar 3: Choice</h2>
                            <p style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;"><?php echo $answer3['description']; ?></p>
                          </td>
                        </tr>
                      </table> 
                    
                    </td>
                    <td width="20"></td>
                  </tr>

                  <tr><td height="30" colspan="5"></td></tr>

                  <!--result -->
                   <?php $answer4 = getResults($result['pillar4_score']);  ?>
                  <tr>
                    <td width="20"></td>
                    <td valign="top" width="94">
                      <img src="<?php echo get_template_directory_uri(). '/images/' . $answer4['email-icon']; ?>">
                    </td>
                    <td width="20"></td>
                    <td width="446">

                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;">
                         
                            <h2 style="font-family: helvetica, arial, sans-serif; font-size:18px; color:#74358b; text-transform:uppercase; margin:0;">Pillar 4: Partnerships</h2>
                            <p style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;"><?php echo $answer4['description']; ?></p>
                          </td>
                        </tr>
                      </table> 
                    
                    </td>
                    <td width="20"></td>
                  </tr>

                  <tr><td height="30" colspan="5"></td></tr>

                  <!--result -->
                   <?php $answer5 = getResults($result['pillar5_score']);  ?>
                  <tr>
                    <td width="20"></td>
                    <td valign="top" width="94">
                      <img src="<?php echo get_template_directory_uri(). '/images/' . $answer5['email-icon']; ?>">
                    </td>
                    <td width="20"></td>
                    <td width="446">

                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;">                         
                            <h2 style="font-family: helvetica, arial, sans-serif; font-size:18px; color:#74358b; text-transform:uppercase; margin:0;">Pillar 5: Communication</h2>
                            <p style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;"><?php echo $answer5['description']; ?></p>
                          </td>
                        </tr>
                      </table> 
                    
                    </td>
                    <td width="20"></td>
                  </tr>

                  <tr><td height="30" colspan="5"></td></tr>

                   <!--result -->
                   <?php $answer6 = getResults($result['pillar6_score']);  ?>
                  <tr>
                    <td width="20"></td>
                    <td valign="top" width="94">
                      <img src="<?php echo get_template_directory_uri(). '/images/' . $answer6['email-icon']; ?>">
                    </td>
                    <td width="20"></td>
                    <td width="446">

                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;">
                          
                            <h2 style="font-family: helvetica, arial, sans-serif; font-size:18px; color:#74358b; text-transform:uppercase; margin:0;">Pillar 6: Policy</h2>
                            <p style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;"><?php echo $answer6['description']; ?></p>
                          </td>
                        </tr>
                      </table> 
                    
                    </td>
                    <td width="20"></td>
                  </tr>

                  <tr><td height="30" colspan="5"></td></tr>

                  <!--result -->
                  <?php $answer7 = getResults($result['pillar7_score']);  ?>
                  <tr>
                    <td width="20"></td>
                    <td valign="top" width="94">
                      <img src="<?php echo get_template_directory_uri(). '/images/' . $answer7['email-icon']; ?>">
                    </td>
                    <td width="20"></td>
                    <td width="446">

                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;">
                          
                            <h2 style="font-family: helvetica, arial, sans-serif; font-size:18px; color:#74358b; text-transform:uppercase; margin:0;">Pillar 7: Opportunities</h2>
                            <p style="color:#000000; font-family: helvetica, arial, sans-serif; line-height:24px; font-size: 14px;"><?php echo $answer7['description']; ?></p>
                          </td>
                        </tr>
                      </table> 
                    
                    </td>
                    <td width="20"></td>
                  </tr>

                  <tr><td height="30" colspan="5"></td></tr>

                </tbody>
              </table>

              <!-- footer -->
              <table width="600px" border="0" cellpadding="20" bgcolor="#010101">
                <tbody>
                  <tr>
                    <td valign="top" width="25%"><p><h3 style="font-family:Helvetica, Arial, sans-serif;; color:#EC6666; font-size:9px; text-align:left"><strong>NETBALL IN YOUR STATE</strong></h3></p>
                    <p><h4 style="font-family:Helvetica; font-size:8px; color:#ffffff; text-align:left">
                    <a href="http://act.netball.com.au/" target="_blank" style="color:#ffffff; text-decoration:none;"><strong>Australian Capital Territory</strong></a><br><br>
                    <a href="http://nsw.netball.com.au/" target="_blank" style="color:#ffffff; text-decoration:none;"><strong>New South Wales</strong></a><br><br>
                    <a href="http://nt.netball.com.au/" target="_blank" style="color:#ffffff; text-decoration:none;"><strong>Northern Territory</strong></a><br><br>
                    <a href="http://qld.netball.com.au/" target="_blank" style="color:#ffffff; text-decoration:none;"><strong>Queensland</strong></a><br><br>
                    <a href="http://sa.netball.com.au/" target="_blank" style="color:#ffffff; text-decoration:none;"><strong>South Australia</strong></a><br><br>
                    <a href="http://tas.netball.com.au/" target="_blank" style="color:#ffffff; text-decoration:none;"><strong>Tasmaniav</strong></a><br><br>
                    <a href="http://vic.netball.com.au/" target="_blank" style="color:#ffffff; text-decoration:none;"><strong>Victoria</strong></a><br><br>
                    <a href="http://wa.netball.com.au/" target="_blank" style="color:#ffffff; text-decoration:none;"><strong>Western Australia</strong></a></h4></p>
                          </td>
                   <td valign="top" width="25%"><p><h3 style="font-family:Helvetica, Arial, sans-serif; color:#EC6666; font-size:9px; text-align:left"><strong>CONNECT WITH US</h3></p>
                    <p><h4 style="font-family:Helvetica; font-size:8px; color:#ffffff; text-align:left"><a href="https://twitter.com/NetballAust?lang=en" target="_blank" style="color:#ffffff; text-decoration:none;"><strong>#onenetball</strong></a></h4></p>
                    <p><h4 style="font-family:Helvetica; font-size:8px; color:#ffffff; text-align:left"><a href="https://www.facebook.com/NetballAust/" target="_blank" style="color:#ffffff; text-decoration:none;"><strong>/netballaustralia</strong></a></h4></p>
                    </td>
                    <td valign="top" width="25%"><p><h3 style="font-family:Helvetica, Arial, sans-serif;; font-size:9px; color:#EC6666; text-align:left"><strong>SUPPORTED BY</strong></h3></p>
                    <a href="http://auspost.com.au/" target="_blank"><img style="border:0" src="<?php echo get_template_directory_uri(); ?>/images/Auspost-logo.png" width="100" height="35" alt="AusPostLogo"/></a><h4 style="font-family:Helvetica; font-size:8px; line-height:1.5em; color:#ffffff; text-align:left">Australia Post are the National Diversity and Community Inclusion  Partner of Netball Australia</h4></td>
              <td valign="top" width="25%"><p><h3 style="font-family:Helvetica, Arial, sans-serif;; font-size:9px; color:#EC6666; text-align:left"><strong>ACKNOWLEDGMENT</strong></h3></p><p><h4 style="font-family:Helvetica; font-size:8px; line-height:1.5em; color:#ffffff; text-align:left"><strong>Netball Australia acknowledges and pays respect to the Traditional Owners of country across Australia, to their culture and to their Elders, past, present and future.</strong></h4></p>
                    <br>
                    <h3 style="font-family:Helvetica, Arial, sans-serif; font-size:9px; color:#EC6666; text-align:left;"> <a href="http://netball.com.au/about-netball-australia/governance/privacy-statement/" style="color:#ffffff; text-decoration:none;"><strong>PRIVACY</strong></a></h3>
                    </td>
                  </tr>
                </tbody>
              </table>

        </td>
        </tr>
      </tbody>
    </table>

  </td>
  	</tr>
  </table>
</body>