<?php 


add_shortcode('past_results_shortcode', function (){

    $status = null;
    $pillars = get_completed_pillars();

    $results_for_user = getResult($pillars['results']);

    include(locate_template('/7-pillars-includes/results-html.php'));

});

function get_progress($pillar_number){

    $complete_pillars = get_completed_pillars();
    $number_of_pillars = count($complete_pillars);


    $html = '<h3 class="progress" data-pillar-quiz-id="pillar-'.$pillar_number.'-quiz">Progress <span class="current">'. $pillar_number .'</span><span class="total">/'.$number_of_pillars.'</span></h3><ul>';
    $index = 1;
    foreach ($complete_pillars as $key => $value) {

        $completed = '';

        if ( $value > -1 ){

            $completed = 'completed';
        }

        $html .= '<li class="' . ($index == $pillar_number ? 'current ': '') .$completed.' pilar-'.$index.'"><a href="'. get_site_url(null, '/quiz/'. $index, null ) . '">pilar-'.$index.'</a></li>';

        $index++;

    }
    $html .= '</ul>';

    echo '<div class="progress-wrapper">';
    echo $html;
    echo '</div>';
}


// get results table and data for user who is logged in.
function getResult($resultsArray)
{
    $results = [];
    foreach ($resultsArray as $row) {
        $count = 1;
        $pillars = [];
        $total_score = 0;
        $completed = null;
        foreach ($row as $key => $r) {

            // get the quiz_id
            if ($key == 'id') {
                $quiz_id = $r;
            }
            // get the date
            if ($key == 'date_filled') {
                $d1 = new DateTime($r);
                $date = $d1->format('d F Y') . '<br><br>';
            }

            $pillarNo = 'pillar' . $count . '_score';
            if ($key == $pillarNo) {

                if ($r == null && $completed !== false) {
                    $completed = false;
                    $pillars['current'] = $count;
                   
                }
                $pillars[$pillarNo] = $r;
                $total_score = $total_score + $r;
                $count++;
            }

            if ($completed !== false && $count > 6) {
                $completed = true;
            }

        };


        $division = 3;
        $pillars['quiz_id'] = $quiz_id;
        $pillars['date'] = $date;
        $pillars['total_score'] = $total_score;
        $pillars['completed'] = $completed;
        $pillars['total_score_out_of_70'] = ($completed) ? round($total_score / $division) : 0;

        array_push($results, $pillars);
    }

    return $results;

    }


    // univerdsal function to get something that starts with
    function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }



    // get roles of current user
    function getRoles($current_user){

        $rolesArray = [];
        if ( !empty( $current_user->roles ) && is_array( $current_user->roles ) ) {
            foreach ( $current_user->roles as $role )
                 array_push( $rolesArray, $role );
        }

        return $rolesArray;
    }




    // show correct icon  and description depending on score
    function getResults($score){



        if ($score ==  null ){
            $result =  ["description" => "Incomplete",
                'title'=>'Not Complete',
                "icon"=> 'can-improve-icon.svg',
                "email-icon" => 'icon-fail.png',
                "className"=> 'null'];


        } elseif ( $score > 20 ){
            $result =  ["description" => "<p>You’re leading the way with your actions to create a more inclusive welcoming netball environment at your Club or Association. The actions you are taking are making a difference!</p><p>Make sure you share your success with us by tagging #onenetball on social media or emailing us with your updates at <a style='color:#f16261; text-decoration:underline;'  href='mailto://onenetball@netball.asn.au'>onenetball@netball.asn.au</a></p>",
                "title"=>'Fantastic Effort!',
                "icon"=> 'pass-icon.svg',
                "email-icon" => 'icon-pass.png',
                "className"=> 'pass',
                "statusClass" => 'passed' ];

        }  elseif ( $score <=10 ){

            $result =  ["description" => "<p>You haven’t started on your inclusion journey just yet, but you have come to the right place. </p><p>Get started by downloading the <a style='color:#f16261; text-decoration:underline;' title='Inclusion Action Plan' target='_blank' href='". get_site_url(). "/wp-content/uploads/2016/04/Inclusion-Action-Plan-FINAL-290316.pdf" ."'>Inclusion Action Plan</a> to start your path to inclusion success!</p>",
                "title"=>'Needs Work',
                "icon"=> 'needs-work-icon.svg',
                "email-icon" => 'icon-fail.png',
                "className"=> 'fail',
                "statusClass" => 'failed' ];


        }  else {
            $result =  ["description" => "Terrific! You’re on the path to inclusion success! Head to the <a style='color:#f16261; text-decoration:underline;' href='". get_site_url(). "/resources/" ."'>Resource Library</a> for more ideas of what you can do at your Club or Association. Keep up the great work!",
                'title'=>'Great Start!',
                "icon"=> 'can-improve-icon.svg',
                "email-icon" => 'icon-average.png',
                "className"=> 'average',
                "statusClass" => 'can-improve'];

        }

        return $result;
    }


/* get the completed pillars */
function get_completed_pillars() {

error_reporting(E_ALL);
ini_set("display_errors", 1);

 // Load the WordPress environment functions.
define( '__ROOT__', $_SERVER['DOCUMENT_ROOT'] );
require_once( __ROOT__ . '/wp-load.php');

global $wpdb;

$all_results = [];


// Prepare the data we will use.
$current_user= wp_get_current_user();
$user_info = get_user_meta( $current_user->ID);
$table_name = $wpdb->prefix . "mc7p";
$user_id = $current_user->ID;
$all_results['user_id'] = $current_user->ID;
$all_results['current_user'] = $current_user;

/*
    Check if user is in the db so they can jump b/n steps 
*/
$user_in_system = false;

$all_results['results'] = $wpdb->get_results( 'SELECT *  FROM ' . $table_name . ' WHERE user_id = ' . $user_id, ARRAY_A);

return $all_results;
    
}
// 


add_filter ("wp_mail_content_type", "onenetball_mail_content_type");
function onenetball_mail_content_type() {
	return "text/html";
}
	
add_filter ("wp_mail_from", "onenetball_mail_from");
function onenetball_mail_from() {
	return "onenetbal@netball.asn.au";
}
	
add_filter ("wp_mail_from_name", "onenetball_mail_email_from_name");
function onenetball_mail_email_from_name() {
	return "Onenetball";
}


function send_email_results($quiz_id){

	$pillars = get_completed_pillars();
    $hasResults = false;
    $email = 'support@workingthree.com';
    $subject = 'there was an error sending the email response, no quiz id ' . $quiz_id;

    // var_dump($pillars['current_user']->user_email);
    // var_dump($quiz_id);

	$result = null;

    if (count($pillars['results'])) {

    	foreach ($pillars['results'] as $r) {
    		if ($r['id'] == $quiz_id){
    				$result = $r;

                    $hasResults = true;

    				break;
    		}
    	}

    	$email = $pillars['current_user']->user_email;
    	$subject = "Your results for the Inclusion Action Survey on " . date('d-m-Y') ;
    	$intro_paragraph = [

            'headline' => 'WELL DONE!',
    		'first' => 'You have completed the Inclusion Action Survey! <br> Check back in regularly with the Survey as your Club or Association completes more inclusion actions and initiatives in your Plan. Track your improvements and watch your score climb on your Results Page! You can do the Survey as many times as you like.' ,
    		'second' => 'All the resources you need to get started are at your fingertips!',
            'call_to_action' => 'GO TO THE WEBSITE!'

    	];


	   ob_start(); 

        // here is the chaning part of the email
	   include('email-7pillar-results.php');

	   $message = ob_get_contents();
	   ob_end_clean();

    } else { 

        $message = '<p>no data found</p>';

    }

    if ($email === '' || $email === null){
        $email = 'support@workingthree.com';
        $subject = 'there was an error sending the email response. ';
    } 

    if ($hasResults){
        wp_mail($email, $subject, $message);
    }

    
	// var_dump($pillars);
}


?>