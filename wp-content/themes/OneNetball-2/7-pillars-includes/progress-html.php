<?php 

/* Progress bar*/
function get_progress_bar($status){

?>

<ol class='progress-indicator'>

    <?php
if ($status === null){
    $status = -1;
    
}
switch ($status) {
        case 1:
        ?>
                <li class="current">
                    <span>1</span>
                    <strong>Access</strong>
                </li>
                <li class="">
                    <span>2</span>
                    <strong>Attitude</strong>
                </li>
                <li class="">
                    <span>3</span>
                    <strong>Choice</strong>
                </li>
                <li class="">
                    <span>4</span>
                    <strong>Partnerships</strong>
                </li>
                <li class="">
                    <span>5</span>
                    <strong>Communication</strong>
                </li>
                <li class="">
                    <span>6</span>
                    <strong>Policy</strong>
                </li>
                <li class="">
                    <span>7</span>
                    <strong>Opportunities</strong>
                </li> 
        <?php 
        break;
        case 2:
        ?>
                <li class="passed">
                    <span>1</span>
                    <strong>Access</strong>
                </li>
                <li class="current">
                    <span>2</span>
                    <strong>Attitude</strong>
                </li>
                <li class="">
                    <span>3</span>
                    <strong>Choice</strong>
                </li>
                <li class="">
                    <span>4</span>
                    <strong>Partnerships</strong>
                </li>
                <li class="">
                    <span>5</span>
                    <strong>Communication</strong>
                </li>
                <li class="">
                    <span>6</span>
                    <strong>Policy</strong>
                </li>
                <li class="">
                    <span>7</span>
                    <strong>Opportunities</strong>
                </li> 
        <?php 
        break;
        case 3:
        ?>
                <li class="passed">
                    <span>1</span>
                    <strong>Access</strong>
                </li>
                <li class="passed">
                    <span>2</span>
                    <strong>Attitude</strong>
                </li>
                <li class="current">
                    <span>3</span>
                    <strong>Choice</strong>
                </li>
                <li class="">
                    <span>4</span>
                    <strong>Partnerships</strong>
                </li>
                <li class="">
                    <span>5</span>
                    <strong>Communication</strong>
                </li>
                <li class="">
                    <span>6</span>
                    <strong>Policy</strong>
                </li>
                <li class="">
                    <span>7</span>
                    <strong>Opportunities</strong>
                </li> 
        <?php 
        break;
        case 4:
        ?>
                <li class="passed">
                    <span>1</span>
                    <strong>Access</strong>
                </li>
                <li class="passed">
                    <span>2</span>
                    <strong>Attitude</strong>
                </li>
                <li class="passed">
                    <span>3</span>
                    <strong>Choice</strong>
                </li>
                <li class="current">
                    <span>4</span>
                    <strong>Partnerships</strong>
                </li>
                <li class="">
                    <span>5</span>
                    <strong>Communication</strong>
                </li>
                <li class="">
                    <span>6</span>
                    <strong>Policy</strong>
                </li>
                <li class="">
                    <span>7</span>
                    <strong>Opportunities</strong>
                </li> 
        <?php 
        break;
        case 5:
        ?>
                <li class="passed">
                    <span>1</span>
                    <strong>Access</strong>
                </li>
                <li class="passed">
                    <span>2</span>
                    <strong>Attitude</strong>
                </li>
                <li class="passed">
                    <span>3</span>
                    <strong>Choice</strong>
                </li>
                <li class="passed">
                    <span>4</span>
                    <strong>Partnerships</strong>
                </li>
                <li class="current">
                    <span>5</span>
                    <strong>Communication</strong>
                </li>
                <li class="">
                    <span>6</span>
                    <strong>Policy</strong>
                </li>
                <li class="">
                    <span>7</span>
                    <strong>Opportunities</strong>
                </li> 
        <?php 
        break;
        case 6:
        ?>
                <li class="passed">
                    <span>1</span>
                    <strong>Access</strong>
                </li>
                <li class="passed">
                    <span>2</span>
                    <strong>Attitude</strong>
                </li>
                <li class="passed">
                    <span>3</span>
                    <strong>Choice</strong>
                </li>
                <li class="passed">
                    <span>4</span>
                    <strong>Partnerships</strong>
                </li>
                <li class="passed">
                    <span>5</span>
                    <strong>Communication</strong>
                </li>
                <li class="current">
                    <span>6</span>
                    <strong>Policy</strong>
                </li>
                <li class="">
                    <span>7</span>
                    <strong>Opportunities</strong>
                </li> 
        <?php 
        break;
                case 7:
                ?>
                <li class="passed">
                    <span>1</span>
                    <strong>Access</strong>
                </li>
                <li class="passed">
                    <span>2</span>
                    <strong>Attitude</strong>
                </li>
                <li class="passed">
                    <span>3</span>
                    <strong>Choice</strong>
                </li>
                <li class="passed">
                    <span>4</span>
                    <strong>Partnerships</strong>
                </li>
                <li class="passed">
                    <span>5</span>
                    <strong>Communication</strong>
                </li>
                <li class="passed">
                    <span>6</span>
                    <strong>Policy</strong>
                </li>
                <li class="current">
                    <span>7</span>
                    <strong>Opportunities</strong>
                </li> 
                <?php 
                break;
        default:
        ?>
                <li class="">
                    <span>1</span>
                    <strong>Access</strong>
                </li>
                <li class="">
                    <span>2</span>
                    <strong>Attitude</strong>
                </li>
                <li class="">
                    <span>3</span>
                    <strong>Choice</strong>
                </li>
                <li class="">
                    <span>4</span>
                    <strong>Partnerships</strong>
                </li>
                <li class="">
                    <span>5</span>
                    <strong>Communication</strong>
                </li>
                <li class="">
                    <span>6</span>
                    <strong>Policy</strong>
                </li>
                <li class="">
                    <span>7</span>
                    <strong>Opportunities</strong>
                </li> 
        <?php 
        break;
      } ?>
</ol>
      <?php 

}
?>