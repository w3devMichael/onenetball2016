<?php 


add_shortcode('past_results_shortcode', function (){

    $status = null;
    $results = get_completed_pillars();

    //get_progress_bar($status);

    $results_for_user = getResult($results['results']);

    include(locate_template('/7-pillars-includes/results-html.php'));




});

function get_progress($pillar_number){

    $complete_pillars = get_completed_pillars();
    $number_of_pillars = count($complete_pillars);


//    if ($number_of_pillars < 1 ){
//
//        $complete_pillars = array( "pillar1_score" => 'NULL',
//            "pillar2_score" =>'NULL', "pillar3_score"=>'NULL',
//            "pillar4_score"=>'NULL' ,"pillar5_score"=>'NULL',
//            "pillar6_score"=>'NULL', "pillar7_score"=>'NULL');
//        $number_of_pillars = count($complete_pillars);
//
//    }


    $html = '<h3 class="progress" data-pillar-quiz-id="pillar-'.$pillar_number.'-quiz">Progress <span class="current">'. $pillar_number .'</span><span class="total">/'.$number_of_pillars.'</span></h3><ul>';
    $index = 1;
    foreach ($complete_pillars as $key => $value) {

        $completed = '';

        if ( $value > -1 ){

            $completed = 'completed';
        }

        $html .= '<li class="' . ($index == $pillar_number ? 'current ': '') .$completed.' pilar-'.$index.'"><a href="'. get_site_url(null, '/quiz/'. $index, null ) . '">pilar-'.$index.'</a></li>';

        $index++;

    }
    $html .= '</ul>';

    echo '<div class="progress-wrapper">';
    echo $html;
    echo '</div>';
}



function getResult($resultsArray)
{
    $results = [];
    foreach ($resultsArray as $row) {
        $count = 1;
        $pillars = [];
        $total_score = 0;
        $completed = null;
        foreach ($row as $key => $r) {

            // get the quiz_id
            if ($key == 'id') {
                $quiz_id = $r;
            }
            // get the date
            if ($key == 'date_filled') {
                $d1 = new DateTime($r);
                $date = $d1->format('d F Y') . '<br><br>';
            }

            $pillarNo = 'pillar' . $count . '_score';
            if ($key == $pillarNo) {

                if ($r == null && $completed !== false) {
                    $completed = false;
                    $pillars['current'] = $count;
                   
                }
                $pillars[$pillarNo] = $r;
                $total_score = $total_score + $r;
                $count++;
            }

            if ($completed !== false && $count > 6) {
                $completed = true;
            }

        };


        $division = 3;
        $pillars['quiz_id'] = $quiz_id;
        $pillars['date'] = $date;
        $pillars['total_score'] = $total_score;
        $pillars['completed'] = $completed;
        $pillars['total_score_out_of_70'] = ($completed) ? round($total_score / $division) : 0;

        array_push($results, $pillars);
    }

    return $results;

    }

    function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }







    function getResults($score){



        if ($score ==  null ){
            $result =  ["description" => "incomplete",
                'title'=>'Not Complete',
                "icon"=> 'can-improve-icon.svg',
                "className"=> 'null'];


        } elseif ( $score > 20 ){
            $result =  ["description" => "<p>You’re leading the way with your actions to create a more inclusive welcoming netball environment at your Club or Association. The actions you are taking are making a difference!</p><p>Make sure you share your success with us by tagging #onenetball on social media or emailing us with your updates at <a style='color:#f16261; text-decoration:underline;'  href='mailto://onenetball@netball.asn.au'>onenetball@netball.asn.au</a></p>",
                "title"=>'Fantastic Effort!',
                "icon"=> 'pass-icon.svg',
                "className"=> 'pass'];

        }  elseif ( $score <=10 ){

            $result =  ["description" => "<p>You haven’t started on your inclusion journey just yet, but you have come to the right place. </p><p>Get started by downloading the <a style='color:#f16261; text-decoration:underline;' title='Inclusion Action Plan' target='_blank' href='". get_site_url(). "/wp-content/uploads/2016/04/Inclusion-Action-Plan-FINAL-290316.pdf" ."'>Inclusion Action Plan</a> to start your path to inclusion success!</p>",
                "title"=>'Needs Work',
                "icon"=> 'needs-work-icon.svg',
                "className"=> 'fail'];


        }  else {
            $result =  ["description" => "Terrific! You’re on the path to inclusion success! Head to the <a style='color:#f16261; text-decoration:underline;' href='". get_site_url(). "/resources/" ."'>Resource Library</a> for more ideas of what you can do at your Club or Association. Keep up the great work!",
                'title'=>'Great Start!',
                "icon"=> 'can-improve-icon.svg',
                "className"=> 'average'];

        }

        return $result;
    }


/* get the completed pillars */
function get_completed_pillars() {

error_reporting(E_ALL);
ini_set("display_errors", 1);

 // Load the WordPress environment functions.
define( '__ROOT__', $_SERVER['DOCUMENT_ROOT'] );
require_once( __ROOT__ . '/wp-load.php');

global $wpdb;

$all_results = [];


// Prepare the data we will use.
$user_id = get_current_user_id();
$user_info = get_user_meta( $user_id );
$table_name = $wpdb->prefix . "mc7p";
$all_results['user_id'] = $user_id;

/*
    Check if user is in the db so they can jump b/n steps 
*/
$user_in_system = false;

$all_results['results'] = $wpdb->get_results( 'SELECT *  FROM ' . $table_name . ' WHERE user_id = ' . $user_id, ARRAY_A);

return $all_results;
    
}
// 


add_filter ("wp_mail_content_type", "onenetball_mail_content_type");
function onenetball_mail_content_type() {
	return "text/html";
}
	
add_filter ("wp_mail_from", "onenetball_mail_from");
function onenetball_mail_from() {
	return "onenetbal@netball.asn.au";
}
	
add_filter ("wp_mail_from_name", "onenetball_mail_email_from_name");
function onenetball_mail_email_from_name() {
	return "Onenetball";
}


function send_email_results($quiz_id){

	$results = get_completed_pillars();

	$result = null;

	foreach ($results['results'] as $r) {
		if ($r['id'] == $quiz_id){
				$result = $r;

				break;
		}
	}

	$email = "mikoop@mac.com";
	$subject = "love ya!";

	$intro_paragraph = [

		'first' => 'First line of text paragraph.' ,
		'second' => 'Second line of text paragraph.'

	];


	ob_start(); 

	include('email-7pillar-results.php');

	?>
	

	<?php 


	$message = ob_get_contents();
	ob_end_clean();


	wp_mail($email, $subject, $message);
    
}


?>