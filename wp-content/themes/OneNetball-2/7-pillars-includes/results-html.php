<?php

// var_dump($results_for_user);
global $wp_query;
$page = $wp_query->query_vars['page'];

$page = ($page > 1) ? $page : 1;
$number_of_scores = count($results_for_user);
$pages =  ceil($number_of_scores/16);

if ($number_of_scores):
?>
<section class="result-table-wrapper">

    <header class="result-header">
        <ul class="result-header-inner row-list">

            <li class="result-steps">
                <div class="result-step-number">1</div>
                <span class="result-step-name">Access</span>
            </li>

            <li class="result-steps">
                <div class="result-step-number">2</div>
                <span class="result-step-name">Attitude</span>
            </li>

            <li class="result-steps">
                <div class="result-step-number">3</div>
                <span class="result-step-name">Choice</span>
            </li>

            <li class="result-steps">
                <div class="result-step-number">4</div>
                <span class="result-step-name">Partnerships</span>
            </li>

            <li class="result-steps">
                <div class="result-step-number">5</div>
                <span class="result-step-name">Communication</span>
            </li>

            <li class="result-steps">
                <div class="result-step-number">6</div>
                <span class="result-step-name">Policy</span>
            </li>

            <li class="result-steps">
                <div class="result-step-number">7</div>
                <span class="result-step-name">Opportunities</span>
            </li>

        </ul>
    </header>

    <main class="result-rows">
        <?php foreach ($results_for_user as $result): ?>
        <div class="result-row">
            <div class="col-1 result-date"><?php echo $result['date']; ?></div>
            <ul class="results row-list">
                <li class="access <?php $answer = getResults($result['pillar1_score']); echo $answer['className']; ?>"></li>
                <li class="attitude <?php $answer = getResults($result['pillar2_score']); echo $answer['className']; ?>"></li>
                <li class="choice <?php $answer = getResults($result['pillar3_score']); echo $answer['className']; ?>"></li>
                <li class="partnerships <?php $answer = getResults($result['pillar4_score']); echo $answer['className']; ?>"></li>
                <li class="communication <?php $answer = getResults($result['pillar5_score']); echo $answer['className']; ?>"></li>
                <li class="policy <?php $answer = getResults($result['pillar6_score']); echo $answer['className']; ?>"></li>
                <li class="opportunities <?php $answer = getResults($result['pillar7_score']); echo $answer['className']; ?>"></li>
            </ul>
            <div class="col-last result-score">
                <?php if (!$result['completed']): ?>
                <a class="site-btn btn-score" href="<?php echo home_url('wp-content/themes/OneNetball-2/7-pillars-includes/find-result-page.php?current='. $result['current'] .'&quiz_id='. $result['quiz_id'] .'') ?>">Complete Survey</a>
                <?php else: ?>
                <h3><?php echo $result['total_score_out_of_70']; ?>/70</h3>
                <?php endif; ?>
            </div>
        </div>
        <?php endforeach; ?>
    </main>

    <footer class="result-footer">
        <?php else : ?>
        <div class="result-incomplete">
            <h2>You haven't completed the survey yet. <br />
                Fill in those blanks &amp; take the <a href="<?php echo home_url('inclusion-action-survey'); ?>">inclusion action survey.</a></h2>
            <a class="site-btn purple" href="<?php echo home_url('inclusion-action-survey'); ?>">Start your survey!</a>
        </div>
       <?php endif; ?>

        <?php if ( $pages > 1 ):   ?>
            <div class="result-pagination">
                <?php if ( $page > 1):   ?>
                    <a href="<?php echo home_url('past-results/?page=' . ( $page-1 )  ) ?>" class="pag pag-prev"><span class="arrow"></span> View more recent results</a>
                <?php endif; ?>
                <?php if ( $page < $pages ):  ?>
                    <a href="<?php echo home_url('past-results/?page=' . ( $page+1 )  ) ?>" class="pag pag-next">View older results <span class="arrow"></span></a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </footer>
</section>