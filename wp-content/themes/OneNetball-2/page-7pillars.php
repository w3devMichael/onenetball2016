<?php
/**
 * Template Name: 7 Pillar Page
 *
 * @package wpbuilder
 * @subpackage wpbuilder
 * @since wpbuilder
 */

get_header();

if (have_posts()) : while (have_posts()) : the_post(); 

    $post_id = get_the_ID();
    $title = get_the_title();
    $read_more = get_permalink();
    $step_numb = get_post_meta($post_id, 'step_numb')[0]; 
    $status = $step_numb;
    

    $isPillar = ($status !== null) ? true : false;
    if ($isPillar){

        $title = get_post_meta($post_id, 'title')[0];
        $subtitle = get_post_meta($post_id, 'subtitle')[0];     
        $shortcode = get_post_meta($post_id, 'shortcode')[0]; 
        $youtube_video_id = get_post_meta($post_id, 'youtube_video_id')[0];  
        $youtube_video_cover_img = get_post_meta($post_id, 'youtube_video_cover_img')[0];   

    }
   
     
    
    
    

    
endwhile; endif; ?>


<section class="page-accent seven-pillars-accent">
    <div class="wrapper">
        <div class="left">

        <?php if ($isPillar):  ?>
            <h1 class="page-accent-title"><?php echo ot_get_option('7pillar_page_title'); ?></h1>

            <a href="<?php echo home_url( '/past-results' ); ?>" title="" class="site-btn ">SEE PREVIOUS RESULTS</a>
            <a href="<?php echo home_url( '/update-details' ); ?>" class="site-btn purple ">UPDATE YOUR DETAILS</a>

        <?php else: ?>

            <h1 class="page-accent-title"><?php echo the_title(); ?></h1>

        <?php endif; ?>

        </div>
        <div class="right logos">
            <img src="<?php echo ot_get_option('7pillar_page_logo_1'); ?>" alt="7 Pillars" width="275" height="55"/>
            <img src="<?php echo ot_get_option('7pillar_page_logo_2'); ?>" alt="Play by the Rules" width="182" height="170"/>
        </div>
    </div>
</section>


<?php

if ($isPillar) : ?>
<section class="tabbed-content seven-pillars-test">
    <header>
        <div class="wrapper">


			<?php
                echo get_progress_bar($status);
 			?>

            <h2><?php echo $title; ?></h2>
            <p><?php echo $subtitle; ?></p>
        </div>
        <nav class="tab-navigation wrapper">
        <?php if($status < 8): ?>
            <a href="javascript:;" title="" class="active">Overview</a>
            <a href="javascript:;" title="">Update your actions</a>
       	<?php else: ?>
            <a href="javascript:;" title="" >Overview</a>
            <a href="javascript:;" title="" class="active">Update your actions</a>
		<?php endif; ?>
        </nav>
    </header>

    <!-- Every direct child div of .tab-items is a tab -->
    <div class="tab-items wrapper">
        <!-- tab 1 -->
        <!-- tab 1 -->
        <?php if($status < 8): ?>
            <div class="active" aria-hidden="false"> 
       	<?php else: ?>
            <div aria-hidden="true">
		<?php endif; ?>
	            <div class="two-columns-story">
	                <div class="left text">
	                    <p><?php the_content(); ?></p>                     
	                </div>
	                <div class="right">
                        <?php if(!empty($youtube_video_id)) : ?>
	                    <div class="video-link-open active clearfix">
	                       	<div class="wapp">
	                       		<img src="<?php echo $youtube_video_cover_img;  ?>" alt="" width="534" height="342"/>
	                       		<figure>
	                       			<iframe class="video" width="560" height="315" src="https://www.youtube.com/embed/<?php echo $youtube_video_id; ?>" frameborder="0" allowfullscreen></iframe>
	                       		</figure>
	                       	</div>
	                       	<p><?php echo ot_get_option('video_text'); ?></p>
	                     </div>
                        <?php endif; ?>
	                </div>
	            </div>
	            <div class="centered padded">
	            </div>
       		 </div>
        <!-- tab 2 -->
        <?php if($status < 8): ?>
            <div aria-hidden="true">
       	<?php else: ?>
            <div class="active" aria-hidden="false"> 
		<?php endif; ?>
   			<?php 
				echo do_shortcode($shortcode);
			?>
        </div>
    </div>

<section class="bottom-progress-indicator">
    <h2 class="sr-only"> progress</h2>
	<?php
         get_progress_bar($status);
    ?>

</section>

<script type="text/javascript">
    
/**

    Validation errors scroll to point on the page

**/

  jQuery(document).ready(function(){
    var delay = 0;
    var offset = 200;

    document.addEventListener('invalid', function(e){
       jQuery(e.target).closest('li').addClass("invalid");
       jQuery('html, body').animate({scrollTop: jQuery(jQuery(".invalid")[0]).offset().top - offset }, delay);
    }, true);
    document.addEventListener('change', function(e){
        jQuery(e.target).closest('li').removeClass("invalid");
       //jQuery(e.target).removeClass("invalid")
    }, true);

  });


</script>
<?php else: // if not a pillar quiz page. ?>
    <div class="tab-items wrapper">
        <div class="two-columns-story">
            <?php the_content(); ?>
        </div>
    </div>
<?php endif; ?>

<?php 
	get_template_part( 'templates/content', 'newsletter' );	
?>

<?php get_footer(); ?>


