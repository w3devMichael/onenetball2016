<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); 

	$category = get_the_category();
	$datetiem = get_post_time('j, F, Y', true);
	$post_id = get_the_ID();
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium_post' )[0];
	$placeholder_img = get_post_meta($post->ID,'placeholder_image')[0];

	$title_in_bold = get_post_meta($post->ID,'page_title_in_blod', true);
	$title_in_normal = get_post_meta($post->ID,'page_title_in_regular', true);
?>		

	<section class="page-accent image-bottom">
	    <div class="wrapper">
	        <div class="left">
            	<h1 class="page-accent-title"><span class="accent"><?php echo $title_in_bold; ?></span> <?php echo $title_in_normal; ?></h1>
	           
	            <p><?php echo get_post_meta($post->ID,'page_short_description')[0]; ?></p>
	        </div>
	        <div class="right resource-page-placeholde-container">
	        	<img src="<?php echo $placeholder_img;?>" alt="" width="" height=""/>
	        </div>
	    </div>
	</section> 

	<div class="article-page wrapper">
	    <article>
	        <div class="text">
	         	<?php the_content(); ?>
	        </div>
	    </article>
	    <aside>

	      <?php get_sidebar('resources'); ?>
	   
	    </aside>

	   	<nav class="prev-next-article">
	    	<?php 
				get_template_part( 'templates/nav', 'article' );
	    	?>    
	    </nav>
	</div> 


<?php endwhile; endif; ?>


<?php  

	$newsletter = get_post_meta($post->ID,'newsletter-option')[0];

	if( $newsletter == 'on' ) :

		get_template_part( 'templates/content', 'newsletter' );

	endif;
?>

<?php get_footer(); ?>
