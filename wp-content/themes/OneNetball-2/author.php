<?php get_header(); ?>


			<div class="page-content single-page">

				<section id="content" role="main">
					<header class="header">
						<?php the_post(); ?>
						<h1 class="entry-title author"><?php _e( 'Author Archives', 'wpt' ); ?>: <?php the_author_link(); ?></h1>
						<?php if ( '' != get_the_author_meta( 'user_description' ) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . get_the_author_meta( 'user_description' ) . '</div>' ); ?>
						<?php rewind_posts(); ?>
					</header>

					<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'templates/entry' ); ?>
					<?php endwhile; ?>
					<?php get_template_part( 'templates/nav', 'below' ); ?>
				</section>

				<aside>
					<?php get_sidebar(); ?>					
				</aside>	

			</div>
	
<?php get_footer(); ?>
