<?php get_header(); ?>

			<div class="page-content single-page">

				<section id="content" role="main">
					<header class="header">
						<h1 class="entry-title"><?php _e( 'Tag Archives: ', 'wtp' ); ?><?php single_tag_title(); ?></h1>
					</header>

					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'templates/entry' ); ?>
					<?php endwhile; endif; ?>
					<?php get_template_part( 'templates/nav', 'below' ); ?>

				</section>

				<aside>
					<?php get_sidebar(); ?>					
				</aside>	

			</div>

<?php get_footer(); ?>