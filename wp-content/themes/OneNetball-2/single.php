<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); 

	$category = get_the_category();
	$datetiem = get_post_time('j, F, Y', true);

	$title_in_bold = get_post_meta($post->ID, 'page_title_in_blod', true);
	$title_in_normal = get_post_meta($post->ID, 'page_title_in_regular', true);

	$title = get_the_title();
	$use_title = false;

	if ($title_in_bold == '' && $title_in_normal == ''){
		$use_title = true;
	}
?>		

	<section class="page-accent article-inner-page image-bottom">
	    <div class="wrapper">
	        <div class="left">
            	
		<h1 class="page-accent-title">
		<?php if ($use_title) : ?> 
			<span class="accent">
			<?php echo $title_in_bold . ($use_title) ? $title : ''  ?></span>
			<?php echo ($use_title) ? $title_in_normal  : ''; ?>
			
		<?php else : ?>

			<span class="accent">
			<?php echo $title;  ?></span>
		
		<?php endif; ?>

		</h1>
	        
		<p><?php echo get_post_meta($post->ID,'page_short_description')[0]; ?></p>
	        </div>        
	        <div class="right">
	        </div>
	    </div>
	</section> 

	<div class="article-page wrapper">
	    <article id="post-<?php the_ID(); ?>">

	        <header class="article-header">
	            Posted on 
	            <time class="important-text" datetime="YYYY-MM-DD HH:II:SS"><?php echo $datetiem; ?></time> in 
	            <a href="" title="" class="important-text"><?php foreach ($category as $cat) {echo $cat->cat_name. " "; } ?></a>
	        </header>
	        <div class="text">
	           <?php the_content(); ?>
	        </div>

	        <?php edit_post_link(__('Edit this entry'),'','.'); ?>

	    </article>

	    <aside> 
	      <?php get_sidebar('article'); ?>
	    </aside>
		
	    <nav class="prev-next-article">
	    	<?php 
				get_template_part( 'templates/nav', 'article' );
	    	?>    
	    </nav>

	</div>

<?php endwhile; endif; ?>


<?php  

	$newsletter = get_post_meta($post->ID,'newsletter-option')[0];

	if( $newsletter == 'on' ) :

		get_template_part( 'templates/content', 'newsletter' );

	endif;
?>

<?php get_footer(); ?>
