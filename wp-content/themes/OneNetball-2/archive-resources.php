<?php get_header();
					
$archive_tags = post_type_tags( 'resources' );

$page_title = ot_get_option('resources_page_title'); 
$page_description = ot_get_option('resources_page_mode_desc'); 

$post_count = 0;
$args = array(
	'post_type'			=> 'resources',
	'posts_per_page' 	=>  -1,
	);

$the_query_count = new WP_Query( $args );

if ($the_query_count->have_posts()) :
	while ($the_query_count->have_posts()): $the_query_count->the_post();		          
	$post_count++;
endwhile; endif; 
?>   
	<section class="page-accent">
	    <div class="wrapper">
	        <div class="left">
	            <h1 class="page-accent-title"><span class="accent"><?php echo $page_title; ?></span></h1>
	            <p><?php echo $page_description; ?></p>
	        </div>
	        <div class="right">
	            <div class="text"><h3 class="accent">LATEST UPLOADS</h3></div>	            
	            <ul class="resource-list">

	            <?php 

		            $args = array(
		            	'post_type'			=> 'resources',
		            	'posts_per_page' 	=>  5,
		            	'order'				=> 'ASC',
		            );

		            $the_query = new WP_Query( $args );

		            if ($the_query->have_posts()) :
		            	while ($the_query->have_posts()): $the_query->the_post();
		            ?>

	                <li><a href="<?php echo get_permalink(); ?>" title="><?php the_title(); ?>" class="text-link"><?php the_title(); ?></a></li>

	        	<?php endwhile; endif; ?>

	            </ul> 
	        </div>
	    </div>
	</section> 

	<section class="resource-results wrapper">
	    <header>
	        <h2>SHOWING RESULTS FOR <span class="accent">ALL RESOURCES</span></h2>
	        <span class="results-count"><?php echo $post_count; ?> results</span>
	        <div class="sorting clear">
	            <h3>Filter resources</h3>
	            <div class="styled-select">
	                <select name="" id="select-sorting">
	                    <option value="DESC">Newest First</option>
	                    <option value="ASC">Oldest First</option>
	                </select>
	            </div>
	            <br class="clear"/>
	        </div>
	    </header>
	    <div class="resource-columns">
	        <aside>
	            <form method="get" action="" class="site-form">
	                <fieldset>
	                    <legend class="sr-only">Filter form</legend>
	                    <button type="button" class="site-btn purple mobile-filters-toggle block">FILTER RESOURCES</button>
	                    <ol>
	                        <li>
	                            <div class="aside-label">By topic</div>
	                            <div class="mobile-checkbox-wrap">
	                            <?php foreach( $archive_tags as $tag ) { ?>
	                            	<label class="styled-checkbox">
	                            		<input type="checkbox" value="<?php echo $tag->term_id; ?>" name="<?php echo esc_html( $tag->name ); ?>"/>
	                            		<span><?php echo esc_html( $tag->name ); ?></span>
	                            	</label> 
	                            <?php } ?>	       
	                           
	                            </div>
	                        </li>
	                        <li>
	                            <label for="" class="aside-label">By content type</label>
	                            <div class="styled-select">
	                                <select name="" id="content-type">
	                                    <option selected value="0">All content types</option>
	                                   	<?php 						
										$categorys = get_terms( 'resources-libraries', array(
										    'hide_empty' => false,
										) );

										foreach ($categorys as $cat) { ?>

											<option value="<?php echo $cat->name ?>"><?php echo $cat->name ?></option>
											
										<?php }
                                	 ?>         
	                                </select>
	                            </div>
	                        </li>
	                        <li>
	                            <label for="" class="aside-label">With keywords</label>
	                            <input type="text" name="" value="" id="keyword-input" placeholder="Add keyword(s)..."/>
	                        </li>  
	                        <li>
	                            <button type="submit" id="search-resources" class="site-btn block">UPDATE RESULTS</button>
	                        </li>
	                    </ol>
	                </fieldset>
	            </form>
	        </aside>
	        <div class="results-list">
	            <ul>

		    	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 

		    		$format = (get_post_format()) ? get_post_format() : "standard";

		    		if($format === "video") :
		    			get_template_part('templates/format/resources-video' );
		    		else : 
		    			get_template_part('templates/format/resources-standard');
		    		endif;

		    	endwhile; endif; ?>

	            </ul>

	            <div id="ajax-loading" class="loading">
	            	<img src="<?php bloginfo('template_directory'); ?>/images/ellipsis.gif" alt="">
	            </div>	

	            <a href="javascript:;" title="" id="load-more-resources" class="site-btn purple block">LOAD MORE RESULTS</a>
	        </div>
	    </div>
	</section>

	<?php if(!is_user_logged_in() ) : ?>
        <div class="modal-wrapper">
            <div class="modal-inner">
                <div class="close-layer"></div>
                <div class="modal-box">
                    <div class="close"></div>

                    <div class="modal-step-1">
                        <h1>Thanks for requesting a download of this resource</h1>
                        <p class="sub-head">Would you like to stay up to date with the latest One Netball News?</p>
                        <p>Want to know when the One Netball Community Award nominations are open? Interested in other important information? Sign up to the newsletter for the occasional One Netball update.</p>

                        <div class="button-wrapper">
                            <a class="sign-up site-btn" href="#">Sign up & Download Resource <span class="icon"></a>
                            <a class="download site-btn purple" href="#" download>No thanks, download only <span class="icon"></a>
                        </div>
                    </div>

                    <div class="modal-step-2">
                        <?php echo do_shortcode('[gravityform id="3" title="true" description="true" ajax="true"]'); ?>
                    </div>

                </div>
            </div>
        </div>
    <?php endif; ?>

<?php 

	$show_newsletter = ot_get_option('show_newsletter');

	if( $show_newsletter === 'on'):
		get_template_part( 'templates/content', 'newsletter' );
	endif;
 ?>



<script>
jQuery(document).ready(function($){

	    var count = 2;
	    var status = 100;
	    var selchbox = {};
	    var firstSorting = true;
	    var checkSorting = false;
	    var lastResult = false;


		$( "#select-sorting" ).on( "selectmenuchange", function( event, ui ) {
		    var sorting = $(this).val();  

		    if(status == 101) {
		    	var inpfields = $('.mobile-checkbox-wrap input');
		    	for(var i=0; i<inpfields.length; i++) {
		    		if(inpfields[i].type == 'checkbox' && inpfields[i].checked == true) selchbox[inpfields[i].name] = inpfields[i].value;
		    	}

		    	var inputKeyword = $('#keyword-input').val();
		    	var optionKeyword = $( "#content-type option:selected" ).val();
			    var checkboxKeyword = JSON.stringify(selchbox);

			    loadArticle(-1, 101, sorting, inputKeyword, optionKeyword, checkboxKeyword );	

		    }else {
		    	if(firstSorting) {
		    		count = 1;
			    	loadArticle(count, 100, sorting);  
			    	firstSorting = false;
			    	checkSorting = true;
			    	count++;	
		    	}else {
		    		if(lastResult) {
		    		    count = 1;
				    	loadArticle(count, 100, sorting);  
				    	firstSorting = false;
				    	checkSorting = true;
				    	count++;				
		    		}else {
		    			loadArticle(count, 100, sorting);  
		    			checkSorting = true;
		    		}
		    	}
		    }
		});	 

	    $('#search-resources').click(function(event) {

	    	event.preventDefault();
	    	selchbox = {};

	    	var inpfields = $('.mobile-checkbox-wrap input');
	    	for(var i=0; i<inpfields.length; i++) {
	    		if(inpfields[i].type == 'checkbox' && inpfields[i].checked == true) selchbox[inpfields[i].name] = inpfields[i].value;
	    	}

	    	var sorting = $( "#select-sorting option:selected" ).val();
	    	var inputKeyword = $('#keyword-input').val();
	    	var optionKeyword = $( "#content-type option:selected" ).val();
		    var checkboxKeyword = JSON.stringify(selchbox);

		    loadArticle(-1, 101, sorting, inputKeyword, optionKeyword, checkboxKeyword );
		    status = 101;
	    });  
	
	    $('#load-more-resources').click(function(event) {

	    	event.preventDefault();
	    	var sorting = $( "#select-sorting option:selected" ).val();

	        loadArticle(count, 100, sorting);
	        count++; 
	        status = 100;
	        checkSorting = false;
	    });

	    function loadArticle(paged, status, sorting, inputKeyword, optionKeyword, checkboxKeyword){  

	    	var html = '';  
	    	var  data = new FormData();   
	    	data.append( 'paged', paged);
	    	data.append( 'status', status);
	    	data.append( 'sorting', sorting);
	    	data.append( 'inputKeyword', inputKeyword);
	    	data.append( 'optionKeyword', optionKeyword);
	    	data.append( 'checkboxKeyword', checkboxKeyword);   	
	    	data.append('action', 'load_resources_ajax'); 


	    	jQuery.ajax({
	    		type:"post",
	    		url: "<?php echo admin_url('admin-ajax.php'); ?>",
	    		data: data,	 
	    		contentType: false,
	    		processData: false,				    	   	
				beforeSend: function () {
					$('#ajax-loading').show();
				},
				complete: function () {
					$('#ajax-loading').hide();
				},	            		
	    		success: function(response){
	    			
	                resOBJ = JSON.parse(response);
                    html = resOBJ.html;

                    // console.log(resOBJ.args);
                    // console.log(resOBJ.html);
                    totalReuslts = resOBJ.total_results;

	    				if(status === 101) {
	    					if(totalReuslts) {
		    					$('.results-count').empty().append(totalReuslts);   
		    					$('#load-posts').hide();
 								$(".results-list ul").empty().append(html);  
	    						$('#load-more-resources').hide();	
	    					}else {
	    						$(".results-list ul").empty().append(html);  
	    						$('#load-more-resources').hide();	
	    					}	  
	    				}else {
	    					
	    					if(totalReuslts == null) {
	    						lastResult = true;
	    					}
	    					if(checkSorting) {
	    						if(lastResult) {
	    							
	    							$(".results-list ul").empty().append(html);  
	    						}else {
	    							
	    							$(".results-list ul").html(html);
	    							  
	    						}	    						
	    					}else {
	    						
	    						$(".results-list ul").empty().append(html);   
	    					}
	    				}
	                }
	            });
	    	return false;
	    }

  	});

</script>



<?php get_footer(); ?>