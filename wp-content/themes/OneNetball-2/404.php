
<?php get_header(); ?>


	<div class="page-content single-page 404page">

		<section class="main-content">

			<div class="page-accent">

				<div class="wrapper">
					<h1 class="page-accent-title">
						<span class="accent">Oops!</span><br/>
						This is embarrassing!
					</h1>

					<p>The page you're looking for doesn't exist, or may have changed.</p>
				</div>
				
			</div>

		</section>

	</div>

<?php 
	get_template_part( 'templates/content', 'newsletter' );	
?>

<?php get_footer(); ?>
