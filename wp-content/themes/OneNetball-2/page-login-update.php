<?php
/**
 * Template Name: Update Login
 *
 * @package wpbuilder
 * @subpackage wpbuilder
 * @since wpbuilder
 */

get_header(); 

$state_array = array('ACT', 'New South Wales', 'Northern Territory', 'Queensland', 'South Australia', 'Tasmania', 'Victoria', 'Western Australia');

$club_role_array = array('Coach', 'Umpire', 'Committee member', 'President', 'Parent', 'Other');

$lost_password_link = get_page_by_title('Lost Password');

$page_title = get_the_title(); 

$page_description = ot_get_option('login_page_mode_desc'); 

$login_page_logo_1 = ot_get_option('login_page_logo_1'); 

$login_page_logo_2 = ot_get_option('login_page_logo_2');

$current_user = wp_get_current_user();

$user_id = get_current_user_id();

$current_user_meta = get_user_meta( $user_id );

$user_state = $current_user_meta['state'][0];
$user_club_name = $current_user_meta['club_name'][0];
$user_club_role = $current_user_meta['club_role'][0];

$roles = getRoles($current_user);



?>

<section class="page-accent">
    <div class="wrapper">
        <div class="left">
            <h1 class="page-accent-title"><?php echo $page_title; ?></h1>
            <p><?php echo $page_description; ?></p>
        </div>
        <div class="right logos">
            <img src="<?php echo $login_page_logo_1; ?>" alt="" width="275" height="55"/>
            <img src="<?php echo $login_page_logo_2; ?>" alt="" width="182" height="170"/>
        </div>
    </div>
</section>


<?php if(is_user_logged_in()) : ?>

<section class="login-section wrapper two-columns-story">

    <h2 class="sr-only">YOUR DETAILS</h2>
    
    <div class="form-success" id="form-success" style="display:none;">
        <h2>You have succesfuly updated your details.</h2>
    </div>

    <div class="form-errors">
        <strong>There was an error with your submission:</strong>
        <ul>
        </ul>
        <span>Please try again and re-submit</span>
    </div>

    <div class="left">
        <form method="post" action="" class="site-form" id="submit-update-login">      
            <fieldset>
                <legend class="site-legend">YOUR DETAILS</legend>
                <p>Hello <strong><?php echo $current_user->user_login; ?></strong>, if your details have changed, it’s easy to update them in the fields below</p>

                <input type="hidden" name="update-details" value="1"/>
                
                <ol>
                    <!-- <li>
                        <label for="" class="sr-only">Username*</label>
                        <input type="text" name="" placeholder="Username*" value="<?php echo $current_user->user_login; ?>" id="reg-username" readonly/>
                    </li> -->
                    <li>
                        <label for="" class="sr-only">Email address*</label>
                        <input type="email" name="" placeholder="Email address*" value="<?php echo $current_user->user_email; ?>" id="reg-email"/>
                    </li>
                      
                    <li>
                        <label for="" class="sr-only">Name of Club or Association*</label>
                        <input type="text" name="" placeholder="Name of Club or Association*" value="<?php echo $current_user_meta['club_name'][0]; ?>" id="reg-club"/>
                    </li>      
                    <li>
                        <label for="" class="sr-only">Select a state*</label>
                        <div class="styled-select">
                            <select name="" id="reg-state">
                                <option value="">Select a state*</option>
                                <?php 
                                    foreach ( $state_array as $state) {
                                        ?>
                                        <option value="<?php echo $state; ?>" <?php if ($state == $user_state) echo 'selected="selected"'; ?>><?php echo $state; ?></option>
                                        
                                        <?php
                                    }
                                 ?>
                            </select>
                        </div>
                    </li>           
                    <li>
                        <label for="" class="sr-only">Your role at club or association* </label>
                        <div class="styled-select">
                            <select name="" id="reg-role">
                                <option value="">Your role at club or association* </option>
                                <?php 
                                    foreach ( $club_role_array  as $club_role) {
                                        ?>
                                        <option value="<?php echo $club_role; ?>" <?php if ($club_role == $user_club_role) echo 'selected="selected"'; ?>><?php echo $club_role; ?></option>
                                        
                                        <?php
                                    }
                                 ?>
                            </select>
                        </div>
                    </li>

                    <li class="subscribe-fields" >
                        <h2>STAY UP TO DATE WITH ONE NETBALL.</h2>
                        <span>Would you like to receive occasional updates?</span>
                        
                        <label class="styled-checkbox" style="display: table; margin: 10px auto;">
                             <input type="checkbox" name="mc4wp-subscribe" id="updates-required" <?php if ($roles[0] == 'inclusionupdates') echo "checked='checked'"; ?>value="" />
                            <span>Yes please, that’d be great!</span>
                        </label>                        
                    </li>
                    <li class="centered">
                        <button type="submit" id="submit-registraiton" class="site-btn block">SAVE CHANGES <i class="icon-arrow-right"></i></button>
                    </li>
                </ol>
            </fieldset>
        </form>
    </div>
    
    <div class="right"> 
    
        <?php show_error_messages(); ?>

        <form id="submit-password-change" method="post" action="" class="site-form">      
            <fieldset>
                <legend class="site-legend">CHANGE YOUR PASSWORD</legend>
                
                <p>If you need to change your password, just fill in the form below. Try and use something that is easy to remember.</p>
                
                <ol>
                    <li>
                        <label for="" class="sr-only">Type a new password*</label>
                        <input type="password" name="" placeholder="Type a new password*" value="" id="reg-pass" style="background: #FFF !important"/>
                        <div class="field-help">Password must be at least 7 characters long. To make it stronger, use upper and lower case letters, numbers &amp; symbols.</div>
                    </li>        
                    <li>
                        <label for="" class="sr-only">Confirm new password*</label>
                        <input type="password" name="" placeholder="Confirm new password*" value="" id="reg-pass-repeat"/>
                        <div class="field-help">Type your password again.</div>
                        <!-- set password strength as percent width of the span below -->
                        <div class="password-meter">
                            <div>Password strength indicator</div>
                            <span></span>
                        </div>
                    </li>     
                    <li class="sign-in-buttoms">
                        <input type="hidden" name="login_nonce" value="<?php echo wp_create_nonce('login-nonce'); ?>"/>
                        <button type="submit" id="login_submit" class="site-btn">SAVE PASSWORD <i class="icon-arrow-right"></i></button>
                    </li>
                </ol>
            </fieldset>
        </form>
    </div>
</section>

<?php 
    get_template_part( 'templates/content', 'newsletter' ); 
?>


<script>

    (function($) {

        $('#reg-pass').complexify({}, function (valid, complexity) {

            $('.password-meter span').css({ "width" : Math.round(complexity) + '%' });
        });

    })(jQuery);

    (function( $ ) {

        $('#submit-update-login').submit(function(event) {            
            event.preventDefault();

            var email = $('#reg-email').val();
            var username = $('#reg-username').val();
            var clubName = $('#reg-club').val();
            var state = $('#reg-state').val();
            var clubRole = $('#reg-role').val();
            var updatesRequired = $('#updates-required').prop('checked');

            // alert(updatesRequired);

            registerUserUpdateAjax(email,username, clubName, state, clubRole, updatesRequired);
        });


        $('#submit-password-change').submit(function(event) {            
            event.preventDefault();

            password = $('#reg-pass').val();
            passwordRepeat = $('#reg-pass-repeat').val();

            registerPasswordAjax(password, passwordRepeat);
        });

        function registerUserUpdateAjax(email, username, clubName, state, clubRole, updatesRequired){    

            var  data = new FormData();   
            data.append( 'email', email);
            data.append( 'username', username);
            data.append( 'clubName', clubName);
            data.append( 'state', state);
            data.append( 'clubRole', clubRole);
            data.append( 'updatesRequired', updatesRequired);     
            data.append('action', 'user_registration_update_ajax'); 

            jQuery.ajax({
                type:"post",
                url: "<?php echo admin_url('admin-ajax.php'); ?>",
                data: data,  
                contentType: false,
                processData: false,                         
                beforeSend: function () {

                },
                complete: function () {

                },                      
                success: function(response){

                    console.log(response);

                    var resOBJ = jQuery.parseJSON(response);
                    var status = resOBJ.status;

                    console.log(resOBJ.status);
                    if(status == 1) {

                        // console.log("ok");
                        $('.form-errors').hide();

                        $('#form-success h2').html('You have succesfuly updated your details.');
                        $('#submit-registraiton').addClass('purple').fadeIn(500).html('Changes Saved!');
                         

                         setTimeout( function(){
                                $('#submit-registraiton').removeClass('purple').fadeIn().html('Save Changes<i class="icon-arrow-right"></i>');
                         }, 5000);
                        $('#form-success').slideDown().delay(5000).slideUp();

                        $('#submit-update-login input, #submit-update-login select').removeClass('failed');

                    } else {

                        // console.log("not ok");
                        $('.form-errors').show();
                        $('.form-errors ul').empty();
                        $('.form-errors ul').append(resOBJ.input_errors);

                        fieldStauts = resOBJ.files_warnings;

                        if( jQuery.isEmptyObject(fieldStauts) ) {
                            $('.form-errors').hide();
                        }else {
                            if ( fieldStauts.email === 1 ) {
                                $('#reg-email').addClass("failed");
                            }else {
                                $('#reg-email').removeClass("failed");
                            }

                            if ( fieldStauts.username === 1 ) {
                                $('#reg-username').addClass("failed");
                            }else {
                                $('#reg-username').removeClass("failed");
                            }

                            if ( fieldStauts.clubName === 1 ) {
                                $('#reg-club').addClass("failed");
                            }else {
                                $('#reg-club').removeClass("failed");
                            }

                            if ( fieldStauts.clubState === 1 ) {
                                $('#reg-state').addClass("failed");
                            }else {
                                $('#reg-state').removeClass("failed");
                            }

                            if ( fieldStauts.clubRole === 1 ) {
                                $('#reg-role').addClass("failed");
                            }else {
                                $('#reg-role').removeClass("failed");
                            }

                        }
                    }                      
                }
            });
            return false;
        }

        function registerPasswordAjax(password, passwordRepeat){    

            var  data = new FormData();   
            data.append( 'password', password);
            data.append( 'passwordRepeat', passwordRepeat);    
            data.append('action', 'user_registration_password_update_ajax'); 

            jQuery.ajax({
                type:"post",
                url: "<?php echo admin_url('admin-ajax.php'); ?>",
                data: data,  
                contentType: false,
                processData: false,                         
                beforeSend: function () {

                },
                complete: function () {

                },                      
                success: function(response){

                    console.log(response);

                    var resOBJ = JSON.parse(response);
                    var status = resOBJ.status;

                    console.log(response)

                    if(status == 1) {
                        $('.form-errors').hide();

                        $('#form-success h2').html('You have succesfuly updated your password.');

                        $('#form-success').show().delay(5000).fadeOut();

                        $('#submit-password-change input').removeClass('failed');
                        $('#submit-password-change input').val('');

                        $('.password-meter span').css({ "width" : 0 + '%' });

                        $('#login_submit').addClass('purple').fadeIn(500).html('Password updated!');
                         

                         setTimeout( function(){
                                $('#login_submit').removeClass('purple').fadeIn().html('SAVE PASSWORD<i class="icon-arrow-right"></i>');
                         }, 5000);

                    }else {
                        $('.form-errors').show();
                        $('.form-errors ul').empty();
                        $('.form-errors ul').append(resOBJ.input_errors);

                        fieldStauts = resOBJ.files_warnings;

                        if( jQuery.isEmptyObject(fieldStauts) ) {
                            $('.form-errors').hide();
                        }else {
                            if ( fieldStauts.email === 1 ) {
                                $('#reg-email').addClass("failed");
                            }else {
                                $('#reg-email').removeClass("failed");
                            }

                            if ( fieldStauts.password === 1 ) {
                                $('#reg-pass').addClass("failed");
                            }else {
                                $('#reg-pass').removeClass("failed");
                            }

                            if ( fieldStauts.passwordRepeat === 1 ) {
                                $('#reg-pass-repeat').addClass("failed");
                            }else {
                                $('#reg-pass-repeat').removeClass("failed");
                            }

                        }
                    }                      
                }
            });
            return false;
        }

    })( jQuery );

</script>

<?php endif;

?>


<?php get_footer(); ?>
