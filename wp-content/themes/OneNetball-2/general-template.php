<?php
/**
 * Template Name: Default Onenetball Template
 *
 */


?>


<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); 

	$category = get_the_category();
	$datetiem = get_post_time('j, F, Y', true);
	$post_id = get_the_ID();
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full' )[0];

	$placeholder_img = wp_get_attachment_url( get_post_thumbnail_id($post_id) );

	$title_in_bold = get_post_meta($post->ID,'page_title_in_blod', true);
	$title_in_normal = get_post_meta($post->ID,'page_title_in_regular', true);

	//acf fields
	$purple = get_field('title_purple');
	$black = get_field('title_black');
	$intro = get_field('title_intro');
?>		

	<section class="page-accent image-bottom generic">
	    <div class="wrapper">
	        <div class="left">
            	<h1 class="page-accent-title">
            	<span class="accent"><?php echo $purple; ?> </span>
            	<?php echo $black; ?>
            	</h1>
	           
	           <p><?php echo $intro; ?></p>
	            
	        </div>
	        <div class="right resource-page-placeholde-container">
	        	<img src="<?php echo $thumbnail?>" alt="" width="" height=""/>
	        </div>
	    </div>
	</section> 

	<div class="article-page wrapper">
	    <article>
	        <div class="text">
	         	<?php the_content(); ?>
	        </div>
	    </article>
	    <aside>

	      <?php dynamic_sidebar('article-widget-area'); ?>
	   
	    </aside>
	</div> 


<?php endwhile; endif; ?>


<?php  

	$newsletter = get_post_meta($post->ID,'newsletter-option')[0];

	if( $newsletter == 'on' ) :

		get_template_part( 'templates/content', 'newsletter' );

	endif;
?>

<?php get_footer(); ?>
