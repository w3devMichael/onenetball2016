(function( $ ) {

    // Settings
    var OneNetball = {
        tabs: function(list) {
            var $list = $(list);
            if (!$list.length) {
                return false;
            }
            
            $($list).each(function(){
                var $tabContent = $(this);   
                $('.tab-navigation > a', $tabContent).on('click', function(){  
                    $('.tab-navigation > a.active', $tabContent).removeClass('active');
                    $(this).addClass('active');
                    
                    $('.tab-items > div.active', $tabContent).attr('aria-hidden', true).removeClass('active');
                    $('.tab-items > div:eq('+ $(this).index() +')', $tabContent).attr('aria-hidden', false).addClass('active');
                });    
            });
        },
        wordCounter: function() {
            if ($('.word-counter').length) {
                $('.word-counter').each(function(){
                    $(this).on('keyup', function(){ 
                        var regexpResult = $(this).val().match(/\S+/g);
                        var words = regexpResult ? regexpResult.length : 0;
                        $('.word-count span', $(this).parent()).text(words);
                    });
                });
            }
        },
        popup: {
            open: function($popup) {   
                OneNetball.popup.close();
                var $html = [];
                $html.push('<div class="popup">');
                    $html.push('<div class="popup-overlay"></div>');
                    $html.push('<div class="popup-content">');
                        $html.push('<button type="button" class="popup-close" title="close">X</button>');
                        $html.push('<div class="popup-body">');
                            $html.push($($popup).html());
                        $html.push('</div>');
                    $html.push('</div>');
                $html.push('</div>');
                
                $('body').append($html.join(''));
            },
            close: function() {
                $('.popup').remove();
            }
        } 
    };

    // Ready
    $(document).ready(function(){
        
        // Init tabs
        OneNetball.tabs('.tabbed-content');
        
        // Init word counter
        OneNetball.wordCounter();
        
        // Slide to newsletter form
        $('.newsletter-btn').on('click', function(){
            $('html, body').animate({scrollTop: $('.newsletter-form').offset().top}, 1000, function(){
                $('.newsletter-form ol li:first-child input').trigger('focus');
            });
        });
        
        // Mobile menu
    	$('.mobile-menu-btn, .menu-overlay').click(function(){
    		$('body').toggleClass('menu-open');
    	});    
        
        // Mobile filters
        $('.mobile-filters-toggle').on('click', function(){
            $(this).toggleClass('active');
            $('.resource-columns > aside .site-form ol, .latest-news-filters').stop(true, true).slideToggle();
        });
        
        // Bind popups
        $(document).on('click', '[data-popup]', function(event){
            event.preventDefault();
            OneNetball.popup.open($(this).data('popup'));;
        });
        // Close popups
        $(document).on('click', '.popup-close, .popup-overlay', function(){
            OneNetball.popup.close();
        });


        //hide subscribe to newsletter button
        var news_letter_form =  $('.newsletter-form');
        if (news_letter_form.length === 0){
            
            $('.newsletter-btn').fadeOut();
            return false;
        }
    });

    // Escape press
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            OneNetball.popup.close();
        }
    });

})( jQuery );



(function( $ ) {
    
    var $allVideos = $("iframe[src*='//player.vimeo.com'], iframe[src*='//www.youtube.com'], object, embed"),
    $fluidEl = $("figure");
            
    $allVideos.each(function() {
    
      $(this)
        // jQuery .data does not work on object/embed elements
        .attr('data-aspectRatio', this.height / this.width)
        .removeAttr('height')
        .removeAttr('width');
    
    });
    
    $(window).resize(function() {
    
      var newWidth = $fluidEl.width();
      $allVideos.each(function() {
      
        var $el = $(this);
        $el
            .width(newWidth)
            .height(newWidth * $el.attr('data-aspectRatio'));
      
      });
    
    }).resize();

    $(document).ready(function() {
        
      $('.video-link-open').on('click', function(ev) {

        $(this).removeClass('active');

        $(this).find('img').remove();

    
      });


   });



})( jQuery );

//------------------------------ W3 additions
jQuery(document).ready( function($) {

    //jQuery UI for select fields
    //---------------------------------------------------
    $('select').selectmenu();

    //Add validation to select styles
    //---------------------------------------------------
    $(document).ajaxComplete(function(){
        if( $('.form-errors').length > 0 ){

            $('select.failed').each(function(){
                $(this).parent().find('.ui-selectmenu-button').addClass('failed');
            });

            $('select:not(.failed)').each(function(){
                $(this).parent().find('.ui-selectmenu-button').removeClass('failed');
            });

        }
    });  

    // Sticky header
    //---------------------------------------------------
    var hBot = $('.site-header .bottom');
    var pWidth = $(document).width();
    var top = hBot.offset();

    $(window).scroll(function(){
        var y = $(this).scrollTop();

        if( pWidth > 768 ){
            if( y > 46){
                hBot.addClass('sticky');
                $('body').addClass('sticky');
            } else {
                hBot.removeClass('sticky');
                $('body').removeClass('sticky');
            }
        }
    });


    // Download Sign up modal
    //---------------------------------------------------
    var modBtn = $('.modal-btn');
    var dlBtn = $('.modal-wrapper .site-btn.download');
    var close = $('.close, .close-layer, .close-btn');
    var m1 = $('.modal-step-1');
    var m2 = $('.modal-step-2');
    var m3 = $('.modal-step-3');


    if( modBtn.length > 0 ){

        //open modal
        modBtn.click( function(e){
            var _this = $(this);
            var pdfURL = _this.attr('href');
            
            if( $('body').is('.signedup') ){

            } else {
                e.preventDefault();
                 //open modal and apply body class
                $('.modal-wrapper').show(100);
                $('body').addClass('modal-body');

                //update download link with current link
                $('.modal-wrapper .download').attr('href', pdfURL);
            }

        });

        //if no thanks is clicked - dont open modal again
        $('.download').click(function(){
            $('body').addClass('signedup');
            window.open( $(this).attr('href'), 'Download PDF' );

            $('.modal-wrapper').hide(100);
            $('body').removeClass('modal-body');
        });


        //close modal
        close.click(function(){
            var _this = $(this);

            _this.closest('.modal-wrapper').hide(100);
            $('body').removeClass('modal-body');
            m1.show(50);
            m2.hide(50);
        });

        //modal steps
        $('.site-btn.sign-up').click(function(){
            m1.hide(50);
            m2.show(50);

            console.log('changed');
        });

    }

    //check if form is submitted before downloading pdf
    $(document).on('gform_confirmation_loaded', function(e, form_id){
        if(form_id == 3) {
           var pdfURL = $('.modal-wrapper .download').attr('href');
           // window.location.href = pdfURL;
           $('body').addClass('signedup');
           $('.download.resource').attr('href', pdfURL);
       }
    });



    //On window load - if hashtag / parameter, filter results
    //----------------------------------------------------------
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var filter = getUrlParameter('filter');

    if( $('.resource-results').length > 0 ){
        if( filter == 'video' ){
            $('#content-type').val('Video');
            $('#search-resources').click();
        }
    }

});