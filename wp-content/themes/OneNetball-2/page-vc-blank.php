<?php
/**
 * Template Name: VC Blank Page
 *
 * @package wpbuilder
 * @subpackage wpbuilder
 * @since wpbuilder
 */

get_header(); ?>

	<div class="vc-content container-fluid">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
		<article class="post" id="post-<?php the_ID(); ?>">

			<div class="entry">

				<?php the_content(); ?>

			</div>

			<?php edit_post_link(__('Edit this entry'), '<p>', '</p>'); ?>

		</article>
		
		<?php comments_template(); ?>


	<?php endwhile; endif; ?>

</div>
<?php 
	get_template_part( 'templates/content', 'newsletter' );	
?>

<?php get_footer(); ?>
