<?php get_header(); ?>

			<div class="page-content single-page">

				<section id="content" role="main" class="main-content">
					<header class="header">
						<h1 class="entry-title"><?php 
						if ( is_day() ) { printf( __( 'Daily Archives: %s', 'wpt' ), get_the_time( get_option( 'date_format' ) ) ); }
						elseif ( is_month() ) { printf( __( 'Monthly Archives: %s', 'wpt' ), get_the_time( 'F Y' ) ); }
						elseif ( is_year() ) { printf( __( 'Yearly Archives: %s', 'wpt' ), get_the_time( 'Y' ) ); }
						else { _e( 'Archives', 'wpt' ); }
						?></h1>
					</header>

					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'templates/entry' ); ?>
					<?php endwhile; endif; ?>
					<?php get_template_part( 'templates/nav', 'below' ); ?>

				</section>

				<aside>
					<?php get_sidebar(); ?>					
				</aside>	

			</div>

<?php get_footer(); ?>
