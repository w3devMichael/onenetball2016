<?php get_header(); 




?>

<section class="page-accent image-bottom">
    <div class="wrapper">
        <div class="left">
            <h1 class="page-accent-title">GET THE<br/><span class="accent">LATEST NEWS</span></h1>
        </div>
        <div class="right">
            <img src="<?php echo ot_get_option('latest_news_cover_image');  ?>" alt="" width="" height=""/>
        </div>
    </div>
</section>

<?php 

$args = array(
	'type'            => 'monthly',
	'limit'           => '',
	'before'          => '',
	'after'           => '',
	'show_post_count' => false,
	'echo'            => 0,
	'order'           => 'DESC',
    'post_type'     => 'post'
);

$archi = wp_get_archives( $args ); 

$archi = explode( '</li>' , $archi );
?>

<section class="news-section news-inner">
    <h2 class="sr-only">NEWS</h2>
    <header>
        <form method="post" action="" class="site-form wrapper">
            <fieldset>
                <legend class="sr-only">Filter results</legend>
                <button type="button" class="site-btn purple mobile-filters-toggle block">FILTER NEWS</button>
                <ol class="latest-news-filters">
                    <li class="">
                        <label for="" class="sr-only">Select a date</label>
                        <div class="styled-select">
                            <select name="" id="sort">
								<option value="">Select a date</option>
                            <?php 

								foreach( $archi as $link ) {
									$link = str_replace( array( '<li>' , "\n" , "\t" , "\s" ), '' , $link );
									if( '' != $link ) {
										
										preg_match("~'>(.*?)</~", $link, $output, PREG_OFFSET_CAPTURE, 3);

										$value = $output[1][0];

										echo '<option value="'.$value.'">'.$value.'</option>';
									}										
								}
                             ?>
              
                            </select>
                        </div>
                    </li> 
                    <li class=""><div class="or-decoration">OR</div></li>  
                    <li class="">
                        <label for="" class="sr-only">Search by keyword</label>
                        <div class="keyword-box clearfix">
                        	<input type="text" name="" placeholder="Search by keyword" value="" id="keyword-input"/>
                        	<button type="submit" id="keyword-search" class="site-btn block"></button>	
                        </div>	

                    </li>  
                </ol>
            </fieldset>
        </form> 
    </header>

    <ul class="news-list wrapper">  

    	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 

    		$format = (get_post_format()) ? get_post_format() : "standard";

    		get_template_part('templates/format/' . $format );

    	?>

    	<?php endwhile; endif; ?>
    
    </ul>

    <div id="ajax-loading" class="loading">
    	<img src="<?php bloginfo('template_directory'); ?>/images/ellipsis.gif" alt="">
    </div>	

    <div class="centered padded">
        <a href="" title="" id="load-posts" class="site-btn">LOAD MORE ARTICLES</a>
    </div>

</section>


<?php 

	$show_newsletter = ot_get_option('show_newsletter');

	if( $show_newsletter === 'on'):
		get_template_part( 'templates/content', 'newsletter' );
	endif;
 ?>


<script>

    (function( $ ) {

	    var count = 2;

	    $( "#sort" ).on( "selectmenuchange", function( event, ui ) {
		    var keyword = $(this).val();
		    loadArticle(-1, 102, keyword);
		});	    

	    $('#load-posts').click(function(event) {
	    	event.preventDefault();
	        loadArticle(count, 100 );
	        count++; 
	    });

	    $('#keyword-search').click(function(event) {
	    	event.preventDefault();
	    	var keyword = $('#keyword-input').val();
	        loadArticle(-1, 101, keyword);
	    });

	    function loadArticle(paged, status, keyword){    

	    	var  data = new FormData();   
	    	data.append( 'paged', paged);
	    	data.append( 'status', status);
	    	data.append( 'keyword', keyword);
	    	data.append('action', 'load_posts_ajax'); 

	    	jQuery.ajax({
	    		type:"post",
	    		url: "<?php echo admin_url('admin-ajax.php'); ?>",
	    		data: data,	 
	    		contentType: false,
	    		processData: false,				    	   	
				beforeSend: function () {
					$('#ajax-loading').show();
				},
				complete: function () {
					$('#ajax-loading').hide();
				},	            		
	    		success: function(html){
	    				if(status === 101 || status === 102 ) {
	    						if (html == ''){
	    						html = "Sorry, there are no news articles under that search criteria. Please search again.";
	    					}
	    					$(".news-list").empty().append(html);   
	    					$('#load-posts').hide();
	    				}else {


	    					$(".news-list").append(html);   
	    				}
	                }
	            });
	    	return false;
	    }

  	})( jQuery );

</script>

<?php get_footer(); ?>