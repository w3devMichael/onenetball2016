<?php 
	$prev_post = get_adjacent_post(false, '', true);					
	$next_post = get_adjacent_post(false, '', false);

	if(!empty($prev_post)):
		echo '<a href="'.get_permalink($prev_post->ID).'" title="" class="prev"><span>LAST ARTICLE</span><strong>'.$prev_post->post_title.'</strong></a>';
	endif;

	if(!empty($next_post)): 
		echo '<a href="'.get_permalink($next_post->ID).'" title="" class="next"><span>NEXT ARTICLE</span><strong>'.$next_post->post_title.'</strong></a>';
	endif;
?>    
