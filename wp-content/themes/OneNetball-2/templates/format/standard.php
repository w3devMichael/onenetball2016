<?php
/**
 * Post Format - standard
 *
 * @package     wpbuilder
 * @version     v.1.0
 */


$post_id = get_the_ID();
$title = get_the_title();
$content = substr(get_the_content(), 0, 50);
$read_more = get_permalink();
$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium_post' )[0];
$format = 'd F Y';
$date = get_the_date( $format, $post_id );
$tags = get_the_tags();
$categories = get_the_category();
?>

<li>
	<a href="<?php echo $read_more; ?>" title=""><span>View article</span></a>
	<div class="thumb" style="background-image:url(<?php 
		if( !empty($thumbnail) ){
			echo $thumbnail; 
		} else {
			echo get_template_directory_uri() . '/images/news.jpg';
		}
	?>);"></div>
	<div class="standard-info" style="min-height:129px;">
		<strong><?php echo $title; ?></strong>
		<time datetime="YYYY-MM-DD HH:II:SS"><?php echo $date; ?></time>
		<span class="category">
			<?php 
			if ($categories) {
				$i = sizeof($categories);
				foreach($categories as $cat) {					
					if($i > 1) {
						echo $cat->name . ', ';
					}else {
						echo $cat->name . ' ';
					}
				}
			}
			?>	
		</span>
	</div>
</li>
