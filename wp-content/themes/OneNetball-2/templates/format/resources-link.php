<?php
/**
 * Post Format - resources link
 *
 * @package     wpbuilder
 * @version     v.1.0
 */

$post_id = get_the_ID();
$title = get_the_title();
$content = substr(get_the_content(), 0, 50);
$read_more = get_permalink();
$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium_post' )[0];
$topics = get_the_tags();
$excerpt = get_the_excerpt();
$birth_place = get_post_meta($post_id, 'birth_place')[0];
$curent_club = get_post_meta($post_id, 'current_club')[0];   	
$program_name = get_post_meta($post_id, 'program_name')[0]; 
$pdf_link = get_post_meta($post_id, 'pdf_link')[0];  
$page_short_description = get_post_meta($post_id, 'page_short_description')[0];  
$site_link = get_post_meta($post_id, 'site_link')[0];  

?>

<li class="link">
	<div class="titles">
	<a href="" title="" class="title-link"><?php echo $title; ?></a>
		<span class="category">			
			<?php 
			if ($topics) {
				$i = sizeof($topics);
				foreach($topics as $topic) {					
					if($i > 1) {
						echo $topic->name . ', ';
					}else {
						echo $topic->name . ' ';
					}
				}
			}
			?>
		</span>
	</div>
	<?php if ($program_name): ?>
		<a href="" title="" class="category-link"><?php echo $program_name; ?></a>
	<?php endif ?>
	<p><?php echo $page_short_description; ?></p>
	<?php if (!empty($site_link)): ?>
			<a href="<?php echo $site_link; ?>" title="" class="site-btn red">VISIT SITE</a>
	<?php endif ?>

</li>
