<?php
/**
 * Post Format - resources video
 *
 * @package     wpbuilder
 * @version     v.1.0
 */

$post_id = get_the_ID();
$title = get_the_title();
$content = substr(get_the_content(), 0, 50);
$read_more = get_permalink();
$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium_post_ss' )[0];
$topics = get_the_tags();
$excerpt = get_the_excerpt();
$birth_place = get_post_meta($post_id, 'birth_place')[0];
$curent_club = get_post_meta($post_id, 'current_club')[0];   	

?>

<li class="video-result">
	<a href="<?php echo $read_more; ?>" title="" class="video-link">
		<img src="<?php echo $thumbnail; ?>" alt="" width="358" height="230"/>
	</a>
	<div>
		<a href="<?php echo $read_more; ?>" title="" class="title-link"><?php echo $title; ?></a>
		<span class="category">			
			<?php 
			if ($topics) {
				$i = sizeof($topics);
				foreach($topics as $topic) {					
					if($i > 1) {
						echo $topic->name . ', ';
					}else {
						echo $topic->name . ' ';
					}
				}
			}
			?>
		</span>
	</div>
</li>
