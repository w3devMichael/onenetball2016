<?php
/**
 * Post Format - ambassador standard
 *
 * @package     wpbuilder
 * @version     v.1.0
 */

$post_id = get_the_ID();
$title = get_the_title();
$content = substr(get_the_content(), 0);
$read_more = get_permalink();
$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium_post' )[0];
$birth_place = get_post_meta($post_id, 'birth_place', true);
$curent_club = get_post_meta($post_id, 'current_club')[0];   	
$short_descrition = get_post_meta($post_id,'page_short_description')[0];
?>

<li>
	<div href="" title="" class="image-link">
		<img src="<?php echo $thumbnail; ?>" alt="" width="239" height="239"/> 
	</div>
	<div title="" class="title-link"><?php echo $title; ?></div>
	<!-- <strong>Born: <?php echo $birth_place; ?></strong>
	<span><?php echo $curent_club; ?></span> -->
	<p><?php echo $content ; ?></p>
</li> 