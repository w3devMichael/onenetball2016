<?php if(!is_user_logged_in()) : ?>

    <?php 
    $url = get_home_url();
    $attributes['lost_password_sent'] = isset( $_REQUEST['checkemail'] ) && $_REQUEST['checkemail'] == 'confirm';
    $attributes['password_updated'] = isset( $_REQUEST['password'] ) && $_REQUEST['password'] == 'changed';
    $error_code =  $_REQUEST['error'];
    ?>

    <div class="password-lost-form-box">

        <?php if ( $attributes['password_updated'] ) : ?>
            <p class="login-info">
                <?php _e( 'Your password has been changed. You can sign in now.', 'wpbuilder' ); ?>
                <a href="<?php echo home_url( 'login' ); ?>">Click here to Login</a>
            </p>
        <?php else : ?>

            <?php if ( $attributes['lost_password_sent'] ) : ?>

            <p class="login-info">
                <?php _e( 'Check your email for a link to reset your password.', 'wpbuilder' ); ?>
            </p>

            <?php elseif(!empty($error_code))  :

                switch ( $error_code ) {
                    case "expiredkey":
                        echo "The password reset link expired.";
                        break;
                    case "invalidkey":
                        echo "The password reset link you used is not valid anymore.";
                        break;
                    case "password_reset_mismatch":
                        echo "The two passwords you entered don't match.";
                        break;
                    case "password_reset_empty":
                        echo "Sorry, we don't accept empty passwords.";  
                        break;         
                    default:
                         echo "An unknown error occurred. Please try again later.";
                    break;
                }    

            else : ?>

            <div id="password-lost-form"  class="">

                <?php if ( $attributes['show_title'] ) : ?>
                    <h3><?php _e( 'Forgot Your Password?', 'wpbuilder' ); ?></h3>
                <?php else: ?>
                    <h3><?php _e( 'Lost Password Form', 'wpbuilder' ); ?></h3>
                <?php endif; ?> 
             
                <p>
                    <?php
                        _e( "Enter your email address and we'll send you a link you can use to pick a new password.", 'personalize_login' );
                    ?>
                </p>

                <form id="lostpasswordform" class="site-form" action="<?php echo wp_lostpassword_url($url); ?>" method="post">

                    <ol>
                        <li>
                            <input type="text" name="user_login" id="user_login" placeholder="Email">
                        </li>  
                        <li>
                            <input type="submit" name="submit" class="lostpassword-button" value="<?php _e( 'Reset Password', 'wpbuilder' ); ?>"/>
                        </li>
                    </ol>        
                     
                </form>

            </div>

            <?php endif; ?>

        <?php endif; ?>

    </div>

<?php endif; ?>