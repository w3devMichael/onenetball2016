<?php if(!is_user_logged_in()) : ?>


<div class="password-reset-form-box">
    <?php if ( $attributes['show_title'] ) : ?>
        <h3><?php _e( 'Pick a New Password', 'wpbuilder' ); ?></h3>

    <?php else: ?>
        <h3><?php _e( 'Pick a New Password', 'wpbuilder' ); ?></h3>
    <?php endif; ?>
    <?php if ( count( $attributes['errors'] ) > 0 ) : ?>
        <?php foreach ( $attributes['errors'] as $error ) : ?>
            <p>
                <?php echo $error; ?>
            </p>
        <?php endforeach; ?>
    <?php endif; ?>

    <form name="resetpassform" class="site-form" id="resetpassform" action="<?php echo site_url( 'wp-login.php?action=resetpass' ); ?>" method="post" autocomplete="off">
            <input type="hidden" id="user_login" name="rp_login" value="<?php echo esc_attr( $attributes['login'] ); ?>" autocomplete="off" />
            <input type="hidden" name="rp_key" value="<?php echo esc_attr( $attributes['key'] ); ?>" />
        <ol>
            <li>
                <label for="pass1"><?php _e( 'New password', 'wpbuilder' ) ?></label>
                <input type="password" name="pass1" id="pass1" class="input" size="20" value="" autocomplete="off" /> 
            </li>
            <li>
                <label for="pass2"><?php _e( 'Repeat new password', 'wpbuilder' ) ?></label>
                <input type="password" name="pass2" id="pass2" class="input" size="20" value="" autocomplete="off" />   
            </li>
            <li>
                <input type="submit" name="submit" id="resetpass-button"  class="button" value="<?php _e( 'Reset Password', 'wpbuilder' ); ?>" />   
            </li>
        </ol>  
    </form>

   <p class="description"><?php echo wp_get_password_hint(); ?></p>

</div>

<?php endif; ?>
