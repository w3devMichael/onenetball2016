<?php if(!is_user_logged_in()) : ?>

<section class="newsletter-form">
    <h2>SUBSCRIBE TO THE ONE NETBALL NEWSLETTER</h2> 
        <div class="site-form">
            
            <?php 
                $page_description = ot_get_option('newsletter_shortcode'); 

                echo do_shortcode($page_description);   
            ?>

        </div>       
    </form>
</section>
<?php endif; ?>