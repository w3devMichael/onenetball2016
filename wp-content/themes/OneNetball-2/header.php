<!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->
<head>

	<!--[if IE]>
	    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	<![endif]-->	

	<link rel="icon" href="<?php echo ot_get_option('login_logo'); ?>" type="image/x-icon" />

	<link rel="shortcut icon" href="<?php echo ot_get_option('login_logo'); ?>" type="image/x-icon" />

	<link rel="apple-touch-icon" href="<?php echo ot_get_option('apple-touch'); ?>">

	<meta charset="<?php bloginfo( 'charset' ); ?>" />

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">

	

	<meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">	

	<?php
		if (is_search())
			echo '<meta name="robots" content="noindex, nofollow" />';
	?>	

	<title><?php wp_title( ' | ', true, 'right' ); ?></title>

	<!-- Load Scripts -->

	<?php wp_head(); ?>

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
    
	<link rel="profile" href="http://gmpg.org/xfn/11" />

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />	

</head>

<body <?php body_class(); ?>>
<!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

  <header class="site-header">
            <div class="top">
                <nav class="breadcrumbs">
                   <ul class="clearfix">
                   	<?php wpbuilder_breadcrumbs(); ?>
                   </ul>
                </nav>
                <div class="user-area">
                    <a href="javascript:;" title="" class="newsletter-btn">Subscribe to Newsletter</a>
                    <?php if(is_user_logged_in()) : 
                        global $current_user;
                        get_currentuserinfo();
                    ?>
                        <div class="logout-menu login-btn">Your Account
                            <ul class="sub-menu">
                                <li class="drop-down-item"><a href="<?php echo home_url('past-results'); ?>">See your previous results</a></li>
                                <li class="drop-down-item"><a href="<?php echo home_url('update-details'); ?>">Update your contact details</a></li>
                                <li class="drop-down-item"><a href="<?php echo home_url('7-pillars-of-inclusion'); ?>">Inclusion Action Survey</a></li>
                                <li class="drop-down-item"><a href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a></li>
                            </ul>
                        </div>
                    <?php else: ?>
                    <a href="<?php echo home_url( 'login'); ?>" title="" class="login-btn">Survey Log In</a>
                    <?php endif; ?>
                    <button type="button" class="mobile-menu-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                        <strong>Menu</strong>
                    </button>
                    <span class="menu-overlay"></span>
                </div>
                <br class="clear"/>
            </div>
            <div class="bottom">
                <div class="logos">
                    <a href="<?php echo ot_get_option('header_logo_1_url');  ?>" title="">
                        <img src="<?php echo ot_get_option('header_logo_1'); ?>" alt="" width="52" height="52"/>
                        <strong class="sr-only">Australia post</strong>
                    </a>           
                    <a href="<?php echo get_home_url(); ?>" title="">
                        <img src="<?php echo ot_get_option('header_logo_2'); ?>" alt="" width="114" height="52"/>
                        <strong class="sr-only">One Netball</strong>
                    </a>              
                    <a href="<?php echo ot_get_option('header_logo_3_url');  ?>" title="">
                        <img src="<?php echo ot_get_option('header_logo_3'); ?>" alt="" width="52" height="52"/>
                        <strong class="sr-only">Netball australia</strong>
                    </a>
                </div>
                <nav class="site-menu">
                    <h2 class="mobile-menu-title">EXPLORE ONE NETBALL</h2>
                    <?php
                    $args = array(
                        'menu_class' => 'clearfix nav',        
                        'menu' => '',
                        'container' => false,
                        'theme_location' => 'main-menu',
                        );
                    wp_nav_menu( $args ); 
                    ?> 
                </nav>   
                <br class="clear"/>                
            </div>
        </header>