<?php
/**
 * Template Name: Login / Regsiter Page
 *
 * @package wpbuilder
 * @subpackage wpbuilder
 * @since wpbuilder
 */

get_header(); 

$state_array = array('ACT', 'New South Wales', 'Northern Territory', 'Queensland', 'South Australia', 'Tasmania', 'Victoria', 'Western Australia');

$club_role_array = array('Coach', 'Umpire', 'Committee member', 'President', 'Parent', 'Other');

$lost_password_link = get_page_by_title('Lost Password');

$page_title = ot_get_option('login_page_title'); 

$page_description = ot_get_option('login_page_mode_desc'); 

$login_page_logo_1 = ot_get_option('login_page_logo_1'); 

$login_page_logo_2 = ot_get_option('login_page_logo_2'); 

?>

<section class="page-accent">
    <div class="wrapper">
        <div class="left">
            <h1 class="page-accent-title"><?php echo $page_title; ?></h1>
            <p><?php echo $page_description; ?></p>
        </div>
        <div class="right logos">
            <img src="<?php echo $login_page_logo_1; ?>" alt="" width="275" height="55"/>
            <img src="<?php echo $login_page_logo_2; ?>" alt="" width="182" height="170"/>
        </div>
    </div>
</section>


<?php if(!is_user_logged_in()) : ?>

<section class="login-section wrapper two-columns-story">

    <h2 class="sr-only">Log in or Sign up</h2>
    <div class="form-errors">
        <strong>There was an error with your submission:</strong>
        <ul>
        </ul>
        <span>Please try again and re-submit</span>
    </div>

    <div class="left"> 
    
        <?php show_error_messages(); ?>

        <form method="post" action="" class="site-form">      
            <fieldset>
                <h2 class="site-legend black">LOG IN</h2>

                <div class="intro-block">
                    <p>Welcome back to the Inclusion Action Survey!<p>
                    <br>
                    <p>Log in below and complete the Inclusion Action Survey again, to see how you are tracking towards your path to inclusion success!</p>
                    <br>
                    <p>Be sure to check your progress in the ‘Previous Results’ page!</p>
                </div>

                <ol>
                    <li>
                        <label for="" class="sr-only">Email*</label>
                        <input type="text" name="user_login" placeholder="Email*" id="user_login"/>
                    </li>    
                    <li>
                        <label for="" class="sr-only">Password*</label>
                        <input type="password" name="user_pass" placeholder="Password*" id="user_pass"/>
                    </li>    
                    <li class="sign-in-buttoms">
                        <a href="<?php echo $lost_password_link->guid; ?>" title="">Forgot password?</a>
                        <input type="hidden" name="login_nonce" value="<?php echo wp_create_nonce('login-nonce'); ?>"/>
                        <button type="submit" id="login_submit" class="site-btn">LOG IN <i class="icon-arrow-right"></i></button>
                    </li>
                </ol>
            </fieldset>
        </form>
    </div>

    <div class="right">
        <div class="form-success" id="form-success">
            <h2>You have succesfuly completed the registration process. You can now log-in.</h2>
        </div>
        <form method="post" action="" class="site-form" id="sign-up-form">      
            <fieldset>
                <h2 class="site-legend black">SIGN UP</h2>

                <div class="intro-block">
                    <p>This is your first time at the Inclusion Action Survey! Welcome!</p>
                    <br>
                    <p>This Survey measures your progress through the <a class="link" href="<?php echo home_url('inclusion-action-plan'); ?>">Inclusion Action Plan.</a></p>
                    <br>
                    <p>As you complete actions in your Plan, come back and mark each task
                    as ‘Completed’ so you can improve your Survey results!</p>
                    <br>
                    <p>Learn more about the Inclusion Action Survey, or get started by
                    signing up below!</p>
                    <br>
                    <p class="block-quote">Please note: Your Inclusion Action Survey log in details are different to your MyNetball details.</p>
                </div>
                <ol>
                    <li>
                        <label for="" class="sr-only">Email address*</label>
                        <input type="email" name="" placeholder="Email address*" value="" id="reg-email"/>
                    </li>    
                    <li>
                        <label for="" class="sr-only">Create a password*</label>
                        <input type="password" name="" placeholder="Create a password*" value="" id="reg-pass" style="background: #FFF !important"/>
                        <div class="field-help">Password must be at least 7 characters long. To make it stronger, use upper and lower case letters, numbers &amp; symbols.</div>
                    </li>        
                    <li>
                        <label for="" class="sr-only">Confirm password*</label>
                        <input type="password" name="" placeholder="Confirm password*" value="" id="reg-pass-repeat"/>
                        <div class="field-help">Type your password again.</div>
                        <!-- set password strength as percent width of the span below -->
                        <div class="password-meter">
                            <div>Password strength indicator</div>
                            <span></span>
                        </div>
                    </li>   
                    <li>
                        <label for="" class="sr-only">Username*</label>
                        <input type="text" name="" placeholder="Username*" value="" id="reg-username"/>
                    </li>  
                    <li>
                        <label for="" class="sr-only">Name of Club or Association*</label>
                        <input type="text" name="" placeholder="Name of Club or Association*" value="" id="reg-club"/>
                    </li>      
                    <li>
                        <label for="" class="sr-only">Select a state*</label>
                        <div class="styled-select">
                            <select name="" id="reg-state">
                                <option value="">Select a state*</option>
                                <?php 
                                    foreach ( $state_array as $state) {
                                        echo ' <option value="'.$state.'">'.$state.'</option>';
                                    }
                                 ?>
                            </select>
                        </div>
                    </li>           
                    <li>
                        <label for="" class="sr-only">Your role at club or association* </label>
                        <div class="styled-select">
                            <select name="" id="reg-role">
                                <option value="">Your role at club or association* </option>
                                <?php 
                                    foreach ( $club_role_array  as $club_role) {
                                        echo ' <option value="'.$club_role.'">'.$club_role.'</option>';
                                    }
                                 ?>
                            </select>
                        </div>
                    </li>    
                    <li class="subscribe-fields">
                        <h2>STAY UP TO DATE WITH ONE NETBALL.</h2>
                        <span>Would you like to receive occasional updates?</span>
                        
                        <label class="styled-checkbox" style="display: table; margin: 10px auto;">
                            <input type="checkbox" name="mc4wp-subscribe" id="updates-required" value="" />
                            <span>Yes please, that’d be great!</span>
                        </label>                        
                    </li>     
                    <li class="centered">
                        <button type="submit" id="submit-registraiton" class="site-btn block">SIGN UP <i class="icon-arrow-right"></i></button>
                    </li>
                </ol>
            </fieldset>
        </form>
    </div>
</section>

<?php 
    get_template_part( 'templates/content', 'newsletter' ); 
?>


<script>

    (function($) {

        $('#reg-pass').complexify({}, function (valid, complexity) {

            $('.password-meter span').css({ "width" : Math.round(complexity) + '%' });
        });

    })(jQuery);

    (function( $ ) {

        $('#submit-registraiton').click(function(event) {            
            event.preventDefault();

            var email = $('#reg-email').val();
            var password = $('#reg-pass').val();
            var passwordRepeat = $('#reg-pass-repeat').val();
            var username = $('#reg-username').val();
            var clubName = $('#reg-club').val();
            var state = $('#reg-state').val();
            var clubRole = $('#reg-role').val();
            var updatesRequired = $('#updates-required').prop('checked');

            


            registerUserAjax(email, password, passwordRepeat,username, clubName, state, clubRole, updatesRequired);

        });

        function registerUserAjax(email, password, passwordRepeat, username, clubName, state, clubRole, updatesRequired){    

            var  data = new FormData();   
            data.append( 'email', email);
            data.append( 'password', password);
            data.append( 'passwordRepeat', passwordRepeat);
            data.append( 'username', username);
            data.append( 'clubName', clubName);
            data.append( 'state', state);
            data.append( 'clubRole', clubRole);
            data.append( 'updatesRequired', updatesRequired);
            data.append('action', 'user_registration_ajax'); 

            jQuery.ajax({
                type:"post",
                url: "<?php echo admin_url('admin-ajax.php'); ?>",
                data: data,  
                contentType: false,
                processData: false,                         
                beforeSend: function () {

                },
                complete: function () {

                },                      
                success: function(response){

                    console.log(response);

                    var resOBJ = JSON.parse(response);
                    var status = resOBJ.status;

                    // console.log(response);

                    if(status == 1) {
                        $('.form-errors').hide(); 
                        $('#sign-up-form').remove();     
                        $('#form-success').show();

                        window.location = '<?php echo get_site_url(''). '/inclusion-action-survey/'; ?>';

                    } else {
                        $('.form-errors').show();
                        $('.form-errors ul').empty();
                        $('.form-errors ul').append(resOBJ.input_errors);

                        var fieldStauts = resOBJ.files_warnings;

                        if( jQuery.isEmptyObject(fieldStauts) ) {
                            $('.form-errors').hide();
                        }else {
                            if ( fieldStauts.email === 1 ) {
                                $('#reg-email').addClass("failed");
                            }else {
                                $('#reg-email').removeClass("failed");
                            }

                            if ( fieldStauts.password === 1 ) {
                                $('#reg-pass').addClass("failed");
                            }else {
                                $('#reg-pass').removeClass("failed");
                            }

                            if ( fieldStauts.passwordRepeat === 1 ) {
                                $('#reg-pass-repeat').addClass("failed");
                            }else {
                                $('#reg-pass-repeat').removeClass("failed");
                            }

                            if ( fieldStauts.username === 1 ) {
                                $('#reg-username').addClass("failed");
                            }else {
                                $('#reg-username').removeClass("failed");
                            }

                            if ( fieldStauts.clubName === 1 ) {
                                $('#reg-club').addClass("failed");
                            }else {
                                $('#reg-club').removeClass("failed");
                            }

                            if ( fieldStauts.clubState === 1 ) {
                                $('#reg-state').addClass("failed");
                            }else {
                                $('#reg-state').removeClass("failed");
                            }

                            if ( fieldStauts.clubRole === 1 ) {
                                $('#reg-role').addClass("failed");
                            }else {
                                $('#reg-role').removeClass("failed");
                            }

                        }
                    }                      
                }
            });
            return false;
        }

    })( jQuery );

</script>

<?php endif;

?>


<?php get_footer(); ?>
