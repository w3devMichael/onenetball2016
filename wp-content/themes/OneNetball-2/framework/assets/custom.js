jQuery(document).ready(function ($) {

	// Theme Options
	$('#page-ot_theme_options h3.label, .format-setting-label label, .format-setting-label .label').prepend('<i class="doc-text_T20"></i>');
	$('#page-ot_theme_options .option-tree-ui-buttons').append('<div class="save_btn"><span class="sec_message"></span><button class="option-tree-ui-button button button-primary right">Save Changes</button></div><div class="s_message"><div class="inner_sm"><i class="ok_T20"></i>Options was updated successfully.</div></div><div class="e_message"><div class="inner_em">Error while connecting to DB <br /> Please try again.</div></div>');
	var save_btn = $('#page-ot_theme_options .option-tree-ui-buttons .option-tree-ui-button.button-primary').html();
	var popupStatus = 0,
		windowWidth = document.documentElement.clientWidth,
		windowHeight = document.documentElement.clientHeight,
		e_message = $(".e_message").height(),
		e_messages = $(".e_message").width(),
		s_message = $(".s_message").height(),
		s_messages = $(".s_message").width();
	$(".e_message").css({
		"top": windowHeight / 2 - e_message / 2,
		"left": windowWidth / 2 - e_messages / 2
	});
	$(".s_message").css({
		"top": windowHeight / 2 - s_message / 2,
		"left": windowWidth / 2 - s_messages / 2
	});

	$('#page-ot_theme_options #option-tree-settings-api').submit( function () {

		$('#page-ot_theme_options .option-tree-ui-buttons .option-tree-ui-button.button-primary').html('Saving <div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>');

		var b =  $(this).serialize();
		$.post( 'options.php', b ).error( 
			function() {
				$(".e_message").fadeIn();
				setTimeout(function () {
					$(".e_message").fadeOut();
				}, 3000);
				$('#page-ot_theme_options .option-tree-ui-buttons .option-tree-ui-button.button-primary').html(save_btn);
			}).success( function() {
				$(".s_message").fadeIn();
				$(".sec_message").fadeIn();
				$('.sec_message').html('Options was updated.');
				setTimeout(function () {
					$(".s_message, .sec_message").fadeOut();
				}, 3000);
				$('#page-ot_theme_options .option-tree-ui-buttons .option-tree-ui-button.button-primary').html(save_btn);
			}
		);

		return false;
	});

	// More ...
	$('#page-ot_theme_options h2').prepend('<i class="tools_T20"></i>');
	$('.option-tree-ui-radio-images').append('<i class="ok_T20"></i>');
	if ($('.option-tree-ui-radio-image').hasClass('option-tree-ui-radio-image-selected')) {
		$('.option-tree-ui-radio-image-selected').parent().addClass('selected');
	}
	function checkForChangesRadio(){
		if ($('.option-tree-ui-radio-image').hasClass('option-tree-ui-radio-image-selected')) {
			$('.option-tree-ui-radio-image-selected').parent().addClass('selected');
		}
	}
	$('.option-tree-ui-radio-image').click(function() {
		$('.option-tree-ui-radio-image').parent().removeClass('selected');
		setTimeout(checkForChangesRadio, 10);
	});
	
	var formss = '<input id="searchbox" type="text" placeholder="Search" />';
	$(formss).insertBefore('.type-checkbox .format-setting-inner');
	// case insensitive ':contains' selector
	jQuery.expr[':'].Contains = function(a, i, m) {
		return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
	};
	
	$('#searchbox').on('keyup', function() {
		var w = $(this).val();
		if (w) {
			$('.type-checkbox .format-setting-inner label').parent().hide();
			$('.type-checkbox .format-setting-inner label:Contains('+w+')').parent().show();
		} else {
			$('.type-checkbox .format-setting-inner label').parent().show();
		}
	});

	var videotype = $('#setting_video_type input[type=radio]');
	var videotypeCH = $('#setting_video_type input[type=radio]:checked');
	jQuery(videotype).change(function(){
		if ( jQuery(this).val() === 'custom' ) {
			jQuery('#setting_youtube_id, #setting_vimeo_id, #setting_video_lightbox').slideUp();
			jQuery('#setting_video_code').slideDown();
		} else if ( jQuery(this).val() === 'vimeo' ) {
			jQuery('#setting_youtube_id, #setting_video_code').slideUp();
			jQuery('#setting_vimeo_id, #setting_video_lightbox').slideDown();
		} else {
			jQuery('#setting_video_code, #setting_vimeo_id').slideUp();
			jQuery('#setting_youtube_id, #setting_video_lightbox').slideDown();
		}
	});
	if ( jQuery(videotypeCH).val() === 'custom' ) {
		jQuery('#setting_youtube_id, #setting_vimeo_id, #setting_video_lightbox').slideUp();
		jQuery('#setting_video_code').slideDown();
	} else if ( jQuery(videotypeCH).val() === 'vimeo' ) {
		jQuery('#setting_youtube_id, #setting_video_code').slideUp();
		jQuery('#setting_vimeo_id, #setting_video_lightbox').slideDown();
	} else {
		jQuery('#setting_video_code, #setting_vimeo_id').slideUp();
		jQuery('#setting_youtube_id, #setting_video_lightbox').slideDown();
	}

	var rx_player = $('#setting_rx_player input[type=radio]');
	var rx_playerCH = $('#setting_rx_player input[type=radio]:checked');
	jQuery(rx_player).change(function(){
		if ( jQuery(this).val() === 'custom_player' ) {
			jQuery('#setting_playlist, #setting_auto_play, #setting_showcover').slideUp();
			jQuery('#setting_custom_player').slideDown();
		} else {
			jQuery('#setting_custom_player').slideUp();
			jQuery('#setting_playlist, #setting_auto_play, #setting_showcover').slideDown();
		}
	});
	if ( jQuery(rx_playerCH).val() === 'custom_player' ) {
		jQuery('#setting_playlist, #setting_auto_play, #setting_showcover').slideUp();
		jQuery('#setting_custom_player').slideDown();
	} else {
		jQuery('#setting_custom_player').slideUp();
		jQuery('#setting_playlist, #setting_auto_play, #setting_showcover').slideDown();
	}

	jQuery(rx_player).change(function(){
		if ( jQuery(this).val() === 'rx_manager' ) {
			jQuery('#sm_meta').slideDown();
		} else {
			jQuery('#sm_meta').slideUp();
		}
	});
	if ( jQuery(rx_playerCH).val() === 'custom_player' ) {
			jQuery('#sm_meta').slideDown();
	} else {
			jQuery('#sm_meta').slideUp();
	}

	var pageCover = $('#setting_page_cover_type input[type=radio]');
	var pageCoverCH = $('#setting_page_cover_type input[type=radio]:checked');
	jQuery(pageCover).change(function(){
		if ( jQuery(this).val() === '2' || jQuery(this).val() === '1' ) {
			jQuery('#setting_page_cover_image, #setting_page_cover_slider, #setting_page_cover_master').slideUp();
		} else if ( jQuery(this).val() === '3' ) {
			jQuery('#setting_page_cover_image').slideDown(); jQuery('#setting_page_cover_slider, #setting_page_cover_master').slideUp();
		} else if ( jQuery(this).val() === '4' ) {
			jQuery('#setting_page_cover_slider').slideDown(); jQuery('#setting_page_cover_image, #setting_page_cover_master').slideUp();
		} else {
			jQuery('#setting_page_cover_master').slideDown(); jQuery('#setting_page_cover_image, #setting_page_cover_slider').slideUp();
		}
	});
	if ( jQuery(pageCoverCH).val() === '2' || jQuery(pageCoverCH).val() === '1' ) {
		jQuery('#setting_page_cover_image, #setting_page_cover_slider, #setting_page_cover_master').slideUp();
	} else if ( jQuery(pageCoverCH).val() === '3' ) {
		jQuery('#setting_page_cover_image').slideDown(); jQuery('#setting_page_cover_slider, #setting_page_cover_master').slideUp();
	} else if ( jQuery(pageCoverCH).val() === '4' ) {
		jQuery('#setting_page_cover_slider').slideDown(); jQuery('#setting_page_cover_image, #setting_page_cover_master').slideUp();
	} else {
		jQuery('#setting_page_cover_master').slideDown(); jQuery('#setting_page_cover_image, #setting_page_cover_slider').slideUp();
	}

	var artistsCover = $('#setting_artists_cover_type input[type=radio]');
	var artistsCoverCH = $('#setting_artists_cover_type input[type=radio]:checked');
	jQuery(artistsCover).change(function(){
		if ( jQuery(this).val() === '2' || jQuery(this).val() === '1' ) {
			jQuery('#setting_artists_cover_image, #setting_artists_cover_slider, #setting_artists_cover_master').slideUp();
		} else if ( jQuery(this).val() === '3' ) {
			jQuery('#setting_artists_cover_image').slideDown(); jQuery('#setting_artists_cover_slider, #setting_artists_cover_master').slideUp();
		} else if ( jQuery(this).val() === '4' ) {
			jQuery('#setting_artists_cover_slider').slideDown(); jQuery('#setting_artists_cover_image, #setting_artists_cover_master').slideUp();
		} else {
			jQuery('#setting_artists_cover_master').slideDown(); jQuery('#setting_artists_cover_image, #setting_artists_cover_slider').slideUp();
		}
	});
	if ( jQuery(artistsCoverCH).val() === '2' || jQuery(artistsCoverCH).val() === '1' ) {
		jQuery('#setting_artists_cover_image, #setting_artists_cover_slider, #setting_artists_cover_master').slideUp();
	} else if ( jQuery(artistsCoverCH).val() === '3' ) {
		jQuery('#setting_artists_cover_image').slideDown(); jQuery('#setting_artists_cover_slider, #setting_artists_cover_master').slideUp();
	} else if ( jQuery(artistsCoverCH).val() === '4' ) {
		jQuery('#setting_artists_cover_slider').slideDown(); jQuery('#setting_artists_cover_image, #setting_artists_cover_master').slideUp();
	} else {
		jQuery('#setting_artists_cover_master').slideDown(); jQuery('#setting_artists_cover_image, #setting_artists_cover_slider').slideUp();
	}

	var wooCover = $('#setting_woo_cover_type input[type=radio]');
	var wooCoverCH = $('#setting_woo_cover_type input[type=radio]:checked');
	jQuery(wooCover).change(function(){
		if ( jQuery(this).val() === '2' ) {
			jQuery('#setting_woo_cover_image, #setting_woo_cover_slider, #setting_woo_cover_master').slideUp();
		} else if ( jQuery(this).val() === '3' ) {
			jQuery('#setting_woo_cover_image').slideDown(); jQuery('#setting_woo_cover_slider, #setting_woo_cover_master').slideUp();
		} else if ( jQuery(this).val() === '4' ) {
			jQuery('#setting_woo_cover_slider').slideDown(); jQuery('#setting_woo_cover_image, #setting_woo_cover_master').slideUp();
		} else {
			jQuery('#setting_woo_cover_master').slideDown(); jQuery('#setting_woo_cover_image, #setting_woo_cover_slider').slideUp();
		}
	});
	if ( jQuery(wooCoverCH).val() === '2' ) {
		jQuery('#setting_woo_cover_image, #setting_woo_cover_slider, #setting_woo_cover_master').slideUp();
	} else if ( jQuery(wooCoverCH).val() === '3' ) {
		jQuery('#setting_woo_cover_image').slideDown(); jQuery('#setting_woo_cover_slider, #setting_woo_cover_master').slideUp();
	} else if ( jQuery(wooCoverCH).val() === '4' ) {
		jQuery('#setting_woo_cover_slider').slideDown(); jQuery('#setting_woo_cover_image, #setting_woo_cover_master').slideUp();
	} else {
		jQuery('#setting_woo_cover_master').slideDown(); jQuery('#setting_woo_cover_image, #setting_woo_cover_slider').slideUp();
	}

	var songsCover = $('#setting_songs_cover_type input[type=radio]');
	var songsCoverCH = $('#setting_songs_cover_type input[type=radio]:checked');
	jQuery(songsCover).change(function(){
		if ( jQuery(this).val() === '2' || jQuery(this).val() === '1' ) {
			jQuery('#setting_songs_cover_image, #setting_songs_cover_slider, #setting_songs_cover_master').slideUp();
		} else if ( jQuery(this).val() === '3' ) {
			jQuery('#setting_songs_cover_image').slideDown(); jQuery('#setting_songs_cover_slider, #setting_songs_cover_master').slideUp();
		} else if ( jQuery(this).val() === '4' ) {
			jQuery('#setting_songs_cover_slider').slideDown(); jQuery('#setting_songs_cover_image, #setting_songs_cover_master').slideUp();
		} else {
			jQuery('#setting_songs_cover_master').slideDown(); jQuery('#setting_songs_cover_image, #setting_songs_cover_slider').slideUp();
		}
	});
	if ( jQuery(songsCoverCH).val() === '2' || jQuery(songsCoverCH).val() === '1' ) {
		jQuery('#setting_songs_cover_image, #setting_songs_cover_slider, #setting_songs_cover_master').slideUp();
	} else if ( jQuery(songsCoverCH).val() === '3' ) {
		jQuery('#setting_songs_cover_image').slideDown(); jQuery('#setting_songs_cover_slider, #setting_songs_cover_master').slideUp();
	} else if ( jQuery(songsCoverCH).val() === '4' ) {
		jQuery('#setting_songs_cover_slider').slideDown(); jQuery('#setting_songs_cover_image, #setting_songs_cover_master').slideUp();
	} else {
		jQuery('#setting_songs_cover_master').slideDown(); jQuery('#setting_songs_cover_image, #setting_songs_cover_slider').slideUp();
	}

	var videosCover = $('#setting_videos_cover_type input[type=radio]');
	var videosCoverCH = $('#setting_videos_cover_type input[type=radio]:checked');
	jQuery(videosCover).change(function(){
		if ( jQuery(this).val() === '2' || jQuery(this).val() === '1' ) {
			jQuery('#setting_videos_cover_image, #setting_videos_cover_slider, #setting_videos_cover_master').slideUp();
		} else if ( jQuery(this).val() === '3' ) {
			jQuery('#setting_videos_cover_image').slideDown(); jQuery('#setting_videos_cover_slider, #setting_videos_cover_master').slideUp();
		} else if ( jQuery(this).val() === '4' ) {
			jQuery('#setting_videos_cover_slider').slideDown(); jQuery('#setting_videos_cover_image, #setting_videos_cover_master').slideUp();
		} else {
			jQuery('#setting_videos_cover_master').slideDown(); jQuery('#setting_videos_cover_image, #setting_videos_cover_slider').slideUp();
		}
	});
	if ( jQuery(videosCoverCH).val() === '2' || jQuery(videosCoverCH).val() === '1' ) {
		jQuery('#setting_videos_cover_image, #setting_videos_cover_slider, #setting_videos_cover_master').slideUp();
	} else if ( jQuery(videosCoverCH).val() === '3' ) {
		jQuery('#setting_videos_cover_image').slideDown(); jQuery('#setting_videos_cover_slider, #setting_videos_cover_master').slideUp();
	} else if ( jQuery(videosCoverCH).val() === '4' ) {
		jQuery('#setting_videos_cover_slider').slideDown(); jQuery('#setting_videos_cover_image, #setting_videos_cover_master').slideUp();
	} else {
		jQuery('#setting_videos_cover_master').slideDown(); jQuery('#setting_videos_cover_image, #setting_videos_cover_slider').slideUp();
	}

	var eventsCover = $('#setting_events_cover_type input[type=radio]');
	var eventsCoverCH = $('#setting_events_cover_type input[type=radio]:checked');
	jQuery(eventsCover).change(function(){
		if ( jQuery(this).val() === '2' || jQuery(this).val() === '1' ) {
			jQuery('#setting_events_cover_image, #setting_events_cover_slider, #setting_events_cover_master').slideUp();
		} else if ( jQuery(this).val() === '3' ) {
			jQuery('#setting_events_cover_image').slideDown(); jQuery('#setting_events_cover_slider, #setting_events_cover_master').slideUp();
		} else if ( jQuery(this).val() === '4' ) {
			jQuery('#setting_events_cover_slider').slideDown(); jQuery('#setting_events_cover_image, #setting_events_cover_master').slideUp();
		} else {
			jQuery('#setting_events_cover_master').slideDown(); jQuery('#setting_events_cover_image, #setting_events_cover_slider').slideUp();
		}
	});
	if ( jQuery(eventsCoverCH).val() === '2' || jQuery(eventsCoverCH).val() === '1' ) {
		jQuery('#setting_events_cover_image, #setting_events_cover_slider, #setting_events_cover_master').slideUp();
	} else if ( jQuery(eventsCoverCH).val() === '3' ) {
		jQuery('#setting_events_cover_image').slideDown(); jQuery('#setting_events_cover_slider, #setting_events_cover_master').slideUp();
	} else if ( jQuery(eventsCoverCH).val() === '4' ) {
		jQuery('#setting_events_cover_slider').slideDown(); jQuery('#setting_events_cover_image, #setting_events_cover_master').slideUp();
	} else {
		jQuery('#setting_events_cover_master').slideDown(); jQuery('#setting_events_cover_image, #setting_events_cover_slider').slideUp();
	}

	var galleryCover = $('#setting_gallery_cover_type input[type=radio]');
	var galleryCoverCH = $('#setting_gallery_cover_type input[type=radio]:checked');
	jQuery(galleryCover).change(function(){
		if ( jQuery(this).val() === '2' || jQuery(this).val() === '1' ) {
			jQuery('#setting_gallery_cover_image, #setting_gallery_cover_slider, #setting_gallery_cover_master').slideUp();
		} else if ( jQuery(this).val() === '3' ) {
			jQuery('#setting_gallery_cover_image').slideDown(); jQuery('#setting_gallery_cover_slider, #setting_gallery_cover_master').slideUp();
		} else if ( jQuery(this).val() === '4' ) {
			jQuery('#setting_gallery_cover_slider').slideDown(); jQuery('#setting_gallery_cover_image, #setting_gallery_cover_master').slideUp();
		} else {
			jQuery('#setting_gallery_cover_master').slideDown(); jQuery('#setting_gallery_cover_image, #setting_gallery_cover_slider').slideUp();
		}
	});
	if ( jQuery(galleryCoverCH).val() === '2' || jQuery(galleryCoverCH).val() === '1' ) {
		jQuery('#setting_gallery_cover_image, #setting_gallery_cover_slider, #setting_gallery_cover_master').slideUp();
	} else if ( jQuery(galleryCoverCH).val() === '3' ) {
		jQuery('#setting_gallery_cover_image').slideDown(); jQuery('#setting_gallery_cover_slider, #setting_gallery_cover_master').slideUp();
	} else if ( jQuery(galleryCoverCH).val() === '4' ) {
		jQuery('#setting_gallery_cover_slider').slideDown(); jQuery('#setting_gallery_cover_image, #setting_gallery_cover_master').slideUp();
	} else {
		jQuery('#setting_gallery_cover_master').slideDown(); jQuery('#setting_gallery_cover_image, #setting_gallery_cover_slider').slideUp();
	}

});