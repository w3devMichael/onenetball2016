<?php

add_filter( 'vc_load_default_templates', 'my_custom_template_at_first_position' ); // Hook in
 
function my_custom_template_at_first_position( $data ) {
    $template               = array();
    $template['name']       = __( 'Sex template', 'my-text-domain' ); // Assign name for your custom template
    $template['image_path'] = preg_replace( '/\s/', '%20', plugins_url( 'images/custom_template_thumbnail.jpg', __FILE__ ) ); // Always use preg replace to be sure that "space" will not break logic. Thumbnail should have this dimensions: 114x154px.
    $template['custom_class'] = 'custom_template_for_vc_custom_template'; // CSS class name
    $template['content']    = <<<CONTENT
        [vc_row][vc_column width="1/2"][vc_single_image border_color="grey" img_link_target="_self"][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.[/vc_column_text][/vc_column][vc_column width="1/2"][vc_message color="alert-info" style="rounded"]I am message box. Click edit button to change this text.[/vc_message][/vc_column][/vc_row]
CONTENT;
 
    array_unshift( $data, $template );
    return $data;
}

 