<?php

/*  Homepage Tabbbed Content
----------------------------------------------------*/

$more_settings1 = __("Tab 1", "wpbuilder");

$more_settings2 = __("Tab 2", "wpbuilder");

vc_map( array(
    "name"      => __("Homepage Tabbbed Content", "js_composer"),
    "base"      => "homepage_tabbed_content",
    "category"  => 'wpbuilder',
    "icon"      => get_template_directory_uri() . "/framework/visual-composer/assets/vc-icon.png", 
    "description"   => __('Homepage Tabbbed Content', 'js_composer'),
    "params"    => array(
        array(
            "type"      => "textfield",
            "heading"   => __("Extra Class", "wpbuilder"),
            "param_name"    => "extra_class",
            ),
        array(
            "type"      => "textfield",
            "heading"   => __("Tab 1 Name ", "wpbuilder"),
            "param_name"    => "tab_1_name",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "heading"   => __("Tab 2 Name ", "wpbuilder"),
            "param_name"    => "tab_2_name",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings1,
            "heading"   => __("Tab Title", "wpbuilder"),
            "param_name"    => "tab_title_1",
            "admin_label" => false
        ), 
        array(
            "type"      => "textfield",
            "group"         => $more_settings1,
            "heading"   => __("Tab Subtitle", "wpbuilder"),
            "param_name"    => "tab_subtitle_1",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings1,
            "heading"   => __("Step #1 Title", "wpbuilder"),
            "param_name"    => "step_1_title",
            "admin_label" => false
        ), 
        array(
            "type"      => "textfield",
            "group"         => $more_settings1,
            "heading"   => __("Step #1 Link", "wpbuilder"),
            "param_name"    => "step_1_link",
            "admin_label" => false
        ),   
        array(
            "type" => "attach_image",
            "group"         => $more_settings1,
            "heading" => __("Step #1 Image", "wpbuilder"),
            "param_name" => "step_1_image",
            "description" => __("")
        ),  
        array(
            "type"      => "textfield",
            "group"         => $more_settings1,
            "heading"   => __("Step #2 Title", "wpbuilder"),
            "param_name"    => "step_2_title",
            "admin_label" => false
        ), 
        array(
            "type"      => "textfield",
            "group"         => $more_settings1,
            "heading"   => __("Step #2 Link", "wpbuilder"),
            "param_name"    => "step_2_link",
            "admin_label" => false
        ),   
        array(
            "type" => "attach_image",
            "group"         => $more_settings1,
            "heading" => __("Step #2 Image", "wpbuilder"),
            "param_name" => "step_2_image",
            "description" => __("")
        ),  
        array(
            "type"      => "textfield",
            "group"         => $more_settings1,
            "heading"   => __("Step #3 Title", "wpbuilder"),
            "param_name"    => "step_3_title",
            "admin_label" => false
        ), 

        array(
            "type"      => "textfield",
            "group"         => $more_settings1,
            "heading"   => __("Step #3 Link", "wpbuilder"),
            "param_name"    => "step_3_link",
            "admin_label" => false
        ),   
        array(
            "type" => "attach_image",
            "group"         => $more_settings1,
            "heading" => __("Step #3 Image", "wpbuilder"),
            "param_name" => "step_3_image",
            "description" => __("")
        ),  

        array(
            "type"      => "textfield",
            "group"         => $more_settings1,
            "heading"   => __("Register for a workshop link", "wpbuilder"),
            "param_name"    => "workshop_link",
            "admin_label" => false
        ), 

        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("Tab Title", "wpbuilder"),
            "param_name"    => "tab_title_2",
            "admin_label" => false
        ), 
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("Tab Subtitle", "wpbuilder"),
            "param_name"    => "tab_subtitle_2",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("Inclusion Quiz URL", "wpbuilder"),
            "param_name"    => "inclusion_quiz_url",
            "admin_label" => false
        ),
        array(
            "type"      => "textarea_html",
            "group"         => $more_settings2,
            "heading"   => __("Main Text", "wpbuilder"),
            "param_name"    => "content",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#1 Link Name", "wpbuilder"),
            "param_name"    => "link_name_1",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#1 Link URL", "wpbuilder"),
            "param_name"    => "link_url_1",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#1 Link Poster URL", "wpbuilder"),
            "param_name"    => "link_poster_url_1",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#1 Link Manual URL", "wpbuilder"),
            "param_name"    => "link_manual_url_1",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#2 Link Name", "wpbuilder"),
            "param_name"    => "link_name_2",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#2 Link URL", "wpbuilder"),
            "param_name"    => "link_url_2",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#2 Link Poster URL", "wpbuilder"),
            "param_name"    => "link_poster_url_2",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#2 Link Manual URL", "wpbuilder"),
            "param_name"    => "link_manual_url_2",
            "admin_label" => false
        ),        
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#3 Link Name", "wpbuilder"),
            "param_name"    => "link_name_3",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#3 Link URL", "wpbuilder"),
            "param_name"    => "link_url_3",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#3 Link Poster URL", "wpbuilder"),
            "param_name"    => "link_poster_url_3",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#3 Link Manual URL", "wpbuilder"),
            "param_name"    => "link_manual_url_3",
            "admin_label" => false
        ),        
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#4 Link Name", "wpbuilder"),
            "param_name"    => "link_name_4",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#4 Link URL", "wpbuilder"),
            "param_name"    => "link_url_4",
            "admin_label" => false
        ),
         array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#4 Link Poster URL", "wpbuilder"),
            "param_name"    => "link_poster_url_4",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#4 Link Manual URL", "wpbuilder"),
            "param_name"    => "link_manual_url_4",
            "admin_label" => false
        ),       
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#5 Link Name", "wpbuilder"),
            "param_name"    => "link_name_5",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#5 Link URL", "wpbuilder"),
            "param_name"    => "link_url_5",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#5 Link Poster URL", "wpbuilder"),
            "param_name"    => "link_poster_url_5",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("#5 Link Manual URL", "wpbuilder"),
            "param_name"    => "link_manual_url_5",
            "admin_label" => false
        ),
        )   
    ) );

/* extract
----------------------------------------------------*/
if (!function_exists('homepage_tabbed_content')) {
    function homepage_tabbed_content($atts, $content = null) {
        extract(shortcode_atts(array(
            'extra_class'   => '',
            'tab_1_name' => '',
            'tab_2_name' => '',
            'tab_title_1' => '',
            'tab_subtitle_1' => '',
            'step_1_title' => '',
            'step_1_link' => '',
            'step_1_image' => '',             
            'step_2_title' => '',
            'step_2_link' => '',
            'step_2_image' => '',              
            'step_3_title' => '',
            'step_3_link' => '',
            'step_3_image' => '',
            'workshop_link' => '',  
            'tab_title_2' => '',
            'tab_subtitle_2' => '', 
            'link_name_1' => '',
            'link_url_1' => '',
            'link_poster_url_1' => '',
            'link_manual_url_1' => '',
            'link_name_2' => '',
            'link_url_2' => '',
            'link_poster_url_2' => '',
            'link_manual_url_2' => '',            
            'link_name_3' => '',
            'link_url_3' => '',
             'link_poster_url_3' => '',
            'link_manual_url_3' => '',           
            'link_name_4' => '',
            'link_url_4' => '',
            'link_poster_url_4' => '',
            'link_manual_url_4' => '',            
            'link_name_5' => '',
            'link_url_5' => '',
             'link_poster_url_5' => '',
            'link_manual_url_5' => '',           
            'inclusion_quiz_url' => '',  

        ), $atts));

        $step_1_image_source = wp_get_attachment_image_src( $step_1_image, "full")[0];
        $step_2_image_source = wp_get_attachment_image_src( $step_2_image, "full")[0];
        $step_3_image_source = wp_get_attachment_image_src( $step_3_image, "full")[0];

        $out  = '';    
        $out .= '<section class="tabbed-content vc_row '.$extra_class.'">';
        $out .= '<header>';
        $out .= '<nav class="tab-navigation wrapper or-dividers">';
        $out .= '<a href="javascript:;" title="" class="active">'.$tab_1_name.'</a>';
        $out .= '<a href="javascript:;" title="">'.$tab_2_name.'</a>';
        $out .= '</nav>';
        $out .= '</header>';

        $out .= '<div class="tab-items wrapper">';

        $out .= '<div class="active centered home-7-pillars" aria-hidden="false">';
        $out .= '<div class="text">';
        $out .= '<h2>'.$tab_title_1.'</h2>';
        $out .= '<p>'.$tab_subtitle_1.'</p>';
        $out .= '</div>';
        $out .= '<ul class="circle-list">';

        $out .= '<li>';
        $out .= '<a href="'.$step_1_link.'" title="">';
        $out .= '<img src="'.$step_1_image_source.'" alt="" width="275" height="275"/>';
        $out .= '<strong>'.$step_1_title.'</strong>';
        $out .= '</a>';                
        $out .= '</li>';

        $out .= '<li>';
        $out .= '<a href="'.$step_2_link.'" title="">';
        $out .= '<img src="'.$step_2_image_source.'" alt="" width="275" height="275"/>';
        $out .= '<strong>'.$step_2_title.'</strong>';
        $out .= '</a>';                
        $out .= '</li>';

        $out .= '<li>';
        $out .= '<a href="'.$step_3_link.'" title="">';
        $out .= '<img src="'.$step_3_image_source.'" alt="" width="275" height="275"/>';
        $out .= '<strong>'.$step_3_title.'</strong>';
        $out .= '</a>';                
        $out .= '</li>';

        $out .= '</ul>';
        $out .= '<div>';
        $out .= '<span>Not sure where to start?</span>';
        $out .= '<a href="'.$workshop_link.'" title="" class="site-btn">REGISTER FOR A FREE WORKSHOP</a>';
        $out .= '</div>';
        $out .= '</div>';       

        $out .= '<div aria-hidden="true">';
        $out .= '<div class="resource-accents">';
        $out .= '<div class="info">';
        $out .= $content;        
        $out .= '</div>';
        $out .= '<div class="items">';
        $out .= '<h3>GET STARTED HERE!</h3>';
        $out .= '<ul class="resource-list">';



        $out .= '<li>';
        $out .= '<a href="'.$link_url_1.'" title="" class="text-link">';
        $out .= '<i class="download-icon manual" aria-hidden="true"></i>';
        $out .= '<span>'.$link_name_1.'</span>';
        $out .= '</a>';
        $out .= '<div class="additional-links">';
        if(!empty($link_manual_url_1)){
            $out .= '<a href="'.$link_manual_url_1.'" title="" class="site-btn small">PLAN</a>';
        }
        if(!empty($link_poster_url_1)) {
            $out .= '<a href="'.$link_poster_url_1.'" title="" class="site-btn purple small">Poster</a>';

        }
        $out .= '</div>';
        $out .= '</li>';

        $out .= '<li>';
        $out .= '<a href="'.$link_url_2.'" title="" class="text-link">';
        $out .= '<i class="download-icon video" aria-hidden="true"></i>';
        $out .= '<span>'.$link_name_2.'</span>';
        $out .= '</a>';
        $out .= '<div class="additional-links">';
        if(!empty($link_manual_url_2)){
            $out .= '<a href="'.$link_manual_url_2.'" title="" class="site-btn small">Plan</a>';
        }
        if(!empty($link_poster_url_2)) {
            $out .= '<a href="'.$link_poster_url_2.'" title="" class="site-btn purple small">Poster</a>';

        }
        $out .= '</div>';
        $out .= '</li>';

        $out .= '<li>';
        $out .= '<a href="'.$link_url_3.'" title="" class="text-link">';
        $out .= '<i class="download-icon library" aria-hidden="true"></i>';
        $out .= '<span>'.$link_name_3.'</span>';
        $out .= '</a>';
        $out .= '<div class="additional-links">';
        if(!empty($link_manual_url_3)){
            $out .= '<a href="'.$link_manual_url_3.'" title="" class="site-btn small">Manual</a>';
        }
        if(!empty($link_poster_url_3)) {
            $out .= '<a href="'.$link_poster_url_3.'" title="" class="site-btn purple small">Poster</a>';

        }
        $out .= '</div>';
        $out .= '</li>';


        $out .= '<li>';
        $out .= '<a href="'.$link_url_4.'" title="" class="text-link">';
        $out .= '<i class="download-icon online" aria-hidden="true"></i>';
        $out .= '<span>'.$link_name_4.'</span>';
        $out .= '</a>';
        $out .= '<div class="additional-links">';
        if(!empty($link_manual_url_4)){
            $out .= '<a href="'.$link_manual_url_4.'" title="" class="site-btn small">Manual</a>';
        }
        if(!empty($link_poster_url_4)) {
            $out .= '<a href="'.$link_poster_url_4.'" title="" class="site-btn purple small">Poster</a>';

        }
        $out .= '</div>';
        $out .= '</li>';

        $out .= '<li>';
        $out .= '<a href="'.$link_url_5 .'" title="" class="text-link">';
        $out .= '<i class="download-icon help" aria-hidden="true"></i>';
        $out .= '<span>'.$link_name_5   .'</span>';
        $out .= '</a>';
        $out .= '<div class="additional-links">';
        if(!empty($link_manual_url_5    )){
            $out .= '<a href="'.$link_manual_url_5  .'" title="" class="site-btn small">Manual</a>';
        }
        if(!empty($link_poster_url_5    )) {
            $out .= '<a href="'.$link_poster_url_5  .'" title="" class="site-btn purple small">Poster</a>';

        }
        $out .= '</div>';
        $out .= '</li>';


        $out .= '</ul>';
        $out .= '<div class="resource-accents-cta">';
        $out .= '<strong>Want to take a bigger step forward?</strong>';
        $out .= '<a href="'.$inclusion_quiz_url.'" title="" class="site-btn">Take the Netball Inclusion Action Survey</a>';
        $out .= '</div>';
        $out .= '</div>';
        $out .= '</div>';
        $out .= '</div>';
        $out .= '</div>';

        $out .= '</section>';

        return $out;
    }
}
add_shortcode( 'homepage_tabbed_content', 'homepage_tabbed_content' );



 