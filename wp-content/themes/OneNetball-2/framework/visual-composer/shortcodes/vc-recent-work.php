<?php

/*  Recent Work
----------------------------------------------------*/
vc_map( array(
	"name"		=> __("Recent Work", "js_composer"),
	"base"		=> "recent_work",
	"category"	=> 'wpbuilder',
	"icon"		=> get_template_directory_uri() . "/framework/visual-composer/assets/vc-icon.png", 
	"description"	=> __('List of Recent Work', 'js_composer'),
	"params"	=> array(
		array(
			"type" => "textfield",
			"heading" => __("Category Name", "wpbuilder"),
			"param_name" => "category_name",
			"admin_label" => true
			), 
		array(
			"type" => "textfield",
			"heading" => __("Number of event per view", "wpbuilder"),
			"param_name" => "display_posts",
			"value"		=> array('4' => '4'),
			"admin_label" => true
			), 
		array(
			"type"		=> "textfield",
			"heading"	=> __("Extra Class", "wpbuilder"),
			"param_name"	=> "extra_class",
			"admin_label" => true
			)
		)
	) );

/* extract
----------------------------------------------------*/
if (!function_exists('recent_work')) {
	function recent_work($atts, $content = null) {
		extract(shortcode_atts(array(
			'category_name' 	=> '',
			'display_posts'	=> '3',
			'extra_class' 	=> '' 
		), $atts));

		global $post;

		$out .= '<section class="news-section vc_row '.$extra_class.'">';
		$out .= '<h2>LATEST NEWS</h2>';
		$out .= '<ul class="news-list wrapper">';
	
		$args = array(
			'post_type'			=> 'post',
			'posts_per_page' 	=> $display_posts,
			'order'				=> 'DESC',
			'orderby'			=> 'date'
			);

		$the_query = new WP_Query( $args );

		if ($the_query->have_posts()) :

			while ($the_query->have_posts()): $the_query->the_post();

				$post_id = get_the_ID();
				$title = get_the_title();
				$content = substr(get_the_content(), 0, 50);
				$read_more = get_permalink();
				$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium_post_s' )[0];
				$format = 'd F Y';
				$date = get_the_date( $format, $post_id );
				$category = get_the_category();

			    $out .= '<li>';
           		$out .= '<a href="'.$read_more.'" title=""><span>View article</span></a>';
            	$out .= '<div class="thumb" style="background-image: url(';
            	if( !empty( $thumbnail ) ){
    				$out .= $thumbnail;
    			} else {
    				$out .= get_template_directory_uri() . '/images/news.jpg';
    			}
            	$out .= '");></div>';
            	//$out .= '<img src="'.$thumbnail.'" width="348" height="240"/>';
           		$out .= '<div>';
                $out .= '<strong>'.$title .'</strong>';
                $out .= '<time datetime="YYYY-MM-DD HH:II:SS">'.$date.'</time>';
                $out .= '<span class="category">';
                foreach ($category as $cat) { $out .= $cat->cat_name. ' '; }
                $out .='</span>';
            	$out .= '</div>';
        		$out .= '</li>';

			endwhile; 

		endif;

		wp_reset_query(); 

		$out .= '</ul>';
		$out .= '</section>';

		return $out;
	}
}
add_shortcode( 'recent_work', 'recent_work' );



			

				

				

						
							



			