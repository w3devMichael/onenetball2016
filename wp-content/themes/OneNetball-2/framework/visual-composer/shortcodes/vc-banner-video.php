<?php

/*  Banner with Video
----------------------------------------------------*/

$more_settings1 = __("Add Links", "wpbuilder");


vc_map( array(
	"name"		=> __("Banner with Video", "js_composer"),
	"base"		=> "banner_with_video",
	"category"	=> 'wpbuilder',
	"icon"		=> get_template_directory_uri() . "/framework/visual-composer/assets/vc-icon.png", 
	"description"	=> __('Banner with Video', 'js_composer'),
	"params"	=> array(
		array(
			"type"		=> "textfield",
			"heading"	=> __("Extra Class", "wpbuilder"),
			"param_name"	=> "extra_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "wpbuilder"),			
			"admin_label" => true
		), 
		array(
			"type" => "textfield",
			"heading" => __("Title Part in Bold Font", "wpbuilder"),
			"param_name" => "title_in_bold",
			'description' => __( 'Leave this section empty if the you want the whole title to be in regular font.', 'wpbuilder' ),
			"admin_label" => true
		),
		array(
			"type" => "textfield",
			"heading" => __("Title Part in Regular Font", "wpbuilder"),
			"param_name" => "title_in_regular",
			'description' => __( 'Leave this section empty if the you want the whole title to be in bold font.', 'wpbuilder' ),			
			"admin_label" => true
		), 
		array(
			"type"		=> "textarea_html",
			"heading"	=> __("Main Content", "wpbuilder"),
			"param_name"	=> "content",
			"admin_label" => true
		),
		array(
			"type"		=> "attach_image",
			"heading"	=> __("Video Cover Image", "wpbuilder"),
			"param_name"	=> "video_image_cover",
			"admin_label" => false
		),		
		array(
			"type"		=> "textfield",
			"heading"	=> __("Youtube Video ID", "wpbuilder"),
			"param_name"	=> "video_link",
			"admin_label" => flase
		),
		array(
			"type" => "textfield",
			"group"         => $more_settings1,
			"heading" => __("#1 Link Title", "wpbuilder"),
			"param_name" => "link_title_1",
		),	
		array(
			"type" => "textfield",
			"group"         => $more_settings1,
			"heading" => __("#1 Link URL", "wpbuilder"),
			"param_name" => "link_url_1",
		),				
		array(
			"type" => "textfield",
			"group"         => $more_settings1,
			"heading" => __("#2 Link Title", "wpbuilder"),
			"param_name" => "link_title_2",
		),	
		array(
			"type" => "textfield",
			"group"         => $more_settings1,
			"heading" => __("#2 Link URL", "wpbuilder"),
			"param_name" => "link_url_2",
		),

		)
	) );

/* extract
----------------------------------------------------*/
if (!function_exists('banner_with_video')) {
	function banner_with_video($atts, $content = null) {
		extract(shortcode_atts(array(
			'extra_class' 	=> '',
			'title_in_bold' => '',
			'title_in_regular' => '',
			'link_name' => '',
			'link_url' => '',
			'video_image_cover' => '',
			'video_link' => '',
		), $atts));

		$video_image_cover_source = wp_get_attachment_image_src( $video_image_cover, "full")[0];

		$out  = '';
		$out .= '<section class="page-accent vc_row '.$extra_class.'">';
    	$out .= '<div class="wrapper">';
        $out .= '<div class="left">';
        $out .= '<h1 class="page-accent-title"><span class="accent">'.$title_in_bold.'</span><br/>'.$title_in_regular.'</h1>';
        $out .= '<p>'.$content.'</p>';
		if(!empty($link_title_1) && !empty($link_url_1)) {
        $out .= '<a href="'.$link_url_1.'" title="" class="site-btn">'.$link_title_1.'</a>';
		}

		if(!empty($link_title_2) && !empty($link_url_2)) {
        $out .= '<a href="'.$link_url_2.'" title="" class="site-btn purple">'.$link_title_2.'</a>';
		}
        $out .= '</div>';
        $out .= '<div class="right">';
	    $out .= '<div class="video-link-open active">';
        $out .= '<img src="'.$video_image_cover_source.'" alt="" width="534" height="342"/>';
        $out .= ' <figure>';
        $out .= '<iframe class="video" width="560" height="315" src="https://www.youtube.com/embed/'.$video_link.'" frameborder="0" allowfullscreen></iframe>';
    	$out .= '</figure>';
        $out .= '</div>';
        $out .= '</div>';
    	$out .= '</div>';
		$out .= '</section>';

		return $out;
	}
}
add_shortcode( 'banner_with_video', 'banner_with_video' );


