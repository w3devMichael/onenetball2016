<?php

/*  Nominations Box
----------------------------------------------------*/

// if ( ! function_exists( 'ninja_forms_get_all_forms' ) ) {
//             // experimental, maybe not needed
//     require_once( NINJA_FORMS_DIR . '/includes/database.php' );
// }
// $ninja_forms_data = ninja_forms_get_all_forms();
// $ninja_forms = array();
// if ( ! empty( $ninja_forms_data ) ) {
//             // Fill array with Name=>Value(ID)
//     foreach ( $ninja_forms_data as $key => $value ) {
//         if ( is_array( $value ) ) {
//             $ninja_forms[ $value['name'] ] = $value['id'];
//         }
//     }
// }

vc_map( array(
    "name"      => __("Nominations Box", "js_composer"),
    "base"      => "nomination_box",
    "category"  => 'wpbuilder',
    "icon"      => get_template_directory_uri() . "/framework/visual-composer/assets/vc-icon.png", 
    "description"   => __('Nominations Box', 'js_composer'),
    "params"    => array(
        array(
            "type"      => "textfield",
            "heading"   => __("Extra Class", "wpbuilder"),
            "param_name"    => "extra_class",
            ),
        array(
            "type"      => "textfield",
            "heading"   => __("Main Title ", "wpbuilder"),
            "param_name"    => "main_title",
            "admin_label" => false
        ),
         array(
            "type"      => "textfield",
            "heading"   => __("Duration ", "wpbuilder"),
            "param_name"    => "duration",
            "admin_label" => false
        ),           
        // array(
        //     'type' => 'dropdown',
        //     'heading' => __( 'Select ninja form', 'js_composer' ),
        //     'param_name' => 'ninja_id',
        //     'value' => $ninja_forms,
        //     'save_always' => true,
        //     'description' => __( 'Choose previously created ninja form from the drop down list.', 'wpbuilder' )
        // ),     
       
        )   
    ) );

/* extract
----------------------------------------------------*/
if (!function_exists('nomination_box')) {
    function nomination_box($atts, $content = null) {
        extract(shortcode_atts(array(
            'extra_class'   => '',
            'main_title' => '',
            'duration' => '',
            // 'ninja_id' => '',        

        ), $atts));

        $formToggle = get_field('nomination_form_toggle');
        if( is_array($formToggle) && in_array('Activate Nomination Form', $formToggle) ){ 
            $out  = '';    
            $out .= '<section class="nomination-form wrapper '.$extra_class.'">';

            $out .= '<header>';
            $out .= '<h2>'.$main_title.'<span>'.$duration.'</span></h2>';
            $out .= '</header>';

               
            $out .= '<div class="site-form">';
            $out .=  do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');   
            $out .= '</div>';     
         
            $out .= '</section>';
        }

        return $out;
    }
}
add_shortcode( 'nomination_box', 'nomination_box' );


