<?php

/*  Help Page Tabbed Content
----------------------------------------------------*/
// $cf7 = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );

// $contact_forms = array();
// if ( $cf7 ) {
//     foreach ( $cf7 as $cform ) {
//         $contact_forms[ $cform->post_title ] = $cform->ID;
//     }
// } else {
//     $contact_forms[ __( 'No contact forms found', 'js_composer' ) ] = 0;
// }

$more_settings1 = __("Tab 1", "wpbuilder");

$more_settings2 = __("Tab 2", "wpbuilder");

vc_map( array(
    "name"      => __("Help Page Tabbed Content", "js_composer"),
    "base"      => "help_page_tabbed_content",
    "category"  => 'wpbuilder',
    "icon"      => get_template_directory_uri() . "/framework/visual-composer/assets/vc-icon.png", 
    "description"   => __('Help Page Tabbed Content', 'js_composer'),
    "params"    => array(
        array(
            "type"      => "textfield",
            "heading"   => __("Extra Class", "wpbuilder"),
            "param_name"    => "extra_class",
            ),
        array(
            "type"      => "textfield",
            "heading"   => __("Tab 1 Name ", "wpbuilder"),
            "param_name"    => "tab_1_name",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "heading"   => __("Tab 2 Name ", "wpbuilder"),
            "param_name"    => "tab_2_name",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings1,
            "heading"   => __("Contact Form Section Title", "wpbuilder"),
            "param_name"    => "contact_form_section_title",
            "admin_label" => false
        ), 
        array(
            "type"      => "textfield",
            "group"         => $more_settings1,
            "heading"   => __("Contact Form Section Subtitle", "wpbuilder"),
            "param_name"    => "contact_form_section_subtitle",
            "admin_label" => false
        ),
        // array(
        //     'type' => 'dropdown',
        //     "group"         => $more_settings1,
        //     'heading' => __( 'Select contact form', 'wpbuilder' ),
        //     'param_name' => 'contact_form',
        //     'value' => $contact_forms,
        //     'save_always' => true,
        //     'description' => __( 'Choose previously created contact form from the drop down list.', 'js_composer' ),
        // ),        
         array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("Contact Section Title", "wpbuilder"),
            "param_name"    => "contact_section_title",
            "admin_label" => false
        ),  
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("Contact Section Subtitle", "wpbuilder"),
            "param_name"    => "contact_section_subtitle",
            "admin_label" => false
        ),  
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("Get Involved Title", "wpbuilder"),
            "param_name"    => "contact_get_involved_title",
            'description' => __( 'The default is "GOT ALL THE INFORMATION YOU NEED? IT’S TIME TO..."', 'wpbuilder' ),
            "admin_label" => false
        ),  
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("Get Involved Link Title", "wpbuilder"),
            "param_name"    => "contact_get_involved_link_title",
            'description' => __( 'The default is "GET INVOLVED"', 'wpbuilder' ),
            "admin_label" => false
        ),  
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("Get Involved Link URL", "wpbuilder"),
            "param_name"    => "contact_get_involved_link_url",
            "admin_label" => false
        ),  
        )   
    ) );

/* extract
----------------------------------------------------*/
if (!function_exists('help_page_tabbed_content')) {
    function help_page_tabbed_content($atts, $content = null) {
        extract(shortcode_atts(array(
            'extra_class'   => '',
            'tab_1_name' => '',
            'tab_2_name' => '',
            'contact_form_section_title' => '',
            'contact_form_section_subtitle' => '',
            // 'contact_form' => '',
            'contact_section_title' => '',
            'contact_section_subtitle' => '', 
            'contact_get_involved_title' => '',
            'contact_get_involved_link_title' => '',
            'contact_get_involved_link_url' => '',           
        ), $atts));


        if(empty($contact_get_involved_link_title)) {
            $contact_get_involved_link_title = "GET INVOLVED";
        }
        if(empty($contact_get_involved_title)) {
            $contact_get_involved_title = "GOT ALL THE INFORMATION YOU NEED? IT’S TIME TO...";
        }       

        $contact_state_representative_1 = ot_get_option('contact_state_representative_1');
        $contact_preson_1 = ot_get_option('contact_preson_1');
        $contact_phone_1 = ot_get_option('contact_phone_1');
        $contact_email_1 = ot_get_option('contact_email_1');
        $contact_logo_1 = ot_get_option('contact_logo_1');

        $contact_state_representative_2 = ot_get_option('contact_state_representative_2');
        $contact_preson_2 = ot_get_option('contact_preson_2');
        $contact_phone_2 = ot_get_option('contact_phone_2');
        $contact_email_2 = ot_get_option('contact_email_2');
        $contact_logo_2 = ot_get_option('contact_logo_2');

        $contact_state_representative_3 = ot_get_option('contact_state_representative_3');
        $contact_preson_3 = ot_get_option('contact_preson_3');
        $contact_phone_3 = ot_get_option('contact_phone_3');
        $contact_email_3 = ot_get_option('contact_email_3');
        $contact_logo_3 = ot_get_option('contact_logo_3');

        $contact_state_representative_4 = ot_get_option('contact_state_representative_4');
        $contact_preson_4 = ot_get_option('contact_preson_4');
        $contact_phone_4 = ot_get_option('contact_phone_4');
        $contact_email_4 = ot_get_option('contact_email_4');
        $contact_logo_4 = ot_get_option('contact_logo_4');

        $contact_state_representative_5 = ot_get_option('contact_state_representative_5');
        $contact_preson_5 = ot_get_option('contact_preson_5');
        $contact_phone_5 = ot_get_option('contact_phone_5');
        $contact_email_5 = ot_get_option('contact_email_5');
        $contact_logo_5 = ot_get_option('contact_logo_5');

        $out  = $contact_forms;

        $out .= '<section class="tabbed-content vc_row '.$extra_class.'">';
        $out .= '<header>';
        $out .= '<nav class="tab-navigation wrapper">';
        $out .= '<a href="javascript:;" title="" class="active">'.$tab_1_name.'</a>';
        $out .= '<a href="javascript:;" title="">'.$tab_2_name.'</a>';
        $out .= '</nav>';
        $out .= '</header>';
        $out .= '<div class="tab-items wrapper">';

        $out .= '<div class="active" aria-hidden="false">';
        $out .= '<div class="site-form">';
        $out .= '<div class="text centered">';
        $out .= '<h2>'.$contact_form_section_title.'</h2>';
        $out .= '<p>'.$contact_form_section_subtitle.'</p>';
        $out .= '</div>';
        // $out .=  do_shortcode('[contact-form-7 id="'.$contact_form.'"]');
        $out .= do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]');
        $out .= '</div>';
        $out .= '</div>';

        $out .= '<div aria-hidden="true">';
        $out .= '<div class="text centered">';
        $out .= '<h2>'.$contact_section_subtitle.'</h2>';
        $out .= '<p class="important-text">'.$contact_section_subtitle.'</p>';
        $out .= '</div>'; 
        $out .= '<ul class="locations-list">';
    
        if(!empty($contact_state_representative_1)) :
            $out .= '<li>';
            $out .= '<img src="'.$contact_logo_1.'" alt="" width="160" height="160"/>';
            $out .= '<div>';
            $out .= '<strong>'.$contact_state_representative_1.'</strong>';
            $out .= '<span>'.$contact_preson_1.'</span>';
            $out .= '<a href="tel:'.$contact_phone_1 .' title="">'.$contact_phone_1 .'</a>';
            $out .= '<a href="mailto:'.$contact_email_1.'" title="">'.$contact_email_1.'</a>';
            $out .= '</div>';
            $out .= '</li>';
        endif;
        if(!empty($contact_state_representative_2)) :
            $out .= '<li>';
            $out .= '<img src="'.$contact_logo_2.'" alt="" width="160" height="160"/>';
            $out .= '<div>';
            $out .= '<strong>'.$contact_state_representative_2.'</strong>';
            $out .= '<span>'.$contact_preson_2.'</span>';
            $out .= '<a href="tel:'.$contact_phone_2 .' title="">'.$contact_phone_2 .'</a>';
            $out .= '<a href="mailto:'.$contact_email_2.'" title="">'.$contact_email_2.'</a>';
            $out .= '</div>';
            $out .= '</li>';
        endif;
        if(!empty($contact_state_representative_3)) :
            $out .= '<li>';
            $out .= '<img src="'.$contact_logo_3.'" alt="" width="160" height="160"/>';
            $out .= '<div>';
            $out .= '<strong>'.$contact_state_representative_3.'</strong>';
            $out .= '<span>'.$contact_preson_3.'</span>';
            $out .= '<a href="tel:'.$contact_phone_3 .' title="">'.$contact_phone_3 .'</a>';
            $out .= '<a href="mailto:'.$contact_email_3.'" title="">'.$contact_email_3.'</a>';
            $out .= '</div>';
            $out .= '</li>';
        endif;    
        if(!empty($contact_state_representative_4)) :
            $out .= '<li>';
            $out .= '<img src="'.$contact_logo_4.'" alt="" width="160" height="160"/>';
            $out .= '<div>';
            $out .= '<strong>'.$contact_state_representative_4.'</strong>';
            $out .= '<span>'.$contact_preson_4.'</span>';
            $out .= '<a href="tel:'.$contact_phone_4 .' title="">'.$contact_phone_4 .'</a>';
            $out .= '<a href="mailto:'.$contact_email_4.'" title="">'.$contact_email_4.'</a>';
            $out .= '</div>';
            $out .= '</li>';
        endif;
        if(!empty($contact_state_representative_5)) :
            $out .= '<li>';
            $out .= '<img src="'.$contact_logo_5.'" alt="" width="160" height="160"/>';
            $out .= '<div>';
            $out .= '<strong>'.$contact_state_representative_5.'</strong>';
            $out .= '<span>'.$contact_preson_5.'</span>';
            $out .= '<a href="tel:'.$contact_phone_5 .' title="">'.$contact_phone_5 .'</a>';
            $out .= '<a href="mailto:'.$contact_email_5.'" title="">'.$contact_email_5.'</a>';
            $out .= '</div>';
            $out .= '</li>';
        endif;    

        $out .= '<li>';
        $out .= '<strong>'.$contact_get_involved_title.'</strong>';
        $out .= '<br/>';
        $out .= '<a href="'.$contact_get_involved_link_url.'" title="" class="site-btn block">'.$contact_get_involved_link_title.'</a>';
        $out .= '</li>';               
        $out .= '</ul>';               
        $out .= '</div>';
        $out .= '</div>';
        $out .= '</section>';

        return $out;
    }
}
add_shortcode( 'help_page_tabbed_content', 'help_page_tabbed_content' );



