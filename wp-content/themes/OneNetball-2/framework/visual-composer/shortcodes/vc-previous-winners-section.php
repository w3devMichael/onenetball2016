<?php

/*  Previous Winners Tabbbed Content 
----------------------------------------------------*/

$more_settings1 = __("Tab 1", "wpbuilder");

$more_settings2 = __("Tab 2", "wpbuilder");

vc_map( array(
    "name"      => __("Previous Winners Tabbbed Content", "js_composer"),
    "base"      => "previous_winners_tabbed_content",
    "category"  => 'wpbuilder',
    "icon"      => get_template_directory_uri() . "/framework/visual-composer/assets/vc-icon.png", 
    "description"   => __('Previous Winners Tabbbed Content', 'js_composer'),
    "params"    => array(
        array(
            "type"      => "textfield",
            "heading"   => __("Extra Class", "wpbuilder"),
            "param_name"    => "extra_class",
            ),
      
        )   
    ) );

/* extract
----------------------------------------------------*/
if (!function_exists('previous_winners_tabbed_content')) {
    function previous_winners_tabbed_content($atts, $content = null) {
        extract(shortcode_atts(array(
            'extra_class'   => '',         

        ), $atts));

        $step_1_image_source = wp_get_attachment_image_src( $step_1_image, "full")[0];
        $step_2_image_source = wp_get_attachment_image_src( $step_2_image, "full")[0];
        $step_3_image_source = wp_get_attachment_image_src( $step_1_image, "full")[0];

		$taxonomy = 'winner-community-categories';
		$tax_terms = get_terms($taxonomy);

        $out  = '';    
        $out .= '<section class="tabbed-content vc_row '.$extra_class.'">';
        $out .= '<header>';
        $out .= '<h2>PREVIOUS WINNERS</h2>';
        $out .= '<nav class="tab-navigation wrapper">';
        $i = 0;
        foreach ($tax_terms as $tax_term) {
        	$i++;
        	if($i == 1) {
        		$out .= '<a href="javascript:;" title="" class="active">'.$tax_term->name.'</a>';
        	}
        	else {
        		$out .= '<a href="javascript:;" title="">'.$tax_term->name.'</a>';
        	}
        }        
        $out .= '</nav>';
   		$out .= '</header>';
   		$out .= '<div class="tab-items wrapper">';

        $c = 0;
        foreach ($tax_terms as $tax_term) {
        	$c++;
        	if($c == 1) {
        		$out .= '<div class="active" aria-hidden="false">';

        	}
        	else {
        		$out .= '<div aria-hidden="true">';
        	}

	        $out .= '<div class="text accent">';
	        $out .= '<h2>'.$tax_term->description.'</h2>';
	        $out .= '</div>';
	        $out .= '<ul class="winners-list">';

	        $args = array(
	        	'post_type'			=> 'winner-communitys',
	        	'winner-community-categories' => $tax_term->name,
	        );
	        $the_query = new WP_Query( $args );

	        if ($the_query->have_posts()) :

	        	while ($the_query->have_posts()): $the_query->the_post();

				$post_id = get_the_ID();
				$title = get_the_title();
				$content = get_the_content();
				$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'medium_post_ss' )[0];
				$topics = get_the_tags();
				$community_location = get_post_meta($post_id, 'community_location')[0];
				$community_website = get_post_meta($post_id, 'community_website')[0];

                $the_link = get_permalink();	

				$out .=	'<li>';
	            $out .=	'<span href="javascript:void[0]" title="" class="image-link">';
	            $out .=	'<img src="' . $thumbnail . '" alt="" width="348" height="258"/>';
	            $out .=	'</span>';
	            $out .=	'<div>';
	            $out .=	'<span href="javascript:void[0]" title="" class="title-link">'.$title.'</span>';
	            $out .=	'<span href="javascript:void[0]" title="" class="category">'.$community_location.'</span>';
	            $out .=	'<p>'.$content.'</p>';

                $trimmed = trim($community_website, "http");
	            if ($trimmed !== '' || !empty($trimmed)):
                $out .= '<a target="_blank" href="'.$community_website.'" title="" class="visit-site-link">VISIT SITE</a>';
                endif;
	            $out .=	'</div>';
	            $out .=	'</li>';   

			endwhile; 
			endif;

	        $out .= '</ul>';
	        $out .= '</div>';
        }

	    $out .= '</div>';
        $out .= '</section>';

        return $out;
    }
}
add_shortcode( 'previous_winners_tabbed_content', 'previous_winners_tabbed_content' );



