<?php

/*  Workshop Registration Tabbed Content
----------------------------------------------------*/

// if ( ! function_exists( 'ninja_forms_get_all_forms' ) ) {
//             // experimental, maybe not needed
//     require_once( NINJA_FORMS_DIR . '/includes/database.php' );
// }
// $ninja_forms_data = ninja_forms_get_all_forms();
// $ninja_forms = array();
// if ( ! empty( $ninja_forms_data ) ) {
//             // Fill array with Name=>Value(ID)
//     foreach ( $ninja_forms_data as $key => $value ) {
//         if ( is_array( $value ) ) {
//             $ninja_forms[ $value['name'] ] = $value['id'];
//         }
//     }
// }

$more_settings1 = __("Tab 1", "wpbuilder");

$more_settings2 = __("Tab 2", "wpbuilder");

$more_settings3 = __("Tab 3", "wpbuilder");

vc_map( array(
    "name"      => __("Workshop Registration Tabbed Content", "js_composer"),
    "base"      => "workshop_registration_content",
    "category"  => 'wpbuilder',
    "icon"      => get_template_directory_uri() . "/framework/visual-composer/assets/vc-icon.png", 
    "description"   => __('Workshop Registration Tabbed Content', 'js_composer'),
    "params"    => array(
        array(
            "type"      => "textfield",
            "heading"   => __("Extra Class", "wpbuilder"),
            "param_name"    => "extra_class",
            ),
        array(
            "type"      => "textfield",
            "heading"   => __("Tab 1 Name ", "wpbuilder"),
            "param_name"    => "tab_1_name",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "heading"   => __("Tab 2 Name ", "wpbuilder"),
            "param_name"    => "tab_2_name",
            "admin_label" => false
        ),
        array(
            "type"      => "textfield",
            "heading"   => __("Tab 3 Name ", "wpbuilder"),
            "param_name"    => "tab_3_name",
            "admin_label" => false
        ),
        // array(
        //     'type' => 'dropdown',
        //     'group' => $more_settings1,
        //     'heading' => __( 'Select ninja form', 'js_composer' ),
        //     'param_name' => 'ninja_id',
        //     'value' => $ninja_forms,
        //     'save_always' => true,
        //     'description' => __( 'Choose previously created ninja form from the drop down list.', 'wpbuilder' )
        // ),
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("Course Title", "wpbuilder"),
            "param_name"    => "course_title",
            "admin_label" => false
        ), 
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("Course Subtitle", "wpbuilder"),
            "param_name"    => "course_subtitle",
            "admin_label" => false
        ), 
        array(
            "type"      => "textarea_html",
            "group"         => $more_settings2,
            "heading"   => __("Course Short Description", "wpbuilder"),
            "param_name"    => "content",
            "admin_label" => false
        ),
        array(
            "type" => "attach_image",
            "group"         => $more_settings2,
            "heading" => __("Course Image", "wpbuilder"),
            "param_name" => "course_image",
            "description" => __("")
        ),  
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("Course Link Title", "wpbuilder"),
            "param_name"    => "course_link_title",
            'description' => __( 'The default is "START THE INCLUSION ONLINE COURSE"', 'wpbuilder' ),
            "admin_label" => false
        ), 
        array(
            "type"      => "textfield",
            "group"         => $more_settings2,
            "heading"   => __("Course Link URL", "wpbuilder"),
            "param_name"    => "course_link_url",
            "admin_label" => false
        ), 
        array(
            "type"      => "textfield",
            "group"         => $more_settings3,
            "heading"   => __("Contact Section Title", "wpbuilder"),
            "param_name"    => "contact_section_title",
            "admin_label" => false
        ),  
        array(
            "type"      => "textfield",
            "group"         => $more_settings3,
            "heading"   => __("Contact Section Subtitle", "wpbuilder"),
            "param_name"    => "contact_section_subtitle",
            "admin_label" => false
        ),  
        array(
            "type"      => "textfield",
            "group"         => $more_settings3,
            "heading"   => __("Get Involved Title", "wpbuilder"),
            "param_name"    => "contact_get_involved_title",
            'description' => __( 'The default is "GOT ALL THE INFORMATION YOU NEED? IT’S TIME TO..."', 'wpbuilder' ),
            "admin_label" => false
        ),  
        array(
            "type"      => "textfield",
            "group"         => $more_settings3,
            "heading"   => __("Get Involved Link Title", "wpbuilder"),
            "param_name"    => "contact_get_involved_link_title",
            'description' => __( 'The default is "GET INVOLVED"', 'wpbuilder' ),
            "admin_label" => false
        ),  
        array(
            "type"      => "textfield",
            "group"         => $more_settings3,
            "heading"   => __("Get Involved Link URL", "wpbuilder"),
            "param_name"    => "contact_get_involved_link_url",
            "admin_label" => false
        ),  
       
        )   
    ) );

/* extract
----------------------------------------------------*/
if (!function_exists('workshop_registration_content')) {
    function workshop_registration_content($atts, $content = null) {
        extract(shortcode_atts(array(
            'extra_class'   => '',
            'tab_1_name' => '',
            'tab_2_name' => '',
            'tab_3_name' => '',
            // 'ninja_id' => '',
            'course_title' => '',
            'course_subtitle' => '',
            'course_image' => '',
            'course_link_title' => '',
            'course_link_url' => '',
            'contact_section_title' => '',
            'contact_section_subtitle' => '', 
            'contact_get_involved_title' => '',
            'contact_get_involved_link_title' => '',
            'contact_get_involved_link_url' => '',


        ), $atts));

        $course_image_source = wp_get_attachment_image_src( $course_image, "full")[0];

        if(empty($course_link_title)) {
            $course_link_title = "START THE INCLUSION ONLINE COURSE";
        }
        if(empty($contact_get_involved_link_title)) {
            $contact_get_involved_link_title = "GET INVOLVED";
        }
        if(empty($contact_get_involved_title)) {
            $contact_get_involved_title = "GOT ALL THE INFORMATION YOU NEED? IT’S TIME TO...";
        }

        $contact_state_representative_1 = ot_get_option('contact_state_representative_1');
        $contact_preson_1 = ot_get_option('contact_preson_1');
        $contact_phone_1 = ot_get_option('contact_phone_1');
        $contact_email_1 = ot_get_option('contact_email_1');
        $contact_logo_1 = ot_get_option('contact_logo_1');

        $contact_state_representative_2 = ot_get_option('contact_state_representative_2');
        $contact_preson_2 = ot_get_option('contact_preson_2');
        $contact_phone_2 = ot_get_option('contact_phone_2');
        $contact_email_2 = ot_get_option('contact_email_2');
        $contact_logo_2 = ot_get_option('contact_logo_2');

        $contact_state_representative_3 = ot_get_option('contact_state_representative_3');
        $contact_preson_3 = ot_get_option('contact_preson_3');
        $contact_phone_3 = ot_get_option('contact_phone_3');
        $contact_email_3 = ot_get_option('contact_email_3');
        $contact_logo_3 = ot_get_option('contact_logo_3');

        $contact_state_representative_4 = ot_get_option('contact_state_representative_4');
        $contact_preson_4 = ot_get_option('contact_preson_4');
        $contact_phone_4 = ot_get_option('contact_phone_4');
        $contact_email_4 = ot_get_option('contact_email_4');
        $contact_logo_4 = ot_get_option('contact_logo_4');

        $contact_state_representative_5 = ot_get_option('contact_state_representative_5');
        $contact_preson_5 = ot_get_option('contact_preson_5');
        $contact_phone_5 = ot_get_option('contact_phone_5');
        $contact_email_5 = ot_get_option('contact_email_5');
        $contact_logo_5 = ot_get_option('contact_logo_5');

        $out  = '';    

        $out .= '<section class="tabbed-content vc_row">';
        $out .= '<header>';
        $out .= '<nav class="tab-navigation wrapper">';
        $out .= '<a href="javascript:;" title="" class="active">'.$tab_1_name.'</a>';
        $out .= '<a href="javascript:;" title="">'.$tab_2_name.'</a>';
        $out .= '<a href="javascript:;" title="">'.$tab_3_name.'</a>';
        $out .= '</nav>';
        $out .= '</header>';

        $out .= '<div class="tab-items wrapper">';
        $out .= '<div class="active" aria-hidden="false">';
        $out .= '<div class="site-form workshop-form">';
        $out .=  do_shortcode('[gravityform id="4" title="true" description="true" ajax="true"]');   
        $out .= '</div>';
        $out .= '</div>';
        $out .= '<div aria-hidden="true">';
        $out .= '<div class="two-columns-story">';
        $out .= '<div class="left text">';
        $out .= '<h2>'.$course_title.'</h2>';
        $out .= '<p class="important-text">'.$course_subtitle.'</p>';    
        $out .= '<p>'.$content.'</p>';                      
        $out .= '</div>';
        $out .= '<div class="right">';
        $out .= '<img src="'.$course_image_source.'" alt="" width="" height=""/>';
        $out .= '<br/>';
        $out .= '<br/>';
        $out .= '<a href="'.$course_link_url.'" title="" class="site-btn block">'.$course_link_title.'</a>';
        $out .= '</div>';
        $out .= '</div>';
        $out .= '</div>';

        $out .= '<div aria-hidden="true">';
        $out .= '<div class="text">';
        $out .= '<h2>'.$contact_section_subtitle.'</h2>';
        $out .= '<p class="important-text">'.$contact_section_subtitle.'</p>';
        $out .= '</div>';
        $out .= '<ul class="locations-list">';

        if(!empty($contact_state_representative_1)) :
            $out .= '<li>';
            $out .= '<img src="'.$contact_logo_1.'" alt="" width="160" height="160"/>';
            $out .= '<div>';
            $out .= '<strong>'.$contact_state_representative_1.'</strong>';
            $out .= '<span>'.$contact_preson_1.'</span>';
            $out .= '<a href="tel:'.$contact_phone_1 .' title="">'.$contact_phone_1 .'</a>';
            $out .= '<a href="mailto:'.$contact_email_1.'" title="">'.$contact_email_1.'</a>';
            $out .= '</div>';
            $out .= '</li>';
        endif;
        if(!empty($contact_state_representative_2)) :
            $out .= '<li>';
            $out .= '<img src="'.$contact_logo_2.'" alt="" width="160" height="160"/>';
            $out .= '<div>';
            $out .= '<strong>'.$contact_state_representative_2.'</strong>';
            $out .= '<span>'.$contact_preson_2.'</span>';
            $out .= '<a href="tel:'.$contact_phone_2 .' title="">'.$contact_phone_2 .'</a>';
            $out .= '<a href="mailto:'.$contact_email_2.'" title="">'.$contact_email_2.'</a>';
            $out .= '</div>';
            $out .= '</li>';
        endif;
        if(!empty($contact_state_representative_3)) :
            $out .= '<li>';
            $out .= '<img src="'.$contact_logo_3.'" alt="" width="160" height="160"/>';
            $out .= '<div>';
            $out .= '<strong>'.$contact_state_representative_3.'</strong>';
            $out .= '<span>'.$contact_preson_3.'</span>';
            $out .= '<a href="tel:'.$contact_phone_3 .' title="">'.$contact_phone_3 .'</a>';
            $out .= '<a href="mailto:'.$contact_email_3.'" title="">'.$contact_email_3.'</a>';
            $out .= '</div>';
            $out .= '</li>';
        endif;    
        if(!empty($contact_state_representative_4)) :
            $out .= '<li>';
            $out .= '<img src="'.$contact_logo_4.'" alt="" width="160" height="160"/>';
            $out .= '<div>';
            $out .= '<strong>'.$contact_state_representative_4.'</strong>';
            $out .= '<span>'.$contact_preson_4.'</span>';
            $out .= '<a href="tel:'.$contact_phone_4 .' title="">'.$contact_phone_4 .'</a>';
            $out .= '<a href="mailto:'.$contact_email_4.'" title="">'.$contact_email_4.'</a>';
            $out .= '</div>';
            $out .= '</li>';
        endif;
        if(!empty($contact_state_representative_5)) :
            $out .= '<li>';
            $out .= '<img src="'.$contact_logo_5.'" alt="" width="160" height="160"/>';
            $out .= '<div>';
            $out .= '<strong>'.$contact_state_representative_5.'</strong>';
            $out .= '<span>'.$contact_preson_5.'</span>';
            $out .= '<a href="tel:'.$contact_phone_5 .' title="">'.$contact_phone_5 .'</a>';
            $out .= '<a href="mailto:'.$contact_email_5.'" title="">'.$contact_email_5.'</a>';
            $out .= '</div>';
            $out .= '</li>';
        endif;    

        $out .= '<li>';
        $out .= '<strong>'.$contact_get_involved_title.'</strong>';
        $out .= '<br/>';
        $out .= '<a href="'.$contact_get_involved_link_url.'" title="" class="site-btn block">'.$contact_get_involved_link_title.'</a>';
        $out .= '</li>';               
        $out .= '</ul>';               
        $out .= '</div>';
        $out .= '</div>';
        $out .= '</section>';

        return $out;
    }
}
add_shortcode( 'workshop_registration_content', 'workshop_registration_content' );


