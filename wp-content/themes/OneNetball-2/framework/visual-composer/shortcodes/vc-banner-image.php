<?php

/*  Banner with Image
----------------------------------------------------*/

$more_settings1 = __("Add Links", "wpbuilder");


vc_map( array(
	"name"		=> __("Banner with Image", "js_composer"),
	"base"		=> "banner_with_image",
	"category"	=> 'wpbuilder',
	"icon"		=> get_template_directory_uri() . "/framework/visual-composer/assets/vc-icon.png", 
	"description"	=> __('Banner with Image', 'js_composer'),
	"params"	=> array(
		array(
			"type" => "textfield",
			"heading" => __("Extra Class", "wpbuilder"),
			"param_name" => "extra_class",
			"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "wpbuilder"),
			"admin_label" => true
		), 
		array(
			"type" => "textfield",
			"heading" => __("Title Part in Bold Font", "wpbuilder"),
			"param_name" => "title_in_bold",
			'description' => __( 'Leave this section empty if the you want the whole title to be in regular font.', 'wpbuilder' ),
			"admin_label" => true
		),
		array(
			"type" => "textfield",
			"heading" => __("Title Part in Regular Font", "wpbuilder"),
			"param_name" => "title_in_regular",
			'description' => __( 'Leave this section empty if the you want the whole title to be in bold font.', 'wpbuilder' ),			
			"admin_label" => true
		), 
		array(
			"type"		=> "textarea_html",
			"heading"	=> __("Main Text", "wpbuilder"),
			"param_name"	=> "content",
			"admin_label" => true
		),			
        array(
            "type" => "attach_image",
            "heading" => __("Add Banner Image", "wpbuilder"),
            "param_name" => "banner_image",
            "description" => __("")
        ),	
		array(
			"type" => "textfield",
			"group"         => $more_settings1,
			"heading" => __("#1 Link Title", "wpbuilder"),
			"param_name" => "link_title_1",
		),	
		array(
			"type" => "textfield",
			"group"         => $more_settings1,
			"heading" => __("#1 Link URL", "wpbuilder"),
			"param_name" => "link_url_1",
		),				
		array(
			"type" => "textfield",
			"group"         => $more_settings1,
			"heading" => __("#2 Link Title", "wpbuilder"),
			"param_name" => "link_title_2",
		),	
		array(
			"type" => "textfield",
			"group"         => $more_settings1,
			"heading" => __("#2 Link URL", "wpbuilder"),
			"param_name" => "link_url_2",
		),

		)
	) );

/* extract
----------------------------------------------------*/
if (!function_exists('banner_with_image')) {
	function banner_with_image($atts, $content = null) {
		extract(shortcode_atts(array(
			'extra_class' 	=> '',
			'title_in_bold'	=> '',
			'title_in_regular'	=> '',
			'banner_image' 	=> '',
			'link_title_1' => '',
			'link_url_1' => '',
			'link_title_2' => '',
			'link_url_2' => '',
		), $atts));

		$banner_image_source = wp_get_attachment_image_src( $banner_image, "full")[0];

		$out  = '';
		$out .= '<section class="page-accent image-bottom vc_row '.$extra_class.'">';
		$out .= '<div class="wrapper">';
		$out .= '<div class="left">';
		$out .= '<h1 class="page-accent-title"><span class="accent">'.$title_in_bold.'</span><br/>'.$title_in_regular.'</h1>';
		$out .= '<p>'.$content.'</p>';

		if(!empty($link_title_1) && !empty($link_url_1)) {
        $out .= '<a href="'.$link_url_1.'" title="" class="site-btn">'.$link_title_1.'</a>';
		}

		if(!empty($link_title_2) && !empty($link_url_2)){
        $out .= '<a href="'.$link_url_2.'" title="" class="site-btn purple">'.$link_title_2.'</a>';
		}

		$out .= '</div>';
		$out .= '<div class="right">';
		$out .= '<img src="'.$banner_image_source.'" alt="" width="" height=""/>';
		$out .= '</div>';
		$out .= '</div>';
		$out .= '</section>';

		return $out;
	}
}
add_shortcode( 'banner_with_image', 'banner_with_image' );



			

				

				

						
							



			