<?php

/*  Get Involved List
----------------------------------------------------*/

vc_map( array(
    "name" => __("Get Involved List", "wpbuilder"),
    "base" => "get_involved_list",
    "icon"      => get_template_directory_uri() . "/framework/visual-composer/assets/vc-icon.png", 
    "as_parent" => array('only' => 'get_involved_list_item'), 
    "content_element" => true,
    "show_settings_on_create" => true,
    "is_container" => true,
    "params" => array(
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "wpbuilder"),
            "param_name" => "extra_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "wpbuilder")
        ),
        array(
            "type"      => "textfield",
            "heading"   => __("Section Title", "wpbuilder"),
            "param_name"    => "section_title",
        ),
    ),
    "js_view" => 'VcColumnView'
) );

vc_map( array(
    "name" => __("Get Involved List Item", "wpbuilder"),
    "base" => "get_involved_list_item",
    "icon"      => get_template_directory_uri() . "/framework/visual-composer/assets/vc-icon.png", 
    "content_element" => true,
    "as_child" => array('only' => 'get_involved_list'), 
    "params" => array(
        array(
            "type"      => "dropdown",
            "heading"   => __("Class Name", "wpbuilder"),
            "param_name"    => "class_name",
            "admin_label" => true,
              "value"       => array(
                'No Icon'   => '',
                'Video Payer Icon'   => 'type-1',
                '7 Step Icon'   => 'type-2',
                'Laptop Icon'   => 'type-3',
                'Movie Icon'   => 'type-4',
                'Book Icon'   => 'type-5',
                'Conference Icon'   => 'type-6',
                'Conversation Icon'   => 'type-7',
                'Trophy Icon'   => 'type-8',
                'Questionmark Icon'   => 'type-9',
              ),
        ),         
         array(
            "type"      => "textfield",
            "heading"   => __("Title", "wpbuilder"),
            "param_name"    => "title",
            "admin_label" => true
        ), 
        array(
            "type"      => "textfield",
            "heading"   => __("Link", "wpbuilder"),
            "param_name"    => "link",
            "admin_label" => true
        ),   
        array(
            "type" => "attach_image",
            "heading" => __("Cover Image", "wpbuilder"),
            "param_name" => "cover_image",
            "description" => __("")
        ),  
    )
) );		

function get_involved_list( $atts, $content) {
    extract( shortcode_atts( array(
        'extra_class' => '',
        'section_title' => ''
    ), $atts ) );
   
    $out  =  '';

    $out .= '<section class="get-involved-section vc_row '.$extra_class.'">';
    $out .= '<h2>'.$section_title.'</h2>';
    $out .= '<ul class="circle-list with-icons wrapper">';
    $out .= do_shortcode($content);
    $out .= '</ul>';    
    $out .= '</section>';  

    return $out;
} 

add_shortcode( 'get_involved_list', 'get_involved_list');


function get_involved_list_item( $atts, $content) {
    extract( shortcode_atts( array(
        'class_name' => '',
        'title' => '',
        'link' => '',
        'cover_image' => '',
    ), $atts ) );

    $cover_image_source = wp_get_attachment_image_src( $cover_image, "full")[0];


    $out  =  '';
    $out .= '<li class="'.$class_name.'">';
    $out .= '<a href="'.$link.'" title="">';
    $out .= '<img src="'.$cover_image_source.'" alt="" width="275" height="275"/>';
    $out .= '<strong>'.$title.'</strong>';
    $out .= '</a>';           
    $out .= '</li>';

    return $out;
} 

add_shortcode( 'get_involved_list_item', 'get_involved_list_item');



if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Get_Involved_List extends WPBakeryShortCodesContainer {
    }
}

if ( class_exists( 'WPBakeryShortCode' ) ) {

     class WPBakeryShortCode_Get_Involved_List_Item extends WPBakeryShortCode {

 }	
}					



			