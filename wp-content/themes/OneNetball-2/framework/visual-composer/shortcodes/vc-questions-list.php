<?php

/*  Questions Box
----------------------------------------------------*/

vc_map( array(
    "name" => __("Questions Box", "wpbuilder"),
    "base" => "questions_box",
    "icon"      => get_template_directory_uri() . "/framework/visual-composer/assets/vc-icon.png", 
    "as_parent" => array('only' => 'questions_box_item'), 
    "content_element" => true,
    "show_settings_on_create" => true,
    "is_container" => true,
    "params" => array(
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "wpbuilder"),
            "param_name" => "extra_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "wpbuilder")
        ),
        array(
            "type"      => "textfield",
            "heading"   => __("Section Title", "wpbuilder"),
            "param_name"    => "section_title",
        ),
    ),
    "js_view" => 'VcColumnView'
) );

vc_map( array(
    "name" => __("Questions Box Item", "wpbuilder"),
    "base" => "questions_box_item",
    "icon"      => get_template_directory_uri() . "/framework/visual-composer/assets/vc-icon.png", 
    "content_element" => true,
    "as_child" => array('only' => 'questions_box'), 
    "params" => array(
         array(
            "type"      => "textfield",
            "heading"   => __("Question Title", "wpbuilder"),
            "param_name"    => "title",
            "admin_label" => true
        ), 
        array(
            "type"      => "textarea_html",
            "heading"   => __("Question Answer", "wpbuilder"),
            "param_name"    => "content",
            "admin_label" => true
        ),        
    )
) );		

function questions_box( $atts, $content) {
    extract( shortcode_atts( array(
        'extra_class' => '',
        'section_title' => ''
    ), $atts ) );
   
    $out  =  '';
    $out .= '<section class="help-questions-section wrapper'.$extra_class.'">';
    $out .= '<h2 class="sr-only">'.$section_title.'</h2>';
    $out .= '<ol class="questions-list">';
    $out .= do_shortcode($content);
    $out .= '</ol>';    
    $out .= '</section>';  

    return $out;
} 

add_shortcode( 'questions_box', 'questions_box');


function questions_box_item( $atts, $content) {
    extract( shortcode_atts( array(
        'title' => '',
    ), $atts ) );

    $out  =  '';
    $out  = '<li class="text">';
    $out .= '<strong class="important-text">'.$title.'</strong>';
    $out .= '<p>'.$content.'</p>';
    $out .= '</li>';

    return $out;
} 

add_shortcode( 'questions_box_item', 'questions_box_item');



if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Questions_Box extends WPBakeryShortCodesContainer {
    }
}

if ( class_exists( 'WPBakeryShortCode' ) ) {

     class WPBakeryShortCode_Questions_Box_Item extends WPBakeryShortCode {

 }	
}					



			