<?php
/*
Plugin Name: Visual Composer Content Block Extension
Plugin URI: http://wpbakery.com/vc
Description: Adds a Custom Content Block Element to Visual Composer.
Version: 1.0
Author: 
Author URI: 
License: GPLv2 or later
*/

// don't load directly
if (!defined('ABSPATH')) die('-1');

/*
Display notice if Visual Composer is not installed or activated.
*/
if ( !defined('WPB_VC_VERSION') ) { add_action('admin_notices', 'vc_notice'); return; }
function vc_notice() {
  $plugin_data = get_plugin_data(__FILE__);
  echo '
  <div class="updated">
    <p>'.sprintf(__('<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'vc_extend'), $plugin_data['Name']).'</p>
  </div>';
}

/*
Load plugin css and javascript files
*/
add_action('wp_enqueue_scripts', 'vc_js_css', 99);

function vc_js_css() {

	wp_enqueue_style( 'vc_extend_css', get_template_directory_uri() . '/framework/visual-composer/vc_extend.css');
	wp_enqueue_style( 'vc_extend_admin_css', get_template_directory_uri() . '/framework/visual-composer/vc_extend_admin.css');
	wp_enqueue_script( 'vc_extend_js', get_template_directory_uri() . '/framework/visual-composer/vc_extend.js', array('jquery'));


  // If you need any javascript files on front end, here is how you can load them.
  //wp_enqueue_script( 'vc_extend_js', plugins_url('vc_extend.js', __FILE__), array('jquery') );
}


if (class_exists('WPBakeryVisualComposerAbstract')) {	


  function change_vc_rows() {
        // Add parameters we want
    vc_add_param('vc_row', array(
      'type' => 'checkbox',
      'heading' => "Add Container",
      'param_name' => 'add_container',
      'value' => '',
      'description' => __("Assign an ID to the row", "discprofile")
      ));

        // Update 'vc_row' to include custom vc_row template and remap shortcode
    $new_map = vc_map_update( 'vc_row', array('html_template' => locate_template('framework/visual-composer/shortcodes/vc-row.php')) );

        // Remove default vc_row
    vc_remove_element('vc_row');

        // Remap shortcode with custom template
    vc_map($new_map['vc_row']);
  }

    // Include our function when all wordpress stuff is loaded
  add_action( 'wp_loaded', 'change_vc_rows' );

	require_once('vc_layouts.php');

	require_once('shortcodes/content-blocks.php');

	require_once('shortcodes/vc-recent-work.php');

  require_once('shortcodes/vc-banner-image.php');

  require_once('shortcodes/vc-banner-video.php');

  require_once('shortcodes/vc-questions-list.php');

  require_once('shortcodes/vc-help-page-section.php');

  require_once('shortcodes/vc-home-page-section.php');

  require_once('shortcodes/vc-get-involved-list.php');

  require_once('shortcodes/vc-workshop-registration-section.php');

  require_once('shortcodes/vc-previous-winners-section.php');

  require_once('shortcodes/vc-nominations-box.php');

}

