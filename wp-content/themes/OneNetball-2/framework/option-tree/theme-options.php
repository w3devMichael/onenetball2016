<?php

add_action( 'admin_init', 'custom_theme_options', 1 );

/*  Build the custom settings & update
/* ------------------------------------ */
function custom_theme_options() {
	
	$my_theme = wp_get_theme();
	$themeV = 'You"re using ' . $my_theme->get( 'Name' ) . ' v' . $my_theme->get( 'Version' );

	// Get a copy of the saved settings array.
	$saved_settings = get_option( 'option_tree_settings', array() );
	$custom_settings = array(	
		'sections'        => array(
			array(
				'id'		=> 'general',
				'title'		=> '<i class="cog_wpbuilder"></i> '.__('General', 'wpbuilder'),
			),
			array(
				'id'		=> 'header',
				'title'		=> '<i class="cog_wpbuilder"></i> '.__('Header', 'wpbuilder'),
			),
			array(
				'id'		=> 'newsletter',
				'title'		=> '<i class="cog_wpbuilder"></i> '.__('Newsletter Section', 'wpbuilder'),
			),
			array(
				'id'		=> 'latest-news',
				'title'		=> '<i class="cog_wpbuilder"></i> '.__('Latest News', 'wpbuilder'),
			),				
			array(
				'id'		=> 'login-page',
				'title'		=> '<i class="cog_wpbuilder"></i> '.__('Login Page', 'wpbuilder'),
			),	
			array(
				'id'		=> '7-pillar-page',
				'title'		=> '<i class="cog_wpbuilder"></i> '.__('7 Pillar Page', 'wpbuilder'),
			),							
			array(
				'id'		=> 'ambassador-page',
				'title'		=> '<i class="cog_wpbuilder"></i> '.__('Ambassador Page', 'wpbuilder'),
			),	
			array(
				'id'		=> 'resources-page',
				'title'		=> '<i class="cog_wpbuilder"></i> '.__('Resources Page', 'wpbuilder'),
			),
			array(
				'id'		=> 'st-contacts',
				'title'		=> '<i class="cog_wpbuilder"></i> '.__('State Contacts', 'wpbuilder'),
			),

		),
	
/*  Theme options
/* ------------------------------------ */
	'settings'        => array(
		
		array(
			'id'		=> 'welcome_msg',
			'label'		=> __('Welcom to theme options page', 'wpbuilder'),
			'desc'		=> $themeV,
			'type'		=> 'textblock-titled',
			'section'	=> 'general'
		),
		array(
			'id'		=> 'favicon',
			'label'		=> __('Favicon', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> 'general'
		),
		array(
			'id'		=> 'apple-touch',
			'label'		=> __('Apple icon', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> 'general'
		),
		array(
			'id'		=> 'login_logo',
			'label'		=> __('Wordpress login Logo', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> 'general'
		),
		array(
			'id'		=> 'login_bg',
			'label'		=> __('Wordpress login background', 'wpbuilder'),
			'std'		=> '#fff',
			'type'		=> 'colorpicker',
			'section'		=> 'general',
			'class'		=> ''
		),
		
		// Header 
		array(
			'id'		=> 'header_logo_1',
			'label'		=> __('Wordpress header logo 1', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> 'header'
		),
		array(
			'id'		=> 'header_logo_1_url',
			'label'		=> '',
			'desc'		=> __('Wordpress header logo 1 URL', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'header'
		),		
		array(
			'id'		=> 'header_logo_2',
			'label'		=> __('Wordpress header logo 2', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> 'header'
		),		
		array(
			'id'		=> 'header_logo_3',
			'label'		=> __('Wordpress header logo 3', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> 'header'
		),
		array(
			'id'		=> 'header_logo_3_url',
			'label'		=> '',
			'desc'		=> __('Wordpress header logo 3 URL', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'header'
		),			
		array(
			'id'		=> 'sticky',
			'label'		=> __('Sticky Navigation', 'wpbuilder'),
			'std'		=> 'on',
			'type'		=> 'on-off',
			'section'		=> 'header'
		),


		// State/Territhory Contacts

		array(
			'id'		=> 'contact_msg_1',
			'label'		=> __('Contact#1', 'wpbuilder'),
			'desc'		=> '',
			'type'		=> 'textblock-titled',
			'section'	=> 'st-contacts'
		),	

		array(
			'id'		=> 'contact_state_representative_1',
			'label'		=> '',
			'desc'		=> __('Contact#1 State Representaive Name', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_preson_1',
			'label'		=> '',
			'desc'		=> __('Contact#1 Contact Person', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_phone_1',
			'label'		=> '',
			'desc'		=> __('Contact#1 Telephone Number', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_email_1',
			'label'		=> '',
			'desc'		=> __('Contact#1 Email Address', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_logo_1',
			'label'		=> '',
			'desc'		=> __('Contact#1 logo', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> 'st-contacts'
		),

		array(
			'id'		=> 'contact_msg_2',
			'label'		=> __('Contact#2', 'wpbuilder'),
			'desc'		=> '',
			'type'		=> 'textblock-titled',
			'section'	=> 'st-contacts'
		),	

		array(
			'id'		=> 'contact_state_representative_2',
			'label'		=> '',
			'desc'		=> __('Contact#2 State Representaive Name', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_preson_2',
			'label'		=> '',
			'desc'		=> __('Contact#2 Contact Person', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_phone_2',
			'label'		=> '',
			'desc'		=> __('Contact#2 Telephone Number', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_email_2',
			'label'		=> '',
			'desc'		=> __('Contact#2 Email Address', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_logo_2',
			'label'		=> '',
			'desc'		=> __('Contact#2 logo', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> 'st-contacts'
		),

		array(
			'id'		=> 'contact_msg_3',
			'label'		=> __('Contact#3', 'wpbuilder'),
			'desc'		=> '',
			'type'		=> 'textblock-titled',
			'section'	=> 'st-contacts'
		),	

		array(
			'id'		=> 'contact_state_representative_3',
			'label'		=> '',
			'desc'		=> __('Contact#3 State Representaive Name', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_preson_3',
			'label'		=> '',
			'desc'		=> __('Contact#3 Contact Person', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_phone_3',
			'label'		=> '',
			'desc'		=> __('Contact#3 Telephone Number', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_email_3',
			'label'		=> '',
			'desc'		=> __('Contact#3 Email Address', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_logo_3',
			'label'		=> '',
			'desc'		=> __('Contact#3 logo', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> 'st-contacts'
		),

		array(
			'id'		=> 'contact_msg_4',
			'label'		=> __('Contact#4', 'wpbuilder'),
			'desc'		=> '',
			'type'		=> 'textblock-titled',
			'section'	=> 'st-contacts'
		),	

		array(
			'id'		=> 'contact_state_representative_4',
			'label'		=> '',
			'desc'		=> __('Contact#4 State Representaive Name', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_preson_4',
			'label'		=> '',
			'desc'		=> __('Contact#4 Contact Person', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_phone_4',
			'label'		=> '',
			'desc'		=> __('Contact#4 Telephone Number', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_email_4',
			'label'		=> '',
			'desc'		=> __('Contact#4 Email Address', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_logo_4',
			'label'		=> '',
			'desc'		=> __('Contact#4 logo', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> 'st-contacts'
		),

		array(
			'id'		=> 'contact_msg_5',
			'label'		=> __('Contact#5', 'wpbuilder'),
			'desc'		=> '',
			'type'		=> 'textblock-titled',
			'section'	=> 'st-contacts'
		),	

		array(
			'id'		=> 'contact_state_representative_5',
			'label'		=> '',
			'desc'		=> __('Contact#5 State Representaive Name', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_preson_5',
			'label'		=> '',
			'desc'		=> __('Contact#5 Contact Person', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_phone_5',
			'label'		=> '',
			'desc'		=> __('Contact#5 Telephone Number', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_email_5',
			'label'		=> '',
			'desc'		=> __('Contact#5 Email Address', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'st-contacts'
		),
		array(
			'id'		=> 'contact_logo_5',
			'label'		=> '',
			'desc'		=> __('Contact#5 logo', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> 'st-contacts'
		),

		// Login Page
		array(
			'id'		=> 'login_page_title',
			'label'		=> __('Login page Title', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'login-page'
		),		
		array(
			'id'		=> 'login_page_mode_desc',
			'label'		=> __('Login page Description', 'wpbuilder'),
			'std'		=> '',
			'type'		=> 'textarea',
			'section'	=> 'login-page'
		),
		array(
			'id'		=> 'login_page_logo_1',
			'label'		=> '',
			'desc'		=> __('Login page Logo 1', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> 'login-page'
		),
		array(
			'id'		=> 'login_page_logo_2',
			'label'		=> '',
			'desc'		=> __('Login page Logo 2', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> 'login-page'
		),	


		// 7 Pillar Page

		array(
			'id'		=> '7pillar_page_title',
			'label'		=> __('7 Pillars page Title', 'wpbuilder'),
			'std'		=> '<span class="accent">7 PILLARS</span> OF INCLUSION',
			'type'		=> 'text',
			'section'	=> '7-pillar-page'
		),
			
		array(
			'id'		=> '7pillar_page_logo_1',
			'label'		=> '',
			'desc'		=> __('7 Pillars page Logo 1', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> '7-pillar-page'
		),
		array(
			'id'		=> '7pillar_page_logo_2',
			'label'		=> '',
			'desc'		=> __('7 Pillars page Logo 2', 'wpbuilder'),
			'type'		=> 'upload',
			'section'	=> '7-pillar-page'
		),	


		// Ambasador Page
		array(
			'id'		=> 'ambassador_page_title',
			'label'		=> __('Ambasador group page Title', 'wpbuilder'),
			'type'		=> 'text',
			'section'	=> 'ambassador-page'
		),		
		array(
			'id'		=> 'ambassador_page_mode_desc',
			'label'		=> __('Ambasador group page Description', 'wpbuilder'),
			'std'		=> '',
			'type'		=> 'textarea',
			'section'		=> 'ambassador-page'
		),

		array(
			'id'		=> 'ambassador_image',
			'label'		=> '',
			'desc'		=> __('Banner Image', 'wpbuilder'),
			'type'		=> 'upload',
			'section'		=> 'ambassador-page'
		),

		// Resource Page

		array(
			'id'		=> 'resources_page_title',
			'label'		=> __('Resource group page Title', 'wpbuilder'),
			'std'		=> 'RESOURCES',
			'type'		=> 'text',
			'section'		=> 'resources-page'
		),			
		array(
			'id'		=> 'resources_page_mode_desc',
			'label'		=> __('Resource group page Description', 'wpbuilder'),
			'std'		=> '',
			'type'		=> 'textarea',
			'section'		=> 'resources-page'
		),

		// Newsletter

		array(
			'id'		=> 'newsletter_shortcode',
			'label'		=> __('Newsletter Shortcode', 'wpbuilder'),
			'std'		=> '',
			'type'		=> 'text',
			'section'		=> 'newsletter'
		),	

		// Latest News

		array(
			'id'		=> 'latest_news_cover_image',
			'label'		=> '',
			'desc'		=> __('Page Cover Image', 'wpbuilder'),
			'type'		=> 'upload',
			'section'		=> 'latest-news'
		),

	)
);

/*  Settings are not the same? Update the DB
/* ------------------------------------ */
	if ( $saved_settings !== $custom_settings ) {
		update_option( 'option_tree_settings', $custom_settings ); 
	} 
}

