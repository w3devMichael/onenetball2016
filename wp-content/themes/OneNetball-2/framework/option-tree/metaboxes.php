<?php

/*  Initialize the meta boxes.
/* ------------------------------------ */
add_action( 'admin_init', '_custom_meta_boxes' );
function _custom_meta_boxes() {

$page_title = array(
	'id'          => 'page-title',
	'title'       => __('Page title & content', 'wpbuilder'),
	'desc'        => '',
	'pages'       => array( 'page' ),
	'context'     => 'side',
	'priority'    => 'default',
	'fields'      => array(
		array(
			'label'		=> '',
			'id'		=> 'page_title',
			'std'		=> 'on',
			'type'		=> 'on-off'
		),
		array(
			'id'		=> 'content_position',
			'label'		=> __('Editor content position', 'wpbuilder'),
			'std'		=> 'before',
			'type'		=> 'radio',
			'choices'	=> array(
				array( 
					'value' => 'before',
					'label' => __('Before', 'wpbuilder')
				),
				array( 
					'value' => 'after',
					'label' => __('After', 'wpbuilder')
				),
			)
		),
	)
);

$general_option = array(
	'id'          => 'page-options',
	'title'       => __('Page Options', 'wpbuilder'),
	'desc'        => '',
	'pages'       => array( 'post', 'page' ),
	'context'     => 'normal',
	'priority'    => 'default',
	'fields'      => array(

		array(
			'label'		=> 'Show Newsletter Section',
			'id'		=> 'newsletter-option',
			'std'		=> 'on',
			'type'		=> 'on-off'
		),
	)
);


$ambassador_option = array(
	'id'          => 'ambassador-options',
	'title'       => __('Ambassador Options', 'wpbuilder'),
	'desc'        => '',
	'pages'       => array( 'ambassadors'),
	'context'     => 'normal',
	'priority'    => 'default',
	'fields'      => array(

		array(
			'id'		=> 'birth_place',
			'label'		=> __('Place of Birth', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),
		array(
			'id'		=> 'current_club',
			'label'		=> __('Curent Club', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),
	)
);


$resource_option = array(
	'id'          => 'resource-general-options',
	'title'       => __('Resource Options', 'wpbuilder'),
	'desc'        => '',
	'pages'       => array( 'resources' ),
	'context'     => 'normal',
	'priority'    => 'default',
	'fields'      => array(
		array(
			'id'		=> 'placeholder_image',
			'label'		=> __('Banner Placeholder Image', 'wpbuilder'),
			'std'		=> '',
			'type'		=> 'upload',
			'choices'	=> array()
		),			
		array(
			'id'		=> 'program_name',
			'label'		=> __('Program Name', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),
		array(
			'id'		=> 'pdf_name',
			'label'		=> __('PDF Name', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),
		array(
			'id'		=> 'pdf_link',
			'label'		=> __('PDF Source', 'wpbuilder'),
			'std'		=> '',
			'type'		=> 'upload',
			'choices'	=> array()
		),	
		array(
			'id'		=> 'site_link',
			'label'		=> __('Site Link', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),	

	)
);



$top_post_options = array(
	'id'          => 'top-page-options',
	'title'       => __('Resource Page Option', 'wpbuilder'),
	'desc'        => '',
	'pages'       => array( 'resources', 'post', 'ambassadors'),
	'context'     => 'normal',
	'priority'    => 'high',
	'fields'      => array(

		array(
			'id'		=> 'page_title_in_blod',
			'label'		=> __('Page Title in Bold Font', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),	
		array(
			'id'		=> 'page_title_in_regular',
			'label'		=> __('Page Title in Normal Font', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),			
		array(
			'id'		=> 'page_short_description',
			'label'		=> __('Short Content Description / Subheading', 'wpbuilder'),
			'type'		=> 'textarea',
			'choices'	=> array()
		),
		array(
			'label'		=> 'Show Newsletter Section',
			'id'		=> 'newsletter-option',
			'std'		=> 'on',
			'type'		=> 'on-off'
		),

	)
);



$winner_community_options = array(
	'id'          => 'winner-community-options',
	'title'       => __('Community Award Winners Page Option', 'wpbuilder'),
	'desc'        => '',
	'pages'       => array( 'winner-communitys'),
	'context'     => 'normal',
	'priority'    => 'high',
	'fields'      => array(

		array(
			'id'		=> 'community_location',
			'label'		=> __('Community Location', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),	
		array(
			'id'		=> 'community_website',
			'label'		=> __('Community Website', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),	

	)
);


$pillars_page_options = array(
	'id'          => 'pillars-page-options',
	'title'       => __('Pillars Page Option', 'wpbuilder'),
	'desc'        => '',
	'pages'       => array( 'page' ),
	'context'     => 'normal',
	'priority'    => 'default',
	'fields'      => array(

		array(
			'id'		=> 'title',
			'label'		=> __('Page Top Title', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),	
		array(
			'id'		=> 'subtitle',
			'label'		=> __('Page Top Subtitle', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),	
		array(
			'id'		=> 'youtube_video_id',
			'label'		=> __('Youtube Video ID', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),	
		array(
			'id'		=> 'youtube_video_cover_img',
			'label'		=> __('Youtube Video Cover Image', 'wpbuilder'),
			'std'		=> '',
			'type'		=> 'upload',
			'choices'	=> array()
		),	
		array(
			'id'		=> 'shortcode',
			'label'		=> __('Page pillar Shortcode', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),	
		array(
			'id'		=> 'step_numb',
			'label'		=> __('Page pillar step number', 'wpbuilder'),
			'type'		=> 'text',
			'choices'	=> array()
		),	
	)
);




/*  Register meta boxes
/* ------------------------------------ */
	ot_register_meta_box( $ambassador_option );
	ot_register_meta_box( $resource_option );
	ot_register_meta_box( $top_post_options );	
	ot_register_meta_box( $winner_community_options );	


$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];

$template_file = get_post_meta($post_id, '_wp_page_template', TRUE);

if($template_file == 'page-7pillars.php') {
	ot_register_meta_box( $pillars_page_options );	
}




}
