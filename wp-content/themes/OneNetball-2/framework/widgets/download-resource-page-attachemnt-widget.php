<?php

class download_resource_page_attachment_widget extends WP_Widget {

    function download_resource_page_attachment_widget() {
        $widget_ops = array('classname' => 'widget_similar_post', 'description' => 'Ads Resource Page Download Link.');
        parent::WP_Widget(false, ': Resource Page Download Link Wiget', $widget_ops);
    }

    function widget($args, $instance) {
        global $post;
      
        $id = get_the_ID ();        
        $tags = wp_get_post_tags($id);

        $pdf_link = get_post_meta($post->ID,'pdf_link')[0];
        $pdf_name = get_post_meta($post->ID,'pdf_name')[0];

        $pdf_name = ($pdf_name != '') ? $pdf_name : 'DOWNLOAD'; 


        if ($pdf_link != '' ):

        ?>

        <li>
             <h2>DOWNLOAD RESOURCE</h2>
            <a href="<?php echo $pdf_link ?>" target="_blank" title="" class="site-btn purple block download-resource-button"><?php echo $pdf_name ?></a>
            
        </li><?php

        endif; 

        $post = $orig_post;
        wp_reset_query();
       
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['number_of_posts'] = sanitize_text_field($new_instance['number_of_posts']);

        return $instance;
    }

    function form($instance) {

        //Output admin widget options form
        extract(shortcode_atts(array(
            'number_of_posts' => 'Address',                 
        ), $instance));
        ?>
        <p>
            <label for="<?php esc_attr_e($this->get_field_id('number_of_posts')); ?>"><?php _e("Number of posts:", "themeton"); ?></label>
            <input type="text" class="widefat" id="<?php esc_attr_e($this->get_field_id('number_of_posts')); ?>" name="<?php esc_attr_e($this->get_field_name('number_of_posts')); ?>" value="<?php esc_attr_e($number_of_posts); ?>"  />
        </p>      

        <?php
    }

}

add_action('widgets_init', create_function('', 'return register_widget("download_resource_page_attachment_widget");'));
