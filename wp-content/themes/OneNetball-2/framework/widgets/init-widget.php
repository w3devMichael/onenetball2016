<?php

/* Custom widgets */
require_once file_require(get_template_directory() . '/framework/widgets/recent-posts-widget.php');
require_once file_require(get_template_directory() . '/framework/widgets/address-widget.php');
require_once file_require(get_template_directory() . '/framework/widgets/similar-post-widget.php');
require_once file_require(get_template_directory() . '/framework/widgets/download-resource-page-attachemnt-widget.php');
require_once file_require(get_template_directory() . '/framework/widgets/go-back-to-resource-page-wiget.php');
require_once file_require(get_template_directory() . '/framework/widgets/share-wiget.php');

?>