<?php

class go_back_to_resource_page_widget extends WP_Widget {

    function go_back_to_resource_page_widget() {
        $widget_ops = array('classname' => 'widget_similar_post', 'description' => 'Ads Link to go back to Resource Page.');
        parent::WP_Widget(false, ': Go back to Resource Page Link Wiget', $widget_ops);
    }

    function widget($args, $instance) {
        global $post;
      
        $id = get_the_ID ();        
        $tags = wp_get_post_tags($id);

        ?><li> 
           

            <a href="<?php echo home_url( 'resources' ); ?>" title="" class="site-btn block"><i class="icon-arrow-left"></i> BACK TO RESOURCES</a>
            
 
        </li><?php 

        $post = $orig_post;
        wp_reset_query();
       
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['number_of_posts'] = sanitize_text_field($new_instance['number_of_posts']);

        return $instance;
    }

    function form($instance) {

        //Output admin widget options form
        extract(shortcode_atts(array(
            'number_of_posts' => 'Address',                 
        ), $instance));
        ?>
        <p>
            <label for="<?php esc_attr_e($this->get_field_id('number_of_posts')); ?>"><?php _e("Number of posts:", "themeton"); ?></label>
            <input type="text" class="widefat" id="<?php esc_attr_e($this->get_field_id('number_of_posts')); ?>" name="<?php esc_attr_e($this->get_field_name('number_of_posts')); ?>" value="<?php esc_attr_e($number_of_posts); ?>"  />
        </p>      

        <?php
    }

}

add_action('widgets_init', create_function('', 'return register_widget("go_back_to_resource_page_widget");'));
