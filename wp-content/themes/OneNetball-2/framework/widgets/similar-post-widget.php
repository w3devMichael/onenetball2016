<?php

class similar_post_widget extends WP_Widget {

    function similar_post_widget() {
        $widget_ops = array('classname' => 'widget_similar_post', 'description' => 'Recent posts .');
        parent::WP_Widget(false, ': Similar Post Widget', $widget_ops);
    }

    function widget($args, $instance) {
        global $post;
      
        $id = get_the_ID ();        
        $tags = wp_get_post_tags($id);

        ?><li><ul class="news-list sidebar-list"><?php 

        if ($tags) {            
            $tag_ids = array();

            foreach($tags as $individual_tag) {
                $tag_ids[] = $individual_tag->term_id;
            }

            $args=array(
                'tag__in' => $tag_ids,
                'post__not_in' => array($post->ID),
                'posts_per_page'=> $instance['number_of_posts'], 
                'caller_get_posts'=>1
            );
            
            // run the query

            query_posts( $args );

            while( have_posts() ) {
                

                the_post();

                $post_id = get_the_ID();

                  $placeholder_img = wp_get_attachment_url( get_post_thumbnail_id($post_id), 'full' );
                  $datetime = get_post_time('j F Y', true);
                  $categories = get_the_category();
            ?>

            <li>
                <a href="<?php the_permalink()?>" title=""><span>View article</span></a>
                <img src="<?php echo $placeholder_img; ?>" alt="" width="348" height="auto"/> 
                <div>
                    <strong> <?php the_title(); ?></strong>
                    <time datetime="YYYY-MM-DD HH:II:SS"><?php echo $datetime ?></time>
                    <span class="category"><?php echo $categories[0]->name; ?></span>
                </div>
            </li>
 
        <?php } // end of while loop  

        wp_reset_query();
        } // end of if tags 
        ?>
        </ul>
        </li><?php 

        
        
       
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['number_of_posts'] = sanitize_text_field($new_instance['number_of_posts']);

        return $instance;
    }

    function form($instance) {

        //Output admin widget options form
        extract(shortcode_atts(array(
            'number_of_posts' => 'Number of Posts',                 
        ), $instance));
        ?>
        <p>
            <label for="<?php esc_attr_e($this->get_field_id('number_of_posts')); ?>"><?php _e("Number of posts:", "themeton"); ?></label>
            <input type="text" class="widefat" id="<?php esc_attr_e($this->get_field_id('number_of_posts')); ?>" name="<?php esc_attr_e($this->get_field_name('number_of_posts')); ?>" value="<?php esc_attr_e($number_of_posts); ?>"  />
        </p>      

        <?php
    }

}

add_action('widgets_init', create_function('', 'return register_widget("similar_post_widget");'));
