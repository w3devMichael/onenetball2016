<?php

class share_post_widget extends WP_Widget {

    function share_post_widget() {
        $widget_ops = array('classname' => 'widget_share_post', 'description' => 'Share Post.');
        parent::WP_Widget(false, ': Share Post Widget', $widget_ops);
    }

    function widget($args, $instance) {
        global $post;
      
        $id = get_the_ID ();        
		$w_title = $instance['title'];
        ?>

        <li style="margin: 30px 0px 0px;">
	        <h2><?php echo $w_title; ?></h2>
	        <div class="social-widget">
	          <?php echo do_shortcode('[ssba url="' . get_permalink($id) . '" title="' . get_the_title($id) . '"]'); ?>
	        </div> 
       </li>

       <?php        
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = sanitize_text_field($new_instance['title']);

        return $instance;
    }

    function form($instance) {

        //Output admin widget options form
        extract(shortcode_atts(array(
            'title' => 'SHARE THIS RESOURCE',                 
        ), $instance));
        ?>
        <p>
            <label for="<?php esc_attr_e($this->get_field_id('title')); ?>"><?php _e("Number of posts:", "themeton"); ?></label>
            <input type="text" class="widefat" id="<?php esc_attr_e($this->get_field_id('title')); ?>" name="<?php esc_attr_e($this->get_field_name('title')); ?>" value="<?php esc_attr_e($title); ?>"  />
        </p>      

        <?php
    }

}

add_action('widgets_init', create_function('', 'return register_widget("share_post_widget");'));



