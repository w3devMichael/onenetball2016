<?php

class tt_AddressWidget extends WP_Widget {

    function tt_AddressWidget() {
        $widget_ops = array('classname' => 'widget_recent_news', 'description' => 'Recent posts.');
        parent::WP_Widget(false, ': Address Widget', $widget_ops);
    }

    function widget($args, $instance) {
        global $post;
        extract(array_merge(array(
                    'title' => 'Address',
                    'address' => 'Sydney road, Billboard Street 2219-11C. Apple Town, Your Country.',
                    'phone' => '(305) 533-1122',
                    'email' => 'johndoe@mail.com',
                    'website' => 'www.website.com',
                        ), $instance));

        if (isset($before_widget))
            print($before_widget);

        echo '<aside class="widget widget_address">';

        if ($title != '')
            echo "" . $args['before_title'] . $title . $args['after_title'];



		$phone =str_replace(',', ', <br />', $phone);


        echo '<address>';
        echo $address != '' ? '<abbr class="address" title="address"><span class="icon_pin"></span>'.$address.'</abbr>' : '';
        echo $phone != '' ? '<abbr title="phone"><span class="icon_phone"></span>'.$phone.'</abbr>' : '';
        echo $email != '' ? '<abbr title="email"><span class="icon_mail"></span> <a href="mailto:'.esc_attr($email).'">'.$email.'</a></abbr>' : '';
        echo $website != '' ? '<abbr title="web"><span class="icon_globe-2"></span> <a href="'.esc_attr($website).'">'.$website.'</a></abbr>' : '';
        echo '</address>';
        echo '</aside>';
        
        if (isset($after_widget))
            print($after_widget);

    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = sanitize_text_field($new_instance['title']);
        $instance['address'] = sanitize_text_field($new_instance['address']);
        $instance['phone'] = sanitize_text_field($new_instance['phone']);
        $instance['email'] = sanitize_text_field($new_instance['email']);
        $instance['website'] = strip_tags($new_instance['website']);

        return $instance;
    }

    function form($instance) {

        //Output admin widget options form
        extract(shortcode_atts(array(
                    'title' => 'Address',
                    'address' => 'Sydney road, Billboard Street 2219-11C. Apple Town, Your Country.',
                    'phone' => '(305) 533-1122',
                    'email' => 'johndoe@mail.com',
                    'website' => 'www.website.com',
                        ), $instance));
        ?>
        <p>
            <label for="<?php esc_attr_e($this->get_field_id('title')); ?>"><?php _e("Title:", "themeton"); ?></label>
            <input type="text" class="widefat" id="<?php esc_attr_e($this->get_field_id('title')); ?>" name="<?php esc_attr_e($this->get_field_name('title')); ?>" value="<?php esc_attr_e($title); ?>"  />
        </p>
        <p>
            <label for="<?php esc_attr_e($this->get_field_id('address')); ?>"><?php _e("address:", "themeton"); ?></label>
            <input type="text" class="widefat" id="<?php esc_attr_e($this->get_field_id('address')); ?>" name="<?php esc_attr_e($this->get_field_name('address')); ?>" value="<?php esc_attr_e($address); ?>"  />
        </p>
        <p>
            <label for="<?php esc_attr_e($this->get_field_id('phone')); ?>"><?php _e("phone:", "themeton"); ?></label>
            <input type="text" class="widefat" id="<?php esc_attr_e($this->get_field_id('phone')); ?>" name="<?php esc_attr_e($this->get_field_name('phone')); ?>" value="<?php esc_attr_e($phone); ?>"  />
        </p>
        <p>
            <label for="<?php esc_attr_e($this->get_field_id('email')); ?>"><?php _e("email:", "themeton"); ?></label>
            <input type="text" class="widefat" id="<?php esc_attr_e($this->get_field_id('email')); ?>" name="<?php esc_attr_e($this->get_field_name('email')); ?>" value="<?php esc_attr_e($email); ?>"  />
        </p>
        <p>
            <label for="<?php esc_attr_e($this->get_field_id('website')); ?>"><?php _e("website:", "themeton"); ?></label>
            <input type="text" class="widefat" id="<?php esc_attr_e($this->get_field_id('website')); ?>" name="<?php esc_attr_e($this->get_field_name('website')); ?>" value="<?php esc_attr_e($website); ?>"  />
        </p>

        <?php
    }

}

add_action('widgets_init', create_function('', 'return register_widget("tt_AddressWidget");'));
