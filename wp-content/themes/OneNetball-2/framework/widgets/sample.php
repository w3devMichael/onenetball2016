<?php 

class T20_top_10_list_manual extends WP_Widget
{

  public function __construct()
  {
    parent::__construct(
      'top-10-list-manual',
      'Top 10 List Manual',
      array(
        'description' => 'Top 10 List'
      )
    );
  }

  public function widget( $args, $instance )
  {

    ?>
    <div id="t20_top_10_list_manual" class="def-block widget widget_T20_top_10_list">

      <h4 class="tt"><?php $title = ( $instance["list_title"] ) ? esc_url($instance["list_title"]) : "Custom List"; echo $title; ?></h4><span class="liner"></span>

    <?php 

      for ($i = 1; $i <= 10; $i++) {
         ?>
          <div class="list-item clearfix">

            <figure>
               <img src="<?php echo esc_url($instance["no_'.$i.'_cover"]); ?>" />
            </figure>

            <div class="description">
              <h3 class="artist"><?php echo esc_html($instance["no_'.$i.'_artist"]); ?></h3>
              <h5 class="song"><?php echo esc_html($instance["no_'.$i.'_song"]); ?></h5>
            </div>

            <div class="ranking-number">
              <?php echo $i; ?>
            </div>         

          </div>

         <?php 
      }

    ?>

    </div>

    <?php 
  }

  public function form( $instance )
  {    
    ?>    
      <h5>List Title</h5>
      <p>
        <label for="<?php echo $this->get_field_id("list_title"); ?>">Number <?php echo $i; ?> Song </label><br />
        <input type="text" name="<?php echo $this->get_field_name("list_title"); ?>" id="<?php echo $this->get_field_id("list_title"); ?>" value="<?php echo $instance["list_title"]; ?>" class="widefat" />
      </p>

    <?php 

      for ($i = 1; $i <= 10; $i++) {
          ?>  
            <h5><?php echo $i; ?></h5>
            <p>
              <label for="<?php echo $this->get_field_id("no_'.$i.'_song"); ?>">Number <?php echo $i; ?> Song </label><br />
              <input type="text" name="<?php echo $this->get_field_name("no_'.$i.'_song"); ?>" id="<?php echo $this->get_field_id("no_'.$i.'_song"); ?>" value="<?php echo $instance["no_'.$i.'_song"]; ?>" class="widefat" />
            </p>
            <p>
              <label for="<?php echo $this->get_field_id("no_'.$i.'_artist"); ?>">Number <?php echo $i; ?> Artist </label><br />
              <input type="text" name="<?php echo $this->get_field_name("no_'.$i.'_artist"); ?>" id="<?php echo $this->get_field_id("no_'.$i.'_artist"); ?>" value="<?php echo $instance["no_'.$i.'_artist"]; ?>" class="widefat" />
            </p>    <p>
              <label for="<?php echo $this->get_field_id("no_'.$i.'_cover"); ?>">Number <?php echo $i; ?> Cover</label><br />
              <input type="text" class="img" name="<?php echo $this->get_field_name("no_'.$i.'_cover"); ?>" id="<?php echo $this->get_field_id("no_'.$i.'_cover"); ?>" value="<?php echo $instance["no_'.$i.'_cover"]; ?>" />
              <input type="button" class="select-img button button-primary" value="Select Image" />
            </p>
            <hr/>
          <?php
      }
   
  }


} 
// end class

// init the widget
add_action( 'widgets_init', create_function('', 'return register_widget("T20_top_10_list_manual");') );

// queue up the necessary js
function hrw_enqueue()
{
  wp_enqueue_style('thickbox');
  wp_enqueue_script('media-upload');
  wp_enqueue_script('thickbox');
  // moved the js to an external file, you may want to change the path
  wp_enqueue_script( 'timeline', get_template_directory_uri() . '/js/admin/upload-frame.js', array( 'jquery' ), '', false );
}
add_action('admin_enqueue_scripts', 'hrw_enqueue');