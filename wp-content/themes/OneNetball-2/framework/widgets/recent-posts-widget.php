<?php

class tt_RecentPostsWidget extends WP_Widget {

    function tt_RecentPostsWidget() {
        $widget_ops = array('classname' => 'widget_recent_news', 'description' => 'Recent posts.');
        parent::WP_Widget(false, ': Recent Posts', $widget_ops);
    }

    function widget($args, $instance) {
        global $post;
        extract(array_merge(array(
                    'title' => '',
                    'number_posts' => 5,
                    'exclude_posts' => '',
                    'excerpt' => 'yes',
                    'excerpt_count' => 10,
                        ), $instance));

        $q['posts_per_page'] = $number_posts;
        $q['ignore_sticky_posts'] = 1;
        $q['category__not_in'] = explode(',', $exclude_posts);

        query_posts($q);

        if (isset($before_widget))
            print($before_widget);

        echo '<aside class="widget widget_recent_news">';

        if ($title != '')
            echo "" . $args['before_title'] . $title . $args['after_title'];

        echo '<ul>';
        while (have_posts()) : the_post();
            $class = 'class="no-thumb"';
            $thumb = $desc ='';
            if($excerpt == 'yes') {
               $desc = '<p>'.implode(' ', array_slice(explode(' ', strip_tags(get_the_excerpt())), 0, $excerpt_count)).'</p>';
            }

            if(has_post_thumbnail($post->ID)) {
                $thumb = '<a class="widget-thumb" href="'.get_permalink().'">'.get_the_post_thumbnail($post->ID, 'thumbnail').'"</a>';
                $class = '';
            }
            echo "<li $class>";
            echo $thumb;
            echo '<div class="widget-content">
                    <a href="'.get_permalink().'">'.get_the_title().'</a>
                    '. $desc . '
                    <span class="post-date">'.get_the_date().'</span>
                </div>';
            echo '</li>';
        endwhile;
        echo '</ul>';
        echo '</aside>';
        
        if (isset($after_widget))
            print($after_widget);

        wp_reset_query();
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = sanitize_text_field($new_instance['title']);
        $instance['number_posts'] = sanitize_text_field($new_instance['number_posts']);
        $instance['exclude_posts'] = sanitize_text_field($new_instance['exclude_posts']);
        $instance['excerpt'] = strip_tags($new_instance['excerpt']);
        $instance['excerpt_count'] = sanitize_text_field($new_instance['excerpt_count']);

        return $instance;
    }

    function form($instance) {

        //Output admin widget options form
        extract(shortcode_atts(array(
                    'title' => '',
                    'number_posts' => 5,
                    'exclude_posts' => '',
                    'excerpt' => 'yes',
                    'excerpt_count' => 10,
                        ), $instance));
        ?>
        <p>
            <label for="<?php esc_attr_e($this->get_field_id('title')); ?>"><?php _e("Title:", "themeton"); ?></label>
            <input type="text" class="widefat" id="<?php esc_attr_e($this->get_field_id('title')); ?>" name="<?php esc_attr_e($this->get_field_name('title')); ?>" value="<?php esc_attr_e($title); ?>"  />
        </p>
        <p>
            <input type="text" id="<?php esc_attr_e($this->get_field_id('number_posts')); ?>" name="<?php esc_attr_e($this->get_field_name('number_posts')); ?>" value="<?php esc_attr_e($number_posts); ?>" size="3" />
            <label for="<?php esc_attr_e($this->get_field_id('number_posts')); ?>">Number of posts to show</label>
        </p>
        <p>
            <input type="text" id="<?php esc_attr_e($this->get_field_id('exclude_posts')); ?>" name="<?php esc_attr_e($this->get_field_name('exclude_posts')); ?>" value="<?php esc_attr_e($exclude_posts); ?>" size="3" />
            <label for="<?php esc_attr_e($this->get_field_id('exclude_posts')); ?>">Exclude category ID (optional)</label>
            <br><small>You can include multiple categories with comma separation.</small>
        </p>
        <p>
            <input class="checkbox" type="checkbox" id="<?php esc_attr_e($this->get_field_id('excerpt')); ?>" name="<?php esc_attr_e($this->get_field_name('excerpt')); ?>" value="yes" <?php checked($excerpt, 'yes'); ?>>
            <label for="<?php esc_attr_e($this->get_field_id('excerpt')); ?>">Show Except</label>
        </p>
        <p>
            <input type="text" id="<?php esc_attr_e($this->get_field_id('excerpt_count')); ?>" name="<?php esc_attr_e($this->get_field_name('excerpt_count')); ?>" value="<?php esc_attr_e($excerpt_count); ?>" size="3">
            <label for="<?php esc_attr_e($this->get_field_id('excerpt_count')); ?>">Except count</label>
        </p>

        <?php
    }

}

add_action('widgets_init', create_function('', 'return register_widget("tt_RecentPostsWidget");'));
