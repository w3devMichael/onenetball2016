<?php
/*
Plugin Name: Monitor Settings Page
Description: 
Author: 
Version: 0.1
*/

global $db_activation_status;

function votebox_db_table(){
    global $wpdb;
    
    $table_name = $wpdb->prefix . "votebox_details";
    
    if($wpdb->get_var('SHOW TABLES LIKE ' . $table_name) != $table_name)
    {
        $sql = 'CREATE TABLE ' . $table_name . '(
        id INT NOT NULL AUTO_INCREMENT,
        vote VARCHAR(255),
        username VARCHAR(255),
        email VARCHAR(255),
        ip VARCHAR(255),
        vote_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (id)           
        )';       
        
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        
        dbDelta($sql);
        
        add_option('votebox_db_table_database_version', '1.0');

    } 

    $db_activation_status = 1;    
}


add_action('admin_menu', 'plugin_create_menu');

function plugin_create_menu() {

     //create new top-level menu   add_menu_page('Monitor Settings Page', 'Monitor Settings Page', 'administrator', __FILE__, 'monitor_settings_page',plugins_url('/images/icon.png', __FILE__));

    add_menu_page('Monitor Page', 'Monitor Page', 'administrator', 'monitor_settings_page', 'monitor_settings_page', 'dashicons-lock', 81);

    //call register settings function
    add_action( 'admin_init', 'register_theme_options' );

    // Css rules for Color Picker
    wp_enqueue_style( 'wp-color-picker' );  

    wp_enqueue_script( 'dataTables-js', get_template_directory_uri() . '/js/jquery.dataTables.js', array( 'jquery' ), '', true );
    wp_enqueue_style( 'dataTables-css', get_template_directory_uri() . '/styles/jquery.dataTables.css', array() );

    if($db_activation_status != 1) {
        votebox_db_table();
    }

}

function register_theme_options() {
    //register our settings
    register_setting( 'theme_otions-group-1', 'header_title' );
    register_setting( 'theme_otions-group-1', 'header_subtitle' );
    register_setting( 'theme_otions-group-1', 'home_page_features_articles_category' );
    register_setting( 'theme_otions-group-1', 'home_page_latest_news_category' );
    register_setting( 'theme_otions-group-1', 'color' );
    register_setting( 'theme_otions-group-1', 'sex' );
    register_setting( 'theme_otions-group-1', 'sex1' );
    register_setting( 'theme_otions-group-1', 'sexx1' );
    register_setting( 'theme_otions-group-1', 'sexx2' );
    register_setting( 'theme_otions-group-1', 'sexx3' );
    register_setting( 'theme_otions-group-1', 'demo-file', 'handle_file_upload');
}

function handle_file_upload($option){
  if(!empty($_FILES["demo-file"]["tmp_name"]))
  {
    $urls = wp_handle_upload($_FILES["demo-file"], array('test_form' => FALSE));
    $temp = $urls["url"];
    return $temp;  
  }
 
  return $option;
}

function clear_votebox_ajax(){   
    global $wpdb;
    $wpdb->query( "TRUNCATE TABLE wp_votebox_details"); 
}

add_action('wp_ajax_nopriv_clear_votebox_ajax', 'clear_votebox_ajax');
add_action('wp_ajax_clear_votebox_ajax', 'clear_votebox_ajax');


function monitor_settings_page() {
?>
<div class="wrap">

 <div id="icon-themes" class="icon32"></div>

     <h2>Monitor Settings Page</h2>

     <?php settings_errors(); ?>

     <h2 class="nav-tab-wrapper">
        <a href="?page=monitor_settings_page&tab=tab_one" class="nav-tab">VoteBox Settings</a>
        <a href="?page=monitor_settings_page&tab=tab_two" class="nav-tab">Options</a>
    </h2>

    <?php 

        if( isset( $_GET[ 'tab' ] ) ) {  
            $active_tab = $_GET[ 'tab' ];  
        } else {
            $active_tab = 'tab_one';
        }
    ?>

<?php 

    if($active_tab == 'tab_one') {

?>

    <div style="padding-top: 40px;">

        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
                <tr style="text-align: left;">
                    <th>ID</th>
                    <th>Vote</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>IP</th>
                </tr>
            </thead>

        </table>

        <h4>To clear all voters IP-adress, Names and Emails click on this "Clear Vote Table" button</h4>        

        <button class="button button-primary" id="clear_vote_table"> Clear Vote Table </button>

        <p class="ajax-success">You have succesfuly cleard all data form vote_table table</p>


        <script type="text/javascript">


                // $(document).ready( function () {
                //   $('#example').DataTable( {
                //         "processing": true,
                //         "serverSide": true,
                //            "ajax": {
                //             url: "<?php echo admin_url('admin-ajax.php'); ?>",
                //             "data": function ( d ) {
                //                 d.action = "json_votebox_list";
                //             }
                //         }
                //     } );
                // } );

            (function( $ ) {  

                $(document).ready(function() {
                    $('#example').DataTable( {
                        "processing": true,
                        "serverSide": true,
                        "ajax": "../wp-content/themes/remix/admin/json_votebox_list.php "
                    } );
                } );

                $('.ajax-success').hide();

                $('#clear_vote_table').on('click', function(event){
                    event.preventDefault();

                    console.log('hi');

                    jQuery.ajax({
                        type:"post",
                        dataType:"json",
                        url: "<?php echo admin_url('admin-ajax.php'); ?>",
                        data: {action: "clear_votebox_ajax"},                 
                        success: function(response) {
                            $('.ajax-success').show().delay(3000).fadeOut();
                        }
                    });

                    return false;
                });

            })( jQuery );

        </script>

    </div>


<?php 

}elseif($active_tab == 'tab_two') {

?>

    <form method="post" action="options.php">
        <?php settings_fields( 'theme_otions-group-1' ); ?>
        <?php do_settings_sections( 'theme_otions-group-1' ); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Enter Homepage Header Title</th>
                <td><input type="text" name="header_title" value="<?php echo esc_attr( get_option('header_title') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Enter Homepage Header Subtitle</th>
                <td><input type="text" name="header_subtitle" value="<?php echo esc_attr( get_option('header_subtitle') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Enter Category ID to Diplay Feature Articles</th>
                <td><input type="text" name="home_page_features_articles_category" value="<?php echo esc_attr( get_option('home_page_features_articles_category') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Enter Category ID to Diplay Latest News Articles</th>
                <td><input type="text" name="home_page_latest_news_category" value="<?php echo esc_attr( get_option('home_page_latest_news_category') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Enter Homepage Header Title</th>

                <td><input name="sex" type="checkbox" value="1" <?php checked( '1', get_option( 'sex' ) ); ?> /></td>
            </tr>      

            <tr valign="top">
                <th scope="row">Enter Homepage Header Title</th>
                <td><input name="sex1" type="radio" value="0" <?php checked( '0', get_option( 'sex1' ) ); ?> />
                  <input name="sex1" type="radio" value="1" <?php checked( '1', get_option( 'sex1' ) ); ?> /></td>
              </tr>            

              <tr valign="top">
                <th scope="row">Color</th>
                <td><input type="text" name="color" value="<?php echo esc_attr( get_option('color') ); ?>" class="cpa-color-picker" /></td>
            </tr> 

            <tr valign="top">
                <th scope="row">Enter Homepage Header Title</th>
                <td>

                    <select id="default_role" name="sexx1">
                        <option name="sex2" value='location'>Location</option>
                        <option name="sex2" value='role'>Role</option>
                        <option name="sex2" value='functional'>Functional Area</option>
                        <option name="sex2" value='industry'>Industry</option>
                    </select>

                </td>
            </tr>   
        </table>

        <?php submit_button(); ?>

    </form>

<?php 
}

?>

</div>
<?php }

wp_enqueue_script( 'cpa_custom_js', get_template_directory_uri() . '/js/jquery.monitor-page.js', array( 'jquery', 'wp-color-picker' ), '', true  );



