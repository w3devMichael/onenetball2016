<?php 

/////////////////////////////////////////////////////////////////////////////
//                      Theme Customization API
/////////////////////////////////////////////////////////////////////////////


function sitepoint_customize_register($wp_customize)  {

    // Adding Panel

  $wp_customize->add_panel( 'menus', array(
    'title' => __( 'Menus' ),
        'description' => $description, // Include html tags such as <p>.
        'priority' => 160, // Mixed with top-level-section hierarchy.
        ) );

     // Adding simple Category with Textarea

  $wp_customize->add_section("ads", array(
    "title" => __("Advertising", "customizer_ads_sections"),
    "priority" => 30,
    ));

  $wp_customize->add_setting("ads_code", array(
    "default" => "",
    "transport" => "postMessage",
    ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "ads_code",
    array(
      "label" => __("Enter Ads Code", "customizer_ads_code_label"),
      "section" => "ads",
      "settings" => "ads_code",
      "type" => "textarea",
      )
    ));  

    // Adding simple Category with Colors Example            

  $colors = array();

  $colors[] = array(
    'slug'=>'content_text_color', 
    'default' => '#333',
    'label' => __('Content Text Color', 'Ari')
    );

  $colors[] = array(
    'slug'=>'content_link_color', 
    'default' => '#88C34B',
    'label' => __('Content Link Color', 'Ari')
    );

  foreach( $colors as $color ) {

    $wp_customize->add_setting(
      $color['slug'], array(
        'default' => $color['default'],
        'type' => 'option', 
        'capability' => 
        'edit_theme_options'
        )
      );

    $wp_customize->add_control(
      new WP_Customize_Color_Control(
        $wp_customize,
        $color['slug'], 
        array('label' => $color['label'], 
          'section' => 'colors',
          'settings' => $color['slug'])
        )
      );

  }

    // to get colors use  get_option('content_text_color'); get_option('content_link_color');
    // Adding simple Category with Options  

  $wp_customize->add_setting('sidebar_position', array());
  $wp_customize->add_control('sidebar_position', array(
    'label'      => __('Sidebar position', 'Ari'),
    'section'    => 'layout',
    'settings'   => 'sidebar_position',
    'type'       => 'radio',
    'choices'    => array(
      'left'   => 'left',
      'right'  => 'right',
      ),
    ));
  $wp_customize->add_section('layout' , array(
    'title' => __('Layout','Ari'),
    'panel' => 'menus',
    ));


    // to get option use get_theme_mod('sidebar_position');


    // Adding simple Image Upload  

  $wp_customize->add_section( 'themeslug_logo_section' , array(
    'title'       => __( 'Logo', 'themeslug' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the header',
    ) );

  $wp_customize->add_setting( 'themeslug_logo' );

  
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
    'label'    => __( 'Logo', 'themeslug' ),
    'section'  => 'themeslug_logo_section',
    'settings' => 'themeslug_logo',
    ) ) );

    // <a href='echo esc_url( home_url( '/' ) );' title='echo esc_attr( get_bloginfo( 'name', 'display' ) ); ' rel='home'><img src=' echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ' alt=' echo esc_attr( get_bloginfo( 'name', 'display' ) ); '></a>

  $wp_customize->add_section("_s_f_home_slider", array(
    "title" => __("Sey", "customizer_ads_sections"),
    "priority" => 30,
    ));

    // =====================
    //  = Category Dropdown =
    //  =====================

  $categories = get_categories();
  $cats = array('macka', 'pas', 'auto');
  $i = 0;
  foreach($categories as $category){
    if($i==0){
      $default = $category->slug;
      $i++;
    }
    $cats[$category->slug] = $category->name;
  }

  $wp_customize->add_setting('_s_f_slide_cat', array(
    'default'        => $default
    ));
  $wp_customize->add_control( 'cat_select_box', array(
    'settings' => '_s_f_slide_cat',
    'label'   => 'Select Category:',
    'section'  => '_s_f_home_slider',
    'type'    => 'select',
    'choices' => $cats,
    ));


}

add_action("customize_register","sitepoint_customize_register");