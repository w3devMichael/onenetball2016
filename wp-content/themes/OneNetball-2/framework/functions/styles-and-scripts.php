<?php 

/////////////////////////////////////////////////////////////////////////////
//     REGISTER STYLESHEETS
/////////////////////////////////////////////////////////////////////////////

add_action('wp_enqueue_scripts', 'register_styles');

function register_styles() 
{
    // wp_register_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '1.0', false );
    wp_register_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', false, '1.0', false );
    wp_register_style( 'animation', get_template_directory_uri() . '/css/animate.min.css', false, '1.0', false );
    wp_register_style( 'main-style', get_template_directory_uri() . '/css/main-style.css', false, '1.0', false );
    wp_register_style( 'results', get_template_directory_uri() . '/css/pillar-results.css', false, '1.0', false );

}


add_action('wp_enqueue_scripts', 'enqueue_styles');

function enqueue_styles() 
{

  wp_enqueue_style('font-awesome');
  wp_enqueue_style('animation');
  wp_enqueue_style('main-style');
  wp_enqueue_style('results');
  wp_enqueue_style('style', get_template_directory_uri().'/style.css', false, '1.0' );


  if ( is_front_page() ) {
    wp_enqueue_style('style1');    
  } else if ( is_page_template('special.php') ) {
    wp_enqueue_style('style1');
    wp_enqueue_style('style2');
  } else {
    wp_enqueue_style('style1');
  }

}


/////////////////////////////////////////////////////////////////////////////
//     REGISTER SCRIPTS
/////////////////////////////////////////////////////////////////////////////


add_action('wp_enqueue_scripts', 'enqueue_scripts');

function enqueue_scripts() 
{
  wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr.js', array(), '1.0');
  wp_register_script('complexify', get_template_directory_uri() . '/js/jquery.complexify.min.js', array('jquery'), '1.0');  
  wp_register_script('functions-js', get_template_directory_uri() . '/js/functions.js', array('jquery'), '1.0', true);
  wp_register_script('jqueryui', 'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js', array('jquery'), '1.0');
  wp_register_script('browser', get_template_directory_uri() . '/js/browser-detection.js', array('jquery'), '1.0');

  wp_enqueue_script('modernizr');
  wp_enqueue_script('complexify');
  wp_enqueue_script('functions-js');
  wp_enqueue_script('jqueryui');
  wp_enqueue_script('browser');
}


