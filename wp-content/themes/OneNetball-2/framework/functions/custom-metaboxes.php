<?php 

/////////////////////////////////////////////////////////////////////////////
//      CUSTOM METABOXES     
/////////////////////////////////////////////////////////////////////////////

add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

function my_show_extra_profile_fields( $user ) { ?>

  <h3>Club Information</h3>

  <table class="form-table">

    <tr>
      <th><label for="club_name">Name of Club or Association</label></th>
      <td>
        <input type="text" name="club_name" id="club_name" value="<?php echo esc_attr( get_the_author_meta( 'club_name', $user->ID ) ); ?>" class="regular-text" /><br />
        <span class="description">Please enter.</span>
      </td>
    </tr>
    <tr>
      <th><label for="state">State</label></th>
      <td>
        <input type="text" name="state" id="state" value="<?php echo esc_attr( get_the_author_meta( 'state', $user->ID ) ); ?>" class="regular-text" /><br />
        <span class="description">Please enter.</span>
      </td>
    </tr>
    <tr>
      <th><label for="club_role">Role at club or association</label></th>
      <td>
        <input type="text" name="club_role" id="club_role" value="<?php echo esc_attr( get_the_author_meta( 'club_role', $user->ID ) ); ?>" class="regular-text" /><br />
        <span class="description">Please enter.</span>
      </td>
    </tr>    

  </table>
<?php }


add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

  if ( !current_user_can( 'edit_user', $user_id ) )
    return false;

  update_usermeta( $user_id, 'club_name', $_POST['club_name'] );
  update_usermeta( $user_id, 'state', $_POST['state'] );
  update_usermeta( $user_id, 'club_role', $_POST['club_role'] );
}


