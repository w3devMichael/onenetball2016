<?php 

/////////////////////////////////////////////////////////////////////////////
//   REGISTER WOOCOMERCE
/////////////////////////////////////////////////////////////////////////////


remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() 
{
  echo '<section id="main 999">';
}

function my_theme_wrapper_end() 
{
  echo '</section>';
}

add_action( 'after_setup_theme', 'woocommerce_support' );

function woocommerce_support() 
{
    add_theme_support( 'woocommerce' );
}


/////////////////////////////////////////////////////////////////////////////
//    WOOCOMERCE CART
/////////////////////////////////////////////////////////////////////////////


function woocomerce_cart() 
{

  global $woocommerce;
  // get cart quantity
  $qty = $woocommerce->cart->get_cart_contents_count();
  // get cart total
  $total = $woocommerce->cart->get_cart_total();
  // get cart url
  $cart_url = $woocommerce->cart->get_cart_url();

  if($qty>1)
    echo '<a id="cart_in_menu_11" class="cart_is_in_menu" href="'.$cart_url.'">'.$qty.' products | '.$total.'</a>';

  if($qty==1)
    echo '<a id="cart_in_menu_1" class="cart_is_in_menu"  href="'.$cart_url.'">1 product | '.$total.'</a>';

  if($qty==0)
    echo '<a id="cart_in_menu_1" class="cart_is_in_menu"  href="'.$cart_url.'">0 products</a>';
  
}

