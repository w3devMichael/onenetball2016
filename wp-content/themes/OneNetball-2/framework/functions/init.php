<?php 

if (!function_exists('file_require')) {

    /*
     * The function allows us to include deep directory PHP files if they exist in child theme path.
     * Otherwise it works just regularly include main theme files.
     */

    function file_require($file, $uri = false) {
        $file = str_replace("\\", "/", $file); // Replaces If the customer runs on Win machine. Otherway it doesn't perform
        if (is_child_theme()) {
            if (!$uri) {
                $dir = str_replace("\\", "/", get_template_directory());
                $replace = str_replace("\\", "/", get_stylesheet_directory());
                $file_exist = str_replace($dir, $replace, $file);
                $file = str_replace($replace, $dir, $file);
            } else {
                $dir = get_template_directory_uri();
                $replace = get_stylesheet_directory_uri();
                $file_exist = str_replace($dir, $replace, $file);

                $file_child_url = str_replace($dir, get_stylesheet_directory(), $file);
                if( file_exists($file_child_url) ){
                    return $file_exist;
                }
            }

            if( file_exists($file_exist) ){
                $file_child = str_replace($dir, $replace, $file);
                return $file_child;
            }
            return $file;

        } else {
            return $file;
        }
    }
}


require_once file_require(get_template_directory() . '/framework/functions/styles-and-scripts.php');

require_once file_require(get_template_directory() . '/framework/functions/breadcrumbs.php');

require_once file_require(get_template_directory() . '/framework/functions/theme-settings.php');

require_once file_require(get_template_directory() . '/framework/functions/custom-post-types-and-taxonomies.php');

require_once file_require(get_template_directory() . '/framework/functions/woocomerce-settings.php');

require_once file_require(get_template_directory() . '/framework/functions/custom-functions-and-shortcodes.php');

require_once file_require(get_template_directory() . '/framework/functions/custom-metaboxes.php');

require_once file_require(get_template_directory() . '/framework/functions/ajax-requests.php');

require_once file_require(get_template_directory() . '/framework/functions/wp-customise-page-settings.php');

//require_once file_require(get_template_directory() . '/framework/functions/wpbuilder-monitor-settings-page.php');

require_once file_require(get_template_directory() . '/framework/functions/login-register.php');




