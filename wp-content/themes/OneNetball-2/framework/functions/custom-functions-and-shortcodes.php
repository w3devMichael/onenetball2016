<?php 


/////////////////////////////////////////////////////////////////////////////
//  CUSTOM FUNCTIONS
/////////////////////////////////////////////////////////////////////////////

function post_type_tags( $post_type = '' ) {
    global $wpdb;

    if ( empty( $post_type ) ) {
        $post_type = get_post_type();
    }

    return $wpdb->get_results( $wpdb->prepare( "
        SELECT COUNT( DISTINCT tr.object_id ) 
            AS count, tt.taxonomy, tt.description, tt.term_taxonomy_id, t.name, t.slug, t.term_id 
        FROM {$wpdb->posts} p 
        INNER JOIN {$wpdb->term_relationships} tr 
            ON p.ID=tr.object_id 
        INNER JOIN {$wpdb->term_taxonomy} tt 
            ON tt.term_taxonomy_id=tr.term_taxonomy_id 
        INNER JOIN {$wpdb->terms} t 
            ON t.term_id=tt.term_taxonomy_id 
        WHERE p.post_type=%s 
            AND tt.taxonomy='post_tag' 
        GROUP BY tt.term_taxonomy_id 
        ORDER BY count DESC
    ", $post_type ) );
}


/////////////////////////////////////////////////////////////////////////////
//      
/////////////////////////////////////////////////////////////////////////////


function load_template_part($template_name, $part_name=null) {
    ob_start();
    get_template_part($template_name, $part_name);
    $var = ob_get_contents();
    ob_end_clean();
    return $var;
}



/////////////////////////////////////////////////////////////////////////////
//     
/////////////////////////////////////////////////////////////////////////////

add_shortcode('resource_video', function($atts, $content = null) 
{
    extract(shortcode_atts(array(
        'description' => '',
        'youtubeid' => '',
        'image' => '',
    ), $atts));

      $return_string .= '<div class="video-link-open active clearfix">';
      $return_string .= '<div class="wapp">';
      $return_string .= '<img src="'.$image.'" alt="" width="534" height="342"/>';
      $return_string .= ' <figure>';
      $return_string .= '<iframe class="video" width="560" height="315" src="https://www.youtube.com/embed/'.$youtube_id.'" frameborder="0" allowfullscreen></iframe>';
      $return_string .= '</figure>';
      $return_string .= '</div>';
      $return_string .= '<p>'.$description.'</p>';
      $return_string .= '</div>';

    return $return_string;  
});


/////////////////////////////////////////////////////////////////////////////
//      CUSTOME POST TABLES 
/////////////////////////////////////////////////////////////////////////////

//create an instance
$post_columns = new CPT_columns('book'); // if you want to replace and reorder columns then pass a second parameter as true
//add native column
$post_columns->add_column('title',
  array(
    'label'    => __('Title'),
    'type'     => 'native',
    'sortable' => true
  )
);
//add thumbnail column
$post_columns->add_column('post_thumb',
  array(
    'label' => __('Thumb'),
    'type'  => 'thumb',
    'size'  => array('80,80') //size accepted  by the_post_thumbnail as array or string
  )
);
//add taxonomy
$post_columns->add_column('custom_tax_id',
  array(
    'label'    => __('Custom Taxonomy'),
    'type'     => 'custom_tax',
    'taxonomy' => 'category' //taxonomy name
  )
);
//custom field column
$post_columns->add_column('price',
  array(
    'label'    => __('Custom Field'),
    'type'     => 'post_meta',
    'meta_key' => 'price', //meta_key
    'orderby' => 'meta_value', //meta_value,meta_value_num
    'sortable' => true,
    'prefix' => "$",
    'suffix' => "",
    'def' => "", // default value in case post meta not found
  )
);
//remove date column
$post_columns->remove_column('date');


