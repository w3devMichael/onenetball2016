<?php 

/////////////////////////////////////////////////////////////////////////////
//    REGISTER CUSTOM TAXONOMIES
/////////////////////////////////////////////////////////////////////////////

function ambassador_custom_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Ambassadors Categories', 'Taxonomy General Name', 'wpbuilder' ),
		'singular_name'              => _x( 'Ambassador Categorie', 'Taxonomy Singular Name', 'wpbuilder' ),
		'menu_name'                  => __( 'Ambassadors Categories', 'wpbuilder' ),
		'all_items'                  => __( 'All Items', 'wpbuilder' ),
		'parent_item'                => __( 'Parent Item', 'wpbuilder' ),
		'parent_item_colon'          => __( 'Parent Item:', 'wpbuilder' ),
		'new_item_name'              => __( 'New Item Name', 'wpbuilder' ),
		'add_new_item'               => __( 'Add New Item', 'wpbuilder' ),
		'edit_item'                  => __( 'Edit Item', 'wpbuilder' ),
		'update_item'                => __( 'Update Item', 'wpbuilder' ),
		'view_item'                  => __( 'View Item', 'wpbuilder' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'wpbuilder' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'wpbuilder' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wpbuilder' ),
		'popular_items'              => __( 'Popular Items', 'wpbuilder' ),
		'search_items'               => __( 'Search Items', 'wpbuilder' ),
		'not_found'                  => __( 'Not Found', 'wpbuilder' ),
		'no_terms'                   => __( 'No items', 'wpbuilder' ),
		'items_list'                 => __( 'Items list', 'wpbuilder' ),
		'items_list_navigation'      => __( 'Items list navigation', 'wpbuilder' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'rewrite'					 => array('slug' => 'ambassador-categories', 'with_front'  => true),
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'ambassador-categories', array( 'ambassador' ), $args );

	flush_rewrite_rules();

}
add_action( 'init', 'ambassador_custom_taxonomy', 0 );


/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////

/*
add_filter("manage_edit-ambassador-categories_columns", 'ambassador_columns'); 
 
function ambassador_columns($ambassador_columns) {
    $new_columns = array(
        'cb' => '<input type="checkbox" />',
        'name' => __('Name'),
        'header_icon' => '',
       'description' => __('Description'),
        'slug' => __('Slug'),
        'posts' => __('Posts')
        );
    return $new_columns;
}

add_filter("manage_ambassador-categories_custom_column", 'manage_ambassador_columns', 10, 3);
 
function manage_ambassador_columns($out, $column_name, $ambassador_id) {
    $ambassador = get_term($ambassador_id, 'ambassador-categories');
    switch ($column_name) {
        case 'header_icon': 
            // get header image url
            $data = maybe_unserialize($ambassador->description);
            $out .= "<img src=\"{$data['HEADER_image']}\" width=\"250\" height=\"83\"/>"; 
            break;
 
        default:
            break;
    }
    return $out;    
}

*/


/////////////////////////////////////////////////////////////////////////////
//    REGISTER CUSTOM POSTS
/////////////////////////////////////////////////////////////////////////////

function ambassadors_custome_post_type() 
{
	$labels = array(
		'name'               => _x( 'Ambassadors', 'post type general name', 'wpbuilder' ),
		'singular_name'      => _x( 'Ambassador', 'post type singular name', 'wpbuilder' ),
		'menu_name'          => _x( 'Ambassadors', 'admin menu', 'wpbuilder' ),
		'name_admin_bar'     => _x( 'Ambassador', 'add new on admin bar', 'wpbuilder' ),
		'add_new'            => _x( 'Add New', 'Ambassador', 'wpbuilder' ),
		'add_new_item'       => __( 'Add New Ambassador', 'wpbuilder' ),
		'new_item'           => __( 'New Ambassador', 'wpbuilder' ),
		'edit_item'          => __( 'Edit Ambassador', 'wpbuilder' ),
		'view_item'          => __( 'View Ambassador', 'wpbuilder' ),
		'all_items'          => __( 'All Ambassadors', 'wpbuilder' ),
		'search_items'       => __( 'Search Ambassadors', 'wpbuilder' ),
		'parent_item_colon'  => __( 'Parent Ambassadors:', 'wpbuilder' ),
		'not_found'          => __( 'No Ambassadors found.', 'wpbuilder' ),
		'not_found_in_trash' => __( 'No Ambassadors found in Trash.', 'wpbuilder' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
   		'taxonomies'         => array( 'ambassador-categories', ),
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'ambassadors' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    	'menu_icon'          => 'dashicons-businessman'

	);

	register_post_type( 'ambassadors', $args );
}

add_action( 'init', 'ambassadors_custome_post_type' );


/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////


add_filter('manage_edit-ambassadors_columns', 'add_new_ambassadors_columns');

function add_new_ambassadors_columns($ambassadors_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';     
    $new_columns['id'] = __('ID');
    $new_columns['title'] = _x('Ambassador Name', 'column name');
    $new_columns['images'] = __('Image');
    $new_columns['birth_place'] = __('Birth Place');     
    $new_columns['curent_club'] = __('Curent Club');
 
    return $new_columns;
}


add_action('manage_ambassadors_posts_custom_column', 'manage_ambassadors_columns', 10, 2);
 
function get_featured_image($post_ID) {
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'tumb');       
        return $post_thumbnail_img[0];
    }
}

function manage_ambassadors_columns($column_name, $id) {
    global $wpdb;
    switch ($column_name) {
    case 'id':
        echo $id;
    break; 
    case 'images':
        // Get number of images in gallery
		$post_featured_image = get_featured_image($post_ID);
        if ($post_featured_image) {
            // HAS A FEATURED IMAGE
            echo '<img src="' . $post_featured_image . '" />';
        }
        else {
            // NO FEATURED IMAGE, SHOW THE DEFAULT ONE
        }
    break;
    case 'birth_place':
    	$birth_place = get_post_meta($id,'birth_place')[0];
    	echo $birth_place;
    break; 
    case 'curent_club':
    	$curent_club = get_post_meta($id,'current_club')[0];
    	echo $curent_club;
    break; 
    default:
        break;
    } // end switch
}   


/////////////////////////////////////////////////////////////////////////////
//    REGISTER CUSTOM TAXONOMIES
/////////////////////////////////////////////////////////////////////////////

function resources_custom_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Resources Libraries', 'Taxonomy General Name', 'wpbuilder' ),
		'singular_name'              => _x( 'Resource Library', 'Taxonomy Singular Name', 'wpbuilder' ),
		'menu_name'                  => __( 'Resources Libraries', 'wpbuilder' ),
		'all_items'                  => __( 'All Items', 'wpbuilder' ),
		'parent_item'                => __( 'Parent Item', 'wpbuilder' ),
		'parent_item_colon'          => __( 'Parent Item:', 'wpbuilder' ),
		'new_item_name'              => __( 'New Item Name', 'wpbuilder' ),
		'add_new_item'               => __( 'Add New Item', 'wpbuilder' ),
		'edit_item'                  => __( 'Edit Item', 'wpbuilder' ),
		'update_item'                => __( 'Update Item', 'wpbuilder' ),
		'view_item'                  => __( 'View Item', 'wpbuilder' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'wpbuilder' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'wpbuilder' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wpbuilder' ),
		'popular_items'              => __( 'Popular Items', 'wpbuilder' ),
		'search_items'               => __( 'Search Items', 'wpbuilder' ),
		'not_found'                  => __( 'Not Found', 'wpbuilder' ),
		'no_terms'                   => __( 'No items', 'wpbuilder' ),
		'items_list'                 => __( 'Items list', 'wpbuilder' ),
		'items_list_navigation'      => __( 'Items list navigation', 'wpbuilder' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'rewrite'					 => array('slug' => 'resources-libraries', 'with_front'  => true),
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'resources-libraries', array( 'resources' ), $args );

	flush_rewrite_rules();

}
add_action( 'init', 'resources_custom_taxonomy', 0 );


/////////////////////////////////////////////////////////////////////////////
//    REGISTER CUSTOM POSTS
/////////////////////////////////////////////////////////////////////////////

function resources_custome_post_type() 
{
	$labels = array(
		'name'               => _x( 'Resources', 'post type general name', 'wpbuilder' ),
		'singular_name'      => _x( 'Resource', 'post type singular name', 'wpbuilder' ),
		'menu_name'          => _x( 'Resources', 'admin menu', 'wpbuilder' ),
		'name_admin_bar'     => _x( 'Resource', 'add new on admin bar', 'wpbuilder' ),
		'add_new'            => _x( 'Add New', 'Resource', 'wpbuilder' ),
		'add_new_item'       => __( 'Add New Resource', 'wpbuilder' ),
		'new_item'           => __( 'New Resource', 'wpbuilder' ),
		'edit_item'          => __( 'Edit Resource', 'wpbuilder' ),
		'view_item'          => __( 'View Resource', 'wpbuilder' ),
		'all_items'          => __( 'All Resources', 'wpbuilder' ),
		'search_items'       => __( 'Search Resources', 'wpbuilder' ),
		'parent_item_colon'  => __( 'Parent Resources:', 'wpbuilder' ),
		'not_found'          => __( 'No Resources found.', 'wpbuilder' ),
		'not_found_in_trash' => __( 'No Resources found in Trash.', 'wpbuilder' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
   		'taxonomies'         => array( 'resource-libraries', 'post_tag' ),
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'resources' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor','thumbnail', 'excerpt', 'tags' ),
    	'menu_icon'          => 'dashicons-portfolio'

	);

	register_post_type( 'resources', $args );
}

add_action( 'init', 'resources_custome_post_type' );


/////////////////////////////////////////////////////////////////////////////
//    REGISTER CUSTOM TAXONOMIES
/////////////////////////////////////////////////////////////////////////////


function winner_community_custom_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Winner Communitys Categories', 'Taxonomy General Name', 'wpbuilder' ),
		'singular_name'              => _x( 'Winner Community Categorie', 'Taxonomy Singular Name', 'wpbuilder' ),
		'menu_name'                  => __( 'Winner Cmmunitys Categories', 'wpbuilder' ),
		'all_items'                  => __( 'All Items', 'wpbuilder' ),
		'parent_item'                => __( 'Parent Item', 'wpbuilder' ),
		'parent_item_colon'          => __( 'Parent Item:', 'wpbuilder' ),
		'new_item_name'              => __( 'New Item Name', 'wpbuilder' ),
		'add_new_item'               => __( 'Add New Item', 'wpbuilder' ),
		'edit_item'                  => __( 'Edit Item', 'wpbuilder' ),
		'update_item'                => __( 'Update Item', 'wpbuilder' ),
		'view_item'                  => __( 'View Item', 'wpbuilder' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'wpbuilder' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'wpbuilder' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'wpbuilder' ),
		'popular_items'              => __( 'Popular Items', 'wpbuilder' ),
		'search_items'               => __( 'Search Items', 'wpbuilder' ),
		'not_found'                  => __( 'Not Found', 'wpbuilder' ),
		'no_terms'                   => __( 'No items', 'wpbuilder' ),
		'items_list'                 => __( 'Items list', 'wpbuilder' ),
		'items_list_navigation'      => __( 'Items list navigation', 'wpbuilder' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'rewrite'					 => array('slug' => 'winner-community-categories', 'with_front'  => true),
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'winner-community-categories', array( 'winner-community' ), $args );

	flush_rewrite_rules();

}
add_action( 'init', 'winner_community_custom_taxonomy', 0 );


/////////////////////////////////////////////////////////////////////////////
//    REGISTER CUSTOM POSTS
/////////////////////////////////////////////////////////////////////////////


function winner_communitys_custome_post_type() 
{
	$labels = array(
		'name'               => _x( 'Winner Communitys', 'post type general name', 'wpbuilder' ),
		'singular_name'      => _x( 'Winner Community', 'post type singular name', 'wpbuilder' ),
		'menu_name'          => _x( 'Winner Communitys', 'admin menu', 'wpbuilder' ),
		'name_admin_bar'     => _x( 'Winner Community', 'add new on admin bar', 'wpbuilder' ),
		'add_new'            => _x( 'Add New', 'Winner Community', 'wpbuilder' ),
		'add_new_item'       => __( 'Add New Winner Community', 'wpbuilder' ),
		'new_item'           => __( 'New Winner Community', 'wpbuilder' ),
		'edit_item'          => __( 'Edit Winner Community', 'wpbuilder' ),
		'view_item'          => __( 'View Winner Community', 'wpbuilder' ),
		'all_items'          => __( 'All Winner Communitys', 'wpbuilder' ),
		'search_items'       => __( 'Search Winner Communitys', 'wpbuilder' ),
		'parent_item_colon'  => __( 'Parent Winner Communitys:', 'wpbuilder' ),
		'not_found'          => __( 'No Winner Communitys found.', 'wpbuilder' ),
		'not_found_in_trash' => __( 'No Winner Communitys found in Trash.', 'wpbuilder' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
   		'taxonomies'         => array( 'winner-community-categories', ),
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'winner-communitys' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'tags' ),
    	'menu_icon'          => 'dashicons-businessman'

	);

	register_post_type( 'winner-communitys', $args );
}

add_action( 'init', 'winner_communitys_custome_post_type' );