<?php 


/*
// Change "From" email address
add_filter( 'wp_mail_from', function( $email ) {
  return 'hello@mydomain.com';
});
// Change "From" email name
add_filter( 'wp_mail_from_name', function( $name ) {
  return __( 'My Website' );
});
// Change Subject
add_filter( 'retrieve_password_title', function( $title, $user_login, $user_data ) {
  return __( 'Password Recovery' );
}, 10, 3 );
// Change email type to HTML
add_filter( 'wp_mail_content_type', function( $content_type ) {
  return 'text/html';
});
// Change the message/body of the email
add_filter( 'retrieve_password_message', 'rv_new_retrieve_password_message', 10, 4 );
function rv_new_retrieve_password_message( $message, $key, $user_login, $user_data ){

  $reset_url = add_query_arg( array(
    'action' => 'rp',
    'key' => $key,
    'login' => rawurlencode( $user_login )
  ), wp_login_url() );
  ob_start();
  
  printf( '<p>%s</p>', __( 'Hi, ' ) . get_user_meta( $user_data->ID, 'first_name', true ) );
  printf( '<p>%s</p>', __( 'It looks like you need to reset your password on the site. If this is correct, simply click the link below. If you were not the one responsible for this request, ignore this email and nothing will happen.' ) );
  printf( '<p><a href="%s">%s</a></p>', $reset_url, __( 'Reset Your Password' ) );
  
  $message = ob_get_clean();
  return $message;
}
*/

/////////////////////////////////////////////////////////////////////////////
//         
/////////////////////////////////////////////////////////////////////////////

if( !get_page_by_title('Login Page')) : 

  global $user_ID;

  $login_page = array(
    'post_title' => 'Login Page',
    'post_content' => '',
    'post_status' => 'publish',
    'post_date' => date('Y-m-d H:i:s'),
    'post_author' => $user_ID,
    'post_type' => 'page',
    );
  $login_page_id = wp_insert_post($login_page);

  if( !$login_page_id ) :
    wp_die('Error creating template page');
  else :
    update_post_meta( $login_page_id, '_wp_page_template', 'page-login.php' );
  endif;

  if( $login_page_id ) :

    $lost_password = array(
      'post_title' => 'Lost Password',
      'post_content' => '[lost-password-form]',
      'post_status' => 'publish',
      'post_parent' => $login_page_id,
      'post_date' => date('Y-m-d H:i:s'),
      'post_author' => $user_ID,
      'post_type' => 'page',
      );
    $lost_password_id = wp_insert_post($lost_password);

    $password_reset = array(
      'post_title' => 'Password Reset',
      'post_content' => '[password-reset-form]',
      'post_status' => 'publish',
      'post_parent' => $login_page_id,
      'post_date' => date('Y-m-d H:i:s'),
      'post_author' => $user_ID,
      'post_type' => 'page',
      );
    $password_reset_id = wp_insert_post($password_reset);

  endif;

endif;


/////////////////////////////////////////////////////////////////////////////
//         
/////////////////////////////////////////////////////////////////////////////

function registration_validation( $email, $password, $password_repeat, $username, $club_name, $state, $club_role  )  {
  
    global $reg_errors;

    $state_array = array('ACT', 'New South Wales', 'Northern Territory', 'Queensland', 'South Australia', 'Tasmania', 'Victoria', 'Western Australia');

    $club_role_array = array('Coach', 'Umpire', 'Committee member', 'President', 'Parent', 'Other');

    $containsLetter  = preg_match('/[a-zA-Z]/',    $password);
    $containsDigit   = preg_match('/\d/',          $password);
    $containsSpecial = preg_match('/[^a-zA-Z\d]/', $password);

    $input_errors = "";
    $files_warnings = array();
    $field_check = 1;

    $reg_errors = new WP_Error;

    if ( empty( $username ) ) {
        $reg_errors->add('field-username', 'Required username form field is missing');
        if( empty($files_warnings['username']) ) {
          $files_warnings += ['username' => 1];
        }
        $field_check = 0;
    }

    if ( empty( $password ) ) {
        $reg_errors->add('field-password', 'Required password form field is missing');
         if( empty($files_warnings['password']) ) {
          $files_warnings += ['password' => 1];
        }       
    }

    if (empty( $email ) ) {
        $reg_errors->add('field-email', 'Required email form field is missing');
        if( empty($files_warnings['email']) ) {
          $files_warnings += ['email' => 1];
        } 
        $field_check = 0; 
    }

    if ( empty( $club_name ) ) {
        $reg_errors->add('field-club-name', 'Required club name form field is missing');
        if( empty($files_warnings['clubName']) ) {
          $files_warnings += ['clubName' => 1];       
        }
        $field_check = 0;
    }

    if ( ! in_array($state, $state_array) ) {
        $reg_errors->add('field-state', 'Required state form field is missing');
        $files_warnings += ['clubState' => 1];       
        $field_check = 0;
    }

    if (! in_array($club_role, $club_role_array) ) {
        $reg_errors->add('field-club-role', 'Required club role form field is missing');
        
        $files_warnings += ['clubRole' => 1];
        $field_check = 0;
    }


    if($field_check === 1 ) {

      if ( strlen( $username ) < 4 ) {
          $reg_errors->add('username_length', 'Username too short. At least 4 characters is required');

          if( empty($files_warnings['username']) ) {
         		$files_warnings += ['username' => 1];
         	}
      }

      if ( username_exists( $username ) ) {
          $reg_errors->add('user_name', 'Sorry, that username already exists!');

          if( empty($files_warnings['username']) ) {
         		$files_warnings += ['username' => 1];
         	}
      }

      if ( !validate_username( $username ) ) {
          $reg_errors->add('username_invalid', 'Sorry, the username you entered is not valid');

          if( empty($files_warnings['username']) ) {
         		$files_warnings += ['username' => 1];
         	}
      }

      if ( strlen( $password ) < 7 ) {
          $reg_errors->add('password', 'Password length must be greater than 7');
          if( empty($files_warnings['password']) ) {
         		$files_warnings += ['password' => 1];
         	}
      }

      // if ( !$containsLetter ) {
      //     $reg_errors->add('password', 'Password must contain uppercase letter');
      //     if( empty($files_warnings['password']) ) {
      //       $files_warnings += ['password' => 1];
      //     }
      // }

      // if ( !$containsDigit ) {
      //     $reg_errors->add('password', 'Password must contain number');
      //     if( empty($files_warnings['password']) ) {
      //       $files_warnings += ['password' => 1];
      //     }
      // }

      // if ( !$containsSpecial ) {
      //     $reg_errors->add('password', 'Password must contain symbols');
      //     if( empty($files_warnings['password']) ) {
      //       $files_warnings += ['password' => 1];
      //     }
      // }

      if ( $password !== $password_repeat ) {
          $reg_errors->add('password', 'Passwords do not mach');
          if( empty($files_warnings['passwordRepeat']) ) {
         		$files_warnings += ['passwordRepeat' => 1];
         	}        
      }      

      if ( !is_email( $email ) ) {
          $reg_errors->add('email_invalid', 'Email is not valid');
          if( empty($files_warnings['email']) ) {
         		$files_warnings += ['email' => 1];
         	}  
      }

      if ( email_exists( $email ) ) {
          $reg_errors->add('email', 'Email Already in use');
          if( empty($files_warnings['email']) ) {
         		 $files_warnings += ['email' => 1];
         	}        
      }  

    }

    if (is_wp_error( $reg_errors ) && ! empty( $reg_errors->errors ) ) {

        foreach ( $reg_errors->get_error_messages() as $error ) {
            $input_errors .= '<li>';
            $input_errors .= $error ;
            $input_errors .= '</li>';
        }

		$arr = array('status' => 0, 'input_errors' => $input_errors, 'files_warnings' => $files_warnings );

		echo json_encode($arr);

		die();
    }
}

function complete_registration() {
    global $reg_errors, $email, $username, $password, $club_name, $state, $club_role, $updatesRequired;

    if ( count($reg_errors->get_error_messages()) < 1 ) {
      $userdata = array(
        'user_login'  =>  $username,
        'user_email'  =>  $email,
        'user_pass'   =>  $password,
        'display_name'  =>  $username,
      );

      $user = wp_insert_user( $userdata );

      if ( ! is_wp_error(  $user ) ) {
        $user_id =  $user;

        $role = ($updatesRequired == 'false') ? 'inclusion' : 'inclusionupdates' ;

        $userObj = new WP_User( $user_id );
      
        $userObj->set_role($role);

        update_user_meta( $user_id, 'show_admin_bar_front', false ); 
        update_usermeta( $user_id, 'club_name', $club_name );
        update_usermeta( $user_id, 'state', $state );
        update_usermeta( $user_id, 'club_role', $club_role );

        $creds = array();
            $creds['user_login'] = $userdata['user_login'];
            $creds['user_password'] = $userdata['user_pass'];
            //$creds['remember'] = true;
            $user = wp_signon( $creds, false );


      }

    	$arr = array('status' => 1);

    	echo json_encode($arr);

    	die();
    }
}


function user_registration_ajax() {

  registration_validation( $_POST['email'], $_POST['password'], $_POST['passwordRepeat'], $_POST['username'], $_POST['clubName'], $_POST['state'], $_POST['clubRole']  );

  global $email, $username, $password, $club_name, $state, $club_role, $updatesRequired;

  $username =   sanitize_user($_POST['username']);
  $password   =   esc_attr($_POST['password']);
  $email    =   sanitize_email($_POST['email']);
  $club_name =   sanitize_text_field($_POST['clubName']);
  $state  =   sanitize_text_field($_POST['state']);
  $club_role  =   sanitize_text_field($_POST['clubRole']);
  $updatesRequired  =   sanitize_text_field($_POST['updatesRequired']);

  complete_registration( $email, $username, $password, $club_name, $state, $club_role, $updatesRequired);
 
}

add_action('wp_ajax_nopriv_user_registration_ajax', 'user_registration_ajax');
add_action('wp_ajax_user_registration_ajax', 'user_registration_ajax');


function update_validation( $email, $username, $club_name, $state, $club_role  )  {
  
    global $reg_errors;

    $user_id = get_current_user_id();

    $current_user = wp_get_current_user();

    $state_array = array('ACT', 'New South Wales', 'Northern Territory', 'Queensland', 'South Australia', 'Tasmania', 'Victoria', 'Western Australia');

    $club_role_array = array('Coach', 'Umpire', 'Committee member', 'President', 'Parent', 'Other');

    $input_errors = "";
    $files_warnings = array();
    $field_check = 1;

    $reg_errors = new WP_Error;

    if ( empty( $username ) ) {
        $reg_errors->add('field-username', 'Required username form field is missing');
        if( empty($files_warnings['username']) ) {
          $files_warnings += ['username' => 1];
        }
        $field_check = 0;
    }

    if (empty( $email ) ) {
        $reg_errors->add('field-email', 'Required email form field is missing');
        if( empty($files_warnings['email']) ) {
          $files_warnings += ['email' => 1];
        } 
        $field_check = 0; 
    }

    if ( empty( $club_name ) ) {
        $reg_errors->add('field-club-name', 'Required club name form field is missing');
        if( empty($files_warnings['clubName']) ) {
          $files_warnings += ['clubName' => 1];       
        }
        $field_check = 0;
    }

    if ( ! in_array($state, $state_array) ) {
        $reg_errors->add('field-state', 'Required state form field is missing');
        $files_warnings += ['clubState' => 1];       
        $field_check = 0;
    }

    if (! in_array($club_role, $club_role_array) ) {
        $reg_errors->add('field-club-role', 'Required club role form field is missing');
        
        $files_warnings += ['clubRole' => 1];
        $field_check = 0;
    }


    if($field_check === 1 ) {

      if ($current_user->user_login != $username) {
        if ( username_exists( $username ) ) {
            $reg_errors->add('user_name', 'Sorry, that username already exists!');

            if( empty($files_warnings['username']) ) {
              $files_warnings += ['username' => 1];
            }
        }
      }

      if ( strlen( $username ) < 4 ) {
          $reg_errors->add('username_length', 'Username too short. At least 4 characters is required');

          if( empty($files_warnings['username']) ) {
            $files_warnings += ['username' => 1];
          }
      }

      if ( !validate_username( $username ) ) {
          $reg_errors->add('username_invalid', 'Sorry, the username you entered is not valid');

          if( empty($files_warnings['username']) ) {
            $files_warnings += ['username' => 1];
          }
      }    

      if ( !is_email( $email ) ) {
          $reg_errors->add('email_invalid', 'Email is not valid');
          if( empty($files_warnings['email']) ) {
            $files_warnings += ['email' => 1];
          }  
      }

      if ($current_user->user_email != $email) {

        if (email_exists( $email ) ) {
            $reg_errors->add('email', 'Email Already in use');
            $reg_errors->add('email', 'The email ' . $email . ' is already in use by another user.');
            if( empty($files_warnings['email']) ) {
               $files_warnings += ['email' => 1];
            }        
        }

      }  

    }

    if (is_wp_error( $reg_errors ) && ! empty( $reg_errors->errors ) ) {

        foreach ( $reg_errors->get_error_messages() as $error ) {
            $input_errors .= '<li>';
            $input_errors .= $error ;
            $input_errors .= '</li>';
        }

    $arr = array('status' => 0, 'input_errors' => $input_errors, 'files_warnings' => $files_warnings );

    echo json_encode($arr);

    die();
    }
}


function update_registration() {
    global $reg_errors, $email, $username, $club_name, $state, $club_role, $updatesRequired;

    $user_id = get_current_user_id();

    if ( count($reg_errors->get_error_messages()) < 1 ) {
      $userdata = array(
        'ID' => $user_id,
        'user_email'  =>  $email,
      );

      $user = wp_update_user( $userdata );

      if ( ! is_wp_error(  $user ) ) {
        $user_id =  $user;

        $role = ($updatesRequired == 'false') ? 'inclusion' : 'inclusionupdates' ;

        $userObj = new WP_User( $user_id );


        $roles = getRoles($userObj);

        if ( $roles[0] !== 'administrator' && $roles[0] !== 'one_netball_admin' && $roles[0] !== 'editor' && $roles[0] !== 'author' && $roles[0] !== 'contributor' ){
           $userObj->set_role($role);
        }
       

        update_user_meta( $user_id, 'show_admin_bar_front', false ); 
        update_usermeta( $user_id, 'club_name', $club_name );
        update_usermeta( $user_id, 'state', $state );
        update_usermeta( $user_id, 'club_role', $club_role );

        $creds = array();
            $creds['user_login'] = $userdata['user_login'];
            //$creds['remember'] = true;
            //$user = wp_signon( $creds, true );
      }

      $arr = array('status' => 1, 'updatesRequired' => $updatesRequired);

      echo json_encode($arr);

      die();
    }
}

function user_registration_update_ajax() {

  update_validation( $_POST['email'], $_POST['username'], $_POST['clubName'], $_POST['state'], $_POST['clubRole'] );

  global $email, $username, $club_name, $state, $club_role, $updatesRequired;

  $username =   sanitize_user($_POST['username']);
  $email    =   sanitize_email($_POST['email']);
  $club_name =   sanitize_text_field($_POST['clubName']);
  $state  =   sanitize_text_field($_POST['state']);
  $club_role  =   sanitize_text_field($_POST['clubRole']);
  $updatesRequired  =   sanitize_text_field($_POST['updatesRequired']);

  update_registration( $email, $username, $club_name, $state, $club_role, $updatesRequired);
 
}

add_action('wp_ajax_nopriv_user_registration_update_ajax', 'user_registration_update_ajax');
add_action('wp_ajax_user_registration_update_ajax', 'user_registration_update_ajax');



function registration_password_validation( $password, $password_repeat )  {
  
    global $reg_errors;

    $containsLetter  = preg_match('/[a-zA-Z]/',    $password);
    $containsDigit   = preg_match('/\d/',          $password);
    $containsSpecial = preg_match('/[^a-zA-Z\d]/', $password);

    $input_errors = "";
    $files_warnings = array();
    $field_check = 1;

    $reg_errors = new WP_Error;

    if ( empty( $password ) ) {
        $reg_errors->add('field-password', 'Required password form field is missing');
         if( empty($files_warnings['password']) ) {
          $files_warnings += ['password' => 1];
        }       
    }

    if($field_check === 1 ) {

      if ( strlen( $password ) < 7 ) {
          $reg_errors->add('password', 'Password length must be greater than 7');
          if( empty($files_warnings['password']) ) {
            $files_warnings += ['password' => 1];
          }
      }

      // if ( !$containsLetter ) {
      //     $reg_errors->add('password', 'Password must contain uppercase letter');
      //     if( empty($files_warnings['password']) ) {
      //       $files_warnings += ['password' => 1];
      //     }
      // }

      // if ( !$containsDigit ) {
      //     $reg_errors->add('password', 'Password must contain number');
      //     if( empty($files_warnings['password']) ) {
      //       $files_warnings += ['password' => 1];
      //     }
      // }

      // if ( !$containsSpecial ) {
      //     $reg_errors->add('password', 'Password must contain symbols');
      //     if( empty($files_warnings['password']) ) {
      //       $files_warnings += ['password' => 1];
      //     }
      // }

      if ( $password !== $password_repeat ) {
          $reg_errors->add('password', 'Passwords do not mach');
          if( empty($files_warnings['passwordRepeat']) ) {
            $files_warnings += ['passwordRepeat' => 1];
          }        
      }      

    }

    if (is_wp_error( $reg_errors ) && ! empty( $reg_errors->errors ) ) {

        foreach ( $reg_errors->get_error_messages() as $error ) {
            $input_errors .= '<li>';
            $input_errors .= $error ;
            $input_errors .= '</li>';
        }

    $arr = array('status' => 0, 'input_errors' => $input_errors, 'files_warnings' => $files_warnings );

    echo json_encode($arr);

    die();
    }
}

function complete_password_update() {
    global $reg_errors, $password;

    $user_id = get_current_user_id();

    if ( count($reg_errors->get_error_messages()) < 1 ) {

      $userdata = array(
        'ID' => $user_id,
        'user_pass'  =>  $password,
      );

      $user = wp_update_user( $userdata );

      if ( ! is_wp_error(  $user ) ) {
        $user_id =  $user;

        update_user_meta( $user_id, 'show_admin_bar_front', false ); 

      }

      $arr = array('status' => 1);

      echo json_encode($arr);

      die();
    }
}

function user_registration_password_update_ajax() {

  registration_password_validation( $_POST['password'], $_POST['passwordRepeat'] );

  global $password;

  $password   =   esc_attr($_POST['password']);

  complete_password_update( $password);
 
}

add_action('wp_ajax_nopriv_user_registration_password_update_ajax', 'user_registration_password_update_ajax');
add_action('wp_ajax_user_registration_password_update_ajax', 'user_registration_password_update_ajax');


/////////////////////////////////////////////////////////////////////////////
//         
/////////////////////////////////////////////////////////////////////////////

function login_member() {
 
    if(isset($_POST['user_login']) && wp_verify_nonce($_POST['login_nonce'], 'login-nonce')) {
 
        // this returns the user ID and other info from the user name
        if(!isset($_POST['user_login']) || $_POST['user_login'] == '') {
            // if no password was entered
          errors()->add('empty_username', __('Please enter a username'));

        } else {

          $user = get_user_by('email', $_POST['user_login']);

        }

        
 
        if(!$user) {
            // if the user name doesn't exist
            errors()->add('empty_username', __('Invalid username'));
        }
 
        if(!isset($_POST['user_pass']) || $_POST['user_pass'] == '') {
            // if no password was entered
            errors()->add('empty_password', __('Please enter a password'));
        } else {

          // check the user's login with their password
          $user_pass_set = true;

        }

        if ($user && $user_pass_set){

          if(!wp_check_password($_POST['user_pass'], $user->user_pass, $user->ID)) {
              // if the password is incorrect for the specified user
              errors()->add('empty_password', __('Incorrect password'));
          }

        } else {
          errors()->add('empty_password', __('There was an issue with your login'));
        }
 
        
 
        // retrieve all error messages
        $errors = errors()->get_error_messages();
 
        // only log the user in if there are no errors
        if(empty($errors)) { 
 
            $creds = array();
            $creds['user_login'] = $user->user_login;
            $creds['user_password'] = $_POST['user_pass'];
            $creds['remember'] = true;
            $user = wp_signon( $creds, false );

             
            $login_url = get_site_url() . '/inclusion-action-survey/';   
            wp_redirect($login_url);
            exit;
        }
    }
}
add_action('init', 'login_member');


// used for tracking error messages
function errors(){
    static $wp_error; // Will hold global variable safely
    return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
}

// displays error messages from form submissions
function show_error_messages() {
    if($codes = errors()->get_error_codes()) {
       echo '<div class="form-errors" style="display: block;"><strong>There was an error with your submission:</strong><ul>';

        foreach($codes as $code){
                $message = errors()->get_error_message($code);
                echo '<li>' . $message . '</li>';
            }
        echo '</ul></div>';
    }   
}

/////////////////////////////////////////////////////////////////////////////
//         
/////////////////////////////////////////////////////////////////////////////



add_action( 'login_form_lostpassword', 'do_password_lost' );

add_shortcode( 'lost-password-form', 'render_lost_password_form' );

function render_lost_password_form( $attributes, $content = null ) { 
    if ( is_user_logged_in() ) {
        return __( 'You are already signed in.', 'personalize-login' );
    }else {       
        return get_template_html( 'lost_password_form', $attributes );      
    }
}

function do_password_lost() {
    if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
        $errors = retrieve_password();
        if ( is_wp_error( $errors ) ) {
            // Errors found
            $redirect_url = home_url( 'lost-password' );
            $redirect_url = add_query_arg( 'errors', join( ',', $errors->get_error_codes() ), $redirect_url );
        } else {
            // Email sent
            $redirect_url = home_url( 'lost-password' );
            $redirect_url = add_query_arg( 'checkemail', 'confirm', $redirect_url );
        }
 
        wp_redirect( $redirect_url );
        exit;
    }
}

add_filter( 'retrieve_password_message',  'replace_retrieve_password_message', 10, 4 );


function replace_retrieve_password_message( $message, $key, $user_login, $user_data ) {
    // Create new message
    $msg  = __( 'Hello!', 'personalize-login' ) . "\r\n\r\n";
    $msg .= sprintf( __( 'You asked us to reset your password for your account using the email address %s.', 'personalize-login' ), $user_login ) . "\r\n\r\n";
    $msg .= __( "If this was a mistake, or you didn't ask for a password reset, just ignore this email and nothing will happen.", 'personalize-login' ) . "\r\n\r\n";
    $msg .= __( 'To reset your password, visit the following address:', 'personalize-login' ) . "\r\n\r\n";
    $msg .= site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . "\r\n\r\n";
    $msg .= __( 'Thanks!', 'personalize-login' ) . "\r\n";
 
    return $msg;
}


add_action( 'login_form_rp', 'redirect_to_custom_password_reset'  );
add_action( 'login_form_resetpass', 'redirect_to_custom_password_reset'  );

function redirect_to_custom_password_reset() {
    if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
        // Verify key / login combo
        $user = check_password_reset_key( $_REQUEST['key'], $_REQUEST['login'] );
        if ( ! $user || is_wp_error( $user ) ) {
            if ( $user && $user->get_error_code() === 'expired_key' ) {
                wp_redirect( home_url( 'password-reset?login=expiredkey' ) );
            } else {
                wp_redirect( home_url( 'password-reset?login=invalidkey' ) );
            }
            exit;
        }
 
        $redirect_url = home_url( 'password-reset' );
        $redirect_url = add_query_arg( 'login', esc_attr( $_REQUEST['login'] ), $redirect_url );
        $redirect_url = add_query_arg( 'key', esc_attr( $_REQUEST['key'] ), $redirect_url );
 
        wp_redirect( $redirect_url );
        exit;
    }
}

add_shortcode( 'password-reset-form', 'render_password_reset_form' );

function render_password_reset_form( $attributes, $content = null ) {
    // Parse shortcode attributes
    $default_attributes = array( 'show_title' => false );
    $attributes = shortcode_atts( $default_attributes, $attributes );
 
    if ( is_user_logged_in() ) {
        return __( 'You are already signed in.', 'personalize-login' );
    } else {
        if ( isset( $_REQUEST['login'] ) && isset( $_REQUEST['key'] ) ) {
            $attributes['login'] = $_REQUEST['login'];
            $attributes['key'] = $_REQUEST['key'];
 
            // Error messages
            $errors = array();
            if ( isset( $_REQUEST['error'] ) ) {
                $error_codes = explode( ',', $_REQUEST['error'] );
 
                foreach ( $error_codes as $code ) {
                    $errors []= get_error_message( $code );
                }
            }
            $attributes['errors'] = $errors;
 
            return get_template_html( 'reset_password_form', $attributes );
        } else {
            return __( 'Invalid password reset link.', 'personalize-login' );
        }
    }
}


add_action( 'login_form_rp',  'do_password_reset' );
add_action( 'login_form_resetpass', 'do_password_reset' );


function do_password_reset() {
    if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
        $rp_key = $_REQUEST['rp_key'];
        $rp_login = $_REQUEST['rp_login'];
 
        $user = check_password_reset_key( $rp_key, $rp_login );
 
        if ( ! $user || is_wp_error( $user ) ) {
            if ( $user && $user->get_error_code() === 'expired_key' ) {
                wp_redirect( home_url( 'lost-password?login=expiredkey' ) );
            } else {
                wp_redirect( home_url( 'lost-password?login=invalidkey' ) );
            }
            exit;
        }
 
        if ( isset( $_POST['pass1'] ) ) {
            if ( $_POST['pass1'] != $_POST['pass2'] ) {
                // Passwords don't match
                $redirect_url = home_url( 'lost-password' );
 
                $redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
                $redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
                $redirect_url = add_query_arg( 'error', 'password_reset_mismatch', $redirect_url );
 
                wp_redirect( $redirect_url );
                exit;
            }
 
            if ( empty( $_POST['pass1'] ) ) {
                // Password is empty
                $redirect_url = home_url( 'lost-password' );
 
                $redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
                $redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
                $redirect_url = add_query_arg( 'error', 'password_reset_empty', $redirect_url );
 
                wp_redirect( $redirect_url );
                exit;
            }
 
            // Parameter checks OK, reset password
            reset_password( $user, $_POST['pass1'] );
            wp_redirect( home_url( 'lost-password?password=changed' ) );
        } else {
            echo "Invalid request.";
        }
 
        exit;
    }
}