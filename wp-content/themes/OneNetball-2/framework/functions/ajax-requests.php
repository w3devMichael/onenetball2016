<?php 

/////////////////////////////////////////////////////////////////////////////
//     AJAX REQUESTS            
/////////////////////////////////////////////////////////////////////////////


function load_posts_ajax(){
  
    $paged = $_POST['paged'];
    $status = $_POST['status'];
    $keyword = $_POST['keyword'];


    if($status == 102):

      $date = explode(' ',$keyword);
      $month = date_parse($date[0]);
      $year = $date[1];

      $args = array(
        'post_type' => 'post',  
        'posts_per_page'  =>  $paged,
        'date_query' => array(
          array(
            'year'  => $year,
            'month' => $month["month"],
          ),
        ),
      );
    elseif( $status == 101 ):
      $args = array(
          'post_type' => 'post',  
          's' => $keyword,
          'posts_per_page'  =>  $paged,
      );
    elseif( $status == 100 ):   
      $args = array(
          'post_type' => 'post', 
          'posts_per_page'  =>  get_option('posts_per_page'),
          'paged' => $paged,
      );        
    endif;

    $the_query = new WP_Query( $args );

    if ($the_query->have_posts()) :

      while ($the_query->have_posts()): 

        $the_query->the_post();

        $format = (get_post_format()) ? get_post_format() : "standard";

        get_template_part('templates/format/' . $format );

      endwhile; 

    else:
      if( $status == 101 ):
          echo '<h1>No posts found with "'. $keyword . '" keyword.</h1>';
      endif;
    endif;

    wp_reset_postdata();    
    
   die();
}

add_action('wp_ajax_nopriv_load_posts_ajax', 'load_posts_ajax');
add_action('wp_ajax_load_posts_ajax', 'load_posts_ajax');


/////////////////////////////////////////////////////////////////////////////
//     AJAX REQUESTS            
/////////////////////////////////////////////////////////////////////////////

function load_resources_ajax(){
  
    $paged = $_POST['paged'];
    $status = $_POST['status'];
    $sorting = $_POST['sorting'];
    $input_keyword = $_POST['inputKeyword'];
    $option_keyword = $_POST['optionKeyword'];
    $checkbox_keyword = $_POST['checkboxKeyword'];

    $args = [];
    
   if( $status == 101 ):

      $param = stripslashes($_POST['checkboxKeyword']);
      $tag_options = json_decode($param,true);
      foreach($tag_options as $term_id) {
        $tag_ids[] = $term_id;
      }

      if($option_keyword === 0) {
        $content_type = "";
      }else {
        $content_type = $option_keyword;
      }

      if(!empty($input_keyword)) {
         $keyword = $input_keyword;
      }else {
        $keyword = "";
      }

      $args = array(
          'post_type' => 'resources',
          'tag__in' => $tag_ids,   
          'resources-libraries' => $content_type,
          's' => $keyword,
          'posts_per_page'  =>  $paged,
          'order'   => $sorting,
          'orderby'   => 'modified',
          'posts_per_page'  =>  -1,
          'post_status' => 'publish'
          );
   

      
    elseif( $status == 100 ):   
      $args = array(
          'post_type' => 'resources',
          'posts_per_page'  =>  -1,
          'paged' => $paged,
          'orderby' => 'modified',
          'order'   => $sorting,
          'post_status' => 'publish'
      );        
    endif;

    $the_query = new WP_Query( $args );

    if ($the_query->have_posts()) :

      $html = "";
      $total_results = 0;

      while ($the_query->have_posts()): 

        $total_results++;
        $the_query->the_post();
        $format = (get_post_format()) ? get_post_format() : "standard";

        if($format === "video") :
          $html .= load_template_part('templates/format/resources-video' );
        else : 
          $html .= load_template_part('templates/format/resources-standard');
        endif;

      endwhile; 

    else:
      if( $status == 101 ):
        $html = '<h1>No posts found with selected settings.</h1>';
        $arr = array('html' => $html, 'total_results' => $total_results );
      endif;
    endif;

    if($status === 101) {
      $arr = array('html' => $html, 'total_results' => $total_results );
    }else {
      $arr = array('html' => $html, 'total_results' => $total_results, 'args' => $args );
    }


    echo json_encode($arr);

   wp_reset_query();
    
   die();
}

add_action('wp_ajax_nopriv_load_resources_ajax', 'load_resources_ajax');
add_action('wp_ajax_load_resources_ajax', 'load_resources_ajax');


/////////////////////////////////////////////////////////////////////////////
//     AJAX REQUESTS            
/////////////////////////////////////////////////////////////////////////////

function load_category_resources_ajax(){
  
    $paged = $_POST['paged'];
    $status = $_POST['status'];
    $sorting = $_POST['sorting'];
    $input_keyword = $_POST['inputKeyword'];
    $option_keyword = $_POST['optionKeyword'];
    $checkbox_keyword = $_POST['checkboxKeyword'];

    if($option_keyword === 0) {
      $content_type = "";
    }else {
      $content_type = $option_keyword;
    }

    if( $status == 101 ):

      $param = stripslashes($_POST['checkboxKeyword']);
      $tag_options = json_decode($param,true);
      foreach($tag_options as $term_id) {
        $tag_ids[] = $term_id;
      }

      if(!empty($input_keyword)) {
         $keyword = $input_keyword;
      }else {
        $keyword = "";
      }    

      $args = array(
          'post_type' => 'resources',    
          'tag__in' => $tag_ids,   
          'resources-libraries' => $content_type,
          's' => $keyword,
          'posts_per_page'  =>  $paged,
          'order'   => $sorting,
          'orderby'=> 'modified',
          'post_status' => 'publish'
      );
    elseif( $status == 100 ):   
      $args = array(
          'post_type' => 'resources',
          'resources-libraries' => $content_type,
          'posts_per_page'  =>  -1,
          'paged' => $paged,
          'order'   => $sorting,
          'orderby'=> 'modified',
          'post_status' => 'publish'
      );        
    endif;

    $the_query = new WP_Query( $args );

    if ($the_query->have_posts()) :

      $html = "";
      $total_results = 0;

      while ($the_query->have_posts()): 

        $total_results++;
        $the_query->the_post();

        $html .= load_template_part('templates/format/resources-cat-video' );

      endwhile; 

    else:
      if( $status == 101 ):
        $html = '<h1>No posts found with selected settings.</h1>';
        $arr = array('html' => $html, 'total_results' => $total_results );
      endif;
    endif;

    if($status === 101) {
      $arr = array('html' => $html, 'total_results' => $total_results );
    }else {
      $arr = array('html' => $html, 'total_results' => $total_results);
    }

    echo json_encode($arr);  

    wp_reset_postdata();    
    
   die();
}

add_action('wp_ajax_nopriv_load_category_resources_ajax', 'load_category_resources_ajax');
add_action('wp_ajax_load_category_resources_ajax', 'load_category_resources_ajax');


