<?php

/**
 * Include the TGM_Plugin_Activation class.
 */

require_once file_require( get_template_directory().'/framework/classes/class-tgm-plugin-activation.php');

add_action( 'tgmpa_register', 'register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function  register_required_plugins(){

    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
         array(
            'name'                  => 'Visual Composer',
            'slug'                  => 'js_composer',
            'source'                => get_template_directory().'/framework/plugins/js_composer.zip',
            'required'              => true,
            'version'               => '4.4.3',
            'force_activation'      => true,
            'force_deactivation'    => true,
            'external_url'          => '',
        ),
        array(
            'name'                  => 'Ultimate VC Addons',
            'slug'                  => 'Ultimate_VC_Addons',
            'source'                => get_template_directory().'/framework/plugins/Ultimate_VC_Addons.zip',
            'required'              => false,
            'version'               => '3.10',
            'force_activation'      => true,
            'force_deactivation'    => true,
            'external_url'          => '',
        ),
        array(
            'name'                  => 'Revolution Slider',
            'slug'                  => 'revslider',
            'source'                => get_template_directory().'/framework/plugins/revslider.zip',
            'required'              => false,
            'version'               => '4.6.5',
            'force_activation'      => true,
            'force_deactivation'    => true,
            'external_url'          => '',
        ),
        array(
            'name'                  =>  'Better WordPress Minify',
            'slug'                  =>  'bwp-minify',
            'source'                =>  get_template_directory().'/framework/plugins/bwp-minify.zip',
            'required'              =>  false,
            'version'               =>  '1.3.3',
            'force_activation'      =>  false,
            'force_deactivation'    =>  true,
            ),
        array(
            'name'                  => 'Contact Form 7',
            'slug'                  => 'contact-form-7',
            'required'              => false,
        ),
        array(
            'name'                  => 'Simple Share Buttons Adder',
            'slug'                  => 'simple-share-buttons-adder',
            'required'              => true,
        ),
        array(
            'name'                  => 'Ninja Forms ',
            'slug'                  => 'ninja-forms',
            'required'              => true,
        ),
        array(
            'name'                  => 'Ninja Forms Multi-part',
            'slug'                  => 'ninja-forms-multi-part',
             'source' => get_template_directory().'/framework/plugins/ninja-forms-multi-part-v1.3.5.zip',
            'required'              => true,
        ),
        array(
            'name' => 'Mailchimp',
            'slug' => 'mailchimp-for-wp',
            'source' => get_template_directory().'/framework/plugins/mailchimp-for-wp.3.1.5.zip',
            'required' => false,
        )

    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'default_path' => '', // Default absolute path to pre-packaged plugins.
        'menu' => 'tgmpa-install-plugins', // Menu slug.
        'has_notices' => true, // Show admin notices or not.
        'dismissable' => true, // If false, a user cannot dismiss the nag message.
        'dismiss_msg' => '', // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false, // Automatically activate plugins after installation or not.
        'message' => '', // Message to output right before the plugins table.
        'strings' => array(
            'page_title' => __( 'Install Required Plugins', 'wp-starter-theme' ),
            'menu_title' => __( 'Install Plugins', 'wp-starter-theme' ),
            'installing' => __( 'Installing Plugin: %s', 'wp-starter-theme' ), // %s = plugin name.
            'oops' => __( 'Something went wrong with the plugin API.', 'wp-starter-theme' ),
            'notice_can_install_required' => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s).
            'notice_can_install_recommended' => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_install' => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s).
            'notice_can_activate_required' => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_activate' => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s).
            'notice_ask_to_update' => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_update' => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s).
            'install_link' => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
            'activate_link' => _n_noop( 'Begin activating plugin', 'Begin activating plugins' ),
            'return' => __( 'Return to Required Plugins Installer', 'wp-starter-theme' ),
            'plugin_activated' => __( 'Plugin activated successfully.', 'wp-starter-theme' ),
            'complete' => __( 'All plugins installed and activated successfully. %s', 'wp-starter-theme' ), // %s = dashboard link.
            'nag_type' => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       END OF TGM ACTIVATION PLUGIN
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
