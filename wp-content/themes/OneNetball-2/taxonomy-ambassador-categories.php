<?php get_header(); ?>

	<section class="page-accent">
	    <div class="wrapper">
	        <div class="left">
	            <h1 class="page-accent-title">MEET OUR ONE NETBALL<br/><span class="accent">AMBASSADORS</span></h1>
	            <p>Eighteen women from Australia’s netball community who exemplify everything that One Netball represents.</p>
	            <p>They are inspirational, talented and passionate about ensuring that everyone in Australia can connect with the game they love.</p>
	            <p>From the Tiwi Islands to Echuca, our ambassadors cover the corners of australia to bring the message that everyone, regardless of their background or ability, is welcome in netball.</p>
	        </div>
	        <div class="right">
	            <a href="" title="" class="video-link">
	                <img src="<?php echo get_template_directory_uri(); ?>/images/temp/home-accent-video-image.jpg" alt="" width="534" height="342"/>
	            </a>
	        </div>
	    </div>
	</section>
	  
	<section class="ambassadors-section wrapper">
	    <h2 class="sr-only">AMBASSADORS</h2>
	    <ul class="ambassadors-list">


	    </ul>
	</section>


<?php 

	$show_newsletter = ot_get_option('show_newsletter');

	if( $show_newsletter === 'on'):
		get_template_part( 'templates/content', 'newsletter' );
	endif;
 ?>


<?php get_footer(); ?>