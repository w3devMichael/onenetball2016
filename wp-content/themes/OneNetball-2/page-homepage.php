<?php
/**
 * Template Name: Home Page
 *
 * @package wpbuilder
 * @subpackage wpbuilder
 * @since wpbuilder
 */

get_header(); ?>

	<div class="homepage main-content full-width">	

		<div class="vc-content container-fluid">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
				<?php the_content(); ?>		

			<?php endwhile; endif; ?>

		</div>

	</div>	

<?php 
    get_template_part( 'templates/content', 'newsletter' ); 
?>


<?php get_footer(); ?>