<?php get_header(); ?>

<?php 

$page_title = ot_get_option('ambassador_page_title'); 
$page_description = ot_get_option('ambassador_page_mode_desc'); 
$ambassador_image = ot_get_option('ambassador_image'); 

?>

<section class="page-accent image-bottom">
	<div class="wrapper">
		<div class="left">
			<h1 class="page-accent-title"><?php echo $page_title; ?></h1>
			<p><?php echo $page_description; ?></p>	           

		</div>
		<div class="right">
			<img src="<?php echo $ambassador_image; ?>" alt="" width="" height=""/>
		</div>
	</div>
</section>

<section class="ambassadors-section wrapper">
	<h2 class="sr-only">AMBASSADORS</h2>
	<ul class="ambassadors-list">

		<?php 

		$args = [
		'post_type' => 'ambassadors',
		'posts_per_page' => -1,
		'post_status' => 'publish',

		];


	    	//$the_query = new WP_Query( $args );

		query_posts( $args );

		if ( have_posts() ) : while ( have_posts() ) : 
			the_post(); 

		$format = "ambasador-standard";

		get_template_part('templates/format/' . $format );

		?>

	<?php endwhile; endif; ?>

</ul>
</section>


<?php 

$show_newsletter = ot_get_option('show_newsletter');

if( $show_newsletter === 'on'):
	get_template_part( 'templates/content', 'newsletter' );
endif;
?>


<?php get_footer(); ?>