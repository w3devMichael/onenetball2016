<aside id="sidebar" role="complementary">

<?php if ( is_active_sidebar( 'resources-widget-area' ) ) : ?>
	<div id="primary" class="widget-area">
		<ul class="xoxo">
			<?php dynamic_sidebar( 'resources-widget-area' ); ?>
		</ul>
	</div>
<?php endif; ?>
</aside>
